<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('web/index');
}); */

//Route::get('/', 'WebController@url');//
Route::get('/', 'Auth\LoginController@index');
Route::get('/public', 'WebController@index');
Route::post('/check-vehicle-web', 'WebController@store');
Route::get('/sample_report', 'WebController@sample_report');
Route::get('/how-to-buy', 'WebController@how_to_buy');

/*Route::get('/', function () {
    return view('auth.login');
});*/

Route::get('/public', function () {
    return view('auth.login');
});


Route::get('/send', function () {
    return view('web.register_email.register_email');
});


Route::get('qr-code-g', function () {
  \QrCode::size(500)
            ->format('png')
            ->generate('ItSolutionStuff.com', public_path('images/qrcode.png'));
  return view('admin.report.report_autocheck');
    
});


Auth::routes();



//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/login2', 'Auth\LoginController@login');
//Route::post('/login2', 'Auth\LoginController@login_post');
Route::get('/authenticated', 'Auth\LoginController@authenticated');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/register-user', 'Auth\RegisterController@register_user');

Route::post('/register-buyer', 'BuyerController@store');


Route::group( ['middleware' => ['auth']], function() {

		Route::get('change-password', 'AccountController@change_password');
	Route::post('update-password/{id}', 'AccountController@update_password');
	
	/* Admin */
	Route::get('/home', 'DashboardController@index')->name('home');
	
	/* Dashboard */
	Route::get('/dashboard', 'DashboardController@index');


	//--- Search
	Route::get('/search-vehicle', 'Search\SearchFullController@index');

	//Route::get('/search-vehicle', 'SearchController@index'); 
	Route::post('/search-vehicle', 'Search\SearchFullController@store');
	Route::get('/search-payment', 'SearchController@show');
	

	//Pay
	Route::post('/pay-chassis', 'Payment\PaymentController@store');
	Route::any('callback/{id}', 'Payment\PaymentController@callback');

	//login buyer
	Route::post('/login-buyer', 'Auth\LoginController@buyer');


	  //buat controller baru
	Route::get('/open-vehicle-verify', 'VehicleCheckedController@open_vehicle');


	Route::get('user-profile', 'ProfileController@index');

	/* User */
	Route::get('user', 'UserController@index');
	Route::get('add-user', 'UserController@create');
	Route::post('save-user', 'UserController@store');
	Route::get('edit-user/{id}', 'UserController@edit');
	Route::post('update-user/{id}', 'UserController@update');
	Route::get('delete-user/{id}', 'UserController@destroy');
	Route::get('user/update-status', 'UserController@update_status');

	/* User Group */
	Route::get('user-group', 'UserController@index_group');
	Route::post('save-user-group', 'UserController@store_group');
	Route::get('delete-user-group/{id}', 'UserController@destroy');
	Route::get('edit-user-group/{id}', 'UserController@edit_group');
	Route::post('update-user-group/{id}', 'UserController@update_group');
	

	/* Parameter */
	Route::get('role', 'RoleController@index');
	Route::post('save-role', 'RoleController@store');
	Route::get('edit-role/{id}', 'RoleController@edit');
	Route::post('update-role/{id}', 'RoleController@update');
	Route::get('delete-role/{id}', 'RoleController@destroy');


	Route::get('package', 'PackageController@index');
	Route::post('save-package', 'PackageController@store');
	Route::get('edit-package/{id}', 'PackageController@edit');
	Route::post('update-package/{id}', 'PackageController@update');
	Route::get('delete-package/{id}', 'PackageController@destroy');

	Route::get('email', 'EmailController@index');
	Route::post('save-email', 'EmailController@store');
	Route::get('edit-email/{id}', 'EmailController@edit');
	Route::post('update-email/{id}', 'EmailController@update');
	Route::get('delete-email/{id}', 'EmailController@destroy');


	Route::get('brand', 'BrandController@index');
	Route::post('save-brand', 'BrandController@store');
	Route::get('edit-brand/{id}', 'BrandController@edit');
	Route::post('update-brand/{id}', 'BrandController@update');
	Route::get('delete-brand/{id}', 'BrandController@destroy');


	Route::get('currency', 'CurrencyController@index');
	Route::post('save-currency', 'CurrencyController@store');
	Route::get('edit-currency/{id}', 'CurrencyController@edit');
	Route::post('update-currency/{id}', 'CurrencyController@update');
	Route::get('delete-currency/{id}', 'CurrencyController@destroy');
	

	Route::get('type-vehicle', 'TypeVehicleController@index');
	Route::post('save-type-vehicle', 'TypeVehicleController@store');
	Route::get('edit-type-vehicle/{id}', 'TypeVehicleController@edit');
	Route::post('update-type-vehicle/{id}', 'TypeVehicleController@update');
	Route::get('delete-type-vehicle/{id}', 'TypeVehicleController@destroy');


	Route::get('supplier', 'SupplierController@index');
	Route::post('save-supplier', 'SupplierController@store');
	Route::get('edit-supplier/{id}', 'SupplierController@edit');
	Route::post('update-supplier/{id}', 'SupplierController@update');
	Route::get('delete-supplier/{id}', 'SupplierController@destroy');
	

	Route::get('country-origin', 'CountryOriginController@index');
	Route::post('save-country-origin', 'CountryOriginController@store');
	Route::get('edit-country-origin/{id}', 'CountryOriginController@edit');
	Route::post('update-country-origin/{id}', 'CountryOriginController@update');
	Route::get('delete-country-origin/{id}', 'CountryOriginController@destroy');


	Route::get('partner-overseas', 'CountryOriginController@index');
	Route::post('save-partner-overseas', 'CountryOriginController@store');
	Route::get('edit-partner-overseas/{id}', 'CountryOriginController@edit');
	Route::post('update-partner-overseas/{id}', 'CountryOriginController@update');
	Route::get('delete-partner-overseas/{id}', 'CountryOriginController@destroy');

	Route::get('fuel', 'FuelController@index');
	Route::post('save-fuel', 'FuelController@store');
	Route::get('edit-fuel/{id}', 'FuelController@edit');
	Route::post('update-fuel/{id}', 'FuelController@update');
	Route::get('delete-fuel/{id}', 'FuelController@destroy');


	Route::get('fee', 'FeeController@index');
	Route::post('save-fee', 'FeeController@store');
	Route::get('edit-fee/{id}', 'FeeController@edit');
	Route::post('update-fee/{id}', 'FeeController@update');
	Route::get('delete-fee/{id}', 'FeeController@destroy');


	Route::get('par-model', 'ModelController@index');
	Route::post('save-model', 'ModelController@store');
	Route::get('edit-model/{id}', 'ModelController@edit');
	Route::post('update-model/{id}', 'ModelController@update');
	Route::get('delete-model/{id}', 'ModelController@destroy');

	Route::get('group', 'UserGroupController@index');
	Route::post('save-group', 'UserGroupController@store');
	Route::get('edit-group/{id}', 'UserGroupController@edit');
	Route::post('update-group/{id}', 'UserGroupController@update');
	Route::get('delete-group/{id}', 'UserGroupController@destroy');

	Route::get('branch', 'BranchController@index');
	Route::post('save-branch', 'BranchController@store');
	Route::get('edit-branch/{id}', 'BranchController@edit');
	Route::post('update-branch/{id}', 'BranchController@update');
	Route::get('delete-branch/{id}', 'BranchController@destroy');

	Route::get('balance', 'BalanceController@index');
	Route::post('save-balance/{id}', 'BalanceController@store');
	Route::get('edit-balance/{id}', 'BalanceController@edit');
	Route::post('update-balance/{id}', 'BalanceController@update');
	Route::get('delete-balance/{id}', 'BalanceController@destroy');
	Route::get('history-topup/{id}', 'BalanceController@history');
	Route::get('topup-balance', 'BalanceController@topup');


	Route::get('balance/extra', 'BalanceController@index_extra');
	Route::get('balance/full', 'BalanceController@index_full');
	Route::get('balance/half', 'BalanceController@index_half');
	


	/* ========== NAZA / group ========== */
	Route::get('vehicle-checking', 'Naza\VehicleCheckingController@index');
	Route::post('save-vehicle-checking', 'Search\SearchHalfController@store');

	//Route::post('save-vehicle-checking', 'Naza\VehicleCheckingController@store');

	Route::get('edit-vehicle-checking', 'Naza\VehicleCheckingController@edit');
	Route::post('update-vehicle-checking/{id}', 'Naza\VehicleCheckingController@update');
	Route::get('delete-vehicle-checking', 'Naza\VehicleCheckingController@destroy');

	Route::get('sync/{id}', 'Naza\VehicleCheckingController@sync');
	Route::get('sync_autocheck3/{id}', 'Naza\VehicleCheckingController@sync_autocheck3');
	Route::get('sync_autocheck4', 'Naza\VehicleCheckingController@sync_shortreport_kastam');
	Route::get('sync_autocheck4_mn/{id}', 'Naza\VehicleCheckingController@sync_shortreport_one_by_one');
	Route::post('sync-autocheck4-manual', 'Naza\VehicleCheckingController@sync_autocheck4_manual');

	Route::post('sync_autocheck4', 'SyncController@sync_shortreport_kastam');
	Route::post('sync', 'SyncController@sync');
	Route::get('sync_all', 'SyncController@sync_all');
	Route::post('sync-cadealer-autocheck3', 'SyncController@sync_cadealer_autocheck3');

	/* ============= Buyer ==============*/

	
	Route::get('Extra/vehicle-checking', 'Search\SearchExtraController@index');
	Route::post('Extra/vehicle-checking/store', 'Search\SearchExtraController@store');

	Route::post('/check-vehicle-buyer', 'Naza\VehicleCheckingController@search_vehicle');
	Route::post('/post-check-vehicle-buyer', 'Naza\VehicleCheckingController@post_search_vehicle');

	Route::get("/my-account", function(){
	   return View("web.myaccount.report.index");
	});

	Route::group(['middleware' => ['auth'], 'prefix' => 'my-account', 'as' => 'admin.'], function () {

		Route::get('/report', 'AccountPrivateController@report');
		Route::get('/invoice', 'InvoiceController@invoice');
		Route::get('/profile', 'FrontProfileController@profile');
	});

	Route::get('vehicle-verify-not-found', 'VehicleVerifyNotFoundController@index');
	Route::post('vehicle-verify-not-found/save/{id}', 'VehicleVerifyNotFoundController@store');

	/* Report */

	Route::get('generate-full-report/{id}', 'ReportController@kastam_api');

	Route::get('report-autocheck-kastam-api/{id}', 'ReportController@kastam_api');
	Route::get('report-autocheck-kastam-mn/{id}', 'ReportController@kastam_mn');
	Route::get('report-autocheck-kastam-api-mn/{id}', 'ReportController@kastam_api_mn');
	
	
	Route::get('report-autocheck/{id}', 'ReportController@index');
	Route::get('report-autocheck-mn/{id}', 'ReportController@report_mn');
	Route::get('report-carvx/{id}', 'ReportController@carvx');
	Route::get('report-autocheck-management', 'ReportController@buyer_group');
	Route::post('report-autocheck-management-view', 'ReportController@buyer_group_view');
	Route::get('report-summary', 'ReportController@summary_report');
	Route::post('report-summary-view', 'ReportController@summary_report_view');
	Route::post('report-summary-print', 'ReportController@summary_report_print');

	Route::get('report-vehicle', 'ReportController@report_vehicle');
	Route::post('report-vehicle-na-view', 'ReportController@report_vehicle_view');
	Route::post('report-vehicle-na-print', 'ReportController@report_vehicle_print');

	Route::get('report-short-past/{id}', 'ReportController@report_short_past');
	Route::get('report-monthly', 'ReportController@get_report_monthly');
	Route::post('report_monthly_view', 'ReportController@report_monthly_view');
	Route::post('report-monthly-print', 'ReportController@report_monthly_print');


	/* Report Chart */
	Route::get('report-chart', 'ReportController@get_report_chart');
	Route::get('report-chart/{id}', 'ReportController@report_chart_by_year');
	/* End Report Chart */

	/* Report Old Chart */
	Route::get('report-chart-old', 'ReportController@get_report_chart_old');

	Route::get('report-chart-old/{id}', 'ReportController@get_report_chart_old_by_year');
	
	/* End Report Old Chart */


	/* ======== shared ========= */
	Route::get('account', 'AccountController@index');
	Route::post('update-account/{id}', 'AccountController@update');
	Route::post('save/profile/{id}', 'AccountController@store');


	Route::get('balance-history', 'BalanceHistoryController@index');
	Route::get('history-vehicle/{id}', 'HistoryVehicleController@index');
	Route::get('/vehicle-past', 'VehiclePastController@index');
	


	Route::get('/postcode/{id}', 'ApiController@postcode');

	Route::get('/mo', 'ApiController@mo');



	/* ============ Verified =============== */
	
	Route::post('submit-idreport/full-report/{id}', 'CheckIDController@submit_id_full_report');
	Route::post('submit-idreport/extra-report/{id}', 'CheckIDController@submit_id_extra_report');

	/*============ Submit ID Api Manual ===============*/
	
	Route::get('/kastam/vehicle', 'VehicleCheckedController@kastam_vehicle_complete');
	Route::get('/car-dealer/vehicle', 'VehicleCheckedController@car_dealer_vehicle_complete');
	Route::get('/buyer/vehicle', 'VehicleCheckedController@buyer_vehicle_complete');
	

	Route::get('verification-shortreport-kastam-mn', 'VehicleCheckedController@tasklist_kastam_shortreport_mn');


	Route::get('verify-not-found-id-carvx/{id}', 'VehicleVerifyController@not_found_id_carvx');

	Route::post('kastam-manual-short-report/{id}', 'VehicleCheckedController@kastam_manual_short_report');

	Route::post('kastam-manual-short-report-submit-idreport/{id}', 'VehicleCheckedController@kastam_manual_short_report_submit_idreport');


	Route::get('sync_all', 'Naza\VehicleCheckingController@sync_all');
	
	Route::get('verify-vehicle/{id}', 'VehicleVerifyController@verify');
	Route::post('verify-vehicle-verifier-api/{id}', 'VehicleVerifyController@store_api');
	Route::post('verify-vehicle-verifier-manual/{id}', 'VehicleVerifyController@store_manual');
	Route::post('upload-report-vehicle/{id}', 'VehicleVerifyController@upload_full_report');

	/* sent-vehicle */

	Route::get('/Extra-report/sent/api', 'SentApiController@sent_vehicle_full_report_api');
	//Route::get('/full-report/sent/api', 'SentApiController@sent_vehicle_shortreport_kastam_api');




	


	Route::get('/full-report/sent/api', 'API\FullSentAPIController@index');
	Route::post('sent-full-', 'SentApiController@autocheck4');


	Route::post('sent-to-carvx-autocheck4', 'SentApiController@autocheck4');
	Route::post('sent-to-carvx-autocheck3', 'SentApiController@autocheck3');
	Route::post('sent-to-carvx-autocheck2', 'SentApiController@autocheck2');


	Route::get('open-vehicle-verify-short-report-naza', 'OpenVerifyController@open_vehicle_short_report_naza');
	//Route::get('FullReport/open-verify/index', 'OpenVerifyController@open_vehicle_full_report');
	Route::get('open-vehicle-verify-kastam-report', 'OpenVerifyController@open_vehicle_short_report_kastam');


	Route::get('sent-vehicle', 'SentController@index');
	Route::get('sent-vehicle-kastam', 'SentController@sent_vehicle_kastam');
	Route::post('generate-and-sent-vehicle', 'SentController@generate_and_sent');
	Route::post('reject-before-sent-kadealer/{id}', 'SentController@reject_kadealer');
	Route::post('reject-before-sent-kastam/{id}', 'SentController@reject_kastam');

	Route::get('update-send/{id}', 'SentController@update');
	Route::get('batch-sent', 'SentController@batch');
		
	Route::get('not-found', 'RejectController@index');


	Route::get('user/short-report', 'UserController@user_short_report');
    Route::get('user/full-report', 'UserController@user_full_report');
    //Route::get('user/extra-report', 'UserController@user_extra_report');

    Route::get('user/api', 'UserController@user_api');
    Route::post('user/api/submit', 'UserController@user_api_submit');

    Route::get('user/api/change-status', 'UserController@user_api_change_status');



    Route::get('user-group/half-report', 'UserGroup\UserGroupHalfController@index');
	Route::post('user-group/half-report/save', 'UserGroup\UserGroupHalfController@store');
	Route::post('user-group/half-report/edit/{id}', 'UserGroup\UserGroupHalfController@edit');
	Route::get('user-group/half-report/change-status', 'UserGroup\UserGroupHalfController@delete');


	Route::get('user-group/full-report', 'UserGroup\UserGroupFullController@index');
	Route::post('user-group/full-report/save', 'UserGroup\UserGroupFullController@store');
	Route::post('user-group/full-report/edit/{id}', 'UserGroup\UserGroupFullController@edit');
	Route::get('user-group/full-report/change-status', 'UserGroup\UserGroupFullController@delete');


	Route::get('user-group/extra-report', 'UserGroup\UserGroupExtraController@index');
	Route::post('user-group/extra-report/save', 'UserGroup\UserGroupExtraController@store');
	Route::post('user-group/extra-report/edit/{id}', 'UserGroup\UserGroupExtraController@edit');
	Route::get('user-group/extra-report/change-status', 'UserGroup\UserGroupExtraController@delete');


	Route::get('user/half-report', 'User\UserHalfController@index');
	Route::post('user/half-report/save', 'User\UserHalfController@store');
	Route::get('user/half-report/edit/{id}', 'User\UserHalfController@edit');
	Route::post('user/half-report/update/{id}', 'User\UserHalfController@update');
	Route::get('user/half-report/update-status', 'User\UserHalfController@delete');
	Route::post('user/check_duplicate_username', 'User\UserHalfController@check_duplicate_username');


	Route::get('user/full-report', 'User\UserFullController@index');
	Route::post('user/full-report/save', 'User\UserFullController@store');
	Route::get('user/full-report/edit/{id}', 'User\UserFullController@edit');
	Route::post('user/full-report/update/{id}', 'User\UserFullController@user_full_report_update');
	Route::get('user/full-report/update-status', 'User\UserFullController@delete');
	Route::post('user/full-report/check_duplicate_username', 'User\UserFullController@check_duplicate_username');


	Route::get('user/extra-report', 'User\UserExtraController@index');
	Route::post('user/extra-report/save', 'User\UserExtraController@store');
	Route::get('user/extra-report/edit/{id}', 'User\UserExtraController@edit');
	Route::post('user/extra-report/update/{id}', 'User\UserExtraController@update');
	Route::get('user/extra-report/update-status', 'User\UserExtraController@delete');
	Route::post('user/extra-report/check_duplicate_username', 'User\UserExtraController@check_duplicate_username');

	/*Share*/
	Route::post('Share/Update-Id/{id}', 'Share\IdReportController@store');

	Route::get('Share/Api-change-manual/{id}', 'Share\IdReportController@apichangemanual');
	



	Route::get('/vehicle-checked', 'VehicleCheckedController@index');
	Route::get('extra/vehicle-checked', 'VehicleCheckedController@extra');
	Route::get('full/vehicle-checked', 'VehicleCheckedController@full');
	Route::get('half/vehicle-checked', 'VehicleCheckedController@half');


	Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);



});
