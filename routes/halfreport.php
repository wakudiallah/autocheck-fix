<?php

	Route::group(['prefix' => 'HalfReport'], function () {

		Route::get('/sent/api', 'Verify\SentApi\HalfSentController@index');
		Route::post('/sent/api', 'Verify\SentApi\HalfSentController@store');


		Route::get('/sent/mn', 'Verify\Sent\HalfSentManualController@index');

		Route::post('/generate-and-sent', 'Verify\Sent\HalfSentManualController@store');

		Route::get('/open-verify/index', 'Verify\OpenVerify\HalfVerifyController@index');
		Route::post('/open-verify/store/{id}', 'Verify\OpenVerify\HalfVerifyController@store');	


		Route::get('/check-id', 'Verify\CheckId\HalfCheckController@index');
		Route::post('/check-id/update/{id}', 'Verify\CheckId\HalfCheckController@update');

		Route::get('/sync/api', 'Verify\Sync\HalfSyncController@index');


		Route::post('/sync/store', 'Verify\Sync\HalfSyncController@store');

		Route::get('/inprogress', 'Report\ReportManagement\Inprogress\HalfController@index');
		Route::post('/inprogress/store', 'Report\ReportManagement\Inprogress\HalfController@store');

		Route::get('/vehicle-checked', 'Verify\VehicleChecked\HalfController@index');
		Route::get('/detail/{id}', 'Verify\VehicleChecked\HalfController@detail');


		Route::get('/Report/{id}', 'Verify\Report\HalfController@index');
		

		Route::get('/verifynotfound', 'Verify\VerifyNotFound\HalfController@index');

		Route::post('/verifynotfound/store/{id}', 'Verify\VerifyNotFound\HalfController@store');
	
		
		Route::get('/notfound', 'Verify\NotFound\HalfController@index');


		/* ------ Report ------ */
		Route::get('/report/monthly', 'Report\ReportManagement\Monthly\HalfController@index');
		Route::post('/report/monthly/store', 'Report\ReportManagement\Monthly\HalfController@store');
		Route::post('/report/monthly/print', 'Report\ReportManagement\Monthly\HalfController@print');


		Route::get('/report/summary', 'Report\ReportManagement\Summary\HalfController@index');
		Route::post('/report/summary/store', 'Report\ReportManagement\Summary\HalfController@store');
		Route::post('/report/summary/print', 'Report\ReportManagement\Summary\HalfController@print');



		

	});
	
