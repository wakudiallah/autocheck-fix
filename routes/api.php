<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

	Route::post('register', 'API\AuthController@register');
	Route::post('login', 'API\AuthController@login');


	Route::post('submit', 'API\AuthController@submit_to_autocheck');
	Route::post('get-report', 'API\AuthController@get_report');
	Route::any('autocheck', 'API\ApiSearchController@search_smat_to_autocheck');


    //Route::group(['middleware' => ['auth:api']], function () {
    	
    	Route::any('api_marii', 'API\ApiSearchController@search_vehicle_marii')->middleware('auth:api');

		

		Route::any('submit-autocheck', 'API\ApiSearchController@submit_to_autocheck')->middleware('auth:api');


		//Route::any('get-report-autocheck', 'API\ApiSearchController@get_report_autocheck')->middleware('auth:api');
	

   //});

	





