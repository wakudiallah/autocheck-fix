<?php

	Route::group(['prefix' => 'ExtraReport'], function () {

		Route::get('/sent/mn', 'Verify\Sent\ExtraSentManualController@index');	

		Route::get('/sent/api', 'Verify\SentApi\ExtraSentController@index');
		Route::post('/sent/api', 'Verify\SentApi\ExtraSentController@store');


		Route::post('/generate-and-sent', 'Verify\Sent\ExtraSentManualController@store');

		Route::get('/open-verify/index', 'Verify\OpenVerify\ExtraVerifyController@index');
		Route::post('/open-verify/store/{id}', 'Verify\OpenVerify\ExtraVerifyController@store');


		Route::get('/open-verify/verify/{id}', 'Verify\OpenVerify\ExtraVerifyController@verify');


		Route::get('/check-id', 'Verify\CheckId\ExtraCheckController@index');


		Route::get('/sync/api', 'Verify\Sync\ExtraSyncController@index');
		Route::post('/sync/api', 'Verify\Sync\ExtraSyncController@store');

		Route::get('/report/{id}', 'Verify\Report\ExtraController@index');

		Route::get('/inprogress', 'Report\ReportManagement\Inprogress\ExtraController@index');
		Route::post('/inprogress/store', 'Report\ReportManagement\Inprogress\ExtraController@store');
		
		
		Route::post('/extra-report/step-1', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_1');
		Route::post('/extra-report/step-2', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_2_3');
		Route::post('/extra-report/step-3', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_3');
		Route::post('/extra-report/step-4', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_4');
		Route::post('/extra-report/step-5', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_5');
		Route::post('/extra-report/step-6', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_6');
		Route::post('/extra-report/step-7', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_7');
		Route::post('/extra-report/step-8', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_8');
		Route::post('/extra-report/step-9', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_9');
		Route::post('/extra-report/step-9a', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_9a');
		Route::post('/extra-report/step-10', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_10');
		Route::post('/extra-report/step-11', 'Verify\OpenVerify\ExtraVerifyController@extra_report_step_11');

		Route::post('/extra-report/next-odometer', 'Verify\OpenVerify\ExtraVerifyController@next_odometer');
		Route::post('/extra-report/next-use', 'Verify\OpenVerify\ExtraVerifyController@next_use');
		Route::post('/extra-report/next-detail', 'Verify\OpenVerify\ExtraVerifyController@next_detail');
		Route::post('/extra-report/next-manufacture', 'Verify\OpenVerify\ExtraVerifyController@next_manufacture');

	
		Route::get('/get/extra-report/step-4', 'Verify\OpenVerify\ExtraVerifyController@get_extra_report_step_4');
		Route::post('extra-report/photo', 'Verify\OpenVerify\ExtraVerifyController@upload_photos');
		Route::post('extra-report/uploadphoto/{id}', 'Verify\OpenVerify\ExtraVerifyController@uploadphoto');
		Route::get('deletephoto/{id}', 'Verify\OpenVerify\ExtraVerifyController@deletephoto');



		Route::get('/vehicle-checked', 'Verify\VehicleChecked\ExtraController@index');
		Route::get('/detail/{id}', 'Verify\VehicleChecked\ExtraController@detail');


		Route::get('/verifynotfound', 'Verify\VerifyNotFound\ExtraController@index');
		Route::post('/verifynotfound/store/{id}', 'Verify\VerifyNotFound\ExtraController@store');


		Route::get('/notfound', 'Verify\NotFound\ExtraController@index');



		/* ------ Report ------ */
		Route::get('/reportmanagement/monthly', 'Report\ReportManagement\Monthly\ExtraController@index');
		Route::post('/report/monthly/store', 'Report\ReportManagement\Monthly\ExtraController@store');
		Route::post('/report/monthly/print', 'Report\ReportManagement\Monthly\ExtraController@print');


		Route::get('/reportmanagement/summary', 'Report\ReportManagement\Summary\ExtraController@index');
		Route::post('/report/summary/store', 'Report\ReportManagement\Summary\ExtraController@store');
		Route::post('/report/summary/print', 'Report\ReportManagement\Summary\ExtraController@print');


	});
