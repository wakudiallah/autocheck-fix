<?php

	Route::group(['prefix' => 'FullReport'], function () {

		
		Route::get('/sent/mn', 'Verify\Sent\FullSentManualController@index');
		Route::post('/generate-and-sent', 'Verify\Sent\FullSentManualController@store');

		Route::get('/sent/api', 'Verify\SentApi\FullSentController@index');
		//Route::post('/sent/api/partner', 'API\FullSentAPIController@sent');

		Route::post('/sent/api/partner', 'Verify\SentApi\FullSentController@sent');

		Route::get('/open-verify/index', 'Verify\OpenVerify\FullVerifyController@index');



		Route::get('/check-id', 'Verify\CheckId\FullCheckController@index');
		

		Route::get('/sync/api', 'Verify\Sync\FullSyncController@index');

		Route::get('/vehicle-checked', 'Verify\VehicleChecked\FullController@index');

		Route::get('/detail/{id}', 'Verify\VehicleChecked\FullController@detail');

		Route::get('/Report/{id}', 'Verify\Report\FullController@index');


		Route::post('/sync/store', 'Verify\Sync\FullSyncController@store');

		Route::get('/inprogress', 'Report\ReportManagement\Inprogress\FullController@index');
		Route::post('/inprogress/store', 'Report\ReportManagement\Inprogress\FullController@store');

		Route::get('/verifynotfound', 'Verify\VerifyNotFound\FullController@index');
		Route::post('/verifynotfound/store/{id}', 'Verify\VerifyNotFound\FullController@store');



		Route::get('/notfound', 'Verify\NotFound\FullController@index');

		Route::get('/Report/{id}', 'Verify\Report\FullController@show');


		/* ------ Report ----- */
		Route::get('/report/monthly', 'Report\ReportManagement\Monthly\FullController@index');
		Route::post('/report/monthly/store', 'Report\ReportManagement\Monthly\FullController@store');
		Route::post('/report/monthly/print', 'Report\ReportManagement\Monthly\FullController@print');


		Route::get('/report/summary', 'Report\ReportManagement\Summary\FullController@index');
		Route::post('/report/summary/store', 'Report\ReportManagement\Summary\FullController@store');
		Route::post('/report/summary/print', 'Report\ReportManagement\Summary\FullController@print');
	

	});