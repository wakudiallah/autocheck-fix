<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserApiStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('user_api_statuses'))
        {
            Schema::create('user_api_statuses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('user_id', 5);
                $table->string('type_report_id', 5);
                $table->string('api_status', 5)->nullable(); 
                $table->string('status', 2)->nullable(); 
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_api_statuses');
    }
}
