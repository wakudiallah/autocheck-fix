<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_statuses'))
        {
            Schema::create('parameter_statuses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code', 10)->nullable();
                $table->string('desc', 100)->nullable();
                $table->string('badge', 30)->nullable();
                $table->timestamps();            
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_statuses');
    }
}
