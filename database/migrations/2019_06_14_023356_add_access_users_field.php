<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccessUsersField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('lat', 40)->nullable();
            $table->string('long', 40)->nullable();
            $table->tinyInteger('akses_buyer')->nullable();
            $table->tinyInteger('akses_buyer_group')->nullable();
            $table->tinyInteger('akses_admin')->nullable();
            $table->tinyInteger('akses_verify')->nullable();
            $table->tinyInteger('akses_kastam')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->string('lat', 40)->nullable();
            $table->string('long', 40)->nullable();
            $table->tinyInteger('akses_buyer')->nullable();
            $table->tinyInteger('akses_buyer_group')->nullable();
            $table->tinyInteger('akses_admin')->nullable();
            $table->tinyInteger('akses_verify')->nullable();
            $table->tinyInteger('akses_kastam')->nullable();
        });
    }
}
