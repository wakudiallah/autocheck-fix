<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('history_balances'))
        {
            Schema::create('history_balances', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 100)->nullable();
                $table->double('balance', 13, 2)->nullable();
                $table->string('transaction_fee', 100)->nullable();
                $table->string('desc', 100)->nullable();
                $table->string('created_by', 10)->nullable();
                $table->string('user_id', 10)->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_balances');
    }
}
