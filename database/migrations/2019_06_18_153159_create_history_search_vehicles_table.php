<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorySearchVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('history_search_vehicles'))
        {
            Schema::create('history_search_vehicles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('vehicle', 100)->nullable();
                $table->string('parameter_history_id', 30)->nullable();
                $table->string('user_id', 20)->nullable();
                $table->text('remark', 100)->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_search_vehicles');
    }
}
