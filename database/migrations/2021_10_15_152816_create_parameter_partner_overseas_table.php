<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterPartnerOverseasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_partner_overseas'))
        {
            Schema::create('parameter_partner_overseas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('partner', 50);
                $table->string('email', 90);
                $table->string('country_origin_id', 5)->nullable(); 
                $table->string('report_type_id', 30)->nullable(); 
                $table->string('status', 2)->nullable(); 
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_partner_overseas');
    }
}
