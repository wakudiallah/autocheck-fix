<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('step_statuses'))
        {
            Schema::create('step_statuses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('step_1', 5)->nullable();
                $table->string('step_2', 5)->nullable();
                $table->string('step_3', 5)->nullable();
                $table->string('step_4', 5)->nullable();
                $table->string('step_5', 5)->nullable();
                $table->string('step_6', 5)->nullable();
                $table->string('step_7', 5)->nullable();
                $table->string('step_8', 5)->nullable();
                $table->string('step_9', 5)->nullable();
                $table->string('step_10', 5)->nullable();
                $table->string('step_11', 5)->nullable();
                $table->string('step_12', 5)->nullable();
                $table->string('step_13', 5)->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step_statuses');
    }
}
