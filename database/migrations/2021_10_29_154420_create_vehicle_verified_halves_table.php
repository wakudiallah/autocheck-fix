<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleVerifiedHalvesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vehicle_verified_halfs'))
        {
            Schema::create('vehicle_verified_halfs', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('ver_sn', 30)->nullable();
                $table->string('partner', 50)->nullable();
                $table->string('vehicle', 100)->nullable();
                $table->string('country_origin', 100)->nullable();
                $table->string('brand', 100)->nullable();
                $table->string('model', 100)->nullable();
                $table->string('engine_number', 100)->nullable();
                $table->string('cc', 50)->nullable();
                $table->string('fuel_type', 50)->nullable();
                $table->string('year_manufacture', 50)->nullable();
                $table->string('registation_date', 50)->nullable();
                $table->string('status')->nullable();
                $table->Integer('request_by')->nullable();
                $table->Integer('updated_by')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_verified_halfs');
    }
}
