<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupOldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('user_group_olds'))
        {
            Schema::create('user_group_olds', function (Blueprint $table) {
                $table->increments('id');
                $table->string('user_group_old_id', 5)->nullable();
                $table->string('user_group_old_name', 50)->nullable();
                $table->string('status', 5)->nullable(); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_group_olds');
    }
}

