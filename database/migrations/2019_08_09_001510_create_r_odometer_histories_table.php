<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateROdometerHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *+: "2017-08-03"
    +: "MLIT"
    +: 43900
     * @return void
     */
    public function up()
    {
        
        if(!Schema::hasTable('r_odometer_histories'))
        {
            Schema::create('r_odometer_histories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('report_id', 40)->nullable();
                $table->string('date', 80)->nullable();
                $table->string('source', 80)->nullable();
                $table->string('mileage', 80)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_odometer_histories');
    }
}
