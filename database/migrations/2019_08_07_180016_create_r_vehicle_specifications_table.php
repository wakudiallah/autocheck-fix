<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRVehicleSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *

     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('r_vehicle_specifications'))
        {
        Schema::create('r_vehicle_specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('id_vehicle', 80)->nullable();
            $table->string('report_id', 40)->nullable();
            $table->text('firstGearRatio')->nullable();
            $table->string('secondGearRatio', 80)->nullable();
            $table->string('thirdGearRatio', 80)->nullable();
            $table->string('fourthGearRatio', 80)->nullable();
            $table->string('fifthGearRatio', 80)->nullable();
            $table->string('sixthGearRatio', 80)->nullable();
            $table->string('airbagPosition', 80)->nullable();
            $table->string('bodyRearOverhang', 80)->nullable();
            
            $table->string('bodyType', 80)->nullable();
            $table->text('chassisNumberEmbossingPosition')->nullable();

            $table->string('classificationCode', 80)->nullable();
            $table->string('cylinders', 80)->nullable();
            $table->string('displacement', 80)->nullable();
            $table->string('electricEngineType', 80)->nullable();
            $table->string('electricEngineMaximumOutput', 80)->nullable();
            $table->string('electricEngineMaximumTorque', 80)->nullable();
            $table->string('electricEnginePower', 80)->nullable();
            $table->string('engineMaximumPower', 80)->nullable();
            $table->string('engineMaximumTorque', 80)->nullable();
            $table->string('engineModel', 80)->nullable();
            $table->string('frameType', 80)->nullable();
            $table->string('frontShaftWeight', 80)->nullable();
            $table->string('frontShockAbsorberType', 80)->nullable();
            $table->string('frontStabilizerType', 80)->nullable();
            $table->text('frontTiresSize')->nullable();
            $table->string('frontTread', 80)->nullable();
            $table->string('fuelConsumption', 80)->nullable();
            $table->string('fuelTankEquipment', 80)->nullable();
            $table->string('gradeData', 100)->nullable();
            $table->string('height', 80)->nullable();
            $table->string('length', 80)->nullable();
            $table->text('mainBrakesType')->nullable();
            $table->string('dataMake', 80)->nullable();
            $table->string('maximumSpeed', 80)->nullable();
            $table->string('minimumGroundClearance', 80)->nullable();
            $table->string('minimumTurningRadius', 80)->nullable();
            $table->string('modelData', 80)->nullable();
            $table->string('modelCode', 80)->nullable();
            $table->string('mufflersNumber', 80)->nullable();
            $table->string('rearShaftWeight', 80)->nullable();
            $table->string('rearShockAbsorberType', 80)->nullable();
            $table->string('rearStabilizerType', 80)->nullable();
            $table->text('rearTiresSize')->nullable();
            $table->string('rearTread', 80)->nullable();
            $table->string('reverseRatio', 80)->nullable();
            $table->string('ridingCapacity', 80)->nullable();
            $table->text('sideBrakesType')->nullable();
            $table->string('specificationCode', 80)->nullable();
            $table->string('stoppingDistance', 80)->nullable();
            $table->string('transmissionType', 80)->nullable();
            $table->string('weight', 80)->nullable();
            $table->string('wheelAlignment', 80)->nullable();
            $table->string('wheelbase', 80)->nullable();
            $table->string('width', 80)->nullable();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_vehicle_specifications');
    }
}
