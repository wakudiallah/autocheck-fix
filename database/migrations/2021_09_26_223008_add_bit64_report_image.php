<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBit64ReportImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('r_auction_images', function (Blueprint $table) {
            $table->longText('imagebit64')->nullable()->after('updated_at');
            //$table->string('imagebit64', '4294967295' )->nullable()->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
            //$table->string('imagebit64', 255 )->nullable()->after('updated_at');
     
    }
}
