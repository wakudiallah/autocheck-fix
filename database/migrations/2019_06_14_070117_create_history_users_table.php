<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('history_users'))
        {
            Schema::create('history_users', function (Blueprint $table) {
                $table->increments('id');
                $table->Integer('user_id');
                $table->string('parameter_history_id', 10)->nullable();
                $table->string('vehicle_id', 50)->nullable();
                $table->string('remark', 250)->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_users');
    }
}
