<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleDuplicateCheckingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vehicle_duplicate_checkings'))
        {
            Schema::create('vehicle_duplicate_checkings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle')->nullable();
                $table->string('vehicle', 100)->nullable();
                $table->string('real_type_report_id', 100)->nullable();
                $table->Integer('created_by')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_duplicate_checkings');
    }
}
