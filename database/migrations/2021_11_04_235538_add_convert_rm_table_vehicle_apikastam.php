<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConvertRmTableVehicleApikastam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_api_kastams', function (Blueprint $table) {
            $table->string('currency', 100)->nullable();
            $table->string('rm_convert', 100)->nullable();
            $table->string('date_convert', 100)->nullable();
            $table->string('realtime_convert', 100)->nullable();
            $table->string('date_realtime_convert', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_api_kastams', function (Blueprint $table) {
            //
        });
    }
}
