<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRDetailHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     
     */
    public function up()
    {
        if(!Schema::hasTable('r_detail_histories'))
        {
            Schema::create('r_detail_histories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('report_id', 40)->nullable();
                $table->string('date', 80)->nullable();
                $table->string('source', 80)->nullable();
                $table->string('mileage', 80)->nullable();
                $table->string('action', 80)->nullable();
                $table->text('location')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_detail_histories');
    }
}
