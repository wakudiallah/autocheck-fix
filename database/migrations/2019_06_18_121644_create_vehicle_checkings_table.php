<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleCheckingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vehicle_checkings'))
        {
            Schema::create('vehicle_checkings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('vehicle', 100)->nullable();
                $table->Integer('supplier_id')->nullable();
                $table->Integer('type_id')->nullable();
                $table->Integer('country_origin_id')->nullable();
                $table->string('brand_id', 20)->nullable();
                $table->string('model_id', 20)->nullable();
                $table->string('cc', 20)->nullable();
                $table->string('engine_number', 50)->nullable();
                $table->date('vehicle_registered_date')->nullable();
                $table->integer('fuel_id')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->Integer('created_by')->nullable();
                $table->Integer('updated_by')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_checkings');
    }
}
