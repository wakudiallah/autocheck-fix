<?php

use Illuminate\Database\Seeder;

class ParamPartnerOverseasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parameter_partner_overseas')->insert([
            'partner' => 'carvx',
            'email' => 'autocheckmalaysia2@gmail.com',
            'country_origin_id' => '2',
            'status' => '1',
            'report_type_id' => 'Extra'
        ]);


        DB::table('parameter_partner_overseas')->insert([
            'partner' => 'carvx',
            'email' => 'autocheckmalaysia3@gmail.com',
            'country_origin_id' => '2',
            'status' => '1',
            'report_type_id' => 'Half'
        ]);

        DB::table('parameter_partner_overseas')->insert([
            'partner' => 'carvx',
            'email' => 'autocheckmalaysia4@gmail.com',
            'country_origin_id' => '2',
            'status' => '1',
            'report_type_id' => 'Full'
        ]);


    }
}
