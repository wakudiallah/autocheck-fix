<?php

use Illuminate\Database\Seeder;

class UserGroupTypeReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $today = $date = date('Y-m-d H:i:s');

        //Half Report

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "1",
            'type_report_id' => "Half",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "3",
            'type_report_id' => "Half",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "4",
            'type_report_id' => "Half",
            'created_at' => $today
        ]);

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "5",
            'type_report_id' => "Half",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "12",
            'type_report_id' => "Half",
            'created_at' => $today
        ]);



        //Full Report 

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "2",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "6",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "7",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "8",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "9",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "10",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "11",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);



        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "13",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "14",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "15",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "16",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "17",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "18",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "19",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "20",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "21",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "22",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "23",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "24",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "25",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "26",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "27",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "28",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "29",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);


        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "30",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);

        DB::table('user_group_type_reports')->insert([
            'user_group_id' => "31",
            'type_report_id' => "Full",
            'created_at' => $today
        ]);



    }


}
