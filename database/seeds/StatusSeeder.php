<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parameter_statuses')->insert([
            'code' => '20',
            'desc' => 'New',
            'badge' => 'primary',
        ]);


        DB::table('parameter_statuses')->insert([
            'code' => '10',
            'desc' => 'New',
            'badge' => 'primary',
        ]);


        DB::table('parameter_statuses')->insert([
            'code' => '25',
            'desc' => 'Sent',
            'badge' => 'warning',
        ]);

        DB::table('parameter_statuses')->insert([
            'code' => '26',
            'desc' => 'Check ID',
            'badge' => 'info',
        ]);


        DB::table('parameter_statuses')->insert([
            'code' => '27',
            'desc' => 'Sync',
            'badge' => 'success',
        ]);


        DB::table('parameter_statuses')->insert([
            'code' => '30',
            'desc' => 'Not Found',
            'badge' => 'danger',
        ]);


        DB::table('parameter_statuses')->insert([
            'code' => '40',
            'desc' => 'Complete',
            'badge' => 'success',
        ]);


    }
}
