public function boot(){
    Broadcast::routes(['middleware' => ['jwt.auth']]);
    require base_path('routes/channels.php');
}