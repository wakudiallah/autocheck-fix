<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterPartnerOverseas;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Share\CheckBalanceController;
use App\Http\Controllers\Share\WController;
use App\Http\Controllers\Share\PastVehicleController;
use Session;
use App\Model\VehicleDuplicateChecking;
use Redirect;

class SearchExtraController extends Controller
{
    
    public function __construct()
    {
        $this->needSignature = config('carvx_extrareport.needSignature');
        $this->raiseExceptions = config('carvx_extrareport.raiseExceptions');
        $this->isTest = config('carvx_extrareport.isTest');
        $this->url = config('carvx_extrareport.url');
        $this->userUid = config('carvx_extrareport.userUid');
        $this->apiKey = config('carvx_extrareport.apiKey');

    }


    public function index()
    {
        $myId = Auth::user()->id;


        $vehicle = VehicleChecking::where('created_by', $myId)
                        ->whereNotIn('status', ['30', '40'])
                        ->where('real_type_report_id', 'Extra')
                        ->get();

        return view('search.extra', compact('vehicle'));
    }
    

    public function store(Request $request)
    {

        $vin = $request->vehicle;

        
        /* ==== Check pada vehicle checking*/

        $check_vehiclechecking = VehicleChecking::where('vehicle', $vin)
                ->where('real_type_report_id', 'Extra')
                ->count();


        if($check_vehiclechecking >= '1'){

            return redirect()->back()->with(['warning' => 'Chassis has been searched before, Please contact administrator']);
            

        }else{

            /* === Check Balance */

            $get_balance = UserGroup::where('user_id', Auth::user()->real_user_group_id )->first();

            
            $get_fee = ParameterFee::
                where('role_id', 'US')
                ->first();

            

            if($get_balance->balance < $get_fee->fee){

                 return redirect()->back()->with(['warning' => 'Please Top-Up your balance']);


            }else{

                /* === Tolak balance */

                $balance_now = $get_balance->balance - $get_fee->fee;

                $update_balance = UserGroup::where('user_id', Auth::user()->real_user_group_id)->update(array('balance' => $balance_now ));


                $goto_dummyW = new WController();
                $goto_dummyW->index($request);

                $chassisNumber = Session::get('chassisNumber');


                $id_vehicle     = Uuid::uuid4()->tostring();
                $me           = Auth::user()->id;
                $group_me       = Auth::user()->real_user_group_id;
                $role_me       = Auth::user()->role_id;


                $options=array(

                        'needSignature' => $this->needSignature, 
                        'raiseExceptions' => $this->raiseExceptions,
                        'isTest' => $this->isTest
                    );

                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;
                
               

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                $search = $service->createSearch($chassisNumber);



                 //carvx found
                if(!empty($search->cars[0]->chassisNumber)){

                    $is_sent = '0';

                    $api_from = 'CARVX';

                    $data                   =  new VehicleApi;
                    $data->api_from         = $api_from;
                    $data->istest           = $this->isTest; //
                    $data->search_id        = $search->uid; //
                    $data->car_id           = $search->cars[0]->carId; //
                    $data->id_vehicle       = $id_vehicle;
                    $data->vehicle          = $search->cars[0]->chassisNumber;
                    $data->country_origin   = '';
                    $data->brand            = $search->cars[0]->make;
                    $data->model            = $search->cars[0]->model;
                    $data->engine_number    = $search->cars[0]->engine;
                    $data->cc               = '';
                    $data->fuel_type        = '';
                    $data->year_manufacture = $search->cars[0]->manufactureDate;
                    $data->registation_date = '';
                    $data->status           = 'found';
                    $data->request_by       = $me;
                    $data->save();


                    $carId      = $search->cars[0]->carId;
                    $searchId   = $search->uid;
                    $isTest     = $this->isTest;

                    
                    /* ---------- create report API carvx ----------- */
                    //$reportId   = $service->createReport($searchId, $carId, $isTest);  

                    //$reportId = config('autocheck.reportId');

                

                    /*$data                   =  new ReportVehicle;
                    $data->id_vehicle       = $id_vehicle;
                    $data->report_id        = $reportId;
                    $data->save();*/ 


                    //check match data
                    

                    /*
                    if($search->cars[0]->engine == $request->engine_number){
                        $match_engine_number = '1';
                    }else{
                        $match_engine_number = '0';
                    }
                    */

                    
                    $today  = date('HisdmY');
                    $ver_sn = '00'.$api_from.$today;

                    $data                   =  new VehicleStatusMatch;
                    $data->id_vehicle = $id_vehicle;
                    $data->vehicle = $search->cars[0]->chassisNumber;
                    
                    $data->ver_sn = $ver_sn;
                    $data->save();

                }
                //-------------------------  end carvx

                // searching manual 
                else{

                    $is_sent = '0';


                    $data                   =  new VehicleStatusMatch;
                    $data->id_vehicle = $id_vehicle;
                    $data->vehicle = $request->chassis;
                    $data->brand = '0';
                    $data->model = '0';
                    /*$data->engine_number =  $match_engine_number;*/
                    $data->status =  '0';
                    $data->type_verify      = '0';
                    $data->save();
                }
                // ------------- end searching manual




                if(!empty($search->cars[0]->chassisNumber)){
                    $status_api = '20'; //found
                    $searching_by = 'API';
                }else{
                     $status_api = '10'; //failed
                     $searching_by = 'NOT';
                }

                /*table for naza / group*/

                $data                          =  new VehicleChecking;
                
                $data->id_vehicle              = $id_vehicle;
                $data->vehicle                 = $vin;
                $data->is_sent                 = '0';  
                $data->type_report             = "2";
                $data->created_by              =  $me;
                $data->status                  =  $status_api;
                $data->searching_by            = $searching_by;
                $data->group_by                = $role_me;
                $data->company_name            = Auth::user()->real_user_group_id;
                $data->real_type_report_id     = 'Extra';
                
                $data->save();


                $fee = $get_fee->fee;

                $data             =  new HistoryBalance;
                $data->id_vehicle = $id_vehicle;
                $data->balance    = $balance_now;
                $data->credit    = $fee;
                $data->desc       = 'Search Vehicle';
                $data->created_by =  $me;
                $data->user_id     =  $group_me;
                
                $data->save();



                 //history search
                $data                       =  new HistorySearchVehicle;
                
                $data->vehicle              = $request->chassis;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();

                //history user            
                $data                       =  new HistoryUser;
                
                $data->vehicle_id           = $request->chassis;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();


            }


            return redirect()->back()->with(['success' => 'Data saved successfuly']);

            


        }



    }


}
