<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RAccidentHistory;
use App\Model\RRecallHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Http\Controllers\Share\CheckBalanceController;
use App\Http\Controllers\Share\WController;
use App\Http\Controllers\Share\PastVehicleController;
use Session;
use App\Model\VehicleDuplicateChecking;


class SearchHalfController extends Controller
{
   

    public function __construct()
    {
        $this->middleware('auth');
        $this->needSignature = config('carvx_extrareport.needSignature');
        $this->raiseExceptions = config('carvx_extrareport.raiseExceptions');
        $this->isTest = config('carvx_extrareport.isTest');

        $this->url           = config('carvx_extrareport.url');
        $this->userUid       = config('carvx_extrareport.userUid');
        $this->apiKey        = config('carvx_extrareport.apiKey');
    }



    public function index()
    {
        

        $role  = Auth::user()->role_id;
        $user_group_id  = Auth::user()->user_group_id;

        $country  =  ParameterCountryOrigin::where('status', 1)->orderBy('country_origin', 'ASC')->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->orderBy('supplier', 'ASC')->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::orderBy('brand', 'ASC')->get();
        $model    =  ParameterModel::get();
        
        $vehicle  = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id','DESC')->where('company_name', $user_group_id)->get();

        return view('naza.vehicle_checking', compact('country', 'fuel', 'supplier', 'type', 'brand', 'vehicle', 'model'));

    }



    public function store(Request $request)
    {

        
        $group_id_na       = Auth::user()->role_id;  //delete
        $role_me  = Auth::user()->user_group_id; //delete

        $myrole = Auth::user()->role_id;
        $myGroup = Auth::user()->real_user_group_id;
        $me =  Auth::user()->id;

        

        $get_balance_group = UserGroup::where('user_id', $myGroup)
                        ->first();

        $balance = $get_balance_group->balance;

        $group_code =  Auth::user()->user_group_id; //delete


        $fee = ParameterFee::latest('id')
                ->where('fee_for', '22')
                ->first();
       

        if($balance <= $fee->fee){ /* check balance under 200 */

            return redirect()->back()->with(['warning' => 'Please Top-Up your balance']);
            

        }else{ /* else check balance under 200 */


            $vin = $request->vehicle;
            $is_test = $request->is_test;

            
            /*check 5 first character coz must add W*/
            $check_vin = substr($vin, 0, 5);

            $goto_dummyW = new WController();
            $goto_dummyW->index($request);

            $chassisNumber = Session::get('chassisNumber');
            /* end check 5 first character coz must add W*/



            $model       = $request->model;
            $ret         = explode('|', $model);
            $value_model = $ret[0];
            $value_brand = $request->brand;

            $date =  date('Y-m-d ', strtotime($request->registered_date)); 

            $id_vehicle     = Uuid::uuid4()->tostring();
            $me           = Auth::user()->id;
            $group_me       = Auth::user()->group_by;
            $role_me       = Auth::user()->role_id;
            $balance_me = Auth::user()->role_id; //id balance

            $company_name = Auth::user()->user_group_id;

            $check_brand = Brand::where('id', $value_brand)->first();
            $check_model = ParameterModel::where('id', $value_model)->first();

            $get_brand = $check_brand->brand;
            $get_model = $check_model->model;

            $vehiclepast = VehiclePast::where('vehicle_id_number', $chassisNumber)->count();

            $vehicleapi = VehicleApi::where('vehicle', $chassisNumber)->count();


            $check_vehiclechecking = VehicleChecking::where('vehicle', $vin)
                    ->count();

            

            /*check data in past database*/
            if($vehiclepast >= '1' OR $check_vehiclechecking >= '1'){ //cek di past

                
                $data = new VehicleDuplicateChecking;
                $data->id_vehicle = $id_vehicle;
                $data->vehicle = $vin;
                $data->real_type_report_id = 'Half';
                $data->created_by = $me;
                $data->status = false;
                $data->save();


                return redirect()->back()->with(['warning' => 'Chassis has been searched before']);
            

            }

            
            elseif($vehiclepast == '0' && $vehicleapi == '0')
            {
                $options=array(

                        'needSignature' => $this->needSignature, 
                        'raiseExceptions' => $this->raiseExceptions,
                        'isTest' => $this->isTest
                    );

                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;

                

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                $search = $service->createSearch($chassisNumber);

                
                
                //carvx found
                if(!empty($search->cars[0]->chassisNumber)){

                    
                    $status_api = '20'; //found
                    $searching_by = 'API';
                    $is_sent = '1';




                    $api_from = 'CARVX';

                    $data                   =  new VehicleApi;
                    $data->api_from         = $api_from;
                    $data->istest           = $is_test; //
                    $data->search_id        = $search->uid; //
                    $data->car_id           = $search->cars[0]->carId; //
                    $data->id_vehicle       = $id_vehicle;
                    $data->vehicle          = $search->cars[0]->chassisNumber;
                    $data->country_origin   = '';
                    $data->brand            = $search->cars[0]->make;
                    $data->model            = $search->cars[0]->model;
                    $data->engine_number    = $search->cars[0]->engine;
                    $data->cc               = '';
                    $data->fuel_type        = '';
                    $data->year_manufacture = $search->cars[0]->manufactureDate;
                    $data->registation_date = '';
                    $data->status           = 'found';
                    $data->request_by       = $me;
                    $data->save();
       

                }
                //-------------------------  end carvx

                // searching manual 
                else{

                    

                    $status_api = '20'; //failed
                    $searching_by = 'NOT';
                    $is_sent = '0';

                    $value_fee = $fee->fee; 
      

                }
                // ------------- end searching manual



              

                $data                          =  new VehicleChecking;
                
                $data->id_vehicle              = $id_vehicle;
                $data->vehicle                 = $request->vehicle;
                $data->supplier_id             = $request->supplier;
                $data->type_id                 = $request->type;
                $data->country_origin_id       = $request->country;
                $data->brand_id                = $request->brand;
                $data->model_id                = $value_model;
                $data->cc                      = $request->engine_capacity;
                $data->engine_number           = $request->engine_number;
                $data->vehicle_registered_date = $date;
                $data->fuel_id                 = $request->fuel;
                $data->is_sent                 =  $is_sent;
                $data->created_by              =  $me;
                $data->status                  =  $status_api;
                $data->searching_by            = $searching_by;
                $data->group_by                = "NA";
                $data->company_name            = Auth::user()->real_user_group_id;
                $data->type_report             = "3";
                $data->real_type_report_id     = "Half";
                
                $data->save();

                

                // Balance 
                $value_fee = $fee->fee; 
                $balance_now = $balance - $value_fee;

                $update_balance = UserGroup::where('user_id', $myGroup)->update(array('balance' => $balance_now ));


               


                $data             =  new HistoryBalance;

                $data->id_vehicle = $id_vehicle;
                $data->balance    = $balance_now;
                $data->credit     = $value_fee;
                $data->desc       = 'Search Vehicle';
                $data->created_by =  $me;
                $data->user_id    =  $myGroup;
                
                $data->save();


                //history search
                $data                       =  new HistorySearchVehicle;
                
                $data->vehicle              = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();

                //history user            
                $data                       =  new HistoryUser;
                
                $data->vehicle_id           = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();


                $get_user_submit = User::where('id', $me)->first();
                $user_submit =  $get_user_submit->name;
                $user_company = $get_user_submit->company_website;
                $user_phone = $get_user_submit->phone;


                 $user_group_id = Auth::user()->user_group_id;

                $user_group_submit = UserGroup::where('user_id', $user_group_id)->first();
                //$company_submit = $user_group_submit->group_name;

                $today  = date('d F Y');


               /*$dataq = array('name'=>$vin, 'me' => $user_submit, 'today' => $today, 'user_company' => $user_company, 'user_phone' => $user_phone, 'company_submit' => $company_submit);*/
                

               /* Mail::send('email.notif', $dataq, function($message) {
                     $message->to('wakudiallah05@gmail.com', 'Autocheck')->subject('Submit Vehicle');
                     $message->cc('autocheckmalaysia@gmail.com');
                     $message->from('autocheckmalaysia2@gmail.com','Submit Vehicle');
           });*/



                return redirect()->back()->with(['success' => 'Data saved successfuly']);


            }else{
                return redirect()->back()->with(['success' => 'Data saved successfuly']);
            }


        }/* end check balance under 200 */
        
        
    }



}
