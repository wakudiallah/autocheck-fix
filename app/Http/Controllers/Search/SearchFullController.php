<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterPartnerOverseas;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Share\CheckBalanceController;
use App\Http\Controllers\Share\WController;
use App\Http\Controllers\Share\PastVehicleController;
use Session;
use App\Model\VehicleDuplicateChecking;



class SearchFullController extends Controller
{
    //use CheckBalanceController;
    protected $soapWrapper;

    public function __construct()
    {
        $this->middleware('auth');
        $this->needSignature = config('carvx_extrareport.needSignature');
        $this->raiseExceptions = config('carvx_extrareport.raiseExceptions');
        $this->isTest = config('carvx_extrareport.isTest');
        $this->url = config('carvx_extrareport.url');
        $this->userUid = config('carvx_extrareport.userUid');
        $this->apiKey = config('carvx_extrareport.apiKey');

    }


    public function index(){

        $me = Auth::user()->id;

        $vehicle    =  VehicleChecking::whereNotIn('status', ['40','30'])->where('created_by', $me)->get();

        

        //return view('admin.search.index', compact('vehicle'));
        return view('fullreport.search.index', compact('vehicle'));

    }


   


    public function store(Request $request)
    {
         

       /*check total balance <= 200 */
        $role_me  = Auth::user()->role_id;
        $myGroup  = Auth::user()->real_user_group_id;
        
        $get_balance_group = UserGroup::where('user_id', $myGroup)->first();

        $balance = $get_balance_group->balance;


        $user_group_id  = Auth::user()->real_user_group_id;

       
        $fee = ParameterFee::latest('id')->where('fee_for', '33')->first();
                // 33 = Kastam



        if($balance <= $fee->fee){ /* check balance under 200 */

            
            return redirect('search-vehicle')->with(['warning' => 'Please Top-Up your balance']);


        }else{ /* else check balance under 200 */

            /*Check to partner*/

            $vin = $request->vehicle;
            $is_test = $request->is_test;

             
           
            $goto_dummyW = new WController();
            $goto_dummyW->index($request);

            $chassisNumber = Session::get('chassisNumber');




            $model       = $request->model;
            $ret         = explode('|', $model);
            $value_model = $ret[0];
            $value_brand = $request->brand;

            $date =  date('Y-m-d ', strtotime($request->registered_date)); 

            $id_vehicle     = Uuid::uuid4()->tostring();
            $me           = Auth::user()->id;
            $group_me       = Auth::user()->group_by;
            $role_me       = Auth::user()->role_id;
            $balance_me = Auth::user()->balance; //id balance

            

            $vehiclepast = VehiclePast::where('vehicle_id_number', $chassisNumber)
                                    ->OrWhere('vehicle_id_number', $vin)
                                    ->count();

            $vehicleapi = VehicleChecking::where('vehicle', $vin)
                                    ->where('group_by', 'KA')
                                    ->count();


            /*check data in past database*/
            if($vehiclepast >= '1' ){ //cek di past

                return redirect('/search-vehicle')->with(['warning' => 'Chassis has been searched before, Please contact administrator']);

            }
            elseif($vehicleapi >= '1'){  //check di API (ada)

                $data = new VehicleDuplicateChecking;
                $data->id_vehicle = $id_vehicle;
                $data->vehicle = $vin;
                $data->real_type_report_id = 'Full';
                $data->created_by = $me;
                $data->status = false;
                $data->save();

                
               return redirect('/search-vehicle')->with(['warning' => 'Chassis has been searched before, Please contact administrator']);
            }

            /*check API */
            elseif($vehiclepast == '0' && $vehicleapi == '0')
                
            {
                $options=array(

                        'needSignature' => $this->needSignature, 
                        'raiseExceptions' => $this->raiseExceptions,
                        'isTest' => $this->isTest
                    );

                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;
                
               

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                $search = $service->createSearch($chassisNumber);

               
                //exit();
                
                
                //carvx found
                if(!empty($search->cars[0]->chassisNumber)){

                    $is_sent = '0';

                    $api_from = 'CARVX';

                    $data                   =  new VehicleApi;
                    $data->api_from         = $api_from;
                    $data->istest           = $is_test; //
                    $data->search_id        = $search->uid; //
                    $data->car_id           = $search->cars[0]->carId; //
                    $data->id_vehicle       = $id_vehicle;
                    $data->vehicle          = $search->cars[0]->chassisNumber;
                    $data->country_origin   = '';
                    $data->brand            = $search->cars[0]->make;
                    $data->model            = $search->cars[0]->model;
                    $data->engine_number    = $search->cars[0]->engine;
                    $data->cc               = '';
                    $data->fuel_type        = '';
                    $data->year_manufacture = $search->cars[0]->manufactureDate;
                    $data->registation_date = '';
                    $data->status           = 'found';
                    $data->request_by       = $me;
                    $data->save();


                    $carId      = $search->cars[0]->carId;
                    $searchId   = $search->uid;
                    $isTest     = $is_test;

                
                }
                //-------------------------  end carvx

                // searching manual 
                else{

                    $is_sent = '0';


                    $data                   =  new VehicleStatusMatch;
                    $data->id_vehicle = $id_vehicle;
                    $data->vehicle = $request->vehicle;
                    $data->brand = '0';
                    $data->model = '0';
                    /*$data->engine_number =  $match_engine_number;*/
                    $data->status =  '0';
                    $data->type_verify      = '0';
                    $data->save();
                }
                // ------------- end searching manual



                if(!empty($search->cars[0]->chassisNumber)){
                    $status_api = '20'; //found
                    $searching_by = 'API';
                }else{
                     $status_api = '10'; //failed
                     $searching_by = 'NOT';
                }

               

                $data                          =  new VehicleChecking;
                
                $data->id_vehicle              = $id_vehicle;
                $data->vehicle                 = $request->vehicle;
                $data->is_sent                 = $is_sent;  
                $data->type_report             = "4";
                $data->created_by              =  $me;
                $data->status                  =  $status_api;
                $data->searching_by            = $searching_by;
                $data->group_by                = $role_me;
                $data->real_type_report_id     = 'Full';
                $data->company_name            = Auth::user()->real_user_group_id;
                  $data->save();

                

                /* Balance & Fee */

                $value_fee = $fee->fee; 
                $balance_now = $balance - $value_fee;


                //$update_balance = UserGroup::where('user_id', $role_me)->update(array('balance' => $balance_now ));
                $update_balance = UserGroup::where('user_id', $myGroup)->update(array('balance' => $balance_now ));
                /* end Balance & Fee */



                 $data             =  new HistoryBalance;

                $data->id_vehicle = $id_vehicle;
                $data->balance    = $balance_now;
                $data->credit    = $value_fee;
                $data->desc       = 'Search Vehicle';
                $data->created_by =  $me;
                $data->user_id     =  Auth::user()->real_user_group_id;
                //user id is user grup in user table
                
                $data->save();

                /* end Balance & Fee */


                //history search
                $data                       =  new HistorySearchVehicle;
                
                $data->vehicle              = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();

                //history user            
                $data                       =  new HistoryUser;
                
                $data->vehicle_id           = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();


               $get_user_submit = User::where('id', $me)->first();
                $user_submit =  $get_user_submit->name;
                $user_phone = $get_user_submit->phone;

                $user_group_submit = UserGroup::where('user_id', $user_group_id)->first();
                $company_submit = $user_group_submit->group_name;


                $user_company = $user_group_submit->group_name;


                $today  = date('d F Y');


               $dataq = array('name'=>$vin, 'me' => $user_submit, 'today' => $today, 'user_company' => $user_company, 'user_phone' => $user_phone, 'company_submit' => $company_submit);
                

                /*Mail::send('email.notif', $dataq, function($message) {
                     $message->to('wakudiallah05@gmail.com', 'Autocheck')->subject('Submit Vehicle');
                     $message->cc('autocheckmalaysia@gmail.com');
                     $message->from('autocheckmalaysia2@gmail.com','Submit Vehicle');
        });*/



                return redirect('/search-vehicle')->with(['success' => 'Data saved successfuly']);


            }else{
                return redirect('/search-vehicle')->with(['success' => 'Data saved successfuly']);
            }


        }/* end check balance under 200 */


        
    }



     public function storexx(Request $request){


            $me = Auth::user()->id;

            $vin = $request->vehicle;


            //0. Check Past Data
            $goto_vehicle_checked = new PastVehicleController();
            print $goto_vehicle_checked->index($request);

            $count_past = Session::get('count_past');

            if($count_past == '1'){

                return redirect()->back()->with(['warning' => 'Chassis has been searched before, Please contact administrator']);
            }


            //1. Check Balance
            $goto_balcontrol = new CheckBalanceController();
            print $goto_balcontrol->index();

            $must_top_up = Session::get('must_top_up');

            if($must_top_up == '1'){

                return redirect()->back()->with(['warning' => 'Please Top-Up your balance']);
            }

         
  
            //2. Check Chassis from partner
            $goto_dummyW = new WController();
            $goto_dummyW->index($request);


            $chassisNumber = Session::get('key');



    }



}
