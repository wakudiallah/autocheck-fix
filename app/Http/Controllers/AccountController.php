<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Model\DetailBuyer;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $id_user = Auth::user()->id;

        $data = User::where('id', $id_user)->first();
        $role = Role::get();

        return view('share.account.index', compact('data', 'role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function change_password()
    {
        $id_user = Auth::user()->id;

        $data = User::where('id', $id_user)->first();
        $role = Role::get();

        return view('share.account.change_password', compact('data', 'role'));
    }
    
    public function update_password(Request $request, $id)
    {
        $password =  $request->password;

        $data = User::where('id',$id)->update(array('password' => Hash::make($request->password)));

        return redirect()->back()->with(['success' => 'Password successfully updated']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user = Auth::user()->id;
        
        $destinationPath = 'images/profile';

        if($request->hasFile('fileToUpload')) {
            $file = $request->file('fileToUpload');
            $extension = $file->getClientOriginalExtension();
            $file_name = 'profile-'. $id. '.' . $extension;
            
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;

            $st = User::find($id);  
            $st->picture = $upload_file;
            $st->save();

        
        return redirect('/account')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $home_address =  $request->input('home_address');
        $mailing_address =  $request->input('mailing_address');
        $postcode =  $request->input('postcode');
        $state =  $request->input('state');
        $contry =  $request->input('contry');
        $bank_account =  $request->input('bank_account');
        $bank_account_number =  $request->input('bank_account_number');
        $ic =  $request->input('ic');
        $gender =  $request->input('gender');

        $data = DetailBuyer::where('user_id',$id)->update(array('home_address' => $home_address, 'mailing_address' => $mailing_address , 'postcode' => $postcode, 'bandar' => $state, 'negara' => $contry, 'account_bank' => $bank_account, 'account_bank_number' => $bank_account_number,  'ic' => $ic,  'gender' => $gender ));

        return redirect('account')->with(['success' => 'Detail User successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
