<?php

namespace App\Http\Controllers\Share;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use Session;

class CheckBalanceController extends Controller
{
    

    public function index(){

        $me = Auth::user()->id;

        $get_fee = User::join('parameter_fees', 'parameter_fees.role_id', '=', 'users.role_id')
                    ->where('users.id', $me)
                    ->select('fee')
                    ->first();


        $get_balance = User::join('user_groups', 'users.real_user_group_id', '=', 'user_groups.user_id')
                        ->where('users.id', $me)
                        ->select('user_groups.balance')
                        ->first();


        if($get_balance->balance <= $get_fee->fee){ //return to tupup

                Session::put('must_top_up', '1'); 

        }else{ //return to min saldo

            /*$data             =  new HistoryBalance;

            $data->id_vehicle = $id_vehicle;
            $data->balance    = $balance_now;
            $data->credit    = $value_fee;
            $data->desc       = 'Search Vehicle';
            $data->created_by =  $me;
            $data->user_id     =  Auth::user()->is_role_kastam;
            //user id is user grup in user table
            
            $data->save();*/

            Session::put('must_top_up', '0'); 
        }



    }

}
