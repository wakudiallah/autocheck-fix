<?php

namespace App\Http\Controllers\Share;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleDuplicateChecking;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use Session;

class PastVehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }


    public function index(Request $request){

        $me = Auth::user()->id;

        $vin = $request->vehicle;

        $type_report_of_me = User::join('user_groups', 'user_groups.user_id', '=', 'users.real_user_group_id')
                            ->join('user_group_type_reports', 'user_group_type_reports.user_group_id', '=', 'user_groups.id')
                            ->where('users.id', $me)
                            ->first();


        $type_report = $type_report_of_me->type_report_id;


        $check_vehicle_checking = VehicleChecking::where('real_type_report_id', $type_report)
                                ->where('vehicle', $vin)
                                ->count();


        $get_id_vehicle = VehicleChecking::where('real_type_report_id', $type_report)
                                ->where('vehicle', $vin)
                                ->first();

        
        if($check_vehicle_checking > 0){

            /*$data = new VehicleDuplicateChecking;
            $data->id_vehicle = $get_id_vehicle->id_vehicle;
            $data->vehicle = $vin;
            $data->real_type_report_id = $type_report;
            $data->created_by = $me;
            $data->status = true;
            $data->save();*/


            //return redirect()->back()->with(['warning' => 'Chassis has been searched before, Please contact administrator']);
           
        }

        Session::put('count_past', $check_vehicle_checking); 



    }


}
