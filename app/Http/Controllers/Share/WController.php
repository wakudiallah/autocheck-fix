<?php

namespace App\Http\Controllers\Share;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use Session;


class WController extends Controller
{
    
    protected $soapWrapper;
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->needSignature = config('carvx_freesearch.needSignature');
        $this->raiseExceptions = config('carvx_freesearch.raiseExceptions');
        $this->isTest = config('carvx_freesearch.isTest');
        $this->url = config('carvx_freesearch.url');
        $this->userUid = config('carvx_freesearch.userUid');
        $this->apiKey = config('carvx_freesearch.apiKey');

    }


    public function index(Request $request){

            $vin = $request->vehicle; 
            $is_test = $request->is_test;
       
            $options=array(

                        'needSignature' => $this->needSignature, 
                        'raiseExceptions' => $this->raiseExceptions,
                        'isTest' => $this->isTest
                    );

                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;


                $service = new CarvxService($url, $userUid, $apiKey, $options);
                $search = $service->createSearch($vin);


                if(!empty($search->cars[0]->chassisNumber)){  //found in partner

                    $chassisNumber = $search->cars[0]->chassisNumber;
                    
                    Session::put('chassisNumber', $chassisNumber);
                   

                }else{  //check once again with W

                    $vinx = substr_replace( $vin, "W", 5, 0);

                    
                    
                    if(!empty($search->cars[0]->chassisNumber)){

                        $search = $service->createSearch($vinx);
                        $chassisNumber = $search->cars[0]->chassisNumber;
                       
                        Session::put('chassisNumber', $chassisNumber);

                    }else{

                        Session::put('chassisNumber', $vin); //vin is original submit

                    }

                }



               
            

    }



}
