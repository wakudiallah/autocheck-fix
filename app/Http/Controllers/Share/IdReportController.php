<?php

namespace App\Http\Controllers\Share;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;

class IdReportController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function store(Request $request, $id)
    {
        
        $me    = Auth::user()->id;
        $engine = $request->engine;


        $form= new ReportVehicle();
        $form->id_vehicle = $id;;
        $form->report_id = $engine;
        $form->is_ready = "3";
        $form->data = "1";
        $form->save();


       VehicleChecking::where('id_vehicle',$id)->update(array(
            'is_check_id' => '1',
            'status' => '27'
        ));


        return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }


    
    public function apichangemanual($id)
    {
        VehicleChecking::where('id_vehicle',$id)->update(array(
            'searching_by' => 'NOT',
            'status' => '28'
        ));


        return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);  
    }


}
