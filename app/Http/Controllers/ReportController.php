<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Model\VehicleChecking;
use App\Model\ParameterFee;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use DB;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RRecallHistory;
use App\Model\RAccidentHistory;
use App\Model\ParameterSupplier;
use Excel;
use Auth;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\User;
use App\Model\VehicleStatusMatch;
use App\Model\UserGroupOld;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {

        $data = VehicleChecking::where('id_vehicle', $id)->first();

        

        $pdf = PDF::loadView('admin.report.report_autocheck', compact('data'));
             
        return $pdf->stream('report - '.$data->vehicle.'.pdf', array('Attachment'=>false));
        

    }

    public function kastam_api($id)
    {
        $data = VehicleChecking::where('id_vehicle', $id)->first();
        $api_kastam = VehicleApiKastam::where('vehicle_id', $id)->first();
        $image_api = VehicleApiKastamImage::where('vehicle_id', $id)->get();

        //return view('admin.report.short_report_kastam', compact('data', 'api_kastam', 'image_api'));

        $pdf = PDF::loadView('admin.report.short_report_kastam', compact('data', 'api_kastam', 'image_api'));
             
        return $pdf->stream('report - '.$data->vehicle.'.pdf', array('Attachment'=>false));
    }


    public function kastam_mn($id)
    {
        $data = VehicleChecking::where('id_vehicle', $id)->first();
        $api_kastam = VehicleApiKastam::where('vehicle_id', $id)->first();
        $image_api = VehicleApiKastamImage::where('vehicle_id', $id)->get();

        /*foreach($image_api as $image_api){
            dd(json_decode($image_api->image, true));
        }*/
        
        

        $pdf = PDF::loadView('admin.report.short_report_kastam_mn', compact('data', 'api_kastam', 'image_api'));
             
        return $pdf->stream('report - '.$data->vehicle.'.pdf', array('Attachment'=>false));
    }


    public function kastam_api_mn($id)
    {
        $data = VehicleChecking::where('id_vehicle', $id)->first();
        $api_kastam = VehicleApiKastam::where('vehicle_id', $id)->first();
        $image_api = VehicleApiKastamImage::where('vehicle_id', $id)->get();

        /*foreach($image_api as $image_api){
            dd(json_decode($image_api->image, true));
        }*/
        


        $pdf = PDF::loadView('admin.report.short_report_kastam_mn_api', compact('data', 'api_kastam', 'image_api'));
             
        return $pdf->stream('report - '.$data->vehicle.'.pdf', array('Attachment'=>false));
    }
    

    public function report_mn($id)
    {

        $data = VehicleChecking::where('id_vehicle', $id)->first();

        

        $pdf = PDF::loadView('admin.report.report_autocheck_manual', compact('data'));
             
        return $pdf->stream('report - '.$data->vehicle.'.pdf', array('Attachment'=>false));
        

    }


    


    public function report_short_past($id)
    {
        $data = VehiclePast::where('Id_vehicle', $id)->first();

        

        $pdf = PDF::loadView('admin.report.report_short_past', compact('data'));
             
        return $pdf->stream('report - '.$data->VehicleIdNumber.'.pdf', array('Attachment'=>false));
    }


    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function carvx($id)
    {
        $raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->get();
        $raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->get();
        $raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->get();
        $raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->get();
        $raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->get();
        $raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->get();


        $check_raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->first();
        $check_raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->first();
        $check_raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->first();
        $check_raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->first();
        $check_raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->first();
        $check_raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->first();

        $recall = RRecallHistory::where('id_vehicle', $id)
                ->get();

        $summary = RSummary::where('id_vehicle', $id)
                    ->latest('id')
                    ->first();

        $usagehistory = RUsageHistory::where('id_vehicle', $id)
                ->latest('id')
                ->first();
                
        $vehicleassessment = RVehicleAssessment::where('id_vehicle', $id)->latest('id')->first();
        $vehicledetail = RVehicleDetails::where('id_vehicle', $id)->first();
        $vehiclespesification = RVehicleSpecification::where('id_vehicle', $id)->first();
        $auctionimage = RAuctionImages::where('id_vehicle', $id)->orderBy('id', 'DESC')->get();
        $detailhistory = RDetailHistory::where('id_vehicle', $id)->get();
        $auctionhistory = RAuctionHistory::where('id_vehicle', $id)->get();
        $odometerhistory = ROdometerHistory::where('id_vehicle', $id)->get();
        $vehicle = VehicleChecking::where('id_vehicle', $id)->first();

        $pdf = PDF::loadView('admin.report.report_carvx', compact('summary', 'usagehistory', 'vehicleassessment', 'vehicledetail', 'vehiclespesification', 'auctionimage', 'detailhistory', 'auctionhistory', 'odometerhistory', 'vehicle', 'recall', 'raccident_collision', 'raccident_malfunction', 'raccident_theft', 'raccident_fireDamage', 'raccident_waterDamage', 'raccident_hailDamage', 'check_raccident_collision', 'check_raccident_malfunction', 'check_raccident_theft', 'check_raccident_fireDamage', 'check_raccident_waterDamage', 'check_raccident_hailDamage'));

             
        return $pdf->stream('report - '.$vehicle->vehicle.'.pdf', array('Attachment'=>false, 'setIsRemoteEnabled' => false));
        /*
        return view('admin.report.report_carvx', compact('data', 'data2'));*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function buyer_group()
    {
        $data = UserGroup::get();

        return view('management.report_buyer_group.index', compact('data'));
    }


    public function buyer_group_view(Request $request)
    {
         
        $from  = $request->input('from');
        $to  = $request->input('to');
        $buyer_group  = $request->input('buyer_group');


        $viewdate1 = date("d-m-Y", strtotime($from));
        $viewdate2 = date("d-m-Y", strtotime($to));
        $date1 =  date('Y-m-d 00:00:01', strtotime($from)); 
        $date2 =  date('Y-m-d 23:59:59', strtotime($to)); 


        $user_group = UserGroup::get();
        
       

        if($buyer_group == 'All'){
             $data= DB::table('vehicle_checkings')
              ->leftJoin('user_groups', 'user_groups.user_id', '=', 'vehicle_checkings.company_name')
              ->select( 'user_groups.group_name as group_name', DB::raw("sum(CASE WHEN (vehicle_checkings.updated_at >= '$date1' AND vehicle_checkings.updated_at <= '$date2') AND (vehicle_checkings.status = '40' )  THEN 1 ELSE 0 END) AS total"), DB::raw("sum(CASE WHEN (vehicle_checkings.updated_at >= '$date1' AND vehicle_checkings.updated_at <= '$date2') AND (vehicle_checkings.status = '40' ) AND (vehicle_checkings.searching_by = 'NOT')  THEN 1 ELSE 0 END) AS total_not"),
                DB::raw("sum(CASE WHEN (vehicle_checkings.updated_at >= '$date1' AND vehicle_checkings.updated_at <= '$date2') AND (vehicle_checkings.status = '40') AND (vehicle_checkings.searching_by = 'API') THEN 1 ELSE 0 END) AS total_api"))
             //->where('vehicle_checkings.group_by', $buyer_group)
             ->groupBy('user_groups.group_name')
             ->get();
            
        }
        else{
            

            $data= DB::table('vehicle_checkings')
              ->leftJoin('user_groups', 'user_groups.user_id', '=', 'vehicle_checkings.company_name')
              ->select( 'user_groups.group_name as group_name', DB::raw("sum(CASE WHEN (vehicle_checkings.updated_at >= '$date1' AND vehicle_checkings.updated_at <= '$date2') AND (vehicle_checkings.status = '40' )  THEN 1 ELSE 0 END) AS total"), DB::raw("sum(CASE WHEN (vehicle_checkings.updated_at >= '$date1' AND vehicle_checkings.updated_at <= '$date2') AND (vehicle_checkings.status = '40' ) AND (vehicle_checkings.searching_by = 'NOT')  THEN 1 ELSE 0 END) AS total_not"),
                DB::raw("sum(CASE WHEN (vehicle_checkings.updated_at >= '$date1' AND vehicle_checkings.updated_at <= '$date2') AND (vehicle_checkings.status = '40') AND (vehicle_checkings.searching_by = 'API') THEN 1 ELSE 0 END) AS total_api"))
             ->where('vehicle_checkings.company_name', $buyer_group)
             ->groupBy('user_groups.group_name')
             ->get();
            
        }

        return view('management.report_buyer_group.view', compact('data', 'user_group','buyer_group', 'from', 'to'));


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function summary_report()
    {
        
        $user = UserGroup::get();

        return view('management.report_summary.index', compact('user'));
    }


    public function summary_report_view(Request $request)
    {
         
        $date  = $request->input('month');
        $group  = $request->input('group');


        //$viewdate1 = date("d-m-Y", strtotime($month));
        //$viewdate2 = date("d-m-Y", strtotime($to));
        $month =  date('m', strtotime($date)); 
        $year =  date('Y', strtotime($date)); 

        $user = UserGroup::get();
        

        if($group == 'All'){
            
            $vin =  VehicleChecking::whereIn('status', ['40'])->whereYear('created_at', $year)->whereMonth('created_at', $month)->orderBy('id','ASC')->get();
            
	   }elseif($group == "NA" OR $group == "MQ" OR $group == "TU") {
		
		$vin =  VehicleChecking::whereIn('status', ['40'])->whereYear('created_at', $year)->whereMonth('created_at', $month)->where('group_by', $group)->orderBy('id','ASC')->get();
	    
	   }
	   else{
			
	       $ka = UserGroup::where('user_id', $group)->first();

	       $is_ka = $ka->branch;	
            
            $vin =  VehicleChecking::whereIn('status', ['40'])->whereYear('created_at', $year)->whereMonth('created_at', $month)->where('company_name', $is_ka)->orderBy('id','ASC')->get();

        }

        return view('management.report_summary.view', compact('vin', 'month','group', 'user', 'date'));


    }

    public function summary_report_print(Request $request)
    {
        $date  = $request->input('month');
        $group  = $request->input('group');

        $month =  date('m', strtotime($date)); 
        $year =  date('Y', strtotime($date)); 

        $user = UserGroup::get();
        

        if($group == 'All'){
            
            $vin =  VehicleChecking::whereIn('status', ['30', '40'])->whereYear('created_at', $year)->whereMonth('created_at', $month)->orderBy('id','ASC')->get();
            
	   }
	
        elseif($group == "NA" OR $group == "MQ" OR $group == "TU") {
            
            $vin =  VehicleChecking::whereIn('status', ['30', '40'])->whereYear('created_at', $year)->whereMonth('created_at', $month)->where('group_by', $group)->orderBy('id','ASC')->get();

	   }
	   else{
		
        $ka = UserGroup::where('user_id', $group)->first();

        $is_ka = $ka->branch;

            $vin =  VehicleChecking::whereIn('status', ['40'])->whereYear('created_at', $year)->whereMonth('created_at', $month)->where('company_name', $is_ka)->orderBy('id','ASC')->get();


	   }

        $current = date('d-m-Y');

        $excel = Excel::create('Summary Report '.$current, function($excel) use( $vin,$month) {

            $excel->setTitle('Summary Report');

            // Chain the setters
            $excel->setCreator('Autocheck')->setCompany('Autocheck.com.my');

            $excel->sheet('summary', function($sheet) use($vin, $month) {
                 $sheet->setColumnFormat(array(
   
                'D' => '0'
            ));

            

                $i = 1;
                    foreach($vin as $chassis) {

                        if($chassis->status == '40'){
                            $status = 'Complete';
                        }
                        elseif($chassis->status == '30'){
                            $status = 'Not Found';
			}

			if($chassis->group_by == "KA"){
				$getgroup = UserGroup::where('branch', $chassis->company_name)->first();
			}else{
				$getgroup = UserGroup::where('user_id', $chassis->group_by)->first();

			}


                     $data[] = array(
                        $i++,
                        $chassis->vehicle,
                        $status,
                        date("d/m/Y", strtotime($chassis->created_at)),
                        date("d/m/Y", strtotime($chassis->updated_at)),
                        $getgroup->group_name
                    );

                    $sheet->cell("A{$i}:F{$i}", function($cell) {

                    $cell->setBackground('#ffffff')
                        ->setFontColor('#000000')
                        ->setBorder( 'thin','thin','thin','thin');
                    });
                }

                $current = date('Y-m-d ');

                $sheet->fromArray($data, null, 'A1', false, false);

                    $ir = $i-1;
                    $is = $i+1;
                   
                  $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin','thin','thin','thin');

                $headings = array('No', 'Chassis No.','Status','Date Submission', 'Date Verify', 'Groups');
                
                $sheet->prependRow(1, $headings);

                    $sheet->prependRow(1, ["Summary Report"]);
                    $sheet->mergeCells("A1:F1");


                    $sheet->prependRow(2, [""]);
                    $sheet->mergeCells("B2:F2");


                    $sheet->cell("A1:F1", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });



                    $sheet->setSize('F1', 25, 50);

                    $sheet->cell("A3:F3", function($cell) {
                       
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center')
                            ->setFontWeight('bold')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                   

                    

                    $sheet->setSize('A1', 10, 18);
                    $sheet->setSize('B1', 35, 18);
                    $sheet->setSize('C1', 25, 18);
                    $sheet->setSize('D1', 25, 18);
                    $sheet->setSize('E1', 25, 18);
                    $sheet->setSize('F1', 25, 18);  

                    $sheet->setSize('A3', 10, 18);
                    $sheet->setSize('B3', 35, 18);
                    $sheet->setSize('C3', 25, 18);
                    $sheet->setSize('D3', 25, 18);
                    $sheet->setSize('E3', 25, 18); 
                    $sheet->setSize('F3', 25, 18); 

                    
                    
                    
                    /*$sheet->cell("B11", function($cell) {
                        // change header color
                      
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    }); */
                      
                                      
                });

                /* Get Email */
                
               
      

        return redirect('/report-summary-view')->with(['update' => 'Assessment data successfully downloaded']);
              })->download('xlsx');

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function report_vehicle()
    {
        $supplier = ParameterSupplier::get();

        return view('naza.report.index', compact('supplier'));
    }


    public function report_vehicle_view(Request $request)
    {
         
        $date  = $request->month;
        $supplier  = $request->supplier;

        $role_me = Auth::user()->role_id;
        //$viewdate1 = date("d-m-Y", strtotime($month));
        //$viewdate2 = date("d-m-Y", strtotime($to));
        $month =  date('m', strtotime($date)); 
        $year =  date('Y', strtotime($date)); 

        $data_supplier = ParameterSupplier::get();
        

        if($supplier == 'All'){
            
            $vin =  VehicleChecking::where('group_by', $role_me)->whereYear('created_at', $year)->whereMonth('created_at', $month)->orderBy('id','ASC')->whereNull('deleted_at')->get();
            
        }
        else{
            

            $vin =  VehicleChecking::where('supplier_id', $supplier)->whereYear('created_at', $year)->whereMonth('created_at', $month)->where('group_by', $role_me)->whereNull('deleted_at')->orderBy('id','ASC')->get();

        }

        return view('naza.report.view', compact('vin', 'month','supplier', 'data_supplier', 'date'));


    }

    public function report_vehicle_print(Request $request)
    {
        $date  = $request->month;
        $supplier  = $request->supplier;

        $month =  date('m', strtotime($date)); 
        $year =  date('Y', strtotime($date)); 

         $role_me = Auth::user()->role_id;

        $data_supplier = ParameterSupplier::get();
        

        if($supplier == 'All'){
            
            $vin =  VehicleChecking::where('group_by', $role_me)->whereYear('created_at', $year)->whereNull('deleted_at')->whereMonth('created_at', $month)->orderBy('id','ASC')->get();
            
        }
        else{
            

            $vin =  VehicleChecking::where('supplier_id', $supplier)->whereYear('created_at', $year)->whereMonth('created_at', $month)->where('group_by', $role_me)->whereNull('deleted_at')->orderBy('id','ASC')->get();

        }

        $current = date('d-m-Y');
        $get_month = date('F', strtotime($date)); 

        $excel = Excel::create('Vehicle Report '.$get_month.' - '.$current, function($excel) use( $vin,$month, $get_month) {

            $excel->setTitle('Vehicle Report');

            // Chain the setters
            $excel->setCreator('Autocheck')->setCompany('Autocheck.com.my');

            $excel->sheet('summary', function($sheet) use($vin, $month, $get_month) {
                 $sheet->setColumnFormat(array(
   
                'D' => '0'
            ));

            


                $i = 1;
                    foreach($vin as $chassis) {

                        if($chassis->status == '40'){
                            $status = 'Complete';
                        }

                        elseif($chassis->status == '30'){
                            $status = 'Reject';
                        }else{
                            $status = 'Process';
                        }


                     $data[] = array(
                        $i++,
                        $chassis->vehicle,
                        $status,
                        date("d/m/Y", strtotime($chassis->created_at)),
                        $chassis->supplier->supplier
                    );

                    $sheet->cell("A{$i}:E{$i}", function($cell) {

                    $cell->setBackground('#ffffff')
                        ->setFontColor('#000000')
                        ->setBorder( 'thin','thin','thin','thin');
                    });
                }

                $current = date('Y-m-d ');

                $sheet->fromArray($data, null, 'A1', false, false);

                    $ir = $i-1;
                    $is = $i+1;
                   
                  $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin','thin','thin','thin');

                $headings = array('No', 'Chassis No.','Status','Date Verify', 'Supplier');
                
                $sheet->prependRow(1, $headings);

                    $sheet->prependRow(1, ["Vehicle Report ".$get_month]);
                    $sheet->mergeCells("A1:E1");


                    $sheet->prependRow(2, [""]);
                    $sheet->mergeCells("B2:E2");


                    $sheet->cell("A1:E1", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });



                    $sheet->setSize('E1', 25, 50);

                    $sheet->cell("A3:E3", function($cell) {
                       
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center')
                            ->setFontWeight('bold')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                   

                    $sheet->cell("E5:E5", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->setSize('A1', 10, 18);
                    $sheet->setSize('B1', 35, 18);
                    $sheet->setSize('C1', 25, 18);
                    $sheet->setSize('D1', 25, 18);
                    $sheet->setSize('E1', 25, 18); 

                    $sheet->setSize('A3', 10, 18);
                    $sheet->setSize('B3', 35, 18);
                    $sheet->setSize('C3', 25, 18);
                    $sheet->setSize('D3', 25, 18);
                    $sheet->setSize('E3', 25, 18); 

                    
                    
                    
                    /*$sheet->cell("B11", function($cell) {
                        // change header color
                      
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    }); */
                      
                                      
                });

                /* Get Email */
                
               

        return redirect('/report-vehicle')->with(['update' => 'Vehicle data successfully downloaded']);
              })->download('xlsx');

    }



    public function get_report_monthly()
    {
        $data = UserGroup::get();

        return view('management.report_monthly.index', compact('data'));
    }


    


    public function report_monthly_view(Request $request)
    {
        $date        = $request->month;
        $buyer_group = $request->buyer_group;

        $full_date = "01-".$date;

        $separate_date = date('d',strtotime($full_date));
        $separate_month = date('m',strtotime($full_date));
        $separate_year = date('Y',strtotime($full_date));

        $date1 =  date('Y-m-d 00:00:01', strtotime($full_date)); 
        $date2 =  date('Y-m-d 23:59:59', strtotime($full_date)); 

        

        $user_group = UserGroup::get();
        

           

           /*SELECT
            DATE_FORMAT(created_at,"%Y-%m-%d") AS Date,
            COUNT(*) AS count
            FROM managers
            GROUP BY DATE_FORMAT(created_at,"%Y-%m-%d")
            ORDER BY create,d_at DESC*/
        
        if($buyer_group == 'NA' OR $buyer_group == "MQ" OR $buyer_group == "TU" OR $buyer_group == "US"){

            $report = DB::table('vehicle_checkings')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date')    
            )
            ->where('group_by', $buyer_group)
            ->whereNull('deleted_at')
            ->whereMonth('created_at', $separate_month)
            ->whereYear('created_at', $separate_year)
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get();

            $fee = ParameterFee::latest('id')->where('role_id', 'NA')->first();

	}else{

	$ka = UserGroup::where('user_id', $buyer_group)->first();

	$is_ka = $ka->branch;

            $report = DB::table('vehicle_checkings')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date')    
            )
            ->where('company_name', $is_ka)
            ->whereNull('deleted_at')
            ->whereMonth('created_at', $separate_month)
            ->whereYear('created_at', $separate_year)
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get();

            $fee = ParameterFee::latest('id')->where('role_id', 'KA')->first();

        }
   

        return view('management.report_monthly.view', compact('report', 'user_group', 'separate_month', 'separate_year', 'buyer_group', 'buyer_group', 'date', 'fee'));

    }

    public function report_monthly_print(Request $request)
    {
        $date        = $request->month;

        $month_req        = $request->month;

       $buyer_group = $request->buyer_group;

        $full_date = "01-".$date;

        $separate_date = date('d',strtotime($full_date));
        $separate_month = date('m',strtotime($full_date));
        $separate_year = date('Y',strtotime($full_date));

        $date1 =  date('Y-m-d 00:00:01', strtotime($full_date)); 
        $date2 =  date('Y-m-d 23:59:59', strtotime($full_date)); 

        

        $user_group = UserGroup::get();

           
         
        if($buyer_group == 'NA' OR $buyer_group == "MQ" OR $buyer_group == "TU" OR $buyer_group == "US"){

        $report = DB::table('vehicle_checkings')
            ->leftJoin('user_groups', 'user_groups.user_id', '=', 'vehicle_checkings.group_by')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(vehicle_checkings.created_at, "%Y-%m-%d") as date')    
            )
            
            ->where('vehicle_checkings.group_by', $buyer_group)
            ->whereNull('vehicle_checkings.deleted_at')
            ->whereMonth('vehicle_checkings.created_at', $separate_month)
            ->whereYear('vehicle_checkings.created_at', $separate_year)
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get();

            $user_company = UserGroup::where('user_id', $buyer_group)->first();   
            $buyer_group = $user_company->group_name;

            $fee = ParameterFee::latest('id')->where('role_id', 'NA')->first();

	}else{
	
		$ka = UserGroup::where('user_id', $buyer_group)->first();
       
		$is_ka = $ka->branch;


            $report = DB::table('vehicle_checkings')
            ->leftJoin('user_groups', 'user_groups.user_id', '=', 'vehicle_checkings.company_name')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(vehicle_checkings.created_at, "%Y-%m-%d") as date')    
            )
            
            ->where('vehicle_checkings.company_name', $is_ka)
            ->whereNull('vehicle_checkings.deleted_at')
            ->whereMonth('vehicle_checkings.created_at', $separate_month)
            ->whereYear('vehicle_checkings.created_at', $separate_year)
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get();

            $user_company = UserGroup::where('user_id', $buyer_group)->first();   
            $buyer_group = $user_company->group_name;

            $fee = ParameterFee::latest('id')->where('role_id', 'KA')->first();
        }




        $current = date('d-m-Y');

        $excel = Excel::create('Monthly Report '.$current, function($excel) use( $report,$date, $buyer_group, $separate_month, $separate_year, $fee) {

            $excel->setTitle('Monthly Report');

            // Chain the setters
            $excel->setCreator('Autocheck')->setCompany('Autocheck.com.my');

            $excel->sheet('summary', function($sheet) use($report, $date, $buyer_group, $separate_month, $separate_year, $fee) {
                 $sheet->setColumnFormat(array(
   
                'D' => '0'
                ));

            

                $i = 1;
                    foreach($report as $chassis) {

                    $qty = $fee->fee;
                    $format_qty = number_format($qty, 2 , '.' , ',' );

                    
                    
                    $data[] = array(
                        $i++,
                        date('d/m/Y ', strtotime($chassis->date)),
                        $buyer_group,
                        $chassis->total,
                        $format_qty,
                        number_format($chassis->total*$qty, 2 , '.' , ',' ) ,
                        
                    );

                    $sheet->cell("A{$i}:F{$i}", function($cell) {

                    $cell->setBackground('#ffffff')
                        ->setFontColor('#000000')
                        ->setBorder( 'thin','thin','thin','thin')
                        ->setValignment('center');
                    });

                    
                }

                $current = date('Y-m-d ');

                $sheet->fromArray($data, null, 'A1', false, false);

                    $ir = $i-1;
                    $is = $i+1;

                    
               
                $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin','thin','thin','thin');

                $headings = array('No', 'Submisson Date','Users','Qty', 'Cost (Per Case)', 'Total');
                
                $sheet->prependRow(1, $headings);

                    //$sheet->prependRow(1, ["CARFAX SDN BHD(Reg. No. 917913-T)", '','', "(Reg. No. 917913-T)"]);
                    //$sheet->mergeCells("A1:F1");
                    //
                    

                    $head = array("CARFAX SDN BHD", '',"", "(Reg. No. 917913-T)");
                    $sheet->prependRow(1, $head);


                    $sheet->prependRow(2, [""]);
                    $sheet->mergeCells("A2:F2");

                    $sheet->prependRow(3, [""]);
                    $sheet->mergeCells("A3:F3");


                    $bill_to = array('Bill to:', '','','', 'INVOICE', '');
                    $sheet->prependRow(4, $bill_to);

                    $sheet->prependRow(5, [""]);
                    $sheet->mergeCells("A2:F2");

                    $marii = array('Malaysia Automotive Robotics And IoT Institute (898618-T)', '','','', 'Invoice no', ' :CarFax '.$separate_month.'/'.$separate_year);
                    $sheet->prependRow(6, $marii);

                    $block = array('Block 2280, Jalan Usahawan 2, Cyber 6', '','','', 'Payment', ' :Cheque/Cash');
                    $sheet->prependRow(7, $block);

                    $prev_date = '01-'.$date;
                    $next_date = date('Y-m-d', strtotime($prev_date));
                    $date1 = strtotime($next_date);
                    $next_date1 = date('d/m/Y', strtotime("+1 month", $date1));

                    $slg = array('63000 Cyberjaya, Selangor', '','','', 'Date', ' : '.$next_date1);
                    $sheet->prependRow(8, $slg);

                    $mly = array('Malaysia', '','','', 'Page', ' : 1 & 1 ');
                    $sheet->prependRow(9, $mly);


                    $sheet->prependRow(10, [""]);
                    $sheet->mergeCells("A10:F10");

                    $sheet->prependRow(11, [""]);
                    $sheet->mergeCells("A11:F11");

                    
                    //TOTAL (RM) ROW
                    $total_qty = $i+12;

                    $last_row = $total_qty-1;
                    
                    $value_total = array("TOTAL (RM)", '','', "=SUM(D13:D{$last_row})","", "=SUM(F13:F{$last_row})");
                    $sheet->prependRow($total_qty, $value_total);
                    $sheet->mergeCells("A{$total_qty}:C{$total_qty}");

                        
                    $sheet->cell("A{$total_qty}:F{$total_qty}", function($cell) {
                       
                        $cell->setBackground('#92cddc')
                            ->setFontWeight('bold')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });


                    

                    /*Footer*/
                    $foot   = $i+14;

                    //FOOTER RECEIVED BY
                    $foots  = $foot+3;
                    $foots1 = $foots+1;
                    $foots2 = $foots1+1;
                    $foots3 = $foots2+1;
                    $foots4 = $foots3+1;
                    $foots5 = $foots4+1;
                    $foots6 = $foots5+1;
                    $sheet->prependRow($foot , ["Terms :"]);
                    $sheet->cell("A{$foot}:F{$foot}", function($cell) {
                        $cell
                         ->setBackground('#92cddc')
                         ->setFontWeight('bold')
                        ->setFontColor('#000000');
                    });

                    $sheet->cell("B{$foots}:B{$foots6}", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin')
                        ->setBackground('#ffffff');
                    });

                    $sheet->cell("E{$foots}:E{$foots6}", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin')
                        ->setBackground('#ffffff');
                    });



                    $footerRow = count($report) + 10;
                    $footerRows = count($report)+10 ;

                    $sheet->cell("A1:E1", function($cell) {
                        $cell->setBackground('#17c5d8')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold');
                    });

                    //RECEIVED BY
                    $sheet->appendRow(["",
                        ""
                    ]);
                    $sheet->appendRow($foots ,["",
                        "Received by" 
                    ]);
                    $sheet->appendRow($foots1 ,["",
                        "","","","For CarFax Sdn Bhd,"  
                    ]);
                    $sheet->appendRow($foots2 ,["",
                        "" 
                    ]);
                    $sheet->appendRow($foots3 ,["",
                        ".........................."
                    ]);
                    $sheet->appendRow($foots4 ,["",
                        "Name: ","","",".........................."
                    ]);
                    $sheet->appendRow($foots5 ,["",
                        "Date: ","","","Name: "
                    ]);



                    $sheet->cell("A1:C1", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            //->setAlignment('center')
                            //->setValignment('center');
                            ->setFont(array(
                                'family'     => 'times new roman',
                                'size'       => '37',
                                'bold'       =>  true
                            ));
                    });

                    $sheet->cell("D1:F1", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            //->setAlignment('center')
                            //->setValignment('center');
                            ->setFont(array(
                                'family'     => 'times new roman',
                                'size'       => '13',
                                'bold'       =>  true
                            ));
                    });

                    

                    $sheet->cell("A12:F12", function($cell) {
                       
                        $cell->setBackground('#92cddc')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });

                    $sheet->cell("E4:F5", function($cell) {
                       
                        $cell->setBackground('#92cddc')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });

                    


                    

                    $sheet->cell("A12:A12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("B12:B12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("C12:C12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("D12:D12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("E12:E12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("F12:F12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });


                    $sheet->cell("A13:A13", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           
                    });


                    $sheet->cell("F13:F13", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        
                    });





                    
                    $sheet->cell("A4:A4", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#fc050d');
                    });

                    $sheet->cell("A6:A6", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold');
                    });


 


                   
                    /* square first */
                    $sheet->cell("A4:C9", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    /* end square first */

                    /* square first */
                    $sheet->cell("E4:F9", function($cell) {
                       
                        $cell
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    /* end square first */


                    $sheet->setSize('B1', 20, 18);
                    $sheet->setSize('C1', 15, 15);
                    $sheet->setSize('D1', 15, 15);
                    $sheet->setSize('E1', 20, 18); 
                    $sheet->setSize('F1', 20, 18);

                    $sheet->setSize('A3', 10, 18);
                    $sheet->setSize('B3', 35, 18);
                    $sheet->setSize('C3', 25, 18);
                    $sheet->setSize('D3', 25, 18);
                    $sheet->setSize('E3', 25, 18); 

                                      
                });

                /* Get Email */

        return redirect('/report-summary-view')->with(['update' => 'Assessment data successfully downloaded']);
              })->download('xlsx');
        
    }
	
	public function get_report_chart()
    {


        $data = User::orderBy('last_login', 'DESC')->take(5)->get();

        $total_vehicle = VehicleChecking::count();
        $total_vehicle_verified = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua 
        $total_vehicle_pending = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua

        $total_group = UserGroup::count();


        $data_group = UserGroup::get();
        $data_group_chart = UserGroup::get();

        $balance = UserGroup::get();

        $this_year = date("Y");

        $usergroup = UserGroup::whereNotNull('branch')
                    ->where('user_id', '<>' ,'KA_MARII')->get();

        $usergroup_car_dealer = UserGroup::whereNull('branch')->where('user_id', '<>' ,'Car_Dealer_Marii')->get();

        $year_vehicle = VehicleChecking::selectRaw('
            
            YEAR(created_at) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $year_vehicle_kastam = VehicleChecking::selectRaw('               
            YEAR(created_at) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $pra = DB::table('vehicle_checkings')->selectRaw('COUNT(*) as count, YEAR(vehicle_checkings.created_at) year, MONTH(vehicle_checkings.created_at) month')
        ->leftjoin('user_groups', 'vehicle_checkings.company_name', 'user_groups.branch')
        ->whereYear('vehicle_checkings.created_at', $this_year)
          ->groupBy('year', 'month')
          ->get();


        return view('management.chart_report.index', compact('balance','data', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending', 'total_group', 'data_group', 'data_group_chart', 'usergroup', 'usergroup_car_dealer', 'year_vehicle','year_vehicle_kastam'));
        
    }

    public function report_chart_by_year($id)
    {
        $data = User::orderBy('last_login', 'DESC')->take(5)->get();

        $total_vehicle = VehicleChecking::count();
        $total_vehicle_verified = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua 
        $total_vehicle_pending = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua

        $total_group = UserGroup::count();
        $data_group = UserGroup::get();
        $data_group_chart = UserGroup::get();

        $balance = UserGroup::get();

        $this_year = date("Y");

        $usergroup = UserGroup::whereNotNull('branch')->where('user_id', '<>' ,'KA_MARII')
                    //->whereYear('created_at', $id)
                    ->get();


        $usergroup_car_dealer = UserGroup::whereNull('branch')->where('user_id', '<>' ,'Car_Dealer_Marii')
                   // ->whereYear('created_at', $id)
                    ->get();


        $year_vehicle = VehicleChecking::selectRaw('
            
            YEAR(created_at) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $year_vehicle_kastam = VehicleChecking::selectRaw('               
            YEAR(created_at) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $pra = DB::table('vehicle_checkings')->selectRaw('COUNT(*) as count, YEAR(vehicle_checkings.created_at) year, MONTH(vehicle_checkings.created_at) month')
        ->leftjoin('user_groups', 'vehicle_checkings.company_name', 'user_groups.branch')
        ->whereYear('vehicle_checkings.created_at', $this_year)
          ->groupBy('year', 'month')
          ->get();


        return view('management.chart_report.by_year', compact('balance','data', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending', 'total_group', 'data_group', 'data_group_chart', 'usergroup', 'usergroup_car_dealer', 'year_vehicle','year_vehicle_kastam', 'id'));
    }


	public function get_report_chart_old()
    {

        $data = User::orderBy('last_login', 'DESC')->take(5)->get();

        $total_vehicle = VehicleChecking::count();
        $total_vehicle_verified = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua 
        $total_vehicle_pending = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua

        $total_group = UserGroup::count();
        $data_group = UserGroup::get();
        $data_group_chart = UserGroup::get();

        $balance = UserGroup::get();

        $this_year = date("Y");

        $usergroup = UserGroupOld::get();

        $usergroup_car_dealer = UserGroupOld::get();


        $year_vehicle = VehiclePast::selectRaw('
            
            YEAR(CreationTime) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $year_vehicle_kastam = VehiclePast::selectRaw('               
            YEAR(CreationTime) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $pra = DB::table('vehicle_checkings')->selectRaw('COUNT(*) as count, YEAR(vehicle_checkings.created_at) year, MONTH(vehicle_checkings.created_at) month')
        ->leftjoin('user_groups', 'vehicle_checkings.company_name', 'user_groups.branch')
        ->whereYear('vehicle_checkings.created_at', $this_year)
          ->groupBy('year', 'month')
          ->get();


           $vehicle_past = VehiclePast::get();



        return view('management.chart_report.old', compact('balance','data', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending', 'total_group', 'data_group', 'data_group_chart', 'usergroup', 'usergroup_car_dealer', 'year_vehicle','year_vehicle_kastam', 'vehicle_past'));
    }


    public function get_report_chart_old_by_year($id)
    {
        $data = User::orderBy('last_login', 'DESC')->take(5)->get();

        $total_vehicle = VehicleChecking::count();
        $total_vehicle_verified = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua 
        $total_vehicle_pending = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua

        $total_group = UserGroup::count();
        $data_group = UserGroup::get();
        $data_group_chart = UserGroup::get();

        $balance = UserGroup::get();

        $this_year = date("Y");

        $usergroup = UserGroupOld::get();

        $usergroup_car_dealer = UserGroupOld::get();


        $year_vehicle = VehiclePast::selectRaw('
            
            YEAR(CreationTime) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $year_vehicle_kastam = VehiclePast::selectRaw('               
            YEAR(CreationTime) AS year
        ')
        ->groupBy('year')
        ->orderBy('year', 'asc')
        ->get();


        $pra = DB::table('vehicle_checkings')->selectRaw('COUNT(*) as count, YEAR(vehicle_checkings.created_at) year, MONTH(vehicle_checkings.created_at) month')
        ->leftjoin('user_groups', 'vehicle_checkings.company_name', 'user_groups.branch')
        ->whereYear('vehicle_checkings.created_at', $this_year)
          ->groupBy('year', 'month')
          ->get();


           $vehicle_past = VehiclePast::get();



        return view('management.chart_report.old_by_year', compact('balance','data', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending', 'total_group', 'data_group', 'data_group_chart', 'usergroup', 'usergroup_car_dealer', 'year_vehicle','year_vehicle_kastam', 'vehicle_past', 'id'));
    }


}

