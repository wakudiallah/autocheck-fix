<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Model\DetailResult;
use App\Model\HistorySearching;
use RealRashid\SweetAlert\Facades\Alert;
use Session;
use Auth;
use GuzzleHttp\Client;
use Redirect;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;

class PayController extends Controller
{
    
    const USER_UID_HEADER = 'Carvx-User-Uid';

    private $url;
    private $uid = '34ll8i8hPxOY';
    private $key = 'fWU6b-pMs4DWTeSBJE4vcH3heskGEcXdrAFSBcZjV38yZld7DP4PKwXZ4co9lsJF';

    private $needSignature = true;
    private $raiseExceptions = false;
    private $isTest = false;


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function store(Request $request)
    {   


        $id_user = Auth::user()->id;


        $chassisNumber = $request->chassis;
        $id_vehicle     = Uuid::uuid4()->tostring();

        $role_me       = Auth::user()->role_id;

        
        $status_api = '20'; //found
        $searching_by = 'API';
        
        /*table for naza / group*/

        $data                          =  new VehicleChecking;
        
        $data->id_vehicle              = $id_vehicle;
        $data->vehicle                 = $chassisNumber;
        $data->is_sent                 = '1';  
        
        $data->created_by              =  $id_user;
        $data->status                  =  $status_api;
        $data->searching_by            = $searching_by;
        $data->group_by                = $role_me;
        
        $data->save();

        /* End Create Searching */

        //history search
        $data                       =  new HistorySearchVehicle;
        
        $data->vehicle              = $request->vehicle;
        $data->id_vehicle           = $id_vehicle;
        $data->parameter_history_id = "SEARCH";
        $data->user_id              =  $id_user;
        
        $data->save();

        //history user            
        $data                       =  new HistoryUser;
        
        $data->vehicle_id           = $request->vehicle;
        $data->id_vehicle           = $id_vehicle;
        $data->parameter_history_id = "SEARCH";
        $data->user_id              =  $id_user;
        
        $data->save();


       


       $email_user = Auth::user()->email;
       $name_user = Auth::user()->name;
       $chassis = $request->chassis;
       $brand = $request->brand;

       $fee = ParameterFee::where('fee_for', '11')->latest('id')->first();
        

        //Real
        
        $callback_url = url('/callback'.$id_vehicle);
        $redirect_url = url('/callback'.$id_vehicle);
        $client = new Client();
        $res = $client->request('POST', 'https://www.billplz.com/api/v3/bills', [

        
            'auth' => ['ee0c0a5a-8051-4d7e-861d-55c6285a7b7d', '', 'basic'],
            'form_params' => [
                'collection_id' => 'im6xvege6',
                'email' => $email_user,
                'name' => $name_user,
                'amount' => $fee->fee.'00',
                //'mobile' => '0163824356',
                'callback_url' => $callback_url,
                'description' => 'Chassis Number : '.$chassis,
                'redirect_url' => $redirect_url,
            ]
        ]); 
        

         //Staging
        /*$callback_url = url('/callback/'.$id_vehicle);
        $redirect_url = url('/callback/'.$id_vehicle);
        $client = new Client();
        $res = $client->request('POST', 'https://billplz-sandbox.com/api/v3/bills', [
        
            'auth' => ['d553e9ce-2c19-4f27-9763-a43900dd287e', '', 'basic'],
            'form_params' => [
                'collection_id' => 'onareikm',
                'email' => $email_user,
                'name' =>  $name_user,
                'amount' => $fee->fee.'00',
                'callback_url' => $callback_url,
                'description' => 'Chassis Number :'.$chassis,
                'redirect_url' => $redirect_url,
            ]
        ]);*/


        /* end staging */
          
          /*$attempt = Auth::user();
                $loglogin = new LogLogin;
                 
                $loglogin->email        = $user->email;
                $loglogin->id_user      = $user->id;
                $loglogin->name         = $applicant->fullname;
                $loglogin->ip           = $request->ip();
                $loglogin->lat          = $request->latitude;
                $loglogin->lng          = $request->longitude;
                $loglogin->location     = $request->location;
                $loglogin->ip           = $request->ip();
                $loglogin->activities   = 'Pelanggan semakan pembayaran online';
                $loglogin->id_applicant = $applicant->user_id;
                $loglogin->remark       = '141';
                $loglogin->type         = '2';
                $loglogin->save();*/
        //$purchase = Purchase::Where('id_purchase', $id_purchase)->
            //update([
              //"bill_id"   => 13,
              //"confirm_payment_date" => $today
           // ]);

        $array = json_decode($res->getBody()->getContents(), true);
        $var_url =  var_export($array['url'],true);
        $url = substr($var_url, 1, -1); 
        return Redirect::to($url);
    }

    

    public function callback(Request $request)
    { 
       
       return redirect('/dashboard')->with(['success' => 'Thank You, We will immediately process your request']);
    }
}
