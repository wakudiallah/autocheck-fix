<?php

namespace App\Http\Controllers\UserGroup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Model\ParameterTypeReport;
use App\Model\UserGroupTypeReport;



class UserGroupHalfController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        $data_user_group = UserGroup::orderBy('id', 'desc')->get();

        $edit_user_group = UserGroup::orderBy('id', 'desc')->get();
        

        return view('admin.user_group.half_report.index', compact( 'data_user_group', 'edit_user_group'));

    }

   

    public function store(Request $request)
    {

        $data                   = new UserGroup;
        $data->group_name       = $request->group_name;
        $data->user_id          = $request->user_group_code;
        //$data->type_report_id   = '3';
        $data->status           = '1';
        $data->save();


        $usergroup = new UserGroupTypeReport;
        $usergroup->user_group_id = $data->id;
        $usergroup->type_report_id = "Half";
        $usergroup->save();



        return redirect()->back()->with(['success' => 'Data successfully updated']);

    }



    public function edit(Request $request, $id)
    {
        $user = UserGroup::find($id);
        $user->group_name = $request->group_name;
        $user->save(); 

        return redirect()->back()->with(['success' => 'Data successfully updated']);
    }


    public function delete(Request $request)
    {

        $user = UserGroup::find($request->user_id);
        $user->status = $request->status;
        $user->save(); 
    }

}
