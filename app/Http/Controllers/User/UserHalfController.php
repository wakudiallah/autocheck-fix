<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use App\Model\UserApiStatus;
use App\Model\UserGroupTypeReport;
use App\Authorizable;
use App\Model\UserGroup;
use App\Model\ParameterTypeReport;
use App\Model\UserGroupRelationUser;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Redirect;
use App\Model\ParameterBranchKastam;
use Illuminate\Support\Facades\Validator;


class UserHalfController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        $data = User::orderBy('users.id', 'DESC')
                ->join('user_groups', 'users.real_user_group_id', '=', 'user_groups.user_id')
                ->join('user_group_type_reports', 'user_group_type_reports.user_group_id', '=', 'user_groups.id')
                ->where('user_group_type_reports.type_report_id', 'Half')
                ->whereNull('users.deleted_at')
                ->select('users.id', 'users.name', 'users.email', 'users.status', 'user_groups.group_name', 'users.is_status_admin_group')
                ->get();


        $group = UserGroup::join('user_group_type_reports', 'user_group_type_reports.user_group_id', '=', 'user_groups.id')
                ->where('user_group_type_reports.type_report_id', 'Half')
                ->where('status', '1')
                ->orderBy('group_name', 'ASC')
                ->get();


        $data_detail = User::orderBy('users.id', 'DESC')
                ->join('user_groups', 'users.real_user_group_id', '=', 'user_groups.user_id')
                ->join('user_group_type_reports', 'user_group_type_reports.user_group_id', '=', 'user_groups.id')
                ->whereNull('users.deleted_at')
                ->where('user_group_type_reports.type_report_id', 'Half')
                ->select('users.id', 'users.name', 'users.email', 'users.status', 'user_groups.group_name', 'users.is_status_admin_group', 'users.phone', 'users.last_login')
                ->get();




        return view('admin.user.user_creation.user_creation_half_report', compact('data', 'group', 'data_detail'));

    }


    
    public function delete(Request $request)
    {


        $user = User::find($request->id);
        $user->status = $request->status;
        $user->save(); 
    }


    public function store(Request $request)
    {

        $id_user = Auth::user()->id;

        $name = $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->phone;
        $role = $request->role;
        $group = $request->group;

        $check_email = User::where('email', $email)->count();
        $get_email = User::where('email', $email)->first();

        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

       

        

        if($check_email >= '1'){

            return redirect()->back()->with(['warning' => 'Email  already exist']);

        } 



            $data             =  new User;
            $data->name       = $name;
            $data->email      = $email;
            $data->password   = bcrypt($password);
            $data->role_id    =  "NA";
            $data->phone      =  $phone;
            $data->status     =  '1';
            $data->created_by =  $id_user;
            $data->real_user_group_id = $group;
            $data->save();

            $get_id = User::where('email', $email)->first();
            $is_id  = $get_id->id;

           
            
        
            /*$data          =  new UserGroupTypeReport;
            $data->user_id = $is_id;
            $data->save();*/

            $data                       =  new HistoryUser;
            $data->user_id              = $is_id;
            $data->parameter_history_id = 'CREATE';
            $data->save();


        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('email_send', 'password', 'phone'),  function($message) use($use_email , $email_send)
            {
                $message->from($use_email, 'Autocheck');
                $message->to($email_send->email)->subject('Registration');
            });*/


        return redirect()->back()->with(['success' => 'Data successfully save']);
    }


    public function edit($id)
    {
        
        $data = User::orderBy('users.id', 'DESC')
                ->join('user_groups', 'users.real_user_group_id', '=', 'user_groups.user_id')
                ->join('user_group_type_reports', 'user_group_type_reports.user_group_id', '=', 'user_groups.id')
                ->whereNull('users.deleted_at')
                ->where('user_group_type_reports.type_report_id', 'Half')
                ->select('users.id', 'users.name', 'users.email', 'users.status', 'user_groups.group_name', 'users.is_status_admin_group', 'users.phone', 'users.last_login')
                ->first();

        return view('admin.user.user_creation.edit_user_creation_half_report', compact('data'));
 
    }


    public function update($id, Request $request)
    {

        $name =  $request->name;
        $email =  $request->email;
        $phone =  $request->phone;
        //$type_report = $request->type_report;
        

        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'phone' => $phone));

        //$update = UserGroupTypeReport::where('user_id',$id)->update(array('type_report_id' => $type_report));

         return redirect('/user/half-report')->with(['success' => 'Data successfully updated']);

    }


   


}
