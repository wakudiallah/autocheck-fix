<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;

class VehicleCheckedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function extra()
    {
        $myGroup = Auth::user()->real_user_group_id;    
        $me = Auth::user()->id;

        $data = VehicleChecking::where('real_type_report_id', 'Extra')
            ->whereIn('status', ['40'])
            ->where('created_by', $me)
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data2 = VehicleChecking::where('real_type_report_id', 'Extra')
            ->whereIn('status', ['40'])
            ->where('created_by', $me)
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data3 = VehicleChecking::where('real_type_report_id', 'Extra')
            ->whereIn('status', ['40'])
            ->where('created_by', $me)
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data_verify = VehicleChecking::orderBy('id', 'DESC')
            ->get();

        $vehicle_api  = VehicleApi::get();
 
        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();


        return view('share.vehicle_checked.extra', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }



    public function full()
    {
        $me = Auth::user()->id;
        $myGroup = Auth::user()->real_user_group_id;



        $data = VehicleChecking::where('real_type_report_id', 'Full')
            ->whereNull('old_report')
            ->where('company_name', $myGroup)
            ->whereIn('status', ['40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data2 = VehicleChecking::where('real_type_report_id', 'Full')
            ->whereNull('old_report')
            ->where('company_name', $myGroup)
            ->whereIn('status', ['40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data3 = VehicleChecking::where('real_type_report_id', 'Full')
            ->whereNull('old_report')
            ->where('company_name', $myGroup)
            ->whereIn('status', ['40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data_verify = VehicleChecking::orderBy('id', 'DESC')
            ->get();

        $vehicle_api  = VehicleApi::get();

 
        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();


        return view('share.vehicle_checked.full', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }


    public function half()
    {
        $me = Auth::user()->id;

        $data = VehicleChecking::where('real_type_report_id', 'Half')
                    ->where('status', '40')
                    ->where('created_by', $me)
                    ->orderBy('updated_at', 'DESC')
                    ->get();

        $data2 = VehicleChecking::where('real_type_report_id', 'Half')
                ->where('status', '40')
                ->where('created_by', $me)
                ->orderBy('updated_at', 'DESC')
                ->get();

        $data3 = VehicleChecking::where('real_type_report_id', 'Half')
                ->where('status', '40')
                ->where('created_by', $me)
                ->orderBy('updated_at', 'DESC')
                ->get();

        $data_verify = VehicleChecking::orderBy('updated_at', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        
        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('share.vehicle_checked.half', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }


    public function index()
    {
        $role  = Auth::user()->role_id;
        $group = Auth::user()->group_by;
        $me    = Auth::user()->id;
        $company_name = Auth::user()->user_group_id;

        
        if($role == 'VER'){ //verify
            $data = VehicleChecking::whereIn('status', ['30', '40'])
                    ->orderBy('id', 'DESC')
                    ->get();

            $data2 = VehicleChecking::whereIn('status', ['30', '40'])
                    ->orderBy('id', 'DESC')
                    ->get();

            $data3 = VehicleChecking::whereIn('status', ['30', '40'])
                    ->orderBy('id', 'DESC')
                    ->get();

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();


            return view('verifier.vehicle_checked.index', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }


        elseif($role == 'NA'){

            $myGroup = Auth::user()->real_user_group_id;

            $data = VehicleChecking::where('group_by', $myGroup)
                    ->orderBy('id', 'DESC')
                    ->whereIn('status', ['40'])
                    ->get();

            $data2 = VehicleChecking::where('group_by', $myGroup)
                    ->orderBy('id', 'DESC')
                    ->whereIn('status', ['40'])
                    ->get();

            foreach ($data2 as $p) {
                
                $vehicle_manual = VehicleManual::where('id_vehicle', $p->id_vehicle)->first();
            }

            $data3 = VehicleChecking::where('group_by', $myGroup)
                    ->orderBy('id', 'DESC')
                    ->whereIn('status', ['40'])
                    ->get();

             

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            return view('share.vehicle_checked.index', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }


        elseif($role == 'KA'){
            $ka_branch =  Auth::user()->is_role_kastam;
            $user =  Auth::user()->id;

            $mygroup = Auth::user()->real_user_group_id;

            

            $data = VehicleChecking::where('group_by', $mygroup)
                ->orderBy('id', 'DESC')
                ->get();

            $data2 = VehicleChecking::where('group_by', $mygroup)
                ->orderBy('id', 'DESC')
                ->get();

            $data3 = VehicleChecking::where('group_by', $mygroup)
                ->orderBy('id', 'DESC')
                ->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('kastam.vehicle_checked.index', compact('data', 'data2', 'data3','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model'));
        }

        elseif($role == 'MAR'){
            $data = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();
            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('company_name', $company_name)->orderBy('id', 'DESC')->get();

            return view('share.vehicle_checked.index', compact('data', 'data2', 'data3','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model'));
        }

        elseif($role == 'US'){

            $myGroup = Auth::user()->real_user_group_id;

            $me = Auth::user()->id;


            $data = VehicleChecking::where('created_by', $me)
                    ->where('status', '40')
                    ->orderBy('updated_at', 'DESC')
                    ->get();

            $data2 = VehicleChecking::where('created_by', $me)
                    ->orderBy('updated_at', 'DESC')
                    ->where('status', '40')
                    ->get();

            $data3 = VehicleChecking::where('created_by', $me)
                    ->orderBy('updated_at', 'DESC')
                    ->where('status', '40')
                    ->get();


            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('share.vehicle_checked.index', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }


        elseif($role == 'AD'){


            $data = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('admin.vehicle_checked.vehicle_checked', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }

       elseif($role == 'MAN'){


            $data = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('admin.vehicle_checked.vehicle_checked', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }
             
        
    }


    public function open_vehicle()
    {
        $role  = Auth::user()->role_id;
        $group = Auth::user()->group_by;
        $me    = Auth::user()->id;

        
        if($role == 'VER'){ //verify
            $data = VehicleChecking::whereIn('status', ['25','10', '20'])
            ->orderBy('id', 'DESC')
            ->where('is_sent', '1')
            ->where('is_check_id', '1')
            ->where('is_sync', '1')
            ->get();

            
            $data2 = VehicleChecking::whereIn('status', ['25','10', '20'])
            ->orderBy('id', 'DESC')
            ->where('is_sent', '1')
            ->where('is_check_id', '1')
            ->where('is_sync', '1')
            ->get();


            $data3 = VehicleChecking::whereIn('status', ['25','10', '20'])
            ->orderBy('id', 'DESC')
            ->where('is_sent', '1')
            ->where('is_check_id', '1')
            ->where('is_sync', '1')
            ->get();


            $verify_manual = VehicleChecking::whereIn('status', ['25','10', '20'])
                ->orderBy('id', 'DESC')
                ->where('is_sent', '1')
                ->where('is_check_id', '1')
                ->where('is_sync', '1')
                ->get();
            

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();


            $brand2    =  Brand::get();
            $model2    =  ParameterModel::get();

            

            return view('verifier.open_vehicle.index', compact('verify_manual','data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'brand2', 'model2'));
        }

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function kastam_manual_short_report_submit_idreport(Request $request, $id)
    {
        
	   $me    = Auth::user()->id;
        $engine = $request->engine;


        $form= new ReportVehicle();
        $form->id_vehicle = $id;;
        $form->report_id = $engine;
        $form->is_ready = "3";
        $form->data = "1";
        $form->save();


	VehicleChecking::where('id_vehicle',$id)->update(array('is_check_id' => '1'));


        return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }


    public function kastam_manual_short_report(Request $request, $id)
    {


        $no = '02CARMN';
        $date = date("hisdmY");

        $sn = $no.$date;


        $me    = Auth::user()->id;

        $make = $request->make;
        $model = $request->model;
        $engine = $request->engine;
        $body = $request->body;
        $grade = $request->grade;
        $date_of_manufacture = $request->date_of_manufacture;
        $date_of_original_registration = $request->date_of_original_registration;
        $drive = $request->drive;
        $transmission = $request->transmission;
        $cc = $request->cc;
        $currency = $request->currency;
        $price = $request->price;
        $vehicle = $request->vehicle;
        //$file = $request->file;


         if($request->hasfile('filename'))
         {

            foreach($request->file('filename') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/kastam_manual/', $name);  
                $data[] = $name;  
 
            }
         }


        if($request->hasfile('filename'))
        {
            $form= new VehicleApiKastamImage();
            $form->image=json_encode($data);
            $form->vehicle_id=$id;
            $form->save();
        }


        $ret         = explode('|', $model);
        $value_model = $ret[0];


        $get_model = ParameterModel::where('id', $value_model)->first();
        $get_brand = Brand::where('id', $make)->first();


        $data = new VehicleApiKastam;
        $data->vehicle_id = $id;
        $data->api_from = $currency;
        $data->status = 'Complete';
        $data->update_by = $me;
        $data->vehicle = $vehicle;
        $data->make = $make;
        $data->model = $get_model->model;
        $data->engine = $engine;
        $data->body = $body;
        $data->grade = $grade;
        $data->manufactureDate = $date_of_manufacture;
        $data->date_of_origin = $date_of_original_registration;
        $data->drive = $drive;
        $data->transmission = $transmission;
        $data->displacement_cc = $cc;
        //$data->currency = $currency;
        $data->average_market = $price;
        //$data->file = $file;
        $data->save();


        //Update Status Vehicle Checking
        VehicleChecking::where('id_vehicle',$id)->update(array('status' => '40'));


        $data = new HistorySearchVehicle();
        $data->id_vehicle = $id;
        $data->vehicle = $vehicle;
        $data->parameter_history_id = 'COMPLETE';
        $data->user_id = $me;        
        $data->save();


        $update= VehicleStatusMatch::where('id_vehicle', $id)->update(array('ver_sn' => $sn ));


        return redirect()->back()->with(['success' => 'Data successfully updated']);
        

    }



    public function tasklist()
    {
        
        
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_check_id', '1')
            ->whereNull('is_sync')
            ->get();
        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_check_id', '1')
            ->whereNull('is_sync')
            ->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_check_id', '1')
            ->whereNull('is_sync')
            ->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.tasklist.index', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        
    }


    public function tasklist_kastam()
    {
        
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();
        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.tasklist.index_kastam', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        
    }

    
    public function tasklist_kastam_shortreport()
    {
       
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->whereNull('not_found_id_carvx')->get();

        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->whereNull('not_found_id_carvx')->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->whereNull('not_found_id_carvx')->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        
        return view('verifier.tasklist.short_report_kastam', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }



    public function tasklist_kastam_shortreport_mn()
    {
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->where('searching_by', 'NOT')->get();

        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->where('searching_by', 'NOT')->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->where('searching_by', 'NOT')->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        
        return view('verifier.tasklist.short_report_kastam_mn', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function kastam_vehicle_complete()
    {
            
            $data = VehicleChecking::where('group_by', 'KA')->whereNull('old_report')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('group_by', 'KA')->whereNull('old_report')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data3 = VehicleChecking::where('group_by', 'KA')->whereNull('old_report')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            return view('verifier.vehicle_checked.kastam_vehicle_complete', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }


    public function buyer_vehicle_complete()
    {
            
            /*$data = VehicleChecking::where('real_type_report_id', 'Extra')->whereIn('status', ['40'])->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('real_type_report_id', 'Extra')->whereIn('status', ['40'])->orderBy('id', 'DESC')->get();

            $data3 = VehicleChecking::where('real_type_report_id', 'Extra')->whereIn('status', ['40'])->orderBy('id', 'DESC')->get();
            */



            $data = VehicleChecking::where('group_by', 'KA')->where('old_report', '1')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('group_by', 'KA')->where('old_report', '1')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data3 = VehicleChecking::where('group_by', 'KA')->where('old_report', '1')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            return view('verifier.vehicle_checked.buyer_vehicle_complete', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }


    public function car_dealer_vehicle_complete()
    {

            $data = VehicleChecking::where('group_by', 'NA')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('group_by', 'NA')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data3 = VehicleChecking::where('group_by', 'NA')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            return view('verifier.vehicle_checked.car_dealer_vehicle_complete', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }


}
