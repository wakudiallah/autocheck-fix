<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ParameterFee;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use Mail;
use Session;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = ParameterFee::orderBy('id', 'DESC')->get();
        
        return view('admin.parameter.fee.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user             = Auth::user()->id;

        $data               =  new ParameterFee;
        
        $data->fee = $request->fee;
        $data->tax = $request->tax;
        $data->created_by   =  $user;
        $data->fee_for       =  $request->fee_for;
        
        $data->save();


        return redirect('/fee')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParameterFee::where('id',$id)->first();

        return view('admin.parameter.fee.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $fee =  $request->input('fee');
        $tax =  $request->input('tax');

        $data = ParameterFee::where('id',$id)->update(array('fee' => $fee, 'tax' => $tax, 'updated_by' => $user));

         return redirect('fee')->with(['success' => 'Data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterFee::where('id',$id)->delete();   
        
        return redirect('fee')->with(['success' => 'Data successfully deleted']);
    }
}
