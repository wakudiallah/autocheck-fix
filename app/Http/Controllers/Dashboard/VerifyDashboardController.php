<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\User;
use App\Model\VehicleChecking;
use App\Model\UserGroup;
use App\Model\VehicleStatusMatch;
use App\Model\HistoryUser;
use Auth;
use DB;
use DateTime;
use App\Http\Controllers\Dashboard\VerifyDashboardController;
use Session;

class VerifyDashboardController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
            $count_half_sent_mn = VehicleChecking::where('searching_by', 'NOT')
                                                    ->where('status', '10')
                                                    ->whereNotIn('is_sent', ['1'])
                                                    ->whereNotIn('is_sync', ['1'])
                                                    ->count();

            $count_half_sent_api = VehicleChecking::where('searching_by', 'NOT')
                                                    ->where('status', '20')
                                                    ->whereNotIn('is_sent', ['1'])
                                                    ->whereNotIn('is_sync', ['1'])
                                                    ->count();


            $product = collect([$count_half_sent_mn,2,3,4]);
            Session::push('cart', $product);

            $product = array($count_half_sent_mn,2,3,4);
Session::put('cart.product',$product);

            Session::put('count', ['count_half_sent_mn' => $count_half_sent_mn, 
                                    'count_half_sent_api' => $count_half_sent_api] );

            /*$this->parseOptions(
            [
                'needSignature' => 'is_bool',
                'raiseExceptions' => 'is_bool',
                'isTest' => 'is_bool',
            ],
            $options
        );


        $option = ['','']*/


    }


}
