<?php

namespace App\Http\Controllers\Report\ReportManagement\Monthly;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Model\VehicleChecking;
use App\Model\ParameterFee;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use DB;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RRecallHistory;
use App\Model\RAccidentHistory;
use App\Model\ParameterSupplier;
use Excel;
use Auth;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\User;
use App\Model\VehicleStatusMatch;
use App\Model\UserGroupOld;


class ExtraController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        $data = UserGroup::get();


        return view('management.report_monthly.extra', compact('data'));
    }



    public function store(Request $request)
    {
        $date        = $request->month;
        $buyer_group = $request->buyer_group;


        $full_date = "01-".$date;

        $separate_date = date('d',strtotime($full_date));
        $separate_month = date('m',strtotime($full_date));
        $separate_year = date('Y',strtotime($full_date));

        $date1 =  date('Y-m-d 00:00:01', strtotime($full_date)); 
        $date2 =  date('Y-m-d 23:59:59', strtotime($full_date)); 

        
        $user_group = UserGroup::get();
        

        if($buyer_group == "All"){

            $report = DB::table('vehicle_checkings')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date')    
            )
            ->where('real_type_report_id', 'Extra')
            ->whereNull('deleted_at')
            ->whereMonth('created_at', $separate_month)
            ->whereYear('created_at', $separate_year)
            ->where('status', '40')
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get();


            $count_report = DB::table('vehicle_checkings')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date')    
            )
            ->where('real_type_report_id', 'Extra')
            ->where('status', '40')
            ->whereNull('deleted_at')
            ->whereMonth('created_at', $separate_month)
            ->whereYear('created_at', $separate_year)
            ->count();

        }else{


            $report = DB::table('vehicle_checkings')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date')    
            )
            ->where('company_name', $buyer_group)
            ->where('status', '40')
            ->whereNull('deleted_at')
            ->whereMonth('created_at', $separate_month)
            ->whereYear('created_at', $separate_year)
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get();


            $count_report = DB::table('vehicle_checkings')
            ->select( 
                DB::raw('count(*) as total'),
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date')    
            )
            ->where('company_name', $buyer_group)
            ->where('status', '40')
            ->whereNull('deleted_at')
            ->whereMonth('created_at', $separate_month)
            ->whereYear('created_at', $separate_year)
            ->count();
        }


            $fee = ParameterFee::latest('id')
                    ->where('role_id', 'US')
                    ->first();

    
        return view('management.report_monthly.extra_view', compact('report', 'user_group', 'separate_month', 'separate_year', 'buyer_group', 'buyer_group', 'date', 'fee', 'count_report'));

    }

    public function print(Request $request)
    {
        


        $date        = $request->month;


        $month_req        = $request->month;

       $buyer_group = $request->buyer_group;


        $full_date = "01-".$date;

        $separate_date = date('d',strtotime($full_date));
        $separate_month = date('m',strtotime($full_date));
        $separate_year = date('Y',strtotime($full_date));

        $date1 =  date('Y-m-d 00:00:01', strtotime($full_date)); 
        $date2 =  date('Y-m-d 23:59:59', strtotime($full_date)); 

        

        $user_group = UserGroup::get();
           
         
        if($buyer_group == "All"){

             $report = DB::table('vehicle_checkings')
                ->leftJoin('user_groups', 'user_groups.user_id', '=', 'vehicle_checkings.company_name')
                ->select( 
                    DB::raw('count(*) as total'),
                    DB::raw('DATE_FORMAT(vehicle_checkings.created_at, "%Y-%m-%d") as date')    
                )
                
                ->where('vehicle_checkings.real_type_report_id', "Extra")
                ->where('status', '40')
                ->whereNull('vehicle_checkings.deleted_at')
                ->whereMonth('vehicle_checkings.created_at', $separate_month)
                ->whereYear('vehicle_checkings.created_at', $separate_year)
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->get();

                $user_company = "Extra Report User";

                $fee = ParameterFee::latest('id')->where('role_id', 'US')->first();

        }else{


            $report = DB::table('vehicle_checkings')
                ->leftJoin('user_groups', 'user_groups.user_id', '=', 'vehicle_checkings.company_name')
                ->select( 
                    DB::raw('count(*) as total'),
                    DB::raw('DATE_FORMAT(vehicle_checkings.created_at, "%Y-%m-%d") as date')    
                )
                
                ->where('vehicle_checkings.company_name', $buyer_group)
                ->where('status', '40')
                ->whereNull('vehicle_checkings.deleted_at')
                ->whereMonth('vehicle_checkings.created_at', $separate_month)
                ->whereYear('vehicle_checkings.created_at', $separate_year)
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->get();

                $user_company = UserGroup::where('user_id', $buyer_group)->first();   
                $buyer_group = $user_company->group_name;

                $fee = ParameterFee::latest('id')->where('role_id', 'US')->first();
            }






        $current = date('d-m-Y');

        $excel = Excel::create('Monthly Report '.$current, function($excel) use( $report,$date, $buyer_group, $separate_month, $separate_year, $fee) {

            $excel->setTitle('Monthly Report');

            // Chain the setters
            $excel->setCreator('Autocheck')->setCompany('Autocheck.com.my');

            $excel->sheet('summary', function($sheet) use($report, $date, $buyer_group, $separate_month, $separate_year, $fee) {
                 $sheet->setColumnFormat(array(
   
                'D' => '0'
                ));

            

                $i = 1;
                    foreach($report as $chassis) {

                    $qty = $fee->fee;
                    $format_qty = number_format($qty, 2 , '.' , ',' );

                    
                    
                    $data[] = array(
                        $i++,
                        date('d/m/Y ', strtotime($chassis->date)),
                        $buyer_group,
                        $chassis->total,
                        $format_qty,
                        number_format($chassis->total*$qty, 2 , '.' , ',' ) ,
                        
                    );

                    $sheet->cell("A{$i}:F{$i}", function($cell) {

                    $cell->setBackground('#ffffff')
                        ->setFontColor('#000000')
                        ->setBorder( 'thin','thin','thin','thin')
                        ->setValignment('center');
                    });

                    
                }

                $current = date('Y-m-d ');

                $sheet->fromArray($data, null, 'A1', false, false);

                    $ir = $i-1;
                    $is = $i+1;

                    
               
                $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin','thin','thin','thin');

                $headings = array('No', 'Submisson Date','Users','Qty', 'Cost (Per Case)', 'Total');
                
                $sheet->prependRow(1, $headings);

                    //$sheet->prependRow(1, ["CARFAX SDN BHD(Reg. No. 917913-T)", '','', "(Reg. No. 917913-T)"]);
                    //$sheet->mergeCells("A1:F1");
                    //
                    

                    $head = array("CARFAX SDN BHD", '',"", "(Reg. No. 917913-T)");
                    $sheet->prependRow(1, $head);


                    $sheet->prependRow(2, [""]);
                    $sheet->mergeCells("A2:F2");

                    $sheet->prependRow(3, [''.config('autocheck.extra_report').'']);
                    $sheet->mergeCells("A3:F3");


                    $bill_to = array('Bill to:', '','','', 'INVOICE', '');
                    $sheet->prependRow(4, $bill_to);

                    $sheet->prependRow(5, [""]);
                    $sheet->mergeCells("A2:F2");

                    $marii = array('Malaysia Automotive Robotics And IoT Institute (898618-T)', '','','', 'Invoice no', ' :CarFax '.$separate_month.'/'.$separate_year);
                    $sheet->prependRow(6, $marii);

                    $block = array('Block 2280, Jalan Usahawan 2, Cyber 6', '','','', 'Payment', ' :Cheque/Cash');
                    $sheet->prependRow(7, $block);

                    $prev_date = '01-'.$date;
                    $next_date = date('Y-m-d', strtotime($prev_date));
                    $date1 = strtotime($next_date);
                    $next_date1 = date('d/m/Y', strtotime("+1 month", $date1));

                    $slg = array('63000 Cyberjaya, Selangor', '','','', 'Date', ' : '.$next_date1);
                    $sheet->prependRow(8, $slg);

                    $mly = array('Malaysia', '','','', 'Page', ' : 1 & 1 ');
                    $sheet->prependRow(9, $mly);


                    $sheet->prependRow(10, [""]);
                    $sheet->mergeCells("A10:F10");

                    $sheet->prependRow(11, [""]);
                    $sheet->mergeCells("A11:F11");

                    
                    //TOTAL (RM) ROW
                    $total_qty = $i+12;

                    $last_row = $total_qty-1;
                    
                    $value_total = array("TOTAL (RM)", '','', "=SUM(D13:D{$last_row})","", "=SUM(F13:F{$last_row})");
                    $sheet->prependRow($total_qty, $value_total);
                    $sheet->mergeCells("A{$total_qty}:C{$total_qty}");

                        
                    $sheet->cell("A{$total_qty}:F{$total_qty}", function($cell) {
                       
                        $cell->setBackground('#92cddc')
                            ->setFontWeight('bold')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });


                    $sheet->cell("A3:F3", function($cell) {
                       
                        $cell->setBackground('#92cddc')
                            ->setFontWeight('bold')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });

                    $sheet->cell("A3:F3", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin', 'thin');
                    });



                    /*Footer*/
                    $foot   = $i+14;

                    //FOOTER RECEIVED BY
                    $foots  = $foot+3;
                    $foots1 = $foots+1;
                    $foots2 = $foots1+1;
                    $foots3 = $foots2+1;
                    $foots4 = $foots3+1;
                    $foots5 = $foots4+1;
                    $foots6 = $foots5+1;
                    $sheet->prependRow($foot , ["Terms :"]);
                    $sheet->cell("A{$foot}:F{$foot}", function($cell) {
                        $cell
                         ->setBackground('#92cddc')
                         ->setFontWeight('bold')
                        ->setFontColor('#000000');
                    });

                    $sheet->cell("B{$foots}:B{$foots6}", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin')
                        ->setBackground('#ffffff');
                    });

                    $sheet->cell("E{$foots}:E{$foots6}", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin')
                        ->setBackground('#ffffff');
                    });



                    $footerRow = count($report) + 10;
                    $footerRows = count($report)+10 ;

                    $sheet->cell("A1:E1", function($cell) {
                        $cell->setBackground('#17c5d8')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold');
                    });

                    //RECEIVED BY
                    $sheet->appendRow(["",
                        ""
                    ]);
                    $sheet->appendRow($foots ,["",
                        "Received by" 
                    ]);
                    $sheet->appendRow($foots1 ,["",
                        "","","","For CarFax Sdn Bhd,"  
                    ]);
                    $sheet->appendRow($foots2 ,["",
                        "" 
                    ]);
                    $sheet->appendRow($foots3 ,["",
                        ".........................."
                    ]);
                    $sheet->appendRow($foots4 ,["",
                        "Name: ","","",".........................."
                    ]);
                    $sheet->appendRow($foots5 ,["",
                        "Date: ","","","Name: "
                    ]);



                    $sheet->cell("A1:C1", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            //->setAlignment('center')
                            //->setValignment('center');
                            ->setFont(array(
                                'family'     => 'times new roman',
                                'size'       => '37',
                                'bold'       =>  true
                            ));
                    });

                    $sheet->cell("D1:F1", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            //->setAlignment('center')
                            //->setValignment('center');
                            ->setFont(array(
                                'family'     => 'times new roman',
                                'size'       => '13',
                                'bold'       =>  true
                            ));
                    });

                    

                    $sheet->cell("A12:F12", function($cell) {
                       
                        $cell->setBackground('#92cddc')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });

                    $sheet->cell("E4:F5", function($cell) {
                       
                        $cell->setBackground('#92cddc')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });

                    


                    

                    $sheet->cell("A12:A12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("B12:B12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("C12:C12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("D12:D12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("E12:E12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("F12:F12", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });


                    $sheet->cell("A13:A13", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           
                    });


                    $sheet->cell("F13:F13", function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        
                    });





                    
                    $sheet->cell("A4:A4", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#fc050d');
                    });

                    $sheet->cell("A6:A6", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold');
                    });


 


                   
                    /* square first */
                    $sheet->cell("A4:C9", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    /* end square first */

                    /* square first */
                    $sheet->cell("E4:F9", function($cell) {
                       
                        $cell
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    /* end square first */


                    $sheet->setSize('B1', 20, 18);
                    $sheet->setSize('C1', 15, 15);
                    $sheet->setSize('D1', 15, 15);
                    $sheet->setSize('E1', 20, 18); 
                    $sheet->setSize('F1', 20, 18);

                    $sheet->setSize('A3', 10, 18);
                    $sheet->setSize('B3', 35, 18);
                    $sheet->setSize('C3', 25, 18);
                    $sheet->setSize('D3', 25, 18);
                    $sheet->setSize('E3', 25, 18); 

                                      
                });

                /* Get Email */

        return redirect('/report-summary-view')->with(['update' => 'Assessment data successfully downloaded']);
              })->download('xlsx');
        
    }


}
