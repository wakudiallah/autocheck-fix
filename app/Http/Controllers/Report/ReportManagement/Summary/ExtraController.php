<?php

namespace App\Http\Controllers\Report\ReportManagement\Summary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Model\VehicleChecking;
use App\Model\ParameterFee;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use DB;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RRecallHistory;
use App\Model\RAccidentHistory;
use App\Model\ParameterSupplier;
use Excel;
use Auth;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\User;
use App\Model\VehicleStatusMatch;
use App\Model\UserGroupOld;


class ExtraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->type_report  = config('autocheck.extra_report');
    }


    public function index()
    {

        $user = UserGroup::get();

        return view('management.report_summary.extra', compact('user'));
    }


    public function store(Request $request)
    {
         
        $date  = $request->input('month');
        $group  = $request->input('group');


        $full_date = "01-".$date;

        $separate_date = date('d',strtotime($full_date));
        $separate_month = date('m',strtotime($full_date));
        $separate_year = date('Y',strtotime($full_date));

        $date1 =  date('Y-m-d 00:00:01', strtotime($full_date)); 
        $date2 =  date('Y-m-d 23:59:59', strtotime($full_date));  

        $user = UserGroup::get();
        
            
           $ka = UserGroup::where('user_id', $group)->first();



        if($group == "All"){

            $vin =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('real_type_report_id', 'Extra')
                    ->orderBy('id','ASC')
                    ->get();


            $count_data =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('real_type_report_id', 'Extra')
                    ->orderBy('id','ASC')
                    ->count();
        }else{

            $vin =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('company_name', $group)
                    ->orderBy('id','ASC')
                    ->get();


            $count_data =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('company_name', $group)
                    ->orderBy('id','ASC')
                    ->count();

        }   
            
        

        return view('management.report_summary.extra_view', compact( 'separate_month','group', 'user', 'date1', 'count_data', 'vin'));


    }

    public function print(Request $request)
    {
        $date  = $request->input('month');
        $group  = $request->input('group');



        $separate_date = date('d',strtotime($date));
        $separate_month = date('m',strtotime($date));
        $separate_year = date('Y',strtotime($date));
 

        $user = UserGroup::get();
        
        $ka = UserGroup::where('user_id', $group)->first();

    
         if($group == "All"){

            $vin =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('real_type_report_id', 'Extra')
                    ->orderBy('id','ASC')
                    ->get();


            $count_data =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('real_type_report_id', 'Extra')
                    ->orderBy('id','ASC')
                    ->count();
        }else{

            $vin =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('company_name', $group)
                    ->orderBy('id','ASC')
                    ->get();


            $count_data =  VehicleChecking::whereYear('created_at', $separate_year)
                    ->whereMonth('created_at', $separate_month)
                    ->where('company_name', $group)
                    ->orderBy('id','ASC')
                    ->count();

        }  


        $current = date('d-m-Y');

        $excel = Excel::create($this->type_report.' Summary Report '.$current, function($excel) use($vin) {

            $excel->setTitle('Summary Report ( '. $this->type_report . ' )');

            // Chain the setters
            $excel->setCreator('Autocheck')->setCompany('Autocheck.com.my');

            $excel->sheet('summary', function($sheet) use($vin) {
                 $sheet->setColumnFormat(array(
   
                'D' => '0'
            ));

            

                $i = 1;
                foreach($vin as $chassis) {

                    $data[] = array(
                        $i++,
                        $chassis->vehicle,
                        $chassis->vehiclechecking_to_status->desc,
                        date("d/m/Y", strtotime($chassis->created_at)),
                        date("d/m/Y", strtotime($chassis->updated_at)),
                        $chassis->group_company->group_name
                    );

                    $sheet->cell("A{$i}:F{$i}", function($cell) {

                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setBorder( 'thin','thin','thin','thin');
                    });
                }

                $current = date('Y-m-d');

                $sheet->fromArray($data, null, 'A1', false, false);

                    $ir = $i-1;
                    $is = $i+1;
                   
                  $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin','thin','thin','thin');

                $headings = array('No', 'Chassis No.','Status','Date Submission', 'Date Verify', 'Groups');
                
                $sheet->prependRow(1, $headings);

                    $sheet->prependRow(1, ["Summary Report (". $this->type_report .")"]);
                    $sheet->mergeCells("A1:F1");


                    $sheet->prependRow(2, [""]);
                    $sheet->mergeCells("B2:F2");


                    $sheet->cell("A1:F1", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });



                    $sheet->setSize('F1', 25, 50);

                    $sheet->cell("A3:F3", function($cell) {
                       
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center')
                            ->setFontWeight('bold')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                   

                    

                    $sheet->setSize('A1', 10, 18);
                    $sheet->setSize('B1', 35, 18);
                    $sheet->setSize('C1', 25, 18);
                    $sheet->setSize('D1', 25, 18);
                    $sheet->setSize('E1', 25, 18);
                    $sheet->setSize('F1', 25, 18);  

                    $sheet->setSize('A3', 10, 18);
                    $sheet->setSize('B3', 35, 18);
                    $sheet->setSize('C3', 25, 18);
                    $sheet->setSize('D3', 25, 18);
                    $sheet->setSize('E3', 25, 18); 
                    $sheet->setSize('F3', 25, 18); 

                    
                    
                    
                    /*$sheet->cell("B11", function($cell) {
                        // change header color
                      
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    }); */
                      
                                      
                });

                /* Get Email */
                
               
      

        return redirect()->back()->with(['update' => 'Assessment data successfully downloaded']);
              })->download('xlsx');

    }
}
