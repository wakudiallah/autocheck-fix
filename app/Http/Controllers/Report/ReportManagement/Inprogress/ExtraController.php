<?php

namespace App\Http\Controllers\Report\ReportManagement\Inprogress;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Model\VehicleChecking;
use App\Model\ParameterFee;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use DB;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RRecallHistory;
use App\Model\RAccidentHistory;
use App\Model\ParameterSupplier;
use Excel;
use Auth;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\User;
use App\Model\VehicleStatusMatch;
use App\Model\UserGroupOld;

class ExtraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = UserGroup::get();


        /*$data = VehicleChecking::whereNotIn('status', ['40', '30'])
                ->where('real_type_report_id', 'Extra')
                ->get();*/


        return view('management.inprogress.extra', compact('data'));
    }


    public function store(Request $request)
    {
       
       $date        = $request->year;
       $buyer_group = $request->buyer_group;


        if($buyer_group == "All"){

            $vin = VehicleChecking::whereNotIn('status', ['40', '30'])
                ->where('real_type_report_id', 'Extra')
                ->whereYear('created_at', $date)
                ->orderBy('id','ASC')
                ->get();

            $count_data = VehicleChecking::whereNotIn('status', ['40', '30'])
                ->where('real_type_report_id', 'Extra')
                ->whereYear('created_at', $date)
                ->orderBy('id','ASC')
                ->count();

        }else{

            $vin = VehicleChecking::whereNotIn('status', ['40', '30'])
                ->where('real_type_report_id', 'Extra')
                ->whereYear('created_at', $date)
                ->where('company_name', $buyer_group)
                ->orderBy('id','ASC')
                ->get();


            $count_data = VehicleChecking::whereNotIn('status', ['40', '30'])
                ->where('real_type_report_id', 'Extra')
                ->whereYear('created_at', $date)
                ->where('company_name', $buyer_group)
                ->orderBy('id','ASC')
                ->count();

        }

        $user_group = UserGroup::get();
        $separate_month = $date;
        $separate_month = $date;


        return view('management.inprogress.extra_view', compact('vin', 'count_data', 'user_group', 'buyer_group', 'separate_month' ,'date'));
       
    }



}
