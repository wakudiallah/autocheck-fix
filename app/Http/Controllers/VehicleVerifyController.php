<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use App\Model\ParameterFee;
use App\Model\HistoryBalance;
use App\Model\HistorySearchVehicle;
use App\Model\UserGroup;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;

class VehicleVerifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::orderBy('id', 'DESC')->get();

            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            $data_verify = VehicleChecking::where('status', '10')->orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('share.vehicle_checked.index', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store_api(Request $request, $id)
    {
        $me = Auth::user()->id;

        $brand_verify = $request->brand_verify;
        $model_verify = $request->model_verify;
        $engine_number_verify = $request->engine_number_verify;
        $cc_verify = $request->cc_verify;
        $fuel_verify = $request->fuel_verify;
        $registration_date_verify = $request->registration_date_verify;

        $vehicle = $request->vehicle;

        $model       = $request->model_verify;
        $ret         = explode('|', $model);
        $value_model = $ret[0];


        /* Match Semula */
        $brand_data = Brand::where('id', $brand_verify)->first();
        $model_data = ParameterModel::where('id', $model_verify)->first();

        $get_brand = $brand_data->brand; //1
        $get_model = $model_data->model; //2


        $get_vehicle_api = VehicleApi::where('id_vehicle',$id)->first();
        $api_brand = $get_vehicle_api->brand;
        $api_model = $get_vehicle_api->model;
        $api_cc = $get_vehicle_api->cc; //6
        $api_registation_date = $get_vehicle_api->registation_date; //7

        //engine number kadealer null //3
        //fuel type always use kadealer //4
        //year manufacture always api //5


        if($api_brand == $get_brand){
            $brand_match = '1';
        }else{
            $brand_match = '0';
        }


        if($api_model == $get_model){
            $model_match = '1';
        }else{
            $model_match = '0';
        }

        if($cc_verify == $api_cc){
            $cc_match = '1';
        }else{
            $cc_match = '0';
        }

        //registration convert only month n year
        $registation_api =  date('m-Y ', strtotime($api_registation_date));
        $registered_kadealer =  date('m-Y ', strtotime($registration_date_verify));


        if($registation_api == $registered_kadealer){
            $registation_match = '1';
        }else{
            $registation_match = '0';
        }


        if(($brand_match == 1) AND ($model_match == 1) AND ($cc_match == 1) AND ($registation_match == 1) )
        {
            $status_match = '1';

            $data = VehicleStatusMatch::where('id_vehicle',$id)->update(array('brand' => $brand_match,'model' => $model_match,'cc' => $cc_match,'registation_date' => $registation_match,'status'=>$status_match,'verify_by' => $me,'type_verify'=> '2' ));


            $update_vehicle = VehicleChecking::where('id_vehicle',$id)->update(array('updated_by' => $me, 'model_id' => $value_model, 'status' => '40' ));  

            return redirect('open-vehicle-verify')->with(['success' => 'Data updated successfuly']);


        }else{
            $status_match = '0';

            /*echo $brand_match;
            echo $model_match;
            echo $cc_match;
            echo $registation_match;*/

            return redirect('open-vehicle-verify')->with(['warning' => 'Please Recheck Your Update Chassis '.$vehicle]);
        }
        /* End Match Againt */          

    }


    public function store_manual(Request $request, $id)
    {
       

        $role  = Auth::user()->role_id;
        $me = Auth::user()->id;


        $brand_verify = $request->brand_verify;
        $model       = $request->model_verify;
        $ret         = explode('|', $model);
        $value_model = $ret[0];

        $engine_number_verify = $request->engine_number_verify;
        $cc_verify = $request->cc_verify;
        $fuel_verify = $request->fuel_verify;
        $registration_date_verify = $request->registration_date_verify;
        $year_verify = $request->year_verify;

        $vehicle =  $request->vehicle;
        $api_from = $request->api_from;


        /* get key in */
        $getvehicle = VehicleChecking::where('id_vehicle',$id)->first();

        $get_brand = $getvehicle->brand_id;
        $get_model = $getvehicle->model_id;
        $get_cc     = $getvehicle->cc;
        $get_engine = $getvehicle->engine_number;
        $get_registration_date = $getvehicle->vehicle_registered_date;
        $get_fuel_id = $getvehicle->fuel_id;
        /* end get key in */


        /* match proses */
        if($brand_verify == $get_brand){
            $brand_match = '1';
        }else{
            $brand_match = '0';
        }

        if($value_model == $get_model){
            $model_match = '1';
        }else{
            $model_match = '0';
        }

        if($cc_verify == $get_cc){
            $cc_match = '1';
        }else{
            $cc_match = '0';
        }

        if($fuel_verify == $get_fuel_id){
            $fuel_match = '1';
        }else{
            $fuel_match = '0';
        }


        $registation_verify =  date('m-Y ', strtotime($registration_date_verify));
        $registered_kadealer =  date('m-Y ', strtotime($get_registration_date));


        if($registation_verify == $registered_kadealer){
            $registation_match = '1';
        }else{
            $registation_match = '0';
        }
        /* end match proses */


        /* SN */        
        $today  = date('HisdmY');
        $ver_sn = '00'.$api_from.$today;
        /* end SN */

        if(($brand_match == 1) AND ($model_match == 1) AND ($cc_match == 1) AND ($fuel_match == 1) AND ($registation_match == 1) )
        {
            $status_match = '1';

            $data = VehicleStatusMatch::where('id_vehicle',$id)->update(array('brand' => $brand_match,'model' => $model_match,'cc' => $cc_match,'registation_date' => $registation_match,'status'=>$status_match,'verify_by' => $me, 'ver_sn' => $ver_sn));


            $update_vehicle = VehicleChecking::where('id_vehicle',$id)->update(array('updated_by' => $me, 'engine_number' => $engine_number_verify, 'status' => '40')); 

            $data = VehicleManual::where('id_vehicle', $id)->update(array('manual_from' => $api_from,'vehicle' => $vehicle, 'brand' => $brand_verify, 'model' => $value_model, 'engine_number' => $engine_number_verify, 'cc' => $cc_verify, 'fuel_type' => $fuel_verify, 'registation_date' => $registration_date_verify, 'year_manufacture' => $year_verify)); 


            $data                       =  new HistorySearchVehicle;
            
            $data->vehicle              = $vehicle;
            $data->id_vehicle           = $id;
            $data->parameter_history_id = "COMPLETE";
            $data->user_id              =  $me;
            
            $data->save();

            return redirect('open-vehicle-verify')->with(['success' => 'Data updated successfuly']);


        }else{
            $status_match = '0';

            return redirect('open-vehicle-verify')->with(['warning' => 'Please Recheck Chassis Kadealer '.$vehicle]);
        }

        

    }



    public function store_manual_duplicate(Request $request, $id)
    {
        $role  = Auth::user()->role_id;
        $me = Auth::user()->id;

        $brand_verify = $request->brand_verify;
        $model       = $request->model_verify;
        $ret         = explode('|', $model);
        $value_model = $ret[0];

        $engine_number_verify = $request->engine_number_verify;
        $cc_verify = $request->cc_verify;
        $fuel_verify = $request->fuel_verify;
        $registration_date_verify = $request->registration_date_verify;
        $year_verify = $request->year_verify;

        $vehicle =  $request->vehicle;
        $api_from = $request->api_from;


        /* get key in */
        $getvehicle = VehicleChecking::where('id_vehicle',$id)->first();

        $get_brand = $getvehicle->brand_id;
        $get_model = $getvehicle->model_id;
        $get_cc     = $getvehicle->cc;
        $get_engine = $getvehicle->engine_number;
        $get_registration_date = $getvehicle->vehicle_registered_date;
        $get_fuel_id = $getvehicle->fuel_id;
        /* end get key in */


        /* match proses */
        if($brand_verify == $get_brand){
            $brand_match = '1';
        }else{
            $brand_match = '0';
        }

        if($value_model == $get_model){
            $model_match = '1';
        }else{
            $model_match = '0';
        }

        if($cc_verify == $get_cc){
            $cc_match = '1';
        }else{
            $cc_match = '0';
        }

        if($fuel_verify == $get_fuel_id){
            $fuel_match = '1';
        }else{
            $fuel_match = '0';
        }


        $registation_verify =  date('m-Y ', strtotime($registration_date_verify));
        $registered_kadealer =  date('m-Y ', strtotime($get_registration_date));


        if($registation_verify == $registered_kadealer){
            $registation_match = '1';
        }else{
            $registation_match = '0';
        }
        /* end match proses */


        /* SN */        
        $today  = date('HisdmY');
        $ver_sn = '00'.$api_from.$today;
        /* end SN */

        if(($brand_match == 1) AND ($model_match == 1) AND ($cc_match == 1) AND ($fuel_match == 1) AND ($registation_match == 1) )
        {
            $status_match = '1';

            $data = VehicleStatusMatch::where('id_vehicle',$id)->update(array('brand' => $brand_match,'model' => $model_match,'cc' => $cc_match,'registation_date' => $registation_match,'status'=>$status_match,'verify_by' => $me, 'ver_sn' => $ver_sn));


            $update_vehicle = VehicleChecking::where('id_vehicle',$id)->update(array('updated_by' => $me, 'engine_number' => $engine_number_verify, 'status' => '40')); 

            $data = VehicleManual::where('id_vehicle', $id)->update(array('manual_from' => $api_from,'vehicle' => $vehicle, 'brand' => $brand_verify, 'model' => $value_model, 'engine_number' => $engine_number_verify, 'cc' => $cc_verify, 'fuel_type' => $fuel_verify, 'registation_date' => $registration_date_verify, 'year_manufacture' => $year_verify)); 


            $data                       =  new HistorySearchVehicle;
            
            $data->vehicle              = $vehicle;
            $data->id_vehicle           = $id;
            $data->parameter_history_id = "COMPLETE";
            $data->user_id              =  $me;
            
            $data->save();

            return redirect('verification-tasklist')->with(['success' => 'Data updated successfuly']);


        }else{
            $status_match = '0';

            return redirect('verification-tasklist')->with(['warning' => 'Please Recheck Chassis Kadealer '.$vehicle]);
        }

        

    }


    public function store(Request $request, $id)
    {
        
        $role  = Auth::user()->role_id;
        $me = Auth::user()->id;

        $remark = $request->remark;
        $vehicle = $request->vehicle;
        $created_by = $request->created_by;
        $yes = $request->yes;

        $brand_verify = $request->brand_verify;
        $model_verify = $request->model_verify;
        $cc_verify = $request->cc_verify;
        $registration_date_verify =  $request->registration_date_verify;



        $get_user = User::where('id', $created_by)->first();

        /*if($yes == "no"){

            $data = VehicleChecking::where('id_vehicle',$id)->update(array('status' => '30')); //Reject

            //------- refund
            $get_balance = UserGroup::where('id', $get_user->balance)->first(); //get balance now
            $get_fee = ParameterFee::latest('id')->where('fee_for','2')->first(); //get fee group

            $last_balance = $get_balance->balance;
            $fee = $get_fee->fee;

            $total_refund = $last_balance+$fee;

            //----- update balance n add history balance

            $updatebalance = UserGroup::where('id', $get_user->balance)->update(array('balance' => $total_refund));

            
            $data                       =  new HistoryBalance;
            
            $data->id_vehicle           = $id;
            $data->balance              = $total_refund;
            $data->debit                = $fee;
            $data->desc                 = "Refund";
            $data->created_by           =  $me;
            $data->user_id              =  $get_user->balance;
            
            $data->save();


            //Update Remark
            $data                       =  new HistorySearchVehicle;
            
            $data->id_vehicle           = $id;
            $data->vehicle              = $vehicle;
            $data->parameter_history_id  = "REFUND";
            $data->user_id                 = $me;
            $data->remark                 = $remark;

            $data->save();

        }else{*/

            $model       = $request->model_verify;
            $ret         = explode('|', $model);
            $value_model = $ret[0];

            
            $brand_verify       = $request->brand_verify;
            $brand_verify_match = $request->brand_verify_match;

            $model_verify_match = $request->model_verify_match;

            $engine_number_verify = $request->engine_number_verify;
            $engine_number_verify_match = $request->engine_number_verify_match;

            $cc_verify = $request->cc_verify;
            $cc_verify_match = $request->cc_verify_match;

            $fuel_verify = $request->fuel_verify;
            $fuel_verify_match = $request->fuel_verify_match;

            $year_verify = $request->year_verify;
            $year_verify_match = $request->year_verify_match;

           $tarikh =  date('Y-m-d ', strtotime($request->registration_date_verify)); 

            $registration_date_verify = $tarikh;
            $registration_date_verify_match = $request->registration_date_verify_match;

            
            $api_from  = $request->api_from;
            $today  = date('HisdmY');
            $ver_sn = '00'.$api_from.$today;


             $data = VehicleChecking::where('id_vehicle',$id)->update(array('brand_id' => $brand_verify,'model_id' => $value_model,'cc' => $cc_verify,'engine_number' => $engine_number_verify,'vehicle_registered_date' => $registration_date_verify,'updated_by' => $me, 'status' => '40'));  //40 complete

             if(($brand_verify_match == 1) AND ($model_verify_match == 1) AND ($engine_number_verify_match == 1) AND ($cc_verify_match == 1) AND ($fuel_verify_match == 1) AND ($year_verify_match == 1) AND ($registration_date_verify_match == 1))
             {
                $status_match = '1';
             }else{
                $status_match = '0';
             }
             //start
             $data = VehicleStatusMatch::where('id_vehicle',$id)->update(array('brand' => $brand_verify_match,'model' => $model_verify_match,'cc' => $cc_verify_match,'engine_number' => $engine_number_verify_match,'fuel_type' =>$fuel_verify_match,'year_manufacture'=>$year_verify_match,'registation_date' => $registration_date_verify_match,'status'=>$status_match,'verify_by' => $me,'type_verify'=> '2', 'ver_sn' =>$ver_sn ));


                $data                       =  new HistorySearchVehicle;
                $data->id_vehicle           = $id;
                $data->vehicle              = $request->vehicle;
                $data->parameter_history_id = "VERIFY";
                $data->user_id              =  $me;
                $data->remark              =  $request->remark;
                
                $data->save();
        



        return redirect('verification-tasklist')->with(['success' => 'Data successfully updated']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verify($id)
    {
            $data = VehicleChecking::where('id_vehicle',$id)->where('status', '10')->orderBy('id', 'DESC')->first();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();
            
            $vehicle  = VehicleChecking::orderBy('id','DESC')->get();
           
            $vehicle_api  = VehicleApi::where('id_vehicle',$id)->first();

            return view('verifier.match_verify.index', compact('data','country', 'fuel', 'supplier', 'type', 'brand', 'vehicle', 'model', 'vehicle_api'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function upload_full_report(Request $request, $id)
    {
        $user             = Auth::user()->id;
        $vehicle          = $request->vehicle;
        
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $date = date('Y-m-d');
            $extension = $file->getClientOriginalExtension();
            $file_name = $vehicle. '.' . $extension;
            $destinationPath = 'report/manual';
            $file->move($destinationPath, $file_name );
        }
        
            $upload_file = $file_name;


            $api_from  = "MAN";
            $today  = date('HisdmY');
            $ver_sn = '00'.$api_from.$today;


            VehicleChecking::where('id_vehicle',$id)->update(array('full_report' => $upload_file, 'status' => '40'));

            VehicleStatusMatch::where('id_vehicle',$id)->update(array('ver_sn' => $ver_sn));

            $data           =  new HistorySearchVehicle;
         
            $data->parameter_history_id     = "COMPLETE";
            $data->user_id    =  $user;
            $data->vehicle = $vehicle;
            $data->id_vehicle = $id;
            
            $data->save();


        return redirect('verification-tasklist')->with(['success' => 'Data successfully deleted']);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function not_found_id_carvx($id)
    {
        
        VehicleChecking::where('id_vehicle',$id)->update(array(
            'not_found_id_carvx' => '1' ,
            'is_sync' => '1',
            'is_check_id' => '1',
            'status' => '28' 
            ));

        return redirect()->back()->with(['success' => 'Data successfully updated']);
    }


}
