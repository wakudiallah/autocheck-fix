<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ParameterCurrency;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = ParameterCurrency::get();

        return view('admin.parameter.currency.index', compact('data'));
    }


    public function store(Request $request)
    {
        $user             = Auth::user()->id;

        $data             =  new ParameterCurrency;
        $data->id_currency      = $request->id_currency;
        $data->symbol      = $request->symbol;
        $data->currency     =  $request->currency;
        $data->status     =  '1';
        
        $data->save();


        return redirect()->back()->with(['success' => 'Data saved successfuly']);
    }


    public function edit($id)
    {
        $data = ParameterCurrency::where('id',$id)->first();

        return view('admin.parameter.currency.edit', compact('data'));    
    }


    public function update(Request $request, $id)
    {   
        
        $id_currency      = $request->id;
        $symbol =  $request->symbol;
        $currency =  $request->currency;


        $data = ParameterCurrency::where('id',$id)->update(array('symbol' => $symbol, 'currency' => $currency, 'id_currency' => $id_currency ));

         return redirect('currency')->with(['success' => 'Brand data successfully updated']);
    }


    public function destroy($id)
    {
        ParameterCurrency::where('id',$id)->delete();

        return redirect()->back()->with(['success' => 'Brand data successfully deleted']);

    }



}
