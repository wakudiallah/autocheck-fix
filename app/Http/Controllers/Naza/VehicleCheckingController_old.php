<?php

namespace App\Http\Controllers\Naza;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;


class VehicleCheckingController_Old extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    const USER_UID_HEADER = 'Carvx-User-Uid';

    private $url;
    private $uid = '34ll8i8hPxOY';
    private $key = 'fWU6b-pMs4DWTeSBJE4vcH3heskGEcXdrAFSBcZjV38yZld7DP4PKwXZ4co9lsJF';

    private $needSignature = true;
    private $raiseExceptions = false;
    private $isTest = false;


    
    public function __construct()

    {
        $this->middleware('auth');
    }

    public function index()
    {
        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();
        
        $vehicle  = VehicleChecking::orderBy('id','DESC')->get();

        return view('naza.vehicle_checking', compact('country', 'fuel', 'supplier', 'type', 'brand', 'vehicle', 'model'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $vin = $request->vehicle;
        $is_test = $request->is_test;

        
        /*check 5 first character coz must add W*/
        $check_vin = substr($vin, 0, 5);

        if($check_vin == "AGH30"){

            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        }
        else{
            $chassisNumber = $request->vehicle;
        }
        /* end check 5 first character coz must add W*/


        $model       = $request->model;
        $ret         = explode('|', $model);
        $value_model = $ret[0];
        $value_brand = $request->brand;

        $date =  date('Y-m-d ', strtotime($request->registered_date)); 

        $id_vehicle     = Uuid::uuid4()->tostring();
        $me           = Auth::user()->id;
        $group_me       = Auth::user()->group_by;
        $role_me       = Auth::user()->role_id;
        $balance_me = Auth::user()->balance; //id balance

        $check_brand = Brand::where('id', $value_brand)->first();
        $check_model = ParameterModel::where('id', $value_model)->first();

        $get_brand = $check_brand->brand;
        $get_model = $check_model->model;

        //$vehiclepast = VehiclePast::where('vehicle_id_number', $chassisNumber)->count();
        $vehicleapi = VehicleApi::where('vehicle', $chassisNumber)->count();

        $fee = ParameterFee::latest('id')->where('fee_for', '2')->first();
        $balance = UserGroup::where('id', $balance_me)->first();

        /*check data in past database*/
        if($vehiclepast >= '1' ){ //cek di past

            return redirect('/vehicle-checking')->with(['warning' => 'Chassis has been searched before']);
        }
        elseif($vehicleapi >= '1'){  //check di API (ada)

            /*skip dulu*/
            /*$data                        =  new VehicleChecking;
            
            $data->id_vehicle              = $id_vehicle;
            $data->vehicle                 = $request->vehicle;
            $data->supplier_id             = $request->supplier;
            $data->type_id                 = $request->type;
            $data->country_origin_id       = $request->country;
            $data->brand_id                = $request->brand;
            $data->model_id                = $value_model;
            $data->cc                      = $request->engine_capacity;
            $data->engine_number           = $request->engine_number;
            $data->vehicle_registered_date = $date;
            $data->fuel_id                 = $request->fuel;
            
            $data->created_by              =  $me;
            $data->status                  =  $status_api;
            $data->group_by                 = $group_me;
            
            $data->save();*/


            
            return redirect('/vehicle-checking')->with(['success' => 'Chassis has been searched before cccc']);
        }

        /*check API */
        elseif($vehiclepast == '0' && $vehicleapi == '0')
        {
            $options=array(

                    'needSignature' => '1', 
                    'raiseExceptions' => '1',
                    'isTest' => $is_test
                );

            $url           = 'https://carvx.jp';
            $userUid       = '34ll8i8hPxOY';
            $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';
            
            $service = new CarvxService($url, $userUid, $apiKey, $options);

            $search = $service->createSearch($chassisNumber);

            
            //carvx found
            if(!empty($search->cars[0]->chassisNumber)){

                $api_from = 'CARVX';

                $data                   =  new VehicleApi;
                $data->api_from         = $api_from;
                $data->istest           = $is_test; //
                $data->search_id        = $search->uid; //
                $data->car_id           = $search->cars[0]->carId; //
                $data->id_vehicle       = $id_vehicle;
                $data->vehicle          = $search->cars[0]->chassisNumber;
                $data->country_origin   = '';
                $data->brand            = $search->cars[0]->make;
                $data->model            = $search->cars[0]->model;
                $data->engine_number    = $search->cars[0]->engine;
                $data->cc               = '';
                $data->fuel_type        = '';
                $data->year_manufacture = $search->cars[0]->manufactureDate;
                $data->registation_date = '';
                $data->status           = 'found';
                $data->request_by       = $me;
                $data->save();


                $carId      = $search->cars[0]->carId;
                $searchId   = $search->uid;
                $isTest     = $is_test;

                
                /* create report API carvx */
                $reportId   = $service->createReport($searchId, $carId, $isTest); 

                $data                   =  new ReportVehicle;
                $data->id_vehicle       = $id_vehicle;
                $data->report_id        = $reportId;
                $data->save();


                //check match data
                if($search->cars[0]->make == $get_brand){
                    $match_brand = '1';
                }else{
                    $match_brand = '0';
                }

                if($search->cars[0]->model == $get_model){
                    $match_model = '1';
                }else{
                    $match_model = '0';
                }

                /*
                if($search->cars[0]->engine == $request->engine_number){
                    $match_engine_number = '1';
                }else{
                    $match_engine_number = '0';
                }
                */

                if($search->cars[0]->make == $get_brand AND $search->cars[0]->model == $get_model /*AND $search->cars[0]->engine == $request->engine_number*/){
                    $match_status = '1';
                }else{
                    $match_status = '0';
                }

                $today  = date('HisdmY');
                $ver_sn = '00'.$api_from.$today;

                $data                   =  new VehicleStatusMatch;
                $data->id_vehicle = $id_vehicle;
                $data->vehicle = $search->cars[0]->chassisNumber;
                $data->brand = $match_brand;
                $data->model = $match_model;
                /*$data->engine_number =  $match_engine_number;*/
                $data->status =  $match_status;
                $data->ver_sn = $ver_sn;
                $data->type_verify      = '1';
                $data->save();

            }
            //-------------------------  end carvx

            // searching manual 
            else{

                $data                   =  new VehicleStatusMatch;
                $data->id_vehicle = $id_vehicle;
                $data->vehicle = $request->vehicle;
                $data->brand = '0';
                $data->model = '0';
                /*$data->engine_number =  $match_engine_number;*/
                $data->status =  '0';
                $data->type_verify      = '0';
                $data->save();
            }
            // ------------- end searching manual



            if(!empty($search->cars[0]->chassisNumber)){
                $status_api = '20'; //found
                $searching_by = 'API';
            }else{
                 $status_api = '10'; //failed
                 $searching_by = 'NOT';
            }

            /*table for naza / group*/

            $data                          =  new VehicleChecking;
            
            $data->id_vehicle              = $id_vehicle;
            $data->vehicle                 = $request->vehicle;
            $data->supplier_id             = $request->supplier;
            $data->type_id                 = $request->type;
            $data->country_origin_id       = $request->country;
            $data->brand_id                = $request->brand;
            $data->model_id                = $value_model;
            $data->cc                      = $request->engine_capacity;
            $data->engine_number           = $request->engine_number;
            $data->vehicle_registered_date = $date;
            $data->fuel_id                 = $request->fuel;
            
            $data->created_by              =  $me;
            $data->status                  =  $status_api;
            $data->searching_by            = $searching_by;
            $data->group_by                = $role_me;


            
            $data->save();

            

            // Balance 
            $value_fee = $fee->fee; 
            $balance_now = $balance->balance - $fee->fee;

            $update_balance = UserGroup::where('id', $balance_me)->update(array('balance' => $balance_now ));



             $data             =  new HistoryBalance;

            $data->id_vehicle = $id_vehicle;
            $data->balance    = $balance_now;
            $data->credit    = $value_fee;
            $data->desc       = 'Search Vehicle';
            $data->created_by =  $me;
            $data->user_id     =  $balance_me;
            
            $data->save();


            //history search
            $data                       =  new HistorySearchVehicle;
            
            $data->vehicle              = $request->vehicle;
            $data->id_vehicle           = $id_vehicle;
            $data->parameter_history_id = "SEARCH";
            $data->user_id              =  $me;
            
            $data->save();

            //history user            
            $data                       =  new HistoryUser;
            
            $data->vehicle_id           = $request->vehicle;
            $data->id_vehicle           = $id_vehicle;
            $data->parameter_history_id = "SEARCH";
            $data->user_id              =  $me;
            
            $data->save();


            return redirect('/vehicle-checking')->with(['success' => 'Data saved successfuly']);


        }else{
            return redirect('/vehicle-checking')->with(['success' => 'Data saved successfuly']);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sync($id)
    {

        $getreport = ReportVehicle::where('id_vehicle', $id)->first();
        $reportId = $getreport->report_id;

        $options=array(

                'needSignature' => '1', 
                'raiseExceptions' => '1',
                'isTest' => '0'
            );

        $url           = 'https://carvx.jp';
        $userUid       = '34ll8i8hPxOY';
        $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';
        
        $service = new CarvxService($url, $userUid, $apiKey, $options);
       
        $report = $service->getReport($reportId, true);



        var_dump($report);
         //dd($report->data->vehicleDetails->chassisnumber);
        dd($report);
        
        $is_ready       = $report->status;
        $creation_date  = $report->creationDate;
        $due_date       = $report->dueDate;
        $data           = $report->data;

         //var_dump($report->getReport[0]->creationDate);
         //dd($report->Data[0]->vehicleDetails->chassisnumber);

        $data = ReportVehicle::where('id_vehicle',$id)->update(array(
            'is_ready' => $is_ready,  
            'creation_date' => $creation_date,
            'due_date' => $due_date,
            'data' => $data
        ));

        if($is_ready == '6') //status reject carvx
        {
            $status_chassis = '30';
        }
        elseif($is_ready == '5'){ //complete
            $status_chassis = '40';
        }
        elseif($is_ready == '3'){ //new
            $status_chassis = '20';
        }
        elseif($is_ready == '4'){ //inprogress
            $status_chassis = '25';
        }


        $data = VehicleChecking::where('id_vehicle',$id)->update(array(
            'status' => $status_chassis
        ));

        return redirect('/verification-tasklist')->with(['success' => 'Data saved successfuly']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model       = $request->model_edit;
        $ret         = explode('|', $model);
        $value_model = $ret[0];

        $date =  date('Y-m-d ', strtotime($request->registered_date_edit)); 

        $user  = Auth::user()->id;


        $vehicle                 = $request->vehicle_edit;
        $supplier                = $request->supplier_edit;
        $type                    = $request->type_edit;
        $country                 = $request->country_edit;
        $brand                   = $request->brand_edit;
        $model                   = $value_model;
        $cc                      = $request->engine_capacity_edit;
        $engine_number           = $request->engine_number_edit;
        $vehicle_registered_date = $date;
        $fuel                    = $request->fuel_edit;

        $data = VehicleChecking::where('id',$id)->update(array('vehicle' => $vehicle, 'supplier_id' => $supplier, 'type_id' => $type, 'country_origin_id' => $country,'brand_id' => $brand,'model_id' => $model,'cc' => $cc,'engine_number' => $engine_number,'vehicle_registered_date' => $vehicle_registered_date,'fuel_id' => $fuel,'updated_by' => $user));

        return redirect('vehicle-checking')->with(['success' => 'Data successfully updated']);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search_vehicle(Request $request)
    {
        $vin = $request->vehicle;

        /*check 5 first character coz must add W*/
        $check_vin = substr($vin, 0, 5);

        if($check_vin == "AGH30"){

            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        }
        else{
            $chassisNumber = $request->vehicle;
        }
        
        $options=array(
            'needSignature' => '1', 
            'raiseExceptions' => '1',
            'isTest' => '1'
        );
        $id_vehicle     = Uuid::uuid4()->tostring();

        //$url           = 'https://carvx.jp/api/v1/create-search';
        $url           = 'https://carvx.jp';
        $userUid       = '34ll8i8hPxOY';
        $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';
        
        $service    = new CarvxService($url, $userUid, $apiKey, $options);
        $search     = $service->createSearch($chassisNumber);
        

        if(!empty($search->cars[0]->chassisNumber)){


            return view('buyer.search_result', compact('search')); 
        }
        else{
            return redirect('/dashboard')->with(['warning' => 'Not Found']);
        }
    }

    public function post_search_vehicle(Request $request)
    {
        //
    }
}
