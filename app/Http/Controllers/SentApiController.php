<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterEmail;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\Model\BatchSent;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use Excel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;


class SentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function __construct()
    {
        $this->middleware('auth');
	$this->date = date("Y-m-d");
        $this->format =  "AC-".date('Ymd-Hi');
        $this->hash  = str_random(20);
    }



    public function sent_vehicle_short_report_api() 
    {
        $data = VehicleChecking::where('status', '20')->where('searching_by', 'API')->where('group_by', 'NA')->orderBy('id', 'ASC')->WhereNull('limit_submit')->get();

        return view('verifier.sent.api.short_report_api', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sent_vehicle_full_report_api()
    {
        $data = VehicleChecking::where('status', '20')->where('searching_by', 'API')->where('group_by', 'US')->orderBy('id', 'ASC')->WhereNull('limit_submit')->get();

        return view('verifier.sent.api.full_report_api', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sent_vehicle_shortreport_kastam_api()
    {
        
        $data = VehicleChecking::where('status', '20')->where('searching_by', 'API')->where('group_by', 'KA')->orderBy('id', 'ASC')->WhereNull('limit_submit')->get();

        return view('verifier.sent.api.short_report_kastam_api', compact('data'));
    }

   

    







    public function autocheck4(Request $request)
    {
	
        $item = array_map(null, $request->id, $request->vehicle, $request->ci, $request->ids);

	
        $is_test = config('autocheck.is_test_am4_report_kastam');

        $limit_submit_to_partner_everyday = config('autocheck.limit_submit_to_partner_everyday');

        $today = date("Y-m-d");

        $count_today_submit = VehicleChecking::where('today_submit', $today)->where('limit_submit', '1')->count();


        if($count_today_submit < $limit_submit_to_partner_everyday){
		

                foreach($item as $val) {
                    	
			$pra = VehicleChecking::where('id_vehicle',$val[0])->update([
                        
				"date_sent"             => $this->date,
                        	"unique_sent"           => $this->format,
                        	"hash"                  => $this->hash
		            ]);

                  
                }  /* end foreach looping*/


		 $getVehicleToPartner = VehicleChecking::where('unique_sent',$this->format)
					->where('hash', $this->hash)
					->get();

		 foreach($getVehicleToPartner as $getVehicleToPartner){

       		    $check_vin = substr($getVehicleToPartner->vehicle, 0, 5);

                    if($check_vin == "AGH30"){

                        $vinx = substr_replace( $getVehicleToPartner->vehicle, "W", 5, 0);
                        $chassisNumber = $vinx;
                    }elseif($check_vin == "GGH30"){
                        
                        $vinx = substr_replace($getVehicleToPartner->vehicle, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "GGH35"){

                        $vinx = substr_replace($getVehicleToPartner->vehicle, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "ANH20"){
                        
                        $vinx = substr_replace($getVehicleToPartner->vehicle, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "AGL10"){
                        
                        $vinx = substr_replace($getVehicleToPartner->vehicle, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "ANH20"){
                        
                        $vinx = substr_replace($getVehicleToPartner->vehicle, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }
                    else{
                        $chassisNumber = $getVehicleToPartner->vehicle;
                    }

		
		   $options=array(

                        'needSignature' => '0', 
                        'raiseExceptions' => '1',
                        'isTest' => $is_test
                    );

                        $url           = 'https://carvx.jp';
                        $userUid       = 'h1Nigk43M3rP';
                        $apiKey        = '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t';
                        

                        $service = new CarvxService($url, $userUid, $apiKey, $options);

                        $search = $service->createSearch($chassisNumber);

                        $carId      = $search->cars[0]->carId;
                        $searchId   = $search->uid;
                        $isTest     = $is_test;

                        //Real Sent
                        $reportId   = $service->createReport($searchId, $carId, $isTest);

                        //Dummy
                        //$reportId = "dummy";
	

                        //Autocheck 4
                        $data                   =  new ReportVehicle;
                        $data->id_vehicle       = $getVehicleToPartner->id_vehicle;
                        $data->report_id        = $reportId;
                        $data->save();

			
				$pra = VehicleChecking::where('id_vehicle',$getVehicleToPartner->id_vehicle)->update([
		                
				        "limit_submit"         => '1',
				        "is_sent"              => '1', 
					"is_check_id"          => '1'
				    ]);
			
		
		}

                return Redirect::back()->with(['success' => 'Chassis Successfully Sent to Partner']);
        }

        return Redirect::back()->with(['success' => 'Chassis has reached the limit ('.$limit_submit_to_partner_everyday.')']);

        
    }




    public function autocheck3(Request $request)
    {
	
    
	$is_test = config('autocheck.is_test_am3_report_naza');


        $item = array_map(null, $request->id, $request->vehicle, $request->ci, $request->ids);

        foreach($item as $val) {
            
            $pra = VehicleChecking::where('id_vehicle',$val[0])->update([                
                "limit_submit"         => '1',
		"is_sent"              => '1'
            ]);



            $check_vin = substr($val[1], 0, 5);

            if($check_vin == "AGH30"){

                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;
            }elseif($check_vin == "GGH30"){
                
                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "GGH35"){

                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "ANH20"){
                
                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "AGL10"){
                
                $vinx = substr_replace( $$val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "ANH20"){
                
                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }
            else{

                $chassisNumber = $request->vehicle;
            }


            $options=array(

                'needSignature' => '0', 
                'raiseExceptions' => '1',
                'isTest' => $is_test
            );

                $url           = 'https://carvx.jp';
                $userUid       = 'h1Nigk43M3rP';
                $apiKey        = '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t';
                              

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                $search = $service->createSearch($chassisNumber);


                $carId      = $search->cars[0]->carId;
                $searchId   = $search->uid;
                $isTest     = $is_test;

                //Real Sent
                $reportId   = $service->createReport($searchId, $carId, $isTest);

                //Dummy
                //$reportId = "dummy";

                //autocheckmalaysia3
                $data                   =  new ReportVehicle;
                $data->id_vehicle       = $val[0];
                $data->report_id        = $reportId;
                $data->save();

          
        }  /* end foreach looping*/



        return Redirect::back()->with(['success' => 'Chassis Successfully Sent to Partner']);


    }




    public function autocheck2(Request $request)
    {

        $is_test = config('autocheck.is_test_am2_full_report');

        $item = array_map(null, $request->id, $request->vehicle, $request->ci, $request->ids);

        foreach($item as $val) {
            
            $pra = VehicleChecking::where('id_vehicle',$val[0])->update([                
                "limit_submit"         => '1',
		"is_sent"              => '1'

            ]);



            $check_vin = substr($val[1], 0, 5);

            if($check_vin == "AGH30"){

                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;
            }elseif($check_vin == "GGH30"){
                
                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "GGH35"){

                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "ANH20"){
                
                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "AGL10"){
                
                $vinx = substr_replace( $$val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }elseif($check_vin == "ANH20"){
                
                $vinx = substr_replace( $val[1], "W", 5, 0);
                $chassisNumber = $vinx;

            }
            else{

                $chassisNumber = $request->vehicle;
            }


            $options=array(

                'needSignature' => '0', 
                'raiseExceptions' => '1',
                'isTest' => '1'
            );

                $url           = 'https://carvx.jp';
                $userUid       = '34ll8i8hPxOY';
                $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';
                              

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                $search = $service->createSearch($chassisNumber);


                $carId      = $search->cars[0]->carId;
                $searchId   = $search->uid;
                $isTest     = $is_test;

                //Real Sent
                //$reportId   = $service->createReport($searchId, $carId, $isTest);

                //Dummy
                $reportId = "dummy";

                //autocheckmalaysia
                $data                   =  new ReportVehicle;
                $data->id_vehicle       = $val[0];
                $data->report_id        = $reportId;
                $data->save();

          
        }  /* end foreach looping*/



        return Redirect::back()->with(['success' => 'Chassis Successfully Sent to Partner']);


    }



}


