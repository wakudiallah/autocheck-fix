<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;



class SearchController extends Controller
{
    

    protected $soapWrapper;




    public function __construct()
    {
        $this->middleware('auth');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $me           = Auth::user()->id;
        
        $vehicle    =  VehicleChecking::whereNotIn('status', ['40','30'])->where('created_by', $me)->get();

        return view('admin.search.index', compact('vehicle'));
    }




    public function searchxx(Request $request)
    {
	

        /* -------------- Whatsapp Send ------------------*/
        $twilio_whatsapp_number = "+60163824356";
        $account_sid = "ACfdf117ff210e822035f8c81763902147";
        $auth_token = "6db10b48590996ca7a3ec75b9cebefb2";

        $recipient = "+60166038306";


        $client = new Twilio\Rest\Client($account_sid, $auth_token);
        $message = "Your registration pin code is ";
        return $client->messages->create("whatsapp:$recipient", array('from' => "whatsapp:$twilio_whatsapp_number", 'body' => $message));
        /* ---------------  End Whatsapp Send -------------- */


        //$client->messages->create("whatsapp:$recipient", array('from' => "whatsapp:$twilio_whatsapp_number", 'body' => $message));


        return redirect()->back()->with(['success' => 'Data updated successfuly and Please Checklist Need Signature in carvx System']); 

    }
      

   


    public function search(Request $request)
    {
	     

	   /*check total balance <= 200 */
        $role_me  = Auth::user()->role_id;
        $branch_me  = Auth::user()->is_role_kastam;
        //$get_balance_group = UserGroup::where('user_id', $role_me)->first();
        $get_balance_group = UserGroup::where('branch', $branch_me)->first();

        $balance = $get_balance_group->balance;



        $user_group_id  = Auth::user()->user_group_id;

       

        $fee = ParameterFee::latest('id')->where('fee_for', '33')->first();
                // 33 = Kastam

        if($balance <= $fee->fee){ /* check balance under 200 */

            return redirect('search-vehicle')->with(['warning' => 'Please Top-Up your balance']);


        }else{ /* else check balance under 200 */

            /*Check to partner*/

            $vin = $request->vehicle;
            $is_test = $request->is_test;

            
            /*check 5 first character coz must add W*/
            $check_vin = substr($vin, 0, 5);
            if($check_vin == "AGH30"){

                $vinx = substr_replace( $vin, "W", 5, 0);
                $chassisNumber = $vinx;
            }elseif($check_vin == "GGH30"){
                
                $vinx = substr_replace( $vin, "W", 5, 0);
		          $chassisNumber = $vinx;
	    
	       }elseif($check_vin == "GGH35"){

                $vinx = substr_replace( $vin, "W", 5, 0);
                $chassisNumber = $vinx;


            }elseif($check_vin == "ANH20"){
                
                $vinx = substr_replace( $vin, "W", 5, 0);
                $chassisNumber = $vinx;
            }elseif($check_vin == "AGL10"){
                
                $vinx = substr_replace( $vin, "W", 5, 0);
                $chassisNumber = $vinx;
            }elseif($check_vin == "ANH20"){
                
                $vinx = substr_replace( $vin, "W", 5, 0);
                $chassisNumber = $vinx;
            }
            else{
                $chassisNumber = $request->vehicle;
            }
            
            /* end check 5 first character coz must add W*/

            $model       = $request->model;
            $ret         = explode('|', $model);
            $value_model = $ret[0];
            $value_brand = $request->brand;

            $date =  date('Y-m-d ', strtotime($request->registered_date)); 

            $id_vehicle     = Uuid::uuid4()->tostring();
            $me           = Auth::user()->id;
            $group_me       = Auth::user()->group_by;
            $role_me       = Auth::user()->role_id;
            $balance_me = Auth::user()->balance; //id balance

            

            $vehiclepast = VehiclePast::where('vehicle_id_number', $chassisNumber)->count();
            $vehicleapi = VehicleChecking::where('vehicle', $vin)->where('group_by', 'KA')->count();


            /*check data in past database*/
            if($vehiclepast >= '1' ){ //cek di past

                return redirect('/search-vehicle')->with(['warning' => 'Chassis has been searched before, Please contact administrator']);

            }
            elseif($vehicleapi >= '1'){  //check di API (ada)

                /*skip dulu*/
                /*$data                        =  new VehicleChecking;
                
                $data->id_vehicle              = $id_vehicle;
                $data->vehicle                 = $request->vehicle;
                $data->supplier_id             = $request->supplier;
                $data->type_id                 = $request->type;
                $data->country_origin_id       = $request->country;
                $data->brand_id                = $request->brand;
                $data->model_id                = $value_model;
                $data->cc                      = $request->engine_capacity;
                $data->engine_number           = $request->engine_number;
                $data->vehicle_registered_date = $date;
                $data->fuel_id                 = $request->fuel;
                
                $data->created_by              =  $me;
                $data->status                  =  $status_api;
                $data->group_by                 = $group_me;
                
                $data->save();*/


                
               return redirect('/search-vehicle')->with(['warning' => 'Chassis has been searched before, Please contact administrator']);
            }

            /*check API */
            elseif($vehiclepast == '0' && $vehicleapi == '0')
                
            {
                $options=array(

                        'needSignature' => '0', 
                        'raiseExceptions' => '1',
                        'isTest' => $is_test
                    );

                $url           = 'https://carvx.jp';
                $userUid       = 'h1Nigk43M3rP';
                $apiKey        = '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t';
                
               

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                $search = $service->createSearch($chassisNumber);

               
                //exit();
                
                
                //carvx found
                if(!empty($search->cars[0]->chassisNumber)){


                    $is_sent = '0';

                    $api_from = 'CARVX';

                    $data                   =  new VehicleApi;
                    $data->api_from         = $api_from;
                    $data->istest           = $is_test; //
                    $data->search_id        = $search->uid; //
                    $data->car_id           = $search->cars[0]->carId; //
                    $data->id_vehicle       = $id_vehicle;
                    $data->vehicle          = $search->cars[0]->chassisNumber;
                    $data->country_origin   = '';
                    $data->brand            = $search->cars[0]->make;
                    $data->model            = $search->cars[0]->model;
                    $data->engine_number    = $search->cars[0]->engine;
                    $data->cc               = '';
                    $data->fuel_type        = '';
                    $data->year_manufacture = $search->cars[0]->manufactureDate;
                    $data->registation_date = '';
                    $data->status           = 'found';
                    $data->request_by       = $me;
                    $data->save();


                    $carId      = $search->cars[0]->carId;
                    $searchId   = $search->uid;
                    $isTest     = $is_test;

                    
                    /* ---------- create report API carvx ----------- */
                    //$reportId   = $service->createReport($searchId, $carId, $isTest);  

                    //$reportId = config('autocheck.reportId');

                

                    /*$data                   =  new ReportVehicle;
                    $data->id_vehicle       = $id_vehicle;
                    $data->report_id        = $reportId;
                    $data->save();*/ 


                    //check match data
                    

                    /*
                    if($search->cars[0]->engine == $request->engine_number){
                        $match_engine_number = '1';
                    }else{
                        $match_engine_number = '0';
                    }
                    */

                    
                    $today  = date('HisdmY');
                    $ver_sn = '00'.$api_from.$today;

                    $data                   =  new VehicleStatusMatch;
                    $data->id_vehicle = $id_vehicle;
                    $data->vehicle = $search->cars[0]->chassisNumber;
                    
                    $data->ver_sn = $ver_sn;
                    $data->save();

                }
                //-------------------------  end carvx

                // searching manual 
                else{

                    $is_sent = '0';


                    $data                   =  new VehicleStatusMatch;
                    $data->id_vehicle = $id_vehicle;
                    $data->vehicle = $request->vehicle;
                    $data->brand = '0';
                    $data->model = '0';
                    /*$data->engine_number =  $match_engine_number;*/
                    $data->status =  '0';
                    $data->type_verify      = '0';
                    $data->save();
                }
                // ------------- end searching manual



                if(!empty($search->cars[0]->chassisNumber)){
                    $status_api = '20'; //found
                    $searching_by = 'API';
                }else{
                     $status_api = '10'; //failed
                     $searching_by = 'NOT';
                }

                /*table for naza / group*/

                $data                          =  new VehicleChecking;
                
                $data->id_vehicle              = $id_vehicle;
                $data->vehicle                 = $request->vehicle;
                $data->is_sent                 = $is_sent;  
                $data->type_report             = "4";
                $data->created_by              =  $me;
                $data->status                  =  $status_api;
                $data->searching_by            = $searching_by;
                $data->group_by                = $role_me;
                $data->company_name            = Auth::user()->is_role_kastam;
		          $data->save();

                

                /* Balance & Fee */

                $value_fee = $fee->fee; 
                $balance_now = $balance - $value_fee;


                //$update_balance = UserGroup::where('user_id', $role_me)->update(array('balance' => $balance_now ));
                $update_balance = UserGroup::where('branch', $branch_me)->update(array('balance' => $balance_now ));
                /* end Balance & Fee */



                 $data             =  new HistoryBalance;

                $data->id_vehicle = $id_vehicle;
                $data->balance    = $balance_now;
                $data->credit    = $value_fee;
                $data->desc       = 'Search Vehicle';
                $data->created_by =  $me;
                $data->user_id     =  Auth::user()->is_role_kastam;
                //user id is user grup in user table
                
                $data->save();

                /* end Balance & Fee */


                //history search
                $data                       =  new HistorySearchVehicle;
                
                $data->vehicle              = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();

                //history user            
                $data                       =  new HistoryUser;
                
                $data->vehicle_id           = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $me;
                
                $data->save();


               $get_user_submit = User::where('id', $me)->first();
                $user_submit =  $get_user_submit->name;
                $user_phone = $get_user_submit->phone;

                $user_group_submit = UserGroup::where('user_id', $user_group_id)->first();
                $company_submit = $user_group_submit->group_name;


                $user_company = $user_group_submit->group_name;


                $today  = date('d F Y');


               $dataq = array('name'=>$vin, 'me' => $user_submit, 'today' => $today, 'user_company' => $user_company, 'user_phone' => $user_phone, 'company_submit' => $company_submit);
                

                /*Mail::send('email.notif', $dataq, function($message) {
                     $message->to('wakudiallah05@gmail.com', 'Autocheck')->subject('Submit Vehicle');
                     $message->cc('autocheckmalaysia@gmail.com');
                     $message->from('autocheckmalaysia2@gmail.com','Submit Vehicle');
		});*/



                return redirect('/search-vehicle')->with(['success' => 'Data saved successfuly']);


            }else{
                return redirect('/search-vehicle')->with(['success' => 'Data saved successfuly']);
            }


        }/* end check balance under 200 */


        
    }



    public function pay(Request $request)
    { 
       $user = Auth::user();
        /*$applicant          = Applicant::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
            $application_step   = ApplicationStep::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
            $purchase           = Purchase::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
            $id_purchase        = $purchase->id;
            $parameter          = Parameter::get();
            $company_insurance  = InsuranceCompany::get();
             $vehicle = Vehicle::Where('user_id',$user->id)->first();
              $amount = $purchase->amount * 100;*/

        //$callback_url = url('/callback');
        //$redirect_url = url('/callback');
        $client = new Client();
        $res = $client->request('POST', 'https://billplz-staging.herokuapp.com/api/v3/bills', [
        
            'auth' => ['d553e9ce-2c19-4f27-9763-a43900dd287e', '', 'basic'],
            'form_params' => [
                'collection_id' => 'g6fn_mh1',
                'email' => 'wakudiallah05@gmail.com',
                'name' => 'Dial Ganteng',
                'amount' => '100',
                'callback_url' => '$callback_url',
                'description' => 'TEST API',
                'redirect_url' => '$redirect_url',
            ]
        ]);
          
          /*$attempt = Auth::user();
                $loglogin = new LogLogin;
                 
                $loglogin->email        = $user->email;
                $loglogin->id_user      = $user->id;
                $loglogin->name         = $applicant->fullname;
                $loglogin->ip           = $request->ip();
                $loglogin->lat          = $request->latitude;
                $loglogin->lng          = $request->longitude;
                $loglogin->location     = $request->location;
                $loglogin->ip           = $request->ip();
                $loglogin->activities   = 'Pelanggan semakan pembayaran online';
                $loglogin->id_applicant = $applicant->user_id;
                $loglogin->remark       = '141';
                $loglogin->type         = '2';
                $loglogin->save();*/
        //$purchase = Purchase::Where('id_purchase', $id_purchase)->
            //update([
              //"bill_id"   => 13,
              //"confirm_payment_date" => $today
           // ]);

        $array = json_decode($res->getBody()->getContents(), true);
        $var_url =  var_export($array['url'],true);
        $url = substr($var_url, 1, -1); 
        return Redirect::to($url);
    }


    public function callback(Request $request)
    { 
       
       return redirect('/dashboard')->with(['success' => 'Data saved successfuly']);
 
        /*$id = $request->id;
        $collection = $request->collection;
        $paid = $request->paid;
        $state = $request->state;
        $amount = $request->amount;
        */
      $user = Auth::user();
      $applicant          = Applicant::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
        $application_step   = ApplicationStep::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
        $purchase           = Purchase::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
        $id_purchase        = $purchase->id;
        $parameter          = Parameter::get();
        $company_insurance  = InsuranceCompany::get();
        $vehicle = Vehicle::Where('user_id',$user->id)->first();

        $id = $request->billplz;
        $idx = $request->billplz;

        $setgroup = array_map(null, $id, $idx); 
        foreach ($setgroup as $val) {
           


        $client = new Client();
        $res = $client->request('GET', 'https://billplz-staging.herokuapp.com/api/v3/bills/'.$val['0'], [
            'auth' => ['d553e9ce-2c19-4f27-9763-a43900dd287e', '', 'basic']
        ]);

        $array = json_decode($res->getBody()->getContents(), true);
        // export var
        $var_collection_id =  var_export($array['collection_id'],true);
        $collection_id = substr($var_collection_id, 1, -1); 

        $var_state =  var_export($array['state'],true);
        $state = substr($var_state, 1, -1); 

        $var_amount =  var_export($array['amount'],true);
        $amount = substr($var_amount, 1, -1); 

        $var_due_at =  var_export($array['due_at'],true);
        $due_at = substr($var_due_at, 1, -1); 

        $var_name =  var_export($array['name'],true);
        $name = substr($var_name, 1, -1); 

        $var_email =  var_export($array['email'],true);
        $email = substr($var_email, 1, -1); 

        $var_mobile =  var_export($array['mobile'],true);
        $mobile = substr($var_mobile, 1, -1); 

        $var_description =  var_export($array['description'],true);
        $description = substr($var_description, 1, -1); 

        $var_description =  var_export($array['description'],true);
        $description = substr($var_description, 1, -1); 

        $var_paid =  var_export($array['paid'],true);
        $paid = substr($var_paid, 1, -1); 

         $purchase           = Purchase::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
      $id_purchase        = $purchase->id;
      $purchase_amount= $purchase->amount;
        $data = new Payment;
        $data->bill_id = $val[0];
        $data->collection_id =  $collection_id;
        $data->state =  $state;
        $data->amount = $purchase->amount;
        $data->due_at = $due_at;
        $data->name = $name;
        $data->email = $email;
        $data->mobile =  $mobile;
        $data->id_purchase =$id_purchase;
        $data->description =  $description;
        $data->save(); 
         $today              = date('Y-m-d H:i:s');

        $applicationstep        = ApplicationStep::where('id_purchase', $id_purchase)
                ->update(['step'=> '11','payment_date'=>$today]);
         $applicant          = Applicant::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
          $vehicle          = Vehicle::latest('created_at')->where('user_id', $user->id)->limit('1')->first();
                 $emailsystem = EmailSystem::Where('id',1)->first();

                $emailfrom  = $emailsystem->email;
                $namefrom   = $emailsystem->name;

                $nameto =  $applicant->fullname;
                $ic     =  $applicant->ic_number;
                $emailto = $applicant->email;
                $reg_vehicle_number = $vehicle->reg_vehicle_number;
                $purpose_insurance = $applicant->purpose_insurance;
                $type_insurance = $applicant->type_insurance;
                $name_insurance= $purchase->Insurances->companyInsurance->name;
                
                if($state=='paid')
                {
                   Mail::send('mail.applicant.payment', compact('nameto','emailto','reg_vehicle_number','name_insurance','purpose_insurance','ic','purchase_amount','state','type_insurance'), function ($message) use ($emailfrom, $namefrom, $emailto, $nameto) {
                    $message->from($emailfrom, $namefrom);
                    $message->subject('Pembayaran Telah Berjaya');
                    $message->to($emailto,$nameto);
                });

                 $attempt = Auth::user();
                $loglogin = new LogLogin;
                 
                $loglogin->email        = $emailto;
                $loglogin->id_user      = $applicant->user_id;
                $loglogin->name         = $nameto;
                $loglogin->ip           = $request->ip();
                $loglogin->lat          = $request->latitude;
                $loglogin->lng          = $request->longitude;
                $loglogin->location     = $request->location;
                $loglogin->ip           = $request->ip();
                $loglogin->activities   = 'Pembayaran telah berjaya';
                $loglogin->id_applicant = $applicant->user_id;
                $loglogin->remark       = '142';
                $loglogin->type         = '2';
                $loglogin->save();

                }
                else
                {
                  Mail::send('mail.applicant.payment', compact('nameto','emailto','reg_vehicle_number','name_insurance','purpose_insurance','ic','purchase_amount','state','type_insurance'), function ($message) use ($emailfrom, $namefrom, $emailto, $nameto) {
                    $message->from($emailfrom, $namefrom);
                    $message->subject('Pembayaran Tidak Berjaya');
                    $message->to($emailto,$nameto);
                });

                 $attempt = Auth::user();
                $loglogin = new LogLogin;
                 
                $loglogin->email        = $emailto;
                $loglogin->id_user      = $applicant->user_id;
                $loglogin->name         = $nameto;
                $loglogin->ip           = $request->ip();
                $loglogin->lat          = $request->latitude;
                $loglogin->lng          = $request->longitude;
                $loglogin->location     = $request->location;
                $loglogin->ip           = $request->ip();
                $loglogin->activities   = 'Pembayaran tidak berjaya';
                $loglogin->id_applicant = $applicant->user_id;
                $loglogin->remark       = '142';
                $loglogin->type         = '2';
                $loglogin->save();
                }
               
        return Redirect('status_payment/'.$val[0]);
            
        
        } 

     

 
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$buyer_id = Auth::user()->id;

        $search = $request->input('search');


        $soap_Wrapper = new SoapWrapper();

        //$client = new \SoapClient($wsdl, $options);
                       
            $this->soapWrapper->add('cds', function ($service) {
                $service
                    ->wsdl('http://moaqs.com/GBLWS/GBLWS.asmx?WSDL');
            });

             
                $param=array(
                    'IDNo' => $search,
                    'txnCode' => '',
                    'ActCode' => 'R',
                    'usr' => '',
                    'pwd' => ''
                );
            
                $response = $this->soapWrapper->call('cds.GBLCallPndRmk',array($param));

                $smsMessages =  $response->GBLCallPndRmkResult
                ? $response->GBLCallPndRmkResult
                : array( $response->GBLCallPndRmkResult );

                
                        if($smsMessage->IDN == $search){
                            /*$hasil = DetailResult::where('ic', $pra->ic)->latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->whereIn('stage',['W13', 'W17'])
                                ->update(['pending_remark_moaqs' => $smsMessage->Rmk,'status_moaqs' => $smsMessage->TStat]);*/


                                $request                  = new DetailResult;

                                $request->vehicle_id_number          = $search;
                                /*$request->name            = $request->name;
                                $request->activity        = "1";
                                $request->remark_id       = "W0";
                                $request->stage_id        = "W0";
                                $request->user_id         = $user;
                                $request->note            = "";*/

                                $request->save();
                        }
                    

            

            return redirect('/search-vehicle')->with(['Success' => 'Vechile Number Found']);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.search.search_payment', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
