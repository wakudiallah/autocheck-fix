<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Model\DetailResult;
use App\Model\HistorySearching;
use RealRashid\SweetAlert\Facades\Alert;
use Session;
use Auth;
use GuzzleHttp\Client;
use Redirect;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use App\Http\Controllers\Payment\BillPlzController;
use App\Http\Controllers\Search\SearchExtraController;
use App\Http\Controllers\Share\WController;
use App\Http\Controllers\Share\PastVehicleController;


class PaymentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
         $this->needSignature = config('carvx_extrareport.needSignature');
        $this->raiseExceptions = config('carvx_extrareport.raiseExceptions');
        $this->isTest = config('carvx_extrareport.isTest');
        $this->url = config('carvx_extrareport.url');
        $this->userUid = config('carvx_extrareport.userUid');
        $this->apiKey = config('carvx_extrareport.apiKey');
    }


    public function store(Request $request)
    {   
        $id_user = Auth::user()->id;
        $chassisNumber = $request->chassis;

        $id_vehicle     = Uuid::uuid4()->tostring();



        $check_group = User::where('id', $id_user)
                            ->get();

        foreach($check_group as $get_user){

            if(!empty($get_user->user_relate_usergroup->user_id)){  //Member Group

                



                $vin = $request->chassis;

    
                /* ==== Check pada vehicle checking*/

                $check_vehiclechecking = VehicleChecking::where('vehicle', $vin)
                        ->where('group_by', 'US')
                        ->count();


                if($check_vehiclechecking >= '1'){

                    return redirect('/dashboard')->with(['warning' => 'Chassis has been searched before, Please contact administrator']);
                    

                }else{

                    /* === Check Balance */

                    $get_balance = UserGroup::where('user_id', Auth::user()->real_user_group_id )->first();

                    
                    $get_fee = ParameterFee::
                        where('role_id', 'US')
                        ->first();

                    

                    if($get_balance->balance < $get_fee->fee){

                         return redirect('/dashboard')->with(['warning' => 'Please Top-Up your balance']);


                    }else{

                        /* === Tolak balance */

                        $balance_now = $get_balance->balance - $get_fee->fee;

                        $update_balance = UserGroup::where('user_id', Auth::user()->real_user_group_id)->update(array('balance' => $balance_now ));


                        $goto_dummyW = new WController();
                        $goto_dummyW->index($request);

                        $chassisNumber = Session::get('chassisNumber');

                       

                        $id_vehicle     = Uuid::uuid4()->tostring();
                        $me           = Auth::user()->id;
                        $group_me       = Auth::user()->real_user_group_id;
                        $role_me       = Auth::user()->role_id;


                        $options=array(

                                'needSignature' => $this->needSignature, 
                                'raiseExceptions' => $this->raiseExceptions,
                                'isTest' => $this->isTest
                            );

                        $url           = $this->url;
                        $userUid       = $this->userUid;
                        $apiKey        = $this->apiKey;
                        
                       

                        $service = new CarvxService($url, $userUid, $apiKey, $options);

                        $search = $service->createSearch($chassisNumber);



                         //carvx found
                        if(!empty($search->cars[0]->chassisNumber)){

                            $is_sent = '0';

                            $api_from = 'CARVX';

                            $data                   =  new VehicleApi;
                            $data->api_from         = $api_from;
                            $data->istest           = $this->isTest;
                            $data->search_id        = $search->uid; //
                            $data->car_id           = $search->cars[0]->carId; //
                            $data->id_vehicle       = $id_vehicle;
                            $data->vehicle          = $search->cars[0]->chassisNumber;
                            $data->country_origin   = '';
                            $data->brand            = $search->cars[0]->make;
                            $data->model            = $search->cars[0]->model;
                            $data->engine_number    = $search->cars[0]->engine;
                            $data->cc               = '';
                            $data->fuel_type        = '';
                            $data->year_manufacture = $search->cars[0]->manufactureDate;
                            $data->registation_date = '';
                            $data->status           = 'found';
                            $data->request_by       = $me;
                            $data->save();


                            $carId      = $search->cars[0]->carId;
                            $searchId   = $search->uid;
                            $isTest     = $this->isTest;

                            
                            /* ---------- create report API carvx ----------- */
                            //$reportId   = $service->createReport($searchId, $carId, $isTest);  

                            //$reportId = config('autocheck.reportId');

                        

                            /*$data                   =  new ReportVehicle;
                            $data->id_vehicle       = $id_vehicle;
                            $data->report_id        = $reportId;
                            $data->save();*/ 


                            //check match data
                            

                            /*
                            if($search->cars[0]->engine == $request->engine_number){
                                $match_engine_number = '1';
                            }else{
                                $match_engine_number = '0';
                            }
                            */

                            
                            $today  = date('HisdmY');
                            $ver_sn = '00'.$api_from.$today;

                            $data                   =  new VehicleStatusMatch;
                            $data->id_vehicle = $id_vehicle;
                            $data->vehicle = $search->cars[0]->chassisNumber;
                            
                            $data->ver_sn = $ver_sn;
                            $data->save();

                        }
                        //-------------------------  end carvx

                        // searching manual 
                        else{

                            $is_sent = '0';


                            $data                   =  new VehicleStatusMatch;
                            $data->id_vehicle = $id_vehicle;
                            $data->vehicle = $request->chassis;
                            $data->brand = '0';
                            $data->model = '0';
                            /*$data->engine_number =  $match_engine_number;*/
                            $data->status =  '0';
                            $data->type_verify      = '0';
                            $data->save();
                        }
                        // ------------- end searching manual




                        if(!empty($search->cars[0]->chassisNumber)){
                            $status_api = '20'; //found
                            $searching_by = 'API';
                        }else{
                             $status_api = '10'; //failed
                             $searching_by = 'NOT';
                        }



                        /*table for naza / group*/

                        $data                          =  new VehicleChecking;
                        
                        $data->id_vehicle              = $id_vehicle;
                        $data->vehicle                 = $request->chassis;
                        $data->is_sent                 = '0';  
                        $data->type_report             = "2";
                        $data->created_by              =  $me;
                        $data->status                  =  $status_api;
                        $data->searching_by            = $searching_by;
                        $data->group_by                = $role_me;
                        $data->real_type_report_id     = 'Extra';
                        
                          $data->save();



                         //history search
                        $data                       =  new HistorySearchVehicle;
                        
                        $data->vehicle              = $request->chassis;
                        $data->id_vehicle           = $id_vehicle;
                        $data->parameter_history_id = "SEARCH";
                        $data->user_id              =  $me;
                        
                        $data->save();

                        //history user            
                        $data                       =  new HistoryUser;
                        
                        $data->vehicle_id           = $request->chassis;
                        $data->id_vehicle           = $id_vehicle;
                        $data->parameter_history_id = "SEARCH";
                        $data->user_id              =  $me;
                        
                        $data->save();


                    }


                    return redirect('/dashboard')->with(['success' => 'Data saved successfuly']);

                    


                }











                

            }else{

                

                
   

                $role_me       = Auth::user()->role_id;

                
                $status_api = '20'; //found
                $searching_by = 'API';
                
                /*table for naza / group*/

                $data                          =  new VehicleChecking;
                
                $data->id_vehicle              = $id_vehicle;
                $data->vehicle                 = $chassisNumber;
                $data->is_sent                 = '1';  
                
                $data->created_by              =  $id_user;
                $data->status                  =  $status_api;
                $data->searching_by            = $searching_by;
                $data->group_by                = $role_me;
                $data->real_type_report_id     = 'Extra';
                $data->type_report             = "2";
                
                $data->save();

                /* End Create Searching */

                //history search
                $data                       =  new HistorySearchVehicle;
                
                $data->vehicle              = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $id_user;
                
                $data->save();

                //history user            
                $data                       =  new HistoryUser;
                
                $data->vehicle_id           = $request->vehicle;
                $data->id_vehicle           = $id_vehicle;
                $data->parameter_history_id = "SEARCH";
                $data->user_id              =  $id_user;
                
                $data->save();


               


               $email_user = Auth::user()->email;
               $name_user = Auth::user()->name;
               $chassis = $request->chassis;
               $brand = $request->brand;

               $fee = ParameterFee::where('fee_for', '11')->latest('id')->first();
                

                //Real
                
                $callback_url = url('/callback'.$id_vehicle);
                $redirect_url = url('/callback'.$id_vehicle);
                $client = new Client();
                $res = $client->request('POST', 'https://www.billplz.com/api/v3/bills', [

                
                    'auth' => ['ee0c0a5a-8051-4d7e-861d-55c6285a7b7d', '', 'basic'],
                    'form_params' => [
                        'collection_id' => 'im6xvege6',
                        'email' => $email_user,
                        'name' => $name_user,
                        'amount' => $fee->fee.'00',
                        //'mobile' => '0163824356',
                        'callback_url' => $callback_url,
                        'description' => 'Chassis Number : '.$chassis,
                        'redirect_url' => $redirect_url,
                    ]
                ]); 
                

                 //Staging
                /*$callback_url = url('/callback/'.$id_vehicle);
                $redirect_url = url('/callback/'.$id_vehicle);
                $client = new Client();
                $res = $client->request('POST', 'https://billplz-sandbox.com/api/v3/bills', [
                
                    'auth' => ['d553e9ce-2c19-4f27-9763-a43900dd287e', '', 'basic'],
                    'form_params' => [
                        'collection_id' => 'onareikm',
                        'email' => $email_user,
                        'name' =>  $name_user,
                        'amount' => $fee->fee.'00',
                        'callback_url' => $callback_url,
                        'description' => 'Chassis Number :'.$chassis,
                        'redirect_url' => $redirect_url,
                    ]
                ]);*/


                /* end staging */
                  
                  /*$attempt = Auth::user();
                        $loglogin = new LogLogin;
                         
                        $loglogin->email        = $user->email;
                        $loglogin->id_user      = $user->id;
                        $loglogin->name         = $applicant->fullname;
                        $loglogin->ip           = $request->ip();
                        $loglogin->lat          = $request->latitude;
                        $loglogin->lng          = $request->longitude;
                        $loglogin->location     = $request->location;
                        $loglogin->ip           = $request->ip();
                        $loglogin->activities   = 'Pelanggan semakan pembayaran online';
                        $loglogin->id_applicant = $applicant->user_id;
                        $loglogin->remark       = '141';
                        $loglogin->type         = '2';
                        $loglogin->save();*/
                //$purchase = Purchase::Where('id_purchase', $id_purchase)->
                    //update([
                      //"bill_id"   => 13,
                      //"confirm_payment_date" => $today
                   // ]);

                $array = json_decode($res->getBody()->getContents(), true);
                $var_url =  var_export($array['url'],true);
                $url = substr($var_url, 1, -1); 
                return Redirect::to($url);




            }
        }

        
    }

    

    public function callback(Request $request)
    { 
       
       return redirect('/dashboard')->with(['success' => 'Thank You, We will immediately process your request']);
    }


}
