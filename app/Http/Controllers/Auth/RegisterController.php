<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Model\UserGroup;
use App\Model\UserGroupTypeReport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|min:3',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'nullable|string',
            'fax' => 'nullable',
            'company_name' => 'nullable',
            'company_description' => 'nullable',
            'status_buyer' => 'nullable'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        

        /*

        $user->userData = UserGroup::create([
            'group_name' => $data['company_name'],
        ]);

         $user->userData = UserGroupTypeReport::create([
            'user_group_id' => $data['company_name'],
        ]);
        */

        $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'address' => $data['address'],
                'fax' => $data['fax'],
                'company_name' => $data['company_name'],
                'company_description' => $data['company_description'],
                'role_id' => $data['status_buyer'],
                'password' => Hash::make($data['password']),
            ]);

        /*$data             =  new User;
        $data->name    = $data['name'];
        $data->email = $data['email'];
        $data->phone = $data['phone'];
        $data->address = $data['address'];
        $data->fax = $data['fax'];
        $data->company_name = $data['company_name'];
        $data->company_description = $data['company_description'];
        $data->role_id = $data['status_buyer'];
        $data->password = Hash::make($data['password']);
        $data->save();

        
        $usegroup             =  new UserGroup;
        $usegroup->user_id    = $data['name'];
        $usegroup->group_name = $data['company_name'];
        $usegroup->save();*/

        $userData = UserGroup::create([
            'user_id' => $data['name'],
            'group_name' => $data['company_name'],
        ]);

        $userData = UserGroupTypeReport::create([
            'user_group_id' => $userData->id,
            'type_report_id' => "Extra",
        ]);


        return redirect('login')->with(['success' => 'User data successfully created']);

        /*return $user;*/
    }

    
    protected function register_user()
    {
        


        return view('auth.register_user');
        
    }
}
