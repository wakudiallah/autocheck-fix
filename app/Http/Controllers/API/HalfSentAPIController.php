<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterEmail;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\Model\BatchSent;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use Excel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Share\WController;



class HalfSentAPIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->needSignature = config('carvx_halfreport.needSignature');
        $this->raiseExceptions = config('carvx_halfreport.raiseExceptions');
        $this->isTest = config('carvx_halfreport.isTest');

        $this->url           = config('carvx_halfreport.url');
        $this->userUid       = config('carvx_halfreport.userUid');
        $this->apiKey        = config('carvx_halfreport.apiKey');
        $this->date = date("Y-m-d");
        $this->format =  "AC-".date('Ymd-Hi');
        $this->hash  = str_random(20);
        $this->limit_submit_to_partner_everyday = config('autocheck.limit_submit_to_partner_everyday');
        
    }


    public function index() 
    {

        $data = VehicleChecking::where('status', '20')
            ->where('searching_by', 'API')
            ->where('group_by', 'NA')
            ->orderBy('id', 'ASC')
            ->WhereNull('limit_submit')
            ->get();

        return view('verifier.sent.api.half', compact('data'));

    }


    public function store(Request $request)
    {

        $is_test = config('autocheck.is_test_am3_report_naza');


        $count_today_submit = VehicleChecking::where('date_sent', $this->date)
                            ->where('limit_submit', '1')
                            ->where('type_report', '3')
                            ->count();


        $item = array_map(null, $request->id, $request->vehicle, $request->ci, $request->ids);


        if($count_today_submit < $this->limit_submit_to_partner_everyday){

            foreach($item as $val) {
                
                $pra = VehicleChecking::where('id_vehicle',$val[0])->update([                
                    
                    "date_sent"             => $this->date,
                    "unique_sent"           => $this->format,
                    "hash"                  => $this->hash
                ]);



                $this->sent_carvx($val[1]);



                $pra = VehicleChecking::where('id_vehicle',$val[0])->update([                
                    "limit_submit"         => '1',
                    "is_sent"              => '1',
                    "is_check_id"          => '1',
                    "status"               => '27'
                ]);



                return Redirect::back()->with(['success' => 'Chassis Successfully Sent to Partner']);         
            }  





        }else{


            $remains = $this->limit_submit_to_partner_everyday - $count_today_submit; 

            return Redirect::back()->with(['warning' => 'Chassis has reached the limit ('.$this->limit_submit_to_partner_everyday.'), Please send only'. $remains. 'Vehicle']);

        }



    }




    public function sent_carvx($id)
    {

        $data = VehicleChecking::where('hash', $this->hash)
                                ->where('unique_sent', $this->format)
                                ->get();


        foreach($data as $data){

            $vin = $data->vehicle;

            $options=array(

                'needSignature' => $this->needSignature, 
                'raiseExceptions' => $this->raiseExceptions,
                'isTest' => $this->isTest
            );

                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;


            try {

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                $search = $service->createSearch($vin);


                if(!empty($search->cars[0]->chassisNumber)){  //found in partner

                    $chassisNumber = $search->cars[0]->chassisNumber;                  

                }else{  //check once again with W

                    $vinx = substr_replace($vin, "W", 5, 0);

                    $search = $service->createSearch($vinx);
                    $chassisNumber = $search->cars[0]->chassisNumber;     

                }     


                $carId      = $search->cars[0]->carId;
                $searchId   = $search->uid;
                $isTest     = $this->isTest;
          

                if($this->isTest == "1"){

                    $reportId = "dummyhalf";  //dummy

                }else{

                    $reportId = "onlinedummyhalf";

                    //$reportId   = $service->createReport($searchId, $carId, $isTest);

                }

              


                $report                   =  new ReportVehicle;
                $report->id_vehicle       = $data->id_vehicle;
                $report->report_id        = $reportId;
                $report->save();


                /*batch */
                $batch          = new BatchSent;
                $batch->unique_sent =  $this->format;
                $batch->created_by    = Auth::user()->id;
                $batch->partner = "carvx";
                $batch->save();
                /*end batch */


            } catch (\Throwable $e) {

                return redirect()->back()->with(['warning' => 'Please Check Need Signature in carvx System']); 
            }

                      

        }


        
    }





}
