<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\AntiAttrition;
use App\User;
use App\Transformers\VehicleTransformer;
use App\Transformers\PartnerTransformer;
use App\Transformers\HaflTransformer;
use App\Transformers\ExtraTransformer;
use App\Transformers\FullTransformer;
use App\Transformers\ReportTransformer;

use App\Transformers\ExtraMnTransformer;
use App\Transformers\FullMnTransformer;
use App\Transformers\HaflMnTransformer;

use Auth;
use App\Model\SettlementInfo;
use App\Model\VehicleChecking;
use App\Model\VehicleStatusMatch;
use Debugger;
use Illuminate\Support\Facades\Log;
use App\Transformers\MomTransformer;
use Response;
use Carvx\CarvxService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;



class ApiSearchController extends Controller
{
    public function search_vehicle_marii(Request $request,VehicleChecking $mo){
        $this->validate($request, [
            'vehicle'                 =>'required'
        ]);
       // $auth = Auth::user();
        
        //$mo = $mo->find($request->id_mo);
        $mo = VehicleChecking::where('vehicle', $request->vehicle)->first();

        $count = VehicleChecking::where('vehicle', $request->vehicle)->count();

        if($count=='1'){
            return fractal()
                ->item($mo)
                ->transformWith(new VehicleTransformer)
                ->includePosts()
                ->toArray();
        }
        elseif($count>'1'){
            return Response::json(["response" => "Duplicated, Please Contact Admin"]);
        }
        else{
            return Response::json(["response" => "Not Found"]);
        }
    }





    public function search_smat_to_autocheck(Request $request){
        
        $this->validate($request, [
            'vehicle'                 =>'required|max:255'
        ]);

        //$mo = VehicleChecking::where('vehicle', $request->vehicle)->first();

        $count = VehicleChecking::where('vehicle', $request->vehicle)->count();

        $vin = $request->vehicle;
        $type_report = $request->type_report;

        /*check 5 first character coz must add W*/
        $check_vin = substr($vin, 0, 5);
        if($check_vin == "AGH30"){

            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        }elseif($check_vin == "GGH30"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        
        }elseif($check_vin == "GGH35"){

            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;


        }elseif($check_vin == "ANH20"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        }elseif($check_vin == "AGL10"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        }elseif($check_vin == "ANH20"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        }
        else{
            $chassisNumber = $vin;
        }
        /* end check 5 first character coz must add W*/


        return fractal()
            ->item($chassisNumber, $type_report, $vin)
            ->transformWith(new PartnerTransformer)
            ->includePosts()
            ->toArray();
        

    }



    public function  submit_to_autocheck(Request $request){
  

        $this->validate($request, [
            'vehicle'                 =>'required|max:255',
            'type_report'             => 'required|integer|min:1|between: 2,4'
        ]);

           

        //$mo = VehicleChecking::where('vehicle', $request->vehicle)->first();

        $count = VehicleChecking::where('vehicle', $request->vehicle)->count();

        $vin = $request->vehicle;
        $type_report = $request->type_report;

        /*check 5 first character coz must add W*/
        $check_vin = substr($vin, 0, 5);
        if($check_vin == "AGH30"){

            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;

        }elseif($check_vin == "GGH30"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        
        }elseif($check_vin == "GGH35"){

            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;

        }elseif($check_vin == "ANH20"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;

        }elseif($check_vin == "AGL10"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;

        }elseif($check_vin == "ANH20"){
            
            $vinx = substr_replace( $vin, "W", 5, 0);
            $chassisNumber = $vinx;
        }
        else{
            $chassisNumber = $vin;
        }
        /* end check 5 first character coz must add W*/

     

        /* ========== check data ready or not ================*/
        $options=array(

            'needSignature' => '0', 
            'raiseExceptions' => '1',
            'isTest' => '1'

        );


        $url           = 'https://carvx.jp';
        $userUid       = 'h1Nigk43M3rP';
        $apiKey        = '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t';
        
        $service = new CarvxService($url, $userUid, $apiKey, $options);
        $search = $service->createSearch($chassisNumber);

        /* ========== End check data ready or not ================*/



        if(!empty($search->cars[0]->chassisNumber)){  /*=====  submit to Half Report */
                
            if($type_report == "2"){  //extra report

                return fractal()
                    ->item($vin)
                    ->transformWith(new ExtraTransformer)
                    ->includePosts()
                    ->toArray(); 

            }elseif($type_report == "4"){ //FULL report

                return fractal()
                    ->item($vin)
                    ->transformWith(new FullTransformer)
                    ->includePosts()
                    ->toArray();

            }elseif($type_report == "3"){ //HALF report


                return fractal()
                    ->item($vin)
                    ->transformWith(new HaflTransformer)
                    ->includePosts()
                    ->toArray();
            }

        }else{  //cek data manual

            
            if($type_report == "2"){ //extrareport

                return fractal()
                    ->item($vin)
                    ->transformWith(new ExtraMnTransformer)
                    ->includePosts()
                    ->toArray();

            }elseif($type_report == "4"){ //FULL report

                return fractal()
                    ->item($vin)
                    ->transformWith(new FullMnTransformer)
                    ->includePosts()
                    ->toArray();

            }elseif($type_report == "3"){ //HALF report

                return fractal()
                    ->item($vin)
                    ->transformWith(new HaflMnTransformer)
                    ->includePosts()
                    ->toArray();                

            }

        }
   
    }


    public function get_report_autocheck(Request $request,VehicleChecking $mo){
        $this->validate($request, [
            'id'                 =>'required'
        ]);
       // $auth = Auth::user();

	return[
                    "data" => "NF",
                ];
        

        //$mo = $mo->find($request->id_mo);
        $mo = VehicleChecking::where('id_vehicle', $request->id)->first();
        $count = VehicleChecking::where('id_vehicle', $request->id)->count();



        if($count=='1'){

            //check complete status 
            if($mo->status == "40"){  //complete
		
		return[
                    "data" => "NF",
                ];
                
		/*return fractal()
                    ->item($mo)
                    ->transformWith(new ReportTransformer)
                    ->includePosts()
                    ->toArray();*/

            }elseif($mo->status == "30"){ 

                return[
                    "data" => "NF",
                ];

            }elseif($mo->status == "25"){

                return[
                    "data" => "P",
                ];
                
            }elseif($mo->status == "20"){

                return[
                    "data" => "S",
                ];
                
            }elseif($mo->status == "10"){
                
                return[                    
                    "data" => "S",
                ];
                
            }
            //end check complete status 

            
        }
        elseif($count>'1'){
            return Response::json(["response" => "Duplicated, Please Contact Admin"]);
        }
        else{
            return Response::json(["response" => "Not Found"]);
        }
    }
    



}

