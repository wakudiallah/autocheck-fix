<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Transformers\VehicleTransformer;
use App\Transformers\PartnerTransformer;
use App\Transformers\HaflTransformer;
use App\Transformers\ExtraTransformer;
use App\Transformers\FullTransformer;
use App\Transformers\ReportTransformer;
use App\Transformers\ExtraMnTransformer;
use App\Transformers\FullMnTransformer;
use App\Transformers\HaflMnTransformer;
use App\Model\SettlementInfo;
use App\Model\VehicleChecking;
use App\Model\VehicleStatusMatch;
use App\Model\UserGroup;
use App\Model\UserGroupTypeReport;
use Debugger;
use Illuminate\Support\Facades\Log;
use App\Transformers\MomTransformer;
use Response;
use Carvx\CarvxService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use Auth;
use Ramsey\Uuid\Uuid;



class AuthController extends Controller
{
    
    public function register(Request $request){

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => \Hash::make($request->password)
            ]);

            $token = $user->createToken('Token')->accessToken;

            return response()->json(['token' => $token, 'user'=>$user], 200);
    }



    public function login(Request $request){ 

            $data = [
                    'email' => $request->email,
                    'password' => $request->password,
            ];


            if(auth()->attempt($data)){

                $token = auth()->user()->createToken('Token')->accessToken;

                $count  = VehicleChecking::where('vehicle', $request->vehicle)->count();

                return response()->json(['count'=>$count], 200);

            }
            else{


                return response('Unauthorized.', 401);
            }

    }





    public function submit_to_autocheck(Request $request){

       
       $data = [
                    'email' => $request->email,
                    'password' => $request->password,
            ];


        if(auth()->attempt($data)){

            $token = auth()->user()->createToken('Token')->accessToken;

            $count  = VehicleChecking::where('vehicle', $request->vehicle)->count();

           

            if($count == 0){

                    
                    $vin = $request->vehicle;



                    /*======== check 5 first character coz must add W ====== */
                    /*Temporary Kene tukar semula*/
                    
                    $check_vin = substr($vin, 0, 5);
                    if($check_vin == "AGH30"){

                        $vinx = substr_replace( $vin, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "GGH30"){
                        
                        $vinx = substr_replace( $vin, "W", 5, 0);
                        $chassisNumber = $vinx;
                    
                    }elseif($check_vin == "GGH35"){

                        $vinx = substr_replace( $vin, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "ANH20"){
                        
                        $vinx = substr_replace( $vin, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "AGL10"){
                        
                        $vinx = substr_replace( $vin, "W", 5, 0);
                        $chassisNumber = $vinx;

                    }elseif($check_vin == "ANH20"){
                        
                        $vinx = substr_replace( $vin, "W", 5, 0);
                        $chassisNumber = $vinx;
                    }
                    else{
                        $chassisNumber = $vin;
                    }
                    /*============ end check 5 first character coz must add W ============*/



                    /* ========== check data ready or not ================*/
                    $options=array(

                        'needSignature' => '1', 
                        'raiseExceptions' => '1',
                        'isTest' => '1'

                    );


                    $url           = 'https://carvx.jp';
                    $userUid       = 'h1Nigk43M3rP';
                    $apiKey        = '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t';
                    
                    $service = new CarvxService($url, $userUid, $apiKey, $options);
                    $search = $service->createSearch($chassisNumber);

                dd($search);
                
                /*==========  Vehicle Checking Submit ======== */
                    /*first Check Id as Group Type Report 
                    like Hlaf, Full, or Extra Report */


                $checkGroupUser = User::join('user_groups', 'users.real_user_group_id', '=', 'user_groups.user_id')
                        ->join('user_group_type_reports', 'user_group_type_reports.user_group_id', '=', 'user_groups.id')
                        ->select('user_group_type_reports.type_report_id', 'users.id')
                        ->where('users.email', $request->email)
                        ->first();


                
                $id_vehicle     = Uuid::uuid4()->tostring();

                $data                          =  new VehicleChecking;   
                $data->id_vehicle              = $id_vehicle;
                $data->vehicle                 = $vin;
                $data->created_by              =  $checkGroupUser->id;
                $data->save();



                /* ========== End check data ready or not ================*/  
                if(!empty($search->cars[0]->chassisNumber)){ 



                    /*return response()->json(['count'=>$search->cars[0]->chassisNumber], 200);*/


                    if($checkGroupUser->type_report_id == "Extra"){  //Extra report

                        return fractal()
                            ->item($id_vehicle)
                            ->transformWith(new ExtraTransformer)
                            ->includePosts()
                            ->toArray(); 

                    }elseif($checkGroupUser->type_report_id == "Full"){ //FULL report

                        return fractal()
                            ->item($id_vehicle)
                            ->transformWith(new FullTransformer)
                            ->includePosts()
                            ->toArray();

                    }elseif($checkGroupUser->type_report_id == "Half"){ //HALF report

                        return fractal()
                            ->item($id_vehicle)
                            ->transformWith(new HaflTransformer)
                            ->includePosts()
                            ->toArray();
                    }



                }else{  //cek data manual

                    
                    if($checkGroupUser->type_report_id == "Extra"){ //extrareport

                        return fractal()
                            ->item($id_vehicle)
                            ->transformWith(new ExtraMnTransformer)
                            ->includePosts()
                            ->toArray();

                    }elseif($checkGroupUser->type_report_id == "Full"){ //FULL report

                        return fractal()
                            ->item($id_vehicle)
                            ->transformWith(new FullMnTransformer)
                            ->includePosts()
                            ->toArray();

                    }elseif($checkGroupUser->type_report_id == "Half"){ //HALF report

                        return fractal()
                            ->item($id_vehicle)
                            ->transformWith(new HaflMnTransformer)
                            ->includePosts()
                            ->toArray();                

                    }

                }

            }
            else{ //Data already Check

                return response('Chassis Already Checked.', 401);

            }


        }
        else{

            return response('Unauthorized.', 401);

        }



    }




    public function get_report(Request $request)
    {    
        
        $data = [
                    'email' => $request->email,
                    'password' => $request->password,
            ];


        if(auth()->attempt($data)){

            //$token = auth()->user()->createToken('Token')->accessToken;

            
            $mo = VehicleChecking::where('id_vehicle', $request->id)->first();
            $count = VehicleChecking::where('id_vehicle', $request->id)->count();


            if($count=='1'){

                //check complete status 
                if($mo->status == "40"){  //complete
                        

                    return fractal()
                        ->item($mo)
                        ->transformWith(new ReportTransformer)
                        ->includePosts()
                        ->toArray();

                }elseif($mo->status == "30"){ 

                    return[
                        "data" => "NF",
                    ];

                }elseif($mo->status != "30" OR $mo->status != "20" OR $mo->status != "10"){

                    return[
                        "data" => "P",
                    ];
                    
                }elseif($mo->status == "20"){

                    return[
                        "data" => "S",
                    ];
                    
                }elseif($mo->status == "10"){
                    
                    return[                    
                        "data" => "S",
                    ];
                    
                }
                //end check complete status 
                
            }
            elseif($count>'1'){
                return Response::json(["response" => "Duplicated, Please Contact Admin"]);
            }
            else{
                return Response::json(["response" => "Not Found"]);
            }



        }
        else{


            return response('Unauthorized.', 401);
        }



        
    
    }




}
