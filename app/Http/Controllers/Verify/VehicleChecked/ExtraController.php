<?php

namespace App\Http\Controllers\Verify\VehicleChecked;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAccidentHistory;
use App\Model\RRecallHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;


class ExtraController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
            
        $myGroup = Auth::user()->real_user_group_id;    

        $data = VehicleChecking::where('real_type_report_id', 'Extra')
            ->whereIn('status', ['40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data2 = VehicleChecking::where('real_type_report_id', 'Extra')
            ->whereIn('status', ['40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data3 = VehicleChecking::where('real_type_report_id', 'Extra')
            ->whereIn('status', ['40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data_verify = VehicleChecking::orderBy('id', 'DESC')
            ->get();

        $vehicle_api  = VehicleApi::get();
 
        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();


        return view('verifier.vehicle_checked.extra', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));

    }


    public function detail($id)
    {
        
        $data = VehicleChecking::where('id_vehicle', $id)
                ->latest()
                ->first();

        $raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->get();
        $raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->get();
        $raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->get();
        $raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->get();
        $raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->get();
        $raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->get();


        $check_raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->first();
        $check_raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->first();
        $check_raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->first();
        $check_raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->first();
        $check_raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->first();
        $check_raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->first();

        $recall = RRecallHistory::where('id_vehicle', $id)
                ->get();

        $summary = RSummary::where('id_vehicle', $id)
                    ->latest('id')
                    ->first();

        $usagehistory = RUsageHistory::where('id_vehicle', $id)
                ->first();
                
        $vehicleassessment = RVehicleAssessment::where('id_vehicle', $id)->latest('id')->first();
        $vehicledetail = RVehicleDetails::where('id_vehicle', $id)->first();
        $vehiclespesification = RVehicleSpecification::where('id_vehicle', $id)->first();
        $auctionimage = RAuctionImages::where('id_vehicle', $id)->orderBy('id', 'DESC')->get();
        $detailhistory = RDetailHistory::where('id_vehicle', $id)->get();
        $auctionhistory = RAuctionHistory::where('id_vehicle', $id)->get();
        $odometerhistory = ROdometerHistory::where('id_vehicle', $id)->get();
        $vehicle = VehicleChecking::where('id_vehicle', $id)->first();

      

        return view('detail.extra', compact('data', 'summary', 'usagehistory', 'vehicleassessment', 'vehicledetail', 'vehiclespesification', 'auctionimage', 'detailhistory', 'auctionhistory', 'odometerhistory', 'vehicle', 'recall', 'raccident_collision', 'raccident_malfunction', 'raccident_theft', 'raccident_fireDamage', 'raccident_waterDamage', 'raccident_hailDamage', 'check_raccident_collision', 'check_raccident_malfunction', 'check_raccident_theft', 'check_raccident_fireDamage', 'check_raccident_waterDamage', 'check_raccident_hailDamage'));

    }




}
