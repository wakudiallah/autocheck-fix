<?php

namespace App\Http\Controllers\Verify\VehicleChecked;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;



class FullController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
            
        $data = VehicleChecking::where('group_by', 'KA')
            ->whereNull('old_report')
            ->whereIn('status', ['30', '40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data2 = VehicleChecking::where('group_by', 'KA')
            ->whereNull('old_report')
            ->whereIn('status', ['30', '40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data3 = VehicleChecking::where('group_by', 'KA')
            ->whereNull('old_report')
            ->whereIn('status', ['30', '40'])
            ->orderBy('updated_at', 'DESC')
            ->get();


        $data_verify = VehicleChecking::orderBy('id', 'DESC')
            ->get();

        $vehicle_api  = VehicleApi::get();

 
        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();


        return view('verifier.vehicle_checked.full', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));

    }



    public function detail($id)
    {
        
        $data = VehicleChecking::where('id_vehicle', $id)
                ->latest()
                ->first();


        return view('detail.full', compact('data'));

    }





}
