<?php

namespace App\Http\Controllers\Verify\Sync;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RAccidentHistory;
use App\Model\RRecallHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use Redirect;

class ExtraSyncController extends Controller
{
    
    public function __construct()

    {
        $this->middleware('auth');

        $this->needSignature = config('carvx_extrareport.needSignature');
        $this->raiseExceptions = config('carvx_extrareport.raiseExceptions');
        $this->isTest = config('carvx_extrareport.isTest');

        
        $this->url = config('carvx_extrareport.url');
        $this->userUid = config('carvx_extrareport.userUid');
        $this->apiKey = config('carvx_extrareport.apiKey');
        $this->limit_submit_to_partner_everyday = config('autocheck.limit_submit_to_partner_everyday');
        $this->date = date("Y-m-d");
        $this->format =  "AC-".date('Ymd-Hi');
        $this->hash  = str_random(20);
    }


    public function index()
    {
        
        $data = VehicleChecking::where('status', '27')
                ->orderBy('updated_at', 'DESC')
                ->where('real_type_report_id', 'Extra')
                ->get();

        $data2 = VehicleChecking::where('status', '27')
                ->orderBy('updated_at', 'DESC')
                ->where('real_type_report_id', 'Extra')
                ->get();

        $data3 = VehicleChecking::where('status', '27')
                ->orderBy('updated_at', 'DESC')
                ->where('real_type_report_id', 'Extra')
                ->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.sync.extra', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));

        
    }

    public function store(Request $request)
    {

        try {

            $me =  Auth::user()->id;

            $sync = array_map(null, $request->id_vehicle, $request->id, $request->ids);


            foreach ($sync as $val) {


                $id = $val[0];

                $getreport = ReportVehicle::where('id_vehicle', $id)->latest()->first();
                $reportId = $getreport->report_id;


                $options=array(

                        'needSignature' => $this->needSignature, 
                        'raiseExceptions' => $this->raiseExceptions,
                        'isTest' => $this->isTest
                    );

                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;
                
                $service = new CarvxService($url, $userUid, $apiKey, $options);
               
                $report = $service->getReport($reportId, true);


                $is_ready       = $report->status;
                $creation_date  = $report->creationDate;
                $due_date       = $report->dueDate;
                $data           = $report->data;


                ReportVehicle::where('id_vehicle',$id)->update(array(
                    'is_ready' => $is_ready,
                    //'is_ready' => '4',  
                    'creation_date' => $creation_date,
                    'due_date' => $due_date
                    //'data' => $data
                ));

                if($is_ready == '6') //status reject carvx
                {
                    $status_chassis = '30';
                }
                elseif($is_ready == '5'){ //complete
                    $status_chassis = '40';

                    $data                       =  new HistorySearchVehicle;
                    
                    //$data->vehicle              = $request->vehicle;
                    $data->id_vehicle           = $id;
                    $data->parameter_history_id = "Complete";
                    $data->user_id              =  Auth::user()->id;
                    
                    $data->save();

                }
                elseif($is_ready == '3'){ //new
                    $status_chassis = '20';
                }
                elseif($is_ready == '4'){ //inprogress
                    $status_chassis = '27';

                    $data                       =  new HistorySearchVehicle;
                    
                    //$data->vehicle              = $request->vehicle;
                    $data->id_vehicle           = $id;
                    $data->parameter_history_id = "In Progress";
                    $data->user_id              =  Auth::user()->id;
                    
                    $data->save();
                }



                /*if status ready == 5 update date */
                if($is_ready == '5'){


                    $auctionImages = $report->data->auctionImages;
                    $i = 0;

                    foreach($auctionImages as $key) {

                        //$base64 = base64_encode(file_get_contents($report->data->auctionImages[$i++]));


                        $data             =  new RAuctionImages;
                        $data->id_vehicle   = $id;
                        $data->report_id    = $reportId;
                        $data->image    = $report->data->auctionImages[$i++];
                        //$data->imagebit64 = $base64;
                        $data->save();
                        
                    }


                $get_image = RAuctionImages::where('id_vehicle', $id)->get();

                foreach($get_image as $get_image){

                    $b64image = base64_encode(file_get_contents($get_image->image));

                    $data = RAuctionImages::where('id',$get_image->id)->update(array('imagebit64' => $b64image ));

                    
                }



                $body_odometerhistory = json_encode($report->data->odometerHistory, true);
                $body_detailedhistory = json_encode($report->data->detailedHistory, true);
                $body_auctionhistory = json_encode($report->data->auctionHistory, true); 

                $body_recallhistory = json_encode($report->data->recallHistory, true); 

                //Accident History
                $body_accident_history_collision = json_encode($report->data->accidentHistory->collision, true); 
                $body_accident_malfunction = json_encode($report->data->accidentHistory->malfunction, true);

                $body_accident_theft = json_encode($report->data->accidentHistory->theft, true);

                $body_accident_fireDamage = json_encode($report->data->accidentHistory->fireDamage, true);

                $body_accident_waterDamage = json_encode($report->data->accidentHistory->waterDamage, true);

                $body_accident_hailDamage = json_encode($report->data->accidentHistory->hailDamage, true);
                //End Accident History
                 

                $data_odometerhistory = json_decode($body_odometerhistory, true);
                $data_detailedhistory = json_decode($body_detailedhistory, true);
                $data_auctionhistory = json_decode($body_auctionhistory, true);

                $data_recallhistory = json_decode($body_recallhistory, true);

                //Accident History
                $data_accident_history_collision = json_decode($body_accident_history_collision, true);

                $data_accident_history_malfunction = json_decode($body_accident_malfunction, true);

                $data_accident_history_theft = json_decode($body_accident_theft, true);

                $data_accident_history_fireDamage = json_decode($body_accident_fireDamage, true);

                $data_accident_history_waterDamage = json_decode($body_accident_waterDamage, true);

                $data_accident_history_hailDamage = json_decode($body_accident_hailDamage, true);
                //End Accident History

                //Accident History
                $count_accident_history_collision=count($data_accident_history_collision);
                $count_accident_history_malfunction=count($data_accident_history_malfunction);

                $count_accident_history_theft=count($data_accident_history_theft);
                $count_accident_history_fireDamage=count($data_accident_history_fireDamage);
                $count_accident_history_waterDamage=count($data_accident_history_waterDamage);
                $count_accident_history_hailDamage=count($data_accident_history_hailDamage);
                //Accident History

                $count_detailedhistory=count($data_detailedhistory);
                $count_auctionhistory = count($data_auctionhistory);

                $count_auctionhistory = count($data_auctionhistory);

                $count_recallhistory = count($data_recallhistory);
                $count_odometerhistory = count($data_odometerhistory);
                
              

                for($i=0; $i<$count_recallhistory; $i++){
                    $datas = new RRecallHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->date = $data_recallhistory[$i]["date"];
                    $datas->source = $data_recallhistory[$i]["source"];
                    

                    $datas->affected_part = $data_recallhistory[$i]["affected_part"];
                    $datas->details = $data_recallhistory[$i]["details"];
                    
                    $datas->save();
                }

                 //Accident History
                 //
                 
                for($i=0; $i<$count_accident_history_hailDamage; $i++){
                    $datas = new RAccidentHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->field = "hailDamage";
                    $datas->date_reported = $data_accident_history_hailDamage[$i]["date"];
                    $datas->source = $data_accident_history_hailDamage[$i]["source"];
                    $datas->details = $data_accident_history_hailDamage[$i]["problem_scale"];
                    $datas->airbag = $data_accident_history_hailDamage[$i]["airbag"];
                    
                    $datas->save();
                }

                for($i=0; $i<$count_accident_history_waterDamage; $i++){
                    $datas = new RAccidentHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->field = "waterDamage";
                    $datas->date_reported = $data_accident_history_waterDamage[$i]["date"];
                    $datas->source = $data_accident_history_waterDamage[$i]["source"];
                    $datas->details = $data_accident_history_waterDamage[$i]["problem_scale"];
                    $datas->airbag = $data_accident_history_waterDamage[$i]["airbag"];
                    
                    $datas->save();
                }


                for($i=0; $i<$count_accident_history_fireDamage; $i++){
                    $datas = new RAccidentHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->field = "fireDamage";
                    $datas->date_reported = $data_accident_history_fireDamage[$i]["date"];
                    $datas->data_source = $data_accident_history_fireDamage[$i]["source"];
                    $datas->details = $data_accident_history_fireDamage[$i]["problem_scale"];
                    $datas->airbag = $data_accident_history_fireDamage[$i]["airbag"];
                    
                    $datas->save();
                }

                for($i=0; $i<$count_accident_history_theft; $i++){
                    $datas = new RAccidentHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->field = "theft";
                    $datas->date_reported = $data_accident_history_theft[$i]["date"];
                    $datas->data_source = $data_accident_history_theft[$i]["source"];
                    $datas->details = $data_accident_history_theft[$i]["problem_scale"];
                    $datas->airbag = $data_accident_history_theft[$i]["airbag"];
                    
                    $datas->save();
                }


                for($i=0; $i<$count_accident_history_malfunction; $i++){
                    $datas = new RAccidentHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->field = "malfunction";
                    $datas->date_reported = $data_accident_history_malfunction[$i]["date"];
                    $datas->data_source = $data_accident_history_malfunction[$i]["source"];
                    $datas->details = $data_accident_history_malfunction[$i]["problem_scale"];
                    $datas->airbag = $data_accident_history_malfunction[$i]["airbag"];
                    
                    $datas->save();
                }


                for($i=0; $i<$count_accident_history_collision; $i++){
                    $datas = new RAccidentHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->field = "collision";
                    $datas->date_reported = $data_accident_history_collision[$i]["date"];
                    $datas->data_source = $data_accident_history_collision[$i]["source"];
                    $datas->details = $data_accident_history_collision[$i]["problem_scale"];
                    $datas->airbag = $data_accident_history_collision[$i]["airbag"];
                    
                    $datas->save();
                }

                 //Elnd Accident History


                for($i=0; $i<$count_auctionhistory; $i++){
                    $datas = new RAuctionHistory;
                    $datas->id_vehicle = $id;
                    $datas->report_id = $reportId;
                    $datas->date = $data_auctionhistory[$i]["date"];
                    $datas->lotNumber = $data_auctionhistory[$i]["lot_number"];
                    $datas->auction = $data_auctionhistory[$i]["auction"];
                    $datas->make = $data_auctionhistory[$i]["make"];
                    $datas->model = $data_auctionhistory[$i]["model"];
                    $datas->registrationDate = $data_auctionhistory[$i]["registration_date"];
                    $datas->mileage = $data_auctionhistory[$i]["mileage"];
                    $datas->displacement = $data_auctionhistory[$i]["displacement"];
                    $datas->transmission = $data_auctionhistory[$i]["transmission"];
                    $datas->color = $data_auctionhistory[$i]["color"];
                    $datas->body = $data_auctionhistory[$i]["body"];
                    $datas->finalPrice = $data_auctionhistory[$i]["final_price"];
                    $datas->result = $data_auctionhistory[$i]["result"];
                    $datas->assessment = $data_auctionhistory[$i]["assessment"];
                    //$datas->images = $data_auctionhistory[$i]["images"];
                    $datas->save();
                }



                for($i=0; $i<$count_detailedhistory; $i++){
                    $datas             =  new RDetailHistory;
                    $datas->id_vehicle   = $id;
                    $datas->report_id    = $reportId;
                    $datas->mileage = $data_detailedhistory[$i]["mileage"];
                    $datas->source = $data_detailedhistory[$i]["source"];
                    $datas->date = $data_detailedhistory[$i]["date"];
                    $datas->action = $data_detailedhistory[$i]["action"];
                    $datas->location = $data_detailedhistory[$i]["location"];
                    $datas->save();
                }


                for($i=0; $i<$count_odometerhistory; $i++){
                    $datas             =  new ROdometerHistory;
                    $datas->id_vehicle   = $id;
                    $datas->report_id    = $reportId;
                    $datas->mileage = $data_odometerhistory[$i]["mileage"];
                    $datas->source = $data_odometerhistory[$i]["source"];
                    $datas->date = $data_odometerhistory[$i]["date"];
                    $datas->save();
                }


                $data             =  new RVehicleDetails;
                
                $data->id_vehicle       = $id;
                $data->report_id    = $reportId;
                $data->chassisNumber    = $report->data->vehicleDetails->chassisNumber;
                $data->make =  $report->data->vehicleDetails->make;
                $data->model =  $report->data->vehicleDetails->model;
                $data->grade =  $report->data->vehicleDetails->grade;
                $data->manufactureDate =  $report->data->vehicleDetails->manufactureDate;
                $data->body =  $report->data->vehicleDetails->body;
                $data->engine =  $report->data->vehicleDetails->engine;
                $data->drive =  $report->data->vehicleDetails->drive;
                $data->transmission =  $report->data->vehicleDetails->transmission;
                $data->save();

                $data             =  new RUsageHistory;
                
                $data->id_vehicle       = $id;
                $data->report_id    = $reportId;
                $data->contaminatedRegion    = $report->data->usageHistory->contaminatedRegion;
                $data->contaminationTest =  $report->data->usageHistory->contaminationTest;
                $data->commercialUsage =  $report->data->usageHistory->commercialUsage;
                $data->save();
                
                

                $data             =  new RVehicleAssessment;
                
                $data->id_vehicle       = $id;
                $data->report_id    = $reportId;

                $data->driverSeatPoints    = $report->data->vehicleAssessment->driverSeatPoints;
                $data->driverSeatEvaluation    = $report->data->vehicleAssessment->driverSeatEvaluation;
                $data->driverSeatGoalAverage =  $report->data->vehicleAssessment->driverSeatGoalAverage;
                $data->frontPassengerSeatPoints =  $report->data->vehicleAssessment->frontPassengerSeatPoints;
                $data->frontPassengerSeatEvaluation =  $report->data->vehicleAssessment->frontPassengerSeatEvaluation;
                $data->frontPassengerSeatGoalAverage =  $report->data->vehicleAssessment->frontPassengerSeatGoalAverage;
                $data->dryRoadStoppingDistance =  $report->data->vehicleAssessment->dryRoadStoppingDistance;
                $data->wetRoadStoppingDistance =  $report->data->vehicleAssessment->wetRoadStoppingDistance;
                $data->safetyGrade =  $report->data->vehicleAssessment->safetyGrade;
                $data->save();

                

                $data             =  new RVehicleSpecification;
                
                $data->id_vehicle       = $id;
                $data->report_id    = $reportId;
                $data->firstGearRatio    = $report->data->vehicleSpecification->firstGearRatio;
                $data->secondGearRatio =  $report->data->vehicleSpecification->secondGearRatio;
                $data->thirdGearRatio =  $report->data->vehicleSpecification->thirdGearRatio;
                $data->fourthGearRatio =  $report->data->vehicleSpecification->fourthGearRatio;
                $data->fifthGearRatio =  $report->data->vehicleSpecification->fifthGearRatio;
                $data->sixthGearRatio =  $report->data->vehicleSpecification->sixthGearRatio;
                $data->additionalNotes =  $report->data->vehicleSpecification->additionalNotes;
                $data->airbagPosition =  $report->data->vehicleSpecification->airbagPosition;
                $data->bodyRearOverhang =  $report->data->vehicleSpecification->bodyRearOverhang;
                $data->bodyType =  $report->data->vehicleSpecification->bodyType;
                $data->chassisNumberEmbossingPosition =  $report->data->vehicleSpecification->chassisNumberEmbossingPosition;
                $data->classificationCode =  $report->data->vehicleSpecification->classificationCode;
                $data->cylinders =  $report->data->vehicleSpecification->cylinders;
                $data->displacement =  $report->data->vehicleSpecification->displacement;
                $data->electricEngineType =  $report->data->vehicleSpecification->electricEngineType;
                $data->electricEngineMaximumOutput =  $report->data->vehicleSpecification->electricEngineMaximumOutput;
                $data->electricEngineMaximumTorque =  $report->data->vehicleSpecification->electricEngineMaximumTorque;
                $data->electricEnginePower =  $report->data->vehicleSpecification->electricEnginePower;
                $data->engineMaximumPower =  $report->data->vehicleSpecification->engineMaximumPower;
                $data->engineMaximumTorque =  $report->data->vehicleSpecification->engineMaximumTorque;
                $data->engineModel =  $report->data->vehicleSpecification->engineModel;
                $data->frameType =  $report->data->vehicleSpecification->frameType;
                $data->frontShaftWeight =  $report->data->vehicleSpecification->frontShaftWeight;
                $data->frontShockAbsorberType =  $report->data->vehicleSpecification->frontShockAbsorberType;
                $data->frontStabilizerType =  $report->data->vehicleSpecification->frontStabilizerType;
                $data->frontTiresSize =  $report->data->vehicleSpecification->frontTiresSize;
                $data->frontTread =  $report->data->vehicleSpecification->frontTread;
                $data->fuelConsumption =  $report->data->vehicleSpecification->fuelConsumption;
                $data->fuelTankEquipment =  $report->data->vehicleSpecification->fuelTankEquipment;
                $data->gradeData =  $report->data->vehicleSpecification->gradeData;
                $data->height =  $report->data->vehicleSpecification->height;
                $data->length =  $report->data->vehicleSpecification->length;
                $data->mainBrakesType =  $report->data->vehicleSpecification->mainBrakesType;
                $data->dataMake =  $report->data->vehicleSpecification->dataMake;
                $data->maximumSpeed =  $report->data->vehicleSpecification->maximumSpeed;
                $data->minimumGroundClearance =  $report->data->vehicleSpecification->minimumGroundClearance;
                $data->minimumTurningRadius =  $report->data->vehicleSpecification->minimumTurningRadius;
                $data->modelData =  $report->data->vehicleSpecification->modelData;
                $data->modelCode =  $report->data->vehicleSpecification->modelCode;
                $data->mufflersNumber =  $report->data->vehicleSpecification->mufflersNumber;
                $data->rearShaftWeight =  $report->data->vehicleSpecification->rearShaftWeight;
                $data->rearShockAbsorberType =  $report->data->vehicleSpecification->rearShockAbsorberType;
                $data->rearStabilizerType =  $report->data->vehicleSpecification->rearStabilizerType;
                $data->rearTiresSize =  $report->data->vehicleSpecification->rearTiresSize;
                $data->rearTread =  $report->data->vehicleSpecification->rearTread;
                $data->reverseRatio =  $report->data->vehicleSpecification->reverseRatio;
                $data->ridingCapacity =  $report->data->vehicleSpecification->ridingCapacity;
                $data->sideBrakesType =  $report->data->vehicleSpecification->sideBrakesType;
                $data->specificationCode =  $report->data->vehicleSpecification->specificationCode;
                $data->stoppingDistance =  $report->data->vehicleSpecification->stoppingDistance;
                $data->transmissionType =  $report->data->vehicleSpecification->transmissionType;
                $data->weight =  $report->data->vehicleSpecification->weight;
                $data->wheelAlignment =  $report->data->vehicleSpecification->wheelAlignment;

                $data->wheelbase =  $report->data->vehicleSpecification->wheelbase;

                $data->width =  $report->data->vehicleSpecification->width;
                $data->save();


                $data             =  new RSummary;
                
                $data->id_vehicle       = $id;
                $data->report_id    = $reportId;
                $data->registered    = $report->data->summary->registered;
                $data->accident =  $report->data->summary->accident;
                $data->odometer =  $report->data->summary->odometer;
                $data->recall    = $report->data->summary->recall;
                $data->safetyGrade =  $report->data->summary->safetyGrade;
                $data->averagePrice =  $report->data->summary->averagePrice;
                $data->buyback    = $report->data->summary->buyback;
                $data->contaminationRisk =  $report->data->summary->contaminationRisk;
                $data->fillingTime =  $report->data->summary->fillingTime;
                $data->save();


                VehicleChecking::where('id_vehicle',$id)->update(array(
                    'status' => $status_chassis
                ));
                } /* End of is_ready == 5 */


            }

            return redirect()->back()->with(['success' => 'Data saved successfuly']);

        } catch (\Throwable $e) {

            dd($e);

            return redirect()->back()->with(['warning' => 'Connecting problem with partner overseas']); 
        }

    }



}
