<?php

namespace App\Http\Controllers\Verify\Sync;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RAccidentHistory;
use App\Model\RRecallHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use Redirect;

class FullSyncController extends Controller
{
    
    public function __construct()

    {
        $this->middleware('auth');

        $this->needSignature = config('carvx_fullreport.needSignature');
        $this->raiseExceptions = config('carvx_fullreport.raiseExceptions');
        $this->isTest = config('carvx_fullreport.isTest');

        
        $this->url = config('carvx_fullreport.url');
        $this->userUid = config('carvx_fullreport.userUid');
        $this->apiKey = config('carvx_fullreport.apiKey');
        $this->limit_submit_to_partner_everyday = config('autocheck.limit_submit_to_partner_everyday');
        $this->date = date("Y-m-d");
        $this->format =  "AC-".date('Ymd-Hi');
        $this->hash  = str_random(20);

        $this->endpoint = config('currency.endpoint');  
        $this->access_key =  config('currency.access_key');
     
    }



    public function index()
    {
       

        $data3 = VehicleChecking::where('status', '27')
                  ->where('real_type_report_id', 'Full')
                  ->get();


        $data = VehicleChecking::where('status', '27')
                  ->where('real_type_report_id', 'Full')
                  ->get();

        $data2 = VehicleChecking::where('status', '27')
                  ->where('real_type_report_id', 'Full')
                  ->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        
        return view('verifier.sync.full', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }



    public function store(Request $request)
    {        

       try {
      
            $me =  Auth::user()->id;

            $sync = array_map(null, $request->id_vehicle, $request->id, $request->ids);


            foreach ($sync as $val) {

                $id_vehicle = $val[0];

                $getreport = ReportVehicle::where('id_vehicle', $val[0])->first();      
               
                $reportId = $getreport->report_id;





                $client = new Client();
                $body = $client->request('GET', 'https://carvx.jp/api/v1/get-report', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Carvx-User-Uid' => $this->userUid, 
                    'Carvx-Api-Key' => $this->apiKey,
                    'raiseExceptions' => $this->raiseExceptions,
                    'isTest' => $this->isTest,
                    'needSignature'=> $this->needSignature
                    
                ],
                    'query' => [
                        'report_id' => $reportId
                        
                    ]
                ])->getBody()->getContents();

                $contents = (string) $body;
                $result = json_decode($contents, true);

                $count_result = count($result);

          

                $is_ready       = $result["data"]["status"];
                $creation_date  = $result["data"]["creation_date"];
                $due_date       = $result["data"]["due_date"];


                $data = ReportVehicle::where('id_vehicle', $val[0])->update(array(
                        'is_ready' => $is_ready,
                        
                        'creation_date' => $creation_date,
                        'due_date' => $due_date
                ));



                if($is_ready == '6') //status reject carvx
                    {
                        $status_chassis = '30';

                        $data                       =  new HistorySearchVehicle;
                        
                        $data->id_vehicle           = $val[0];
                        $data->parameter_history_id = "NOT FOUND";
                        $data->user_id              =  $me;
                        
                        $data->save();
                    }
                    elseif($is_ready == '5'){ //complete
                        $status_chassis = '40';

                        $data                       =  new HistorySearchVehicle;
                        
                        $data->id_vehicle           = $val[0];
                        $data->parameter_history_id = "COMPLETE";
                        $data->user_id              =  $me;
                        
                        $data->save();

                    }
                    elseif($is_ready == '3'){ //new
                        $status_chassis = '20';
                    }
                    elseif($is_ready == '4'){ //inprogress
                        $status_chassis = '25';
                    }
                    else{
                        $status_chassis = $is_ready;
                    }



                    $data = VehicleChecking::where('id_vehicle',$val[0])->update(array(
                        'status' => $status_chassis
                    ));

                    
                    /* --- status == 5 update data --- */
                    if($is_ready == "5"){
                    /* Update API */
                    $api = VehicleApi::where('id_vehicle',$val[0])->update(array(
                        'vehicle' => $result["data"]["data"]["chassis_number"],
                        'country_origin' => "",
                        'brand' => $result["data"]["data"]["make"],
                        'model' => $result["data"]["data"]["model"],
                        'engine_number' => $result["data"]["data"]["engine"],
                        'cc' => $result["data"]["data"]["displacement"],
                        'fuel_type' => $result["data"]["data"]["fuel"],
                        'year_manufacture' => $result["data"]["data"]["manufacture_date"],
                        'registation_date' => $result["data"]["data"]["registration_date"]
                    )); /*End update API */

                    $today  = date('HisdmY');
                    $ver_sn = 'F'.$today;

                    $datas = new VehicleApiKastam;
                    $datas->vehicle_id = $val[0];
                    $datas->api_from = 'carvx';
                    $datas->is_test = '1';
                    $datas->car_id = '';
                    $datas->search_id = '';
                    $datas->status = 'Complete';
                    $datas->request_by = '';
                    $datas->update_by = $me;
                    $datas->ver_sn = $ver_sn;
                    $datas->vehicle = $result["data"]["data"]["chassis_number"];
                    $datas->make = $result["data"]["data"]["make"];
                    $datas->model = $result["data"]["data"]["model"];
                    $datas->engine = $result["data"]["data"]["engine"];
                    $datas->body = $result["data"]["data"]["body"];
                    $datas->grade = $result["data"]["data"]["grade"];
                    $datas->manufactureDate = $result["data"]["data"]["manufacture_date"];
                    $datas->date_of_origin = $result["data"]["data"]["registration_date"];
                    $datas->drive = $result["data"]["data"]["drive"];
                    $datas->transmission = $result["data"]["data"]["transmission"];
                    $datas->displacement_cc = $result["data"]["data"]["displacement"];
                    $datas->average_market = $result["data"]["data"]["average_price"];
                    $datas->save();  


                    $photos = $result["data"]["data"]["photos"];
                    $photo_implode = implode(", ", $photos);
                    
                    $photos_encode = json_encode($photos);
                    
                    $count_photos = count($photos);
               
                    for($i=0; $i<$count_photos; $i++){

                        $image64 = file_get_contents($photo_implode);

                        $datas = new VehicleApiKastamImage;
                        $datas->vehicle_id = $val[0];
                        $datas->image = $photo_implode;
                        $datas->base64 = 'data:image/jpg;base64,'.base64_encode($image64);
                        $datas->save();  

                    }




                $endpoint = $this->endpoint;
                $access_key = $this->access_key;

                // Initialize CURL:
                $ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Store the data:
                $json = curl_exec($ch);
                curl_close($ch);

                // Decode JSON response:
                $exchangeRates = json_decode($json, true);

                $count_country= count($exchangeRates);
        

                $price = $result["data"]["data"]["average_price"];

                $currency_usdyen = $exchangeRates["quotes"]["USDJPY"];
                $currency_usdmyr = $exchangeRates["quotes"]["USDMYR"];
                //$yen = ;

                $priceusd = $price / $currency_usdyen;
                $pricemyr = $priceusd * $currency_usdmyr;


                $rate =  $pricemyr / $price;
                $rete6 = round($rate, 6);


                $today  = date('d-m-Y');

                $api = VehicleApiKastam::where('vehicle_id',$val[0])->update(array(
                        'currency' => 'japan',
                        'rm_convert' => $pricemyr,
                        'rate' => $rete6,
                        'date_convert' => $today
                        
                    )); 

                }
            }


           

            return redirect()->back()->with(['success' => 'Data updated successfuly and Please Checklist Need Signature in carvx System']); 


        } catch (\Throwable $e) {

            dd($e);

            return redirect()->back()->with(['warning' => 'Please UnCheck Need Signature in carvx System']); 
        }



    }





}
