<?php

namespace App\Http\Controllers\Verify\Sync;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RAccidentHistory;
use App\Model\RRecallHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\VehicleVerifiedHalf;
use Redirect;



class HalfSyncController extends Controller
{
    
   
    public function __construct()

    {
        $this->middleware('auth');
        $this->needSignature = config('carvx_halfreport.needSignature');
        $this->raiseExceptions = config('carvx_halfreport.raiseExceptions');
        $this->isTest = config('carvx_halfreport.isTest');

        $this->url           = config('carvx_halfreport.url');
        $this->userUid       = config('carvx_halfreport.userUid');
        $this->apiKey        = config('carvx_halfreport.apiKey');   
    }

    public function index()
    {
        
        
        $data = VehicleChecking::where('status', '27')
                ->where('real_type_report_id', 'Half')
                ->get();

        $data2 = VehicleChecking::where('status', '27')
                ->where('real_type_report_id', 'Half')
                ->get();

        $data3 = VehicleChecking::where('status', '27')
                ->where('real_type_report_id', 'Half')
                ->get();

        

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.sync.half', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        
    }


    public function storexx() //short_report_all
    {
        dd('a');

        $me    = Auth::user()->id;

        $sync = VehicleChecking::whereIn('status',['20', '25'])->where('group_by','NA')->where('searching_by','API')->get();

        /* Foreach */
        foreach ($sync as $syncna) {

            $getreport = ReportVehicle::where('id_vehicle', $syncna->id_vehicle)->first();
            $reportId = $getreport->report_id;

            $get_searchid = VehicleApi::where('id_vehicle', $syncna->id_vehicle)->first();
            $searchid = $get_searchid->search_id;
            $carid = $get_searchid->car_id;


            $options=array(

                'raiseExceptions' => $this->raiseExceptions,
                'isTest' => $this->isTest,
                'needSignature'=> $this->needSignature
            );

            $url           = $this->url;
            $userUid       = $this->userUid;
            $apiKey        = $this->apiKey;

            //$sha256 = 'car_id1is_test1search_id'.$searchid.'wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';

            $sha256 = 'car_id0is_test1search_idHayhd!r8MTj!wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';

            $hashvalue=hash('sha256', $sha256);


            $client = new Client();
            $body = $client->request('GET', 'https://carvx.jp/api/v1/get-report', [
            'headers' => [
                'Accept'     => 'application/json',
                'Carvx-User-Uid' => $this->userUid, 
                'Carvx-Api-Key' => $this->apiKey,
                'raiseExceptions' => $this->raiseExceptions,
                'isTest' => $this->isTest,
                'needSignature'=> $this->needSignature
                
            ],
                'query' => [
                    'report_id' => $reportId
                    
                ]
            ])->getBody()->getContents();

            $contents = (string) $body;
            $result = json_decode($contents, true);

            $count_result = count($result);

            //dd($result);
            /*$service = new CarvxService($url, $userUid, $hashvalue, $options);
           
            $result = $service->getReport($reportId, true);*/


            $is_ready       = $result["data"]["status"];
            $creation_date  = $result["data"]["creation_date"];
            $due_date       = $result["data"]["due_date"];
            //$data           = $result["data"]["status"];
            

            $data = ReportVehicle::where('id_vehicle', $syncna->id_vehicle)->update(array(
                'is_ready' => $is_ready,
                'creation_date' => $creation_date,
                'due_date' => $due_date
            ));


            if($is_ready == '6') //status reject carvx
            {
                $status_chassis = '30';

                $data                       =  new HistorySearchVehicle;
                
                $data->id_vehicle           = $syncna->id_vehicle;
                $data->parameter_history_id = "REJECT";
                $data->user_id              =  $me;
                
                $data->save();
            }
            elseif($is_ready == '5'){ //complete
                $status_chassis = '40';

                $data                       =  new HistorySearchVehicle;
                
                $data->id_vehicle           = $syncna->id_vehicle;
                $data->parameter_history_id = "COMPLETE";
                $data->user_id              =  $me;
                
                $data->save();

            }
            elseif($is_ready == '3'){ //new
                $status_chassis = '20';
            }
            elseif($is_ready == '4'){ //inprogress
                $status_chassis = '25';
            }
            else{
                $status_chassis = $is_ready;
            }


            $data = VehicleChecking::where('id_vehicle',$syncna->id_vehicle)->update(array(
                'status' => $status_chassis
            ));

            
            /* --- status == 5 update data --- */
            if($is_ready == "5"){
            /* Update API */
            $today  = date('HisdmY');
            $ver_sn = 'H'.$today;


            /*========= Change Gasoline to Petrol =========*/
            if($result["data"]["data"]["fuel"] == "GASOLINE"){
                $fuel_type = "Petrol";
            }else{
                $fuel_type = $result["data"]["data"]["fuel"];
            }

            /*=========  =========*/

            $api = VehicleVerifiedHalf::where('id_vehicle',$syncna->id_vehicle)->update(array(
                'vehicle' => $result["data"]["data"]["chassis_number"],
                'country_origin' => "",
                'partner' => "carvx",
                'ver_sn' => $ver_sn,
                'brand' => $result["data"]["data"]["make"],
                'model' => $result["data"]["data"]["model"],
                'engine_number' => $result["data"]["data"]["engine"],
                'cc' => $result["data"]["data"]["displacement"],
                'fuel_type' => $fuel_type,
                'year_manufacture' => $result["data"]["data"]["manufacture_date"],
                'registation_date' => $result["data"]["data"]["registration_date"]
            ));

            
            /*End update API */



            /*check match */
            $vehicle = $result["data"]["data"]["chassis_number"];
            $brand = $result["data"]["data"]["make"];
            $model = $result["data"]["data"]["model"];
            $engine_number = $result["data"]["data"]["engine"];
            $cc = $result["data"]["data"]["displacement"];
            $fuel_type = $result["data"]["data"]["fuel"];
            $year_manufacture = $result["data"]["data"]["manufacture_date"];
            $registation_date = $result["data"]["data"]["registration_date"];

            $get_vehicle = VehicleChecking::where('id_vehicle',$syncna->id_vehicle)->first();

            $brand_kadealer = $get_vehicle->brand_id;
            $model_kadealer = $get_vehicle->model_id;
            $cc_kadealer = $get_vehicle->cc;
            $engine_number_kadealer = $get_vehicle->engine_number;
            $registered_kadealer = $get_vehicle->vehicle_registered_date;

            $brand_data = Brand::where('id', $brand_kadealer)->first();
            $model_data = ParameterModel::where('id', $model_kadealer)->first();

            $get_brand = $brand_data->brand;
            $get_model = $model_data->model;

            
            if($brand == $get_brand){
                $brand_match = '1';
            }else{
                $brand_match = '0';
            }


            if($model == $get_model){
                $model_match = '1';
            }else{
                $model_match = '0';
            }


            if($cc_kadealer == $cc){
                $cc_match = '1';
            }else{
                $cc_match = '0';
            }

            $registation_date =  date('m-Y ', strtotime($registation_date));
            $registered_kadealer =  date('m-Y ', strtotime($registered_kadealer));

            if($registered_kadealer == $registation_date){
                $register_match = '1';
            }else{
                $register_match = '0';
            }

            //check all match 
            if($brand_match == '1' AND $model_match == '1' AND $cc_match == '1' AND $register_match == '1'){
                $status_match = '1';
            }else{
                $status_match = '0';
            }


            $update_status_match = VehicleStatusMatch::where('id_vehicle',$syncna->id_vehicle)->update(array(
                'brand' => $brand_match,
                'model' => $model_match,
                'cc' => $cc_match,
                //'fuel_type' => $status_chassis ->bynaza
                'registation_date' => $register_match,
                'status' => $status_match
            ));
            /* end check match */


            
            } /* End of status == 5 */




        }
        /* End Foreach */

        return redirect()->back()->with(['success' => 'Data updated successfuly and Please Checklist Need Signature in carvx System']);


    }



    public function store(Request $request) //short_report_all
    {


        try {

            $me    = Auth::user()->id;

            $sync = array_map(null, $request->id_vehicle, $request->id, $request->vehicle);

   


            /* Foreach */
            foreach ($sync as $val) {


                $id_vehicle = $val[0];

                $getreport = ReportVehicle::where('id_vehicle', $val[0])->latest()->first();
                $reportId = $getreport->report_id;

                $get_searchid = VehicleApi::where('id_vehicle', $val[0])->first();


                $searchid = $get_searchid->search_id;
                $carid = $get_searchid->car_id;

                /*============= Masih Problem di Data ===============*/


                /*$options=array(

                    'raiseExceptions' => $this->raiseExceptions,
                    'isTest' => $this->isTest,
                    'needSignature'=> $this->needSignature
                );

                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;*/
    

                //$service = new CarvxService($url, $userUid, $apiKey, $options);


                //$result = $service->getReport($reportId, true);

                //dd($report);

                /*============= End Problem di Data ===============*/

              

                $sha256 = 'car_id0is_test1search_idHayhd!r8MTj!wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';

                $hashvalue=hash('sha256', $sha256);
             

                $client = new Client();
                $body = $client->request('GET', 'https://carvx.jp/api/v1/get-report', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Carvx-User-Uid' => $this->userUid, 
                    'Carvx-Api-Key' => $this->apiKey
                    //'raiseExceptions' => $this->raiseExceptions,
                    //'isTest' => $this->isTest,
                    //'needSignature'=> '0'
                    
                ],
                    'query' => [
                        'report_id' => $reportId
                        
                    ]
                ])->getBody()->getContents();


                $contents = (string) $body;
                $result = json_decode($contents, true);

                $count_result = count($result);

                    
                $is_ready       = $result["data"]["status"];
                $creation_date  = $result["data"]["creation_date"];
                $due_date       = $result["data"]["due_date"];
                //$data           = $result["data"]["status"];
                

                $data = ReportVehicle::where('id_vehicle', $val[0])->update(array(
                    'is_ready' => $is_ready,
                    'creation_date' => $creation_date,
                    'due_date' => $due_date
                ));


                if($is_ready == '6') //status reject carvx
                {
                    $status_chassis = '30';

                    $data                       =  new HistorySearchVehicle;
                    
                    $data->id_vehicle           = $val[0];
                    $data->parameter_history_id = "NOT FOUND";
                    $data->user_id              =  $me;
                    
                    $data->save();
                }
                elseif($is_ready == '5'){ //complete
                    $status_chassis = '40';

                    $data                       =  new HistorySearchVehicle;
                    
                    $data->id_vehicle           = $val[0];
                    $data->parameter_history_id = "COMPLETE";
                    $data->user_id              =  $me;                    
                    $data->save();

                }
                elseif($is_ready == '3'){ //new
                    $status_chassis = '20';
                }
                elseif($is_ready == '4'){ //inprogress
                    $status_chassis = '25';
                }
                else{
                    $status_chassis = $is_ready;
                }


                $data = VehicleChecking::where('id_vehicle',$val[0])->update(array(
                    'status' => $status_chassis,
                    'is_sync' => '1'
                ));

                
                /* --- status == 5 update data --- */
                if($is_ready == "5"){
                /* Update API */

                $today  = date('HisdmY');
                $ver_sn = 'H'.$today;

                $data                          =  new VehicleVerifiedHalf;       
                $data->id_vehicle              = $val[0];
                $data->vehicle                 = $result["data"]["data"]["chassis_number"];
                $data->country_origin          = Null;
                $data->partner                 = "carvx";
                $data->ver_sn                  = $ver_sn;
                $data->brand                = $result["data"]["data"]["make"];
                $data->model                = $result["data"]["data"]["model"];
                $data->engine_number        = $result["data"]["data"]["engine"];
                $data->cc           = $result["data"]["data"]["displacement"];
                $data->fuel_type    = $result["data"]["data"]["fuel"];
                $data->year_manufacture        = $result["data"]["data"]["manufacture_date"];       
                $data->registation_date        =  $result["data"]["data"]["registration_date"];
                $data->save();
                /*End update API */


                /*check match */

                $vehicle = $result["data"]["data"]["chassis_number"];
                $brand = $result["data"]["data"]["make"];
                $model = $result["data"]["data"]["model"];
                $engine_number = $result["data"]["data"]["engine"];
                $cc = $result["data"]["data"]["displacement"];
                $fuel_type = $result["data"]["data"]["fuel"];
                $year_manufacture = $result["data"]["data"]["manufacture_date"];
                $registation_date = $result["data"]["data"]["registration_date"];

                $get_vehicle = VehicleChecking::where('id_vehicle',$val[0])->first();

                $brand_kadealer = $get_vehicle->brand_id;
                $model_kadealer = $get_vehicle->model_id;
                $cc_kadealer = $get_vehicle->cc;
                $engine_number_kadealer = $get_vehicle->engine_number;
                $registered_kadealer = $get_vehicle->vehicle_registered_date;

                $brand_data = Brand::where('id', $brand_kadealer)->first();
                $model_data = ParameterModel::where('id', $model_kadealer)->first();

                $get_brand = $brand_data->brand;
                $get_model = $model_data->model;

                
                if($brand == $get_brand){
                    $brand_match = '1';
                }else{
                    $brand_match = '0';
                }


                if($model == $get_model){
                    $model_match = '1';
                }else{
                    $model_match = '0';
                }


                if($cc_kadealer == $cc){
                    $cc_match = '1';
                }else{
                    $cc_match = '0';
                }

                $registation_date =  date('m-Y ', strtotime($registation_date));
                $registered_kadealer =  date('m-Y ', strtotime($registered_kadealer));

                if($registered_kadealer == $registation_date){
                    $register_match = '1';
                }else{
                    $register_match = '0';
                }

                //check all match 
                if($brand_match == '1' AND $model_match == '1' AND $cc_match == '1' AND $register_match == '1'){
                    $status_match = '1';
                }else{
                    $status_match = '0';
                }


                $update_status_match = VehicleStatusMatch::where('id_vehicle',$val[0])->update(array(
                    'brand' => $brand_match,
                    'model' => $model_match,
                    'cc' => $cc_match,
                    //'fuel_type' => $status_chassis ->bynaza
                    'registation_date' => $register_match,
                    'status' => $status_match
                ));
                /* end check match */

                } /* End of status == 5 */

            }
            /* End Foreach */

            
            return Redirect::back()->with(['success' => 'Data updated successfuly and Please Check Need Signature in carvx System']);


        } catch (\Throwable $e) {

            return redirect()->back()->with(['warning' => 'Please UnCheck Need Signature in carvx System']);

        }

    }





}
