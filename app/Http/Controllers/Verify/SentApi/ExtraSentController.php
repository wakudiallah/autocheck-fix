<?php

namespace App\Http\Controllers\Verify\SentApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterEmail;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\Model\BatchSent;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use Excel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class ExtraSentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->needSignature = config('carvx_extrareport.needSignature');
        $this->raiseExceptions = config('carvx_extrareport.raiseExceptions');
        $this->isTest = config('carvx_extrareport.isTest');


        $this->url = config('carvx_extrareport.url');
        $this->userUid = config('carvx_extrareport.userUid');
        $this->apiKey = config('carvx_extrareport.apiKey');
        $this->limit_submit_to_partner_everyday = config('autocheck.limit_submit_to_partner_everyday');
        $this->date = date("Y-m-d");
        $this->format =  "AC-".date('Ymd-Hi');
        $this->hash  = str_random(20);
        $this->is_test = config('autocheck.is_test_am4_report_kastam');
        $this->limit_submit_to_partner_everyday = config('autocheck.limit_submit_to_partner_everyday');
    }


    public function index()
    {
        $data = VehicleChecking::where('status', '20')
                ->where('searching_by', 'API')
                ->where('real_type_report_id', 'Extra')
                ->orderBy('id', 'ASC')
                ->WhereNull('limit_submit')
                ->get();


        $count_today_submit = VehicleChecking::where('date_sent', $this->date)
                    ->where('limit_submit', '1')
                    ->where('real_type_report_id', 'Extra')
                    ->count();


        return view('verifier.sent.api.extra', compact('data', 'count_today_submit'));
    }


    public function store(Request $request)
    {

        $is_test = config('autocheck.is_test_am2_full_report');

        
        $count_today_submit = VehicleChecking::where('date_sent', $this->date)
                            ->where('limit_submit', '1')
                            ->where('real_type_report_id', 'Extra')
                            ->count();


        if($count_today_submit > $this->limit_submit_to_partner_everyday){

            $remains = $this->limit_submit_to_partner_everyday - $count_today_submit; 

            return Redirect::back()->with(['warning' => 'Chassis has reached the limit ('.$this->limit_submit_to_partner_everyday.'), Please send only'. $remains. 'Vehicle']);

        }else{

            $item = array_map(null, $request->id, $request->vehicle, $request->ci, $request->ids);


            foreach($item as $val) {
                
                $pra = VehicleChecking::where('id_vehicle',$val[0])->update([                                    
                    "date_sent"             => $this->date,
                    "unique_sent"           => $this->format,
                    "hash"                  => $this->hash

                ]);

            }  /* end foreach looping*/

        }

        $this->sent_carvx();


        return Redirect::back()->with(['success' => 'Chassis Successfully Sent to Partner']);

    }



    public function sent_carvx()
    {


        $chassisToPartner = VehicleChecking::where('hash', $this->hash)
                            ->where('unique_sent', $this->format)
                            ->get();

        foreach($chassisToPartner as $chassisToPartner) {

            $vin = $chassisToPartner->vehicle;
            
            $options=array(

                'needSignature' => $this->needSignature, 
                'raiseExceptions' => $this->raiseExceptions,
                'isTest' => $this->isTest
            );

                
                $url           = $this->url;
                $userUid       = $this->userUid;
                $apiKey        = $this->apiKey;
                              

                $service = new CarvxService($url, $userUid, $apiKey, $options);

                
                if(!empty($search->cars[0]->chassisNumber)){  //found in partner

                    $chassisNumber = $search->cars[0]->chassisNumber;
                                      

                }else{  //check once again with W

                    $vinx = substr_replace( $vin, "W", 5, 0);
                    
                    
                    if(!empty($search->cars[0]->chassisNumber)){

                        $search = $service->createSearch($vinx);
                        $chassisNumber = $search->cars[0]->chassisNumber;
                       
                    }else{

                        $chassisNumber = $vin;

                    }

                }


                $search = $service->createSearch($chassisNumber);

                /*dd($search);*/
                $carId      = $search->cars[0]->carId;
                $searchId   = $search->uid;
                $isTest     = $this->isTest;

                //Real Sent
                $reportId   = $service->createReport($searchId, $carId, $isTest);

                //Dummy
                //$reportId = "dummy";

                //autocheckmalaysia
                $data                   =  new ReportVehicle;
                $data->id_vehicle       = $chassisToPartner->id_vehicle;
                $data->report_id        = $reportId;
                $data->save();


                $pra = VehicleChecking::where('id_vehicle',$chassisToPartner->id_vehicle)->update([
                        
                        "limit_submit"         => '1',
                        "is_sent"              => '1', 
                        "is_check_id"          => '1',
                        "status"                => '27'


                    ]);

        }

    }


}
