<?php

namespace App\Http\Controllers\Verify\SentApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterEmail;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\Model\BatchSent;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use Excel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;


class FullSentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->url = config('carvx_fullreport.url');
        $this->userUid = config('carvx_fullreport.userUid');
        $this->apiKey = config('carvx_fullreport.apiKey');
        $this->limit_submit_to_partner_everyday = config('autocheck.limit_submit_to_partner_everyday');
        $this->date = date("Y-m-d");
        $this->format =  "AC-".date('Ymd-Hi');
        $this->hash  = str_random(20);
        $this->is_test = config('autocheck.is_test_am4_report_kastam');
  
    }

    public function index()
    {
        
        $data = VehicleChecking::where('status', '20')
                ->where('searching_by', 'API')
                ->where('real_type_report_id', 'Full')
                ->orderBy('id', 'ASC')
                //->WhereNull('limit_submit')
                ->get();


        $count_today_submit = VehicleChecking::where('date_sent', $this->date)
                    ->where('limit_submit', '1')
                    ->where('real_type_report_id', 'Full')
                    ->count();


        return view('verifier.sent.api.full', compact('data', 'count_today_submit'));
    }


    public function sent(Request $request)
    {


        $item = array_map(null, $request->id, $request->vehicle, $request->ci, $request->ids);


        $is_test = config('autocheck.is_test_am4_report_kastam');


        $today = date("Y-m-d");

        $count_today_submit = VehicleChecking::where('date_sent', $today)
                            ->where('limit_submit', '1')
                            ->where('real_type_report_id', 'Full')
                            ->count();


        if($count_today_submit < $this->limit_submit_to_partner_everyday){

                foreach($item as $val) {

                    $pra = VehicleChecking::where('id_vehicle',$val[0])->update([
                        
                        
                        "date_sent"             => $this->date,
                        "unique_sent"           => $this->format,
                        "hash"                  => $this->hash

                    ]);
                  
                }  /* end foreach looping*/


               $this->sent_carvx();

               return Redirect::back()->with(['success' => 'Chassis Successfully Sent to Partner']);

        }else{

            return Redirect::back()->with(['warning' => 'Chassis has reached the limit ('.$this->limit_submit_to_partner_everyday.')']);
        }
       
        
    }


    public function sent_carvx()
    {

        $data = VehicleChecking::where('hash', $this->hash)
                                ->where('unique_sent', $this->format)
                                ->get();


        foreach($data as $data){

                    $vin = $data->vehicle;
                   
                    $options=array(

                        'needSignature' => '0', 
                        'raiseExceptions' => '1',
                        'isTest' => $this->is_test
                    );

                        $url           = $this->url;
                        $userUid       = $this->userUid;
                        $apiKey        = $this->apiKey;

 
                    try {

                        $service = new CarvxService($url, $userUid, $apiKey, $options);

			$search = $service->createSearch($vin);

                        if(!empty($search->cars[0]->chassisNumber)){  //found in partner

                            $chassisNumber = $search->cars[0]->chassisNumber;
                                              

                        }else{  //check once again with W

                            $vinx = substr_replace( $data->vehicle, "W", 5, 0);
                            
                            
                            if(!empty($search->cars[0]->chassisNumber)){

                                $search = $service->createSearch($vinx);
                                $chassisNumber = $search->cars[0]->chassisNumber;
                               
                            }else{

                                $chassisNumber = $data->vehicle;

                            }

                        }

			$search = $service->createSearch($chassisNumber);

                        $carId      = $search->cars[0]->carId;
                        $searchId   = $search->uid;
                        $isTest     = $this->is_test;

                        
                        //if($this->isTest == "1"){

                            //$reportId = "dummyfull";  //dummy

                        //}else{

                            $reportId   = $service->createReport($searchId, $carId, $isTest);

                        //}


                        $pra = VehicleChecking::where('id_vehicle',$data->id_vehicle)->update([
                        
                        "limit_submit"         => '1',
                        "is_sent"              => '1', 
                        "is_check_id"          => '1',
                        "status"                => '27'

                        ]);
                        

                        $report                   =  new ReportVehicle;
                        $report->id_vehicle       = $data->id_vehicle;
                        $report->report_id        = $reportId;
                        $report->save();


                        
                        $batch          = new BatchSent;
                        $batch->unique_sent =  $this->format;
                        $batch->created_by    = Auth::user()->id;
                        $batch->partner = "carvx";
                        $batch->save();
                       

                    } catch (\Throwable $e) {

                        return redirect()->back()->with(['warning' => 'Please Check Need Signature in carvx System']); 
                    }

           
        }


        
    }



}
