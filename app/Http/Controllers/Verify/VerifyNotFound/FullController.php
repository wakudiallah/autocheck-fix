<?php

namespace App\Http\Controllers\Verify\VerifyNotFound;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\UserGroup;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;
use App\Model\HistoryBalance;


class FullController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])
                ->where('real_type_report_id', 'Full')
                ->where('searching_by', 'NOT')
                ->get();


        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])
                ->where('real_type_report_id', 'Full')
                ->where('searching_by', 'NOT')
                ->get();

        
        return view('verifier.verifyNotFound.full', compact('data', 'data2'));
    }



    public function store(Request $request, $id)
    {

        $me    = Auth::user()->id;
        $get_detail_vehicle =  VehicleChecking::where('id_vehicle', $id)->first();
        $role_customer = $get_detail_vehicle->group_by;
        $id_group_customer = $get_detail_vehicle->company_name;

        $get_fee_data = ParameterFee::where('role_id', $role_customer)->first();
        $fee_data = $get_fee_data->fee;
        
        $get_balance_now = UserGroup::where('branch', $get_detail_vehicle->company_name)->first();

        $balance_now = $get_balance_now->balance;
        $add_refound_balance = $balance_now + $fee_data;

        $data             = new HistoryBalance();
        $data->id_vehicle = $id;;
        $data->balance    = $add_refound_balance;
        $data->debit = $fee_data;
        $data->desc = "Not Found";
        $data->created_by  = $me;
        $data->user_id     = $id_group_customer;
        $data->save();

        VehicleChecking::where('id_vehicle',$id)->update(array('status' => '30'));
        
        $data             = new HistorySearchVehicle();
        $data->id_vehicle = $id;;
        $data->vehicle    = $request->vehicle;
        $data->parameter_history_id = "NOT FOUND";
        $data->user_id    = $me;
        $data->remark     = $request->remark_notfound;
        $data->save();


        $add_refound_balance = UserGroup::where('branch', $id_group_customer)->update(array('balance' => $add_refound_balance));


        return redirect()->back()->with(['success' => 'Data Updated Successfully']);

    }
}
