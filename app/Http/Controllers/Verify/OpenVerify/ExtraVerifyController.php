<?php

namespace App\Http\Controllers\Verify\OpenVerify;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\ParameterCurrency;
use App\Model\StepStatus;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;


class ExtraVerifyController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = VehicleChecking::whereIn('status', ['28'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('real_type_report_id', 'Extra')->get();
        $data2 = VehicleChecking::whereIn('status', ['28'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('real_type_report_id', 'Extra')->get();

        $data3 = VehicleChecking::whereIn('status', ['28'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('real_type_report_id', 'Extra')->get();

        $verify_manual = VehicleChecking::whereIn('status', ['28'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('real_type_report_id', 'Extra')->get();
        

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $brand2    =  Brand::get();
        $model    =  ParameterModel::get();
        $model2    =  ParameterModel::get();

 
        return view('verifier.open_vehicle.extra', compact('verify_manual','data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'brand2', 'model2'));
    }




    public function verify($id)
    {
       

        $submit_id = StepStatus::where('id_vehicle', $id)->count();


        if($submit_id == '0'){

            $data                   =  new StepStatus;
            $data->id_vehicle         = $id;
            $data->save();

        }


        $data = VehicleChecking::where('id_vehicle', $id)->first();
        $param_currency = ParameterCurrency::where('status', '1')->get();
      
        $brand    =  Brand::orderBy('brand', 'ASC')->where('status', '1')->get();
        $model    =  ParameterModel::orderBy('model', 'ASC')->where('status', '1')->get();

        $step_status = StepStatus::where('id_vehicle', $id)->first();

        $data_odometer_reading = ROdometerHistory::where('id_vehicle', $id)->get();
        $data_use_history = RUsageHistory::where('id_vehicle', $id)->get();

        $data_detail_history = RDetailHistory::where('id_vehicle', $id)->get();
        $data_manufacturer_recall = RRecallHistory::where('id_vehicle', $id)->get();
        $data_aution_image = RAuctionImages::where('id_vehicle', $id)->get();

        

        return view('verifier.open_vehicle.step_verify_extra', compact('data', 'param_currency', 'data_odometer_reading', 'brand', 'model', 'step_status', 'data_use_history', 'data_detail_history', 'data_manufacturer_recall', 'data_aution_image', 'id'));

    }


    public function store($id)
    {
        
        $update_step = VehicleChecking::where('id_vehicle', $id)->update(array('status' => "40" ));


        return redirect('ExtraReport/open-verify/index')->with(['success' => 'Data saved successfuly']);


    }


    public function extra_report_step_1(Request $request)
    {

        $brand = $request->make;

        $getBrand = Brand::where('id', $brand)->first();
        $valueBrand = $getBrand->brand;

        $model       = $request->model;
        $ret         = explode('|', $model);
        $modelSpreate = $ret[0];

        $getModel = ParameterModel::where('id', $modelSpreate)->first();
        $valueModel = $getModel->model;


        $vehicleDetail             =  new RVehicleDetails();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->manufactureDate = $request->manufacture_date;
        $vehicleDetail->make = $valueBrand;
        $vehicleDetail->model = $valueModel;
        $vehicleDetail->chassisNumber = $request->chassis_number;
        $vehicleDetail->body = $request->body;
        $vehicleDetail->engine = $request->vehicledetails_enginedrive;
        $vehicleDetail->drive = $request->vehicledetails_enginedrive;
        $vehicleDetail->transmission = $request->vehicledetails_transmission;
        $vehicleDetail->grade = $request->grade;
        $vehicleDetail->save();


        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

            if($check_id_step < "1"){
                $vehicle = new StepStatus();
                $vehicle->id_vehicle = $request->id_vehicle;
                $vehicle->step_1 = "1";
                $vehicle->save();
            }else{

                $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_1' => "1" ));
            }
        

    }

    public function extra_report_step_2_3(Request $request)
    {
        $averageprice = $request->averageprice;


        if($averageprice == "NaN"){
            
            $averageprice = "0";   

        }elseif($averageprice == ""){
            
            $averageprice = "0";   

        }elseif(strpos($averageprice, ',') !== false){

            $averageprice = str_replace( ',', '', $averageprice ); 

        }


        $vehicleDetail             =  new RSummary();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->averagePrice = $averageprice;
        $vehicleDetail->currency = $request->currency;

        $vehicleDetail->registered = $request->vehicle_titleinformation;
        $vehicleDetail->accident = $request->vehicle_accidentrepair;
        $vehicleDetail->odometer = $request->vehicle_odometerrollback;
        $vehicleDetail->recall = $request->vehicle_manufacturerrecall;
        $vehicleDetail->safetyGrade = $request->vehicle_safetygrade;
        $vehicleDetail->contaminationRisk = $request->vehicle_contaminationrisk;
        $vehicleDetail->save();


        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_2 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_2' => "1" ));
        }



    }

    public function extra_report_step_3(Request $request)
    {

        if($request->collision_reported == "1"){
             $vehicleDetail             =  new RAccidentHistory;
             $vehicleDetail->id_vehicle =  $request->id_vehicle;
             $vehicleDetail->field = "collision";
             $vehicleDetail->date_reported  = $request->collision_date_reported;
             $vehicleDetail->data_source = $request->collision_data_source;
             $vehicleDetail->details = $request->collision_detail;
             $vehicleDetail->airbag = $request->collision_air_bag;
             $vehicleDetail->save();
        } 


         if($request->malfunction_reported == "1"){

            $vehicleDetail             =  new RAccidentHistory;
            $vehicleDetail->id_vehicle =  $request->id_vehicle;
            $vehicleDetail->field = "malfunction";
            $vehicleDetail->date_reported  = $request->malfunction_date_reported;
            $vehicleDetail->data_source = $request->malfunction_data_source;
            $vehicleDetail->details = $request->malfunction_detail;
            $vehicleDetail->airbag = $request->malfunction_air_bag;
            $vehicleDetail->save();

         }


         if($request->theft_reported == "1"){

            $vehicleDetail             =  new RAccidentHistory;
            $vehicleDetail->id_vehicle =  $request->id_vehicle;
            $vehicleDetail->field = "theft";
            $vehicleDetail->date_reported  = $request->theft_date_reported;
            $vehicleDetail->data_source = $request->theft_data_source;
            $vehicleDetail->details = $request->theft_detail;
            $vehicleDetail->airbag = $request->theft_air_bag;
            $vehicleDetail->save();
          
         }


         if($request->firedamage_reported == "1"){

            $vehicleDetail             =  new RAccidentHistory;
            $vehicleDetail->id_vehicle =  $request->id_vehicle;
            $vehicleDetail->field = "firedamage";
            $vehicleDetail->date_reported  = $request->firedamage_date_reported;
            $vehicleDetail->data_source = $request->firedamage_data_source;
            $vehicleDetail->details = $request->firedamage_detail;
            $vehicleDetail->airbag = $request->firedamage_air_bag;
            $vehicleDetail->save();
          
         }


         if($request->waterdamage_reported == "1"){

            $vehicleDetail             =  new RAccidentHistory;
            $vehicleDetail->id_vehicle =  $request->id_vehicle;
            $vehicleDetail->field = "waterdamage";
            $vehicleDetail->date_reported  = $request->waterdamage_date_reported;
            $vehicleDetail->data_source = $request->waterdamage_data_source;
            $vehicleDetail->details = $request->waterdamage_detail;
            $vehicleDetail->airbag = $request->waterdamage_air_bag;
            $vehicleDetail->save();
          
         }


          if($request->haildamage_reported == "1"){

            $vehicleDetail             =  new RAccidentHistory;
            $vehicleDetail->id_vehicle =  $request->id_vehicle;
            $vehicleDetail->field = "haildamage";
            $vehicleDetail->date_reported  = $request->haildamage_date_reported;
            $vehicleDetail->data_source = $request->haildamage_data_source;
            $vehicleDetail->details = $request->haildamage_detail;
            $vehicleDetail->airbag = $request->haildamage_air_bag;
            $vehicleDetail->save();
          
         }

        
        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_3 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_3' => "1" ));
        }


    }

    public function extra_report_step_4(Request $request){


        $vehicleDetail             =  new ROdometerHistory();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->date = $request->odometer_datereported;
        $vehicleDetail->source = $request->odometer_datasource;
        $vehicleDetail->mileage = $request->odometer_odometerreading;
        $vehicleDetail->save();

        $data_odometerhistory = ROdometerHistory::select('date','source', 'mileage')->where('id_vehicle', $request->id_vehicle)->get(); 


    }


    public function get_extra_report_step_4(Request $request){
        
        $data = ROdometerHistory::select('date','source', 'mileage')->where('id_vehicle', 'e8c53019-598f-4731-9c9f-f3b97f9b2a97')->get();


        return response()->json($data);
    }


    public function extra_report_step_5(Request $request){

        
        $vehicleDetail             =  new RUsageHistory();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        
        $vehicleDetail->contaminatedRegion = $request->use_contaminatedregions;
        $vehicleDetail->contaminationTest = $request->odometer_radioactivecontamination;
        $vehicleDetail->commercialUsage = $request->odometer_commercialuse;
        $vehicleDetail->save(); 


    }


    public function extra_report_step_6(Request $request){

        $vehicleDetail             =  new RDetailHistory();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->date = $request->detailed_eventdate;
        $vehicleDetail->location = $request->detailed_location;
        $vehicleDetail->source = 
        $request->detailed_datasource;
        $vehicleDetail->mileage  = $request->detailed_odometerreading;
        $vehicleDetail->action   = $request->detailed_details;
        $vehicleDetail->save();


    }


    public function extra_report_step_7(Request $request)
    {

        $vehicleDetail             =  new RRecallHistory();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->date = $request->manufacture_datereported;
        $vehicleDetail->source = 
        $request->manufacture_datasource;
        $vehicleDetail->affected_part  = $request->manufacture_affectedpart;
        $vehicleDetail->details   = $request->manufacture_details;
        $vehicleDetail->save();

        /*$vehicleDetail             =  new RSummary();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->averagePrice = $request->averageprice;
        $vehicleDetail->currency = $request->currency;
        $vehicleDetail->save();*/


    }


    public function extra_report_step_8(Request $request)
    {
      
        $vehicleDetail             =  new RVehicleAssessment();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->driverSeatPoints = $request->driverseat_point;
        $vehicleDetail->driverSeatGoalAverage = $request->driverseat_goalaverage;
        $vehicleDetail->frontPassengerSeatPoints = $request->frontpassenger_point;
        $vehicleDetail->frontPassengerSeatGoalAverage = $request->frontpassenger_goalaverage;
        $vehicleDetail->dryRoadStoppingDistance  = $request->dry_road;
        $vehicleDetail->wetRoadStoppingDistance = $request->wet_road;
        $vehicleDetail->save();


        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_8 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_8' => "1" ));
        }

    }

    public function extra_report_step_9a(Request $request)
    {
        $vehicleDetail             =  new RVehicleSpecification();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        
        $vehicleDetail->save();
    }


    public function extra_report_step_9(Request $request)
    {

        
        $vehicleDetail  =  new RVehicleSpecification();
        $vehicleDetail->id_vehicle = $request->id_vehicle;
        $vehicleDetail->firstGearRatio = $request->vehicle_firstgear;  
        $vehicleDetail->thirdGearRatio= $request->vehicle_thirdgear;
        $vehicleDetail->fifthGearRatio= $request->vehicle_fifthgear;
        $vehicleDetail->additionalNotes = $request->vehicle_addnote;
        $vehicleDetail->bodyRearOverhang = $request->vehicle_bodyrear;
        $vehicleDetail->chassisNumberEmbossingPosition = $request->vehicle_embossing_position;
        $vehicleDetail->cylinders= $request->vehicle_cylinder;
        $vehicleDetail->electricEngineType = $request->vehicle_electric_engine_type; 
        $vehicleDetail->electricEngineMaximumTorque = $request->vehicle_electric_engine_torque;
        $vehicleDetail->engineMaximumPower = $request->vehicle_engine_maximum_power;
        $vehicleDetail->engineModel = $request->vehicle_engine_model;
        $vehicleDetail->frontShaftWeight = $request->vehicle_front_shaft_weight;
        $vehicleDetail->frontStabilizerType = $request->vehicle_front_stabilizer_type;
        $vehicleDetail->frontTread = $request->vehicle_front_tread;
        $vehicleDetail->fuelTankEquipment = $request->vehicle_fuel_tank_equipment;
        $vehicleDetail->height = $request->vehicle_height;
        $vehicleDetail->mainBrakesType = $request->vehicle_main_brakes_type;
        $vehicleDetail->maximumSpeed = $request->vehicle_maximum_speed;
        $vehicleDetail->minimumTurningRadius= $request->vehicle_minimum_turning_radius;
        $vehicleDetail->modelCode= $request->vehicle_model_code;
        $vehicleDetail->rearShaftWeight= $request->vehicle_rear_shaft_weight;
        $vehicleDetail->rearStabilizerType = $request->vehicle_rear_stabilizer_type;
        $vehicleDetail->rearTread = $request->vehicle_rear_tread;
        $vehicleDetail->ridingCapacity = $request->vehicle_riding_capacity;
        $vehicleDetail->specificationCode = $request->vehicle_specification_code;
        $vehicleDetail->transmissionType = $request->vehicle_transmission_type;
        $vehicleDetail->wheelAlignment = $request->vehicle_wheel_alignment;
        $vehicleDetail->width = $request->vehicle_width;
        $vehicleDetail->secondGearRatio= $request->vehicle_2nd_gear_ratio;
        $vehicleDetail->fourthGearRatio = $request->vehicle_4th_gear_ratio;
        $vehicleDetail->sixthGearRatio = $request->vehicle_6th_gear_ratio;
        $vehicleDetail->airbagPosition = $request->vehicle_airbag_position_capacity;
        $vehicleDetail->bodyType = $request->vehicle_body_type;
        $vehicleDetail->classificationCode = $request->vehicle_classification_code;
        $vehicleDetail->displacement = $request->vehicle_displacement;
        $vehicleDetail->electricEngineMaximumOutput = $request->vehicle_electric_engine_maximum_output;
        $vehicleDetail->electricEnginePower = $request->vehicle_electric_engine_power;
        $vehicleDetail->engineMaximumTorque = $request->vehicle_engine_maximum_torque;
        $vehicleDetail->frameType = $request->vehicle_frame_type;
        $vehicleDetail->frontShockAbsorberType = $request->vehicle_front_shock_absorber_type;
        $vehicleDetail->frontTiresSize = $request->vehicle_front_tires_size;
        $vehicleDetail->gradeData = $request->vehicle_grade;
        $vehicleDetail->length = $request->vehicle_length;
        $vehicleDetail->dataMake = $request->vehicle_make;
        $vehicleDetail->minimumGroundClearance = $request->vehicle_minimum_ground_clearance; 
        $vehicleDetail->modelData = $request->vehicle_model;
       $vehicleDetail->mufflersNumber = $request->vehicle_mufflers_number;
        $vehicleDetail->rearShockAbsorberType = $request->vehicle_rear_shock_absorber_type;
        $vehicleDetail->rearTiresSize = $request->vehicle_rear_tires_size;
        $vehicleDetail->reverseRatio = $request->vehicle_reverse_ratio;
        $vehicleDetail->sideBrakesType = $request->vehicle_side_brakes_type;
        $vehicleDetail->stoppingDistance = $request->vehicle_stopping_distance;
        $vehicleDetail->weight = $request->vehicle_weight;
        $vehicleDetail->wheelbase = $request->vehicle_wheelbase;
        $vehicleDetail->fuelConsumption = $request->vehicle_fuel_consumption;
        $vehicleDetail->save();


        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_9 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_9' => "1" ));
        }



    }

    public function extra_report_step_10(Request $request)
    {


        $vehicleDetail             =  new RAuctionHistory();
        $vehicleDetail->date = $request->auction_date;
        $vehicleDetail->auction = $request->auction_auctionname;
        $vehicleDetail->make = $request->auction_make;
        $vehicleDetail->registrationDate = $request->auction_regyear;
        $vehicleDetail->displacement = $request->auction_displacement;
        $vehicleDetail->color = $request->auction_color;
        $vehicleDetail->result = $request->auction_result;
        //$vehicleDetail->id_vehicle = $request->auction_problemtype;
        $vehicleDetail->assessment = $request->auction_contaminated;
        $vehicleDetail->lotNumber = $request->auction_lot;
        //$vehicleDetail->id_vehicle = $request->auction_region;
        $vehicleDetail->model = $request->auction_model;
        $vehicleDetail->mileage = $request->auction_mileage;
        $vehicleDetail->transmission = $request->auction_transmission;
        $vehicleDetail->body = $request->auction_model_code;
        $vehicleDetail->assessment = $request->auction_auctiongrade;
        //$vehicleDetail->id_vehicle = $request->auction_problemscale;
        $vehicleDetail->save();


        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_10 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_10' => "1" ));
        }

    }

    public function uploadphoto(Request $request, $id){

      
        $user = Auth::user()->id;
        
        $base64 = base64_encode(file_get_contents($request->file('photos_vehicle')));

        $id_vehicle = $request->id_vehicle;
        $destinationPath = 'extra_report/'.$id;
        //$file = $request->photos_vehicle;
        $date = date("YmdHis"); 

        if($request->hasFile('photos_vehicle')) {
            $file = $request->file('photos_vehicle');
            $extension = $file->getClientOriginalExtension();
            $file_name =  $id_vehicle.'_'.$date. '.' . $extension;
            $destinationPath = 'extra_report/'.$id;

           
            $file->move($destinationPath, $file_name);

        }
        
            $upload_file = $file_name;

            $file = $request->file('photos_vehicle');

            $document               = new RAuctionImages;
            $document->id_vehicle   = $id;
            $document->image      = $upload_file;
           
            $document->imagebit64         = $base64;
            $document->save();



        
        return redirect()->back()->with(['success' => 'Document save successfully']);
    
    }

    public function deletephoto($id)
    {
        
        $delete = RAuctionImages::where('id',$id)->delete();


        return redirect()->back()->with(['success' => 'Photo deleted successfully']);
    }
    
    


    public function next_odometer(Request $request)
    {
        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_4 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_4' => "1" ));
        }
    }


    public function next_use(Request $request)
    {
        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_5 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_5' => "1" ));
        }
    }


    public function next_detail(Request $request)
    {
        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_6 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_6' => "1" ));
        }
    }


    public function next_manufacture(Request $request)
    {
        $check_id_step = StepStatus::where('id_vehicle', $request->id_vehicle)->count();

        if($check_id_step < "1"){
            $vehicle = new StepStatus();
            $vehicle->id_vehicle = $request->id_vehicle;
            $vehicle->step_7 = "1";
            $vehicle->save();
        }else{

            $update_step = StepStatus::where('id_vehicle', $request->id_vehicle)->update(array('step_7' => "1" ));
        }
    }



    public function extra_report_submit_mn(Request $request, $id)
    {
        $found_data = array($request->use_contaminatedregions);
  
        dd($found_data);
       


        if(!empty($request->use_contaminatedregions) OR !empty($request->odometer_radioactivecontamination) OR !empty($request->odometer_commercialuse)){

            $found_data = $request->use_contaminatedregions;

            $hitungdata =count($found_data);

            for ($i=0; $i<$hitungdata;$i++){

                $vehicleDetail             =  new RUsageHistory();
                $vehicleDetail->id_vehicle = $request->id_vehicle;
                
                $vehicleDetail->contaminatedRegion = $request->use_contaminatedregions[$i];
                $vehicleDetail->contaminationTest = $request->odometer_radioactivecontamination[$i];
                $vehicleDetail->commercialUsage = $request->odometer_commercialuse[$i];
                $vehicleDetail->save(); 

            } 
        }
        
    }

    
    public function upload_photos(Request $request)
    {
        $data = "A";

        return response()->json($data);
    }


    public function verify_extra_report_mn($id)
    {
        

        $data = VehicleChecking::where('id_vehicle', $id)->first();
        $param_currency = ParameterCurrency::where('status', '1')->get();
      
        $brand    =  Brand::orderBy('brand', 'ASC')->where('status', '1')->get();
        $model    =  ParameterModel::orderBy('model', 'ASC')->where('status', '1')->get();

        $step_status = StepStatus::where('id_vehicle', $id)->first();

        $data_odometer_reading = ROdometerHistory::where('id_vehicle', $id)->get();
        $data_use_history = RUsageHistory::where('id_vehicle', $id)->get();

        $data_detail_history = RDetailHistory::where('id_vehicle', $id)->get();
        $data_manufacturer_recall = RRecallHistory::where('id_vehicle', $id)->get();
        $data_aution_image = RAuctionImages::where('id_vehicle', $id)->get();

        



        return view('verifier.open_vehicle.extra_report.step_verify', compact('data', 'param_currency', 'data_odometer_reading', 'brand', 'model', 'brand', 'step_status', 'data_use_history', 'data_detail_history', 'data_manufacturer_recall', 'data_aution_image'));

    }




}
