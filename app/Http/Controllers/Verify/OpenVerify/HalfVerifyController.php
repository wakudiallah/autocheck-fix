<?php

namespace App\Http\Controllers\Verify\OpenVerify;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\VehicleVerifiedHalf;


class HalfVerifyController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        
        $data = VehicleChecking::where('status', '28')
                ->where('real_type_report_id', 'Half')
                ->get();

        $data2 = VehicleChecking::where('status', '28')
                ->where('real_type_report_id', 'Half')
                ->get();

        $data3 = VehicleChecking::where('status', '28')
                ->where('real_type_report_id', 'Half')
                ->get();

        $verify_manual = VehicleChecking::where('status', '28')
                ->where('real_type_report_id', 'Half')
                ->get();
        

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $brand2    =  Brand::get();
        $model    =  ParameterModel::get();
        $model2    =  ParameterModel::get();
       
        return view('verifier.open_vehicle.half', compact('verify_manual','data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'brand2', 'model2'));

    }


    public function store(Request $request, $id)
    {

 

        $role  = Auth::user()->role_id;
        $me = Auth::user()->id;


        $brand_verify = $request->brand_verify;
        $model       = $request->model_verify;
        $ret         = explode('|', $model);
        $value_model = $ret[0];

        $engine_number_verify = $request->engine_number_verify;
        $cc_verify = $request->cc_verify;
        $fuel_verify = $request->fuel_verify;
        $registration_date_verify = $request->registration_date_verify;
        $year_verify = $request->year_verify;


        $vehicle =  $request->vehicle;
        $api_from = $request->api_from;


        /* get key in */
        $getvehicle = VehicleChecking::where('id_vehicle',$id)->first();

        $get_brand = $getvehicle->brand_id;
        $get_model = $getvehicle->model_id;
        $get_cc     = $getvehicle->cc;
        $get_engine = $getvehicle->engine_number;
        $get_registration_date = $getvehicle->vehicle_registered_date;
        $get_fuel_id = $getvehicle->fuel_id;
        /* end get key in */


        /* match proses */
        if($brand_verify == $get_brand){
            $brand_match = '1';
        }else{
            $brand_match = '0';
        }

        if($value_model == $get_model){
            $model_match = '1';
        }else{
            $model_match = '0';
        }

        if($cc_verify == $get_cc){
            $cc_match = '1';
        }else{
            $cc_match = '0';
        }

        if($fuel_verify == $get_fuel_id){
            $fuel_match = '1';
        }else{
            $fuel_match = '0';
        }


        $registation_verify =  date('m-Y ', strtotime($registration_date_verify));
        $registered_kadealer =  date('m-Y ', strtotime($get_registration_date));


        if($registation_verify == $registered_kadealer){
            $registation_match = '1';
        }else{
            $registation_match = '0';
        }
        /* end match proses */


        /* SN */        
        $today  = date('HisdmY');
        $ver_sn = 'H'.$today;
        /* end SN */

        


            $status_match = '1';

            $data = VehicleStatusMatch::where('id_vehicle',$id)->update(array('brand' => $brand_match,'model' => $model_match,'cc' => $cc_match,'registation_date' => $registation_match,'status'=>$status_match,'verify_by' => $me, 'ver_sn' => $ver_sn));


            $update_vehicle = VehicleChecking::where('id_vehicle',$id)->update(array('updated_by' => $me, 'engine_number' => $engine_number_verify, 'status' => '40')); 

            /*$data = VehicleManual::where('id_vehicle', $id)->update(array('manual_from' => $api_from,'vehicle' => $vehicle, 'brand' => $brand_verify, 'model' => $value_model, 'engine_number' => $engine_number_verify, 'cc' => $cc_verify, 'fuel_type' => $fuel_verify, 'registation_date' => $registration_date_verify, 'year_manufacture' => $year_verify)); */


            $data                       =  new HistorySearchVehicle;
            
            $data->vehicle              = $vehicle;
            $data->id_vehicle           = $id;
            $data->parameter_history_id = "COMPLETE";
            $data->user_id              =  $me;
            
            $data->save();


            $get_brand = Brand::where('id', $brand_verify)
                ->first();

            $get_model = ParameterModel::where('id', $value_model)
                ->first();

            $get_fuel = ParameterFuel::where('id', $fuel_verify)
                ->first();




                /*$data = VehicleVerifiedHalf::where('id_vehicle', $id)->update(array(
                        'vehicle' => $vehicle,
                        'country_origin'=> $request->country_origin,
                        'partner'=> $request->api_from,
                        'ver_sn'=> $ver_sn,
                        'brand' =>$get_brand->brand,
                        'model'=> $get_model->model,
                        'engine_number' =>$engine_number_verify,
                        'cc' =>$cc_verify,
                        'fuel_type' =>$get_fuel->fuel,
                        'year_manufacture' =>$year_verify,
                        'registation_date' =>$registration_date_verify 
                       
                ));*/

            $form= new VehicleVerifiedHalf();
            $form->id_vehicle = $id;
            $form->vehicle = $vehicle;
            $form->country_origin= $request->country_origin;
            $form->partner= $request->api_from;
            $form->ver_sn= $ver_sn;
            $form->brand =$get_brand->brand;
            $form->model= $get_model->model;
            $form->engine_number =$engine_number_verify;
            $form->cc =$cc_verify;
            $form->fuel_type =$get_fuel->fuel;
            $form->year_manufacture =$year_verify;
            $form->registation_date =$registration_date_verify;
            $form->save();

        

           

            return redirect()->back()->with(['success' => 'Vehicle Save Successfully '.$vehicle]);
       

        

    }


    
}
