<?php

namespace App\Http\Controllers\Verify\OpenVerify;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;

class FullVerifyController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        
        $data = VehicleChecking::where('status', '28')
                ->orderBy('id', 'DESC')
                ->where('real_type_report_id', 'Full')
                ->get();

        $data2 = VehicleChecking::where('status', '28')
                ->orderBy('id', 'DESC')
                ->where('real_type_report_id', 'Full')
                ->get();

        $data3 = VehicleChecking::where('status', '28')
                ->orderBy('id', 'DESC')
                ->where('real_type_report_id', 'Full')
                ->get();

        
        

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $brand2    =  Brand::get();
        $model    =  ParameterModel::get();
        $model2    =  ParameterModel::get();


        return view('verifier.open_vehicle.full', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'brand2', 'model2'));

    }



}
