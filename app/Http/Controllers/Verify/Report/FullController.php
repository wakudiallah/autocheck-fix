<?php

namespace App\Http\Controllers\Verify\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Model\VehicleChecking;
use App\Model\ParameterFee;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use DB;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RRecallHistory;
use App\Model\RAccidentHistory;
use App\Model\ParameterSupplier;
use Excel;
use Auth;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\User;
use App\Model\VehicleStatusMatch;
use App\Model\UserGroupOld;

class FullController extends Controller
{
    

    public function index($id)
    {
        $data = VehicleChecking::where('id_vehicle', $id)->first();

        $api_kastam = VehicleApiKastam::where('vehicle_id', $id)->first();

        $image_api = VehicleApiKastamImage::where('vehicle_id', $id)->get();

        
        return view('report.fullreport', compact('data', 'api_kastam', 'image_api'));


        //$pdf = PDF::loadView('report.fullreport', compact('data', 'api_kastam', 'image_api'));
             
        //return $pdf->stream('report - '.$data->vehicle.'.pdf', array('Attachment'=>false));
    }


    public function show($id)
    {

        $data = VehicleChecking::where('id_vehicle', $id)->first();
        $api_kastam = VehicleApiKastam::where('vehicle_id', $id)->first();
        $image_api = VehicleApiKastamImage::where('vehicle_id', $id)->get();

        //return view('admin.report.short_report_kastam', compact('data', 'api_kastam', 'image_api'));

        $pdf = PDF::loadView('admin.report.full_report', compact('data', 'api_kastam', 'image_api'));
             
        return $pdf->stream('report - '.$data->vehicle.'.pdf', array('Attachment'=>false));
    }


}
