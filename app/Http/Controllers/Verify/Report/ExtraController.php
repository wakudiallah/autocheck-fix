<?php

namespace App\Http\Controllers\Verify\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Model\VehicleChecking;
use App\Model\ParameterFee;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use DB;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RRecallHistory;
use App\Model\RAccidentHistory;
use App\Model\ParameterSupplier;
use Excel;
use Auth;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\User;
use App\Model\VehicleStatusMatch;
use App\Model\UserGroupOld;

class ExtraController extends Controller
{
    

    public function index($id)
    {
        $raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->get();
        $raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->get();
        $raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->get();
        $raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->get();
        $raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->get();
        $raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->get();


        $check_raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->first();
        $check_raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->first();
        $check_raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->first();
        $check_raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->first();
        $check_raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->first();
        $check_raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->first();

        $recall = RRecallHistory::where('id_vehicle', $id)
                ->get();

        $summary = RSummary::where('id_vehicle', $id)
                    ->latest('id')
                    ->first();

        $usagehistory = RUsageHistory::where('id_vehicle', $id)
                ->first();
                
        $vehicleassessment = RVehicleAssessment::where('id_vehicle', $id)->latest('id')->first();
        $vehicledetail = RVehicleDetails::where('id_vehicle', $id)->first();
        $vehiclespesification = RVehicleSpecification::where('id_vehicle', $id)->first();
        $auctionimage = RAuctionImages::where('id_vehicle', $id)->orderBy('id', 'DESC')->get();
        $detailhistory = RDetailHistory::where('id_vehicle', $id)->get();
        $auctionhistory = RAuctionHistory::where('id_vehicle', $id)->get();
        $odometerhistory = ROdometerHistory::where('id_vehicle', $id)->get();
        $vehicle = VehicleChecking::where('id_vehicle', $id)->first();



        $pdf = PDF::loadView('report.extrareport', compact('summary', 'usagehistory', 'vehicleassessment', 'vehicledetail', 'vehiclespesification', 'auctionimage', 'detailhistory', 'auctionhistory', 'odometerhistory', 'vehicle', 'recall', 'raccident_collision', 'raccident_malfunction', 'raccident_theft', 'raccident_fireDamage', 'raccident_waterDamage', 'raccident_hailDamage', 'check_raccident_collision', 'check_raccident_malfunction', 'check_raccident_theft', 'check_raccident_fireDamage', 'check_raccident_waterDamage', 'check_raccident_hailDamage'));

             
        return $pdf->stream('report - '.$vehicle->vehicle.'.pdf', array('Attachment'=>false, 'setIsRemoteEnabled' => false));
        /*
        return view('admin.report.report_carvx', compact('data', 'data2'));*/
    }


}
