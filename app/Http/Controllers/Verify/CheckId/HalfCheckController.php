<?php

namespace App\Http\Controllers\Verify\CheckId;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;

class HalfCheckController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        $data  =  VehicleChecking::whereNull('is_check_id')
                    ->where('status', '25')
                    ->where('is_sent', '1')
                    ->where('type_report', '3')
                    ->where('searching_by', 'NOT')
                    ->get();

        $data2  =  VehicleChecking::whereNull('is_check_id')
                    ->where('status', '25')
                    ->where('is_sent', '1')
                    ->where('type_report', '3')
                    ->where('searching_by', 'NOT')
                    ->get();

                    
        return view('verifier.check_id.half', compact('data', 'data2'));
    }


    public function update(Request $request, $id)
    {
       


       $engine = $request->engine;


        $form= new ReportVehicle();
            $form->id_vehicle = $id;;
            $form->report_id = $engine;
            $form->is_ready = "3";
            $form->data = "1";
            $form->save();

        $data= new VehicleApi();
            $data->id_vehicle = $id;
            $data->api_from = 'CARVX';
            //$data->is_ready = "3";
            //$data->data = "1";
            $data->save();


        $data = VehicleChecking::where('id_vehicle', $id)->update(array(
                        'is_check_id' =>'1',
                        'is_sent' =>'1',
                        'status' => '27',
                        'is_sync' => Null
                    ));


        $today  = date('HisdmY');
        $ver_sn = 'H'.$today;

        VehicleStatusMatch::where('id_vehicle',$id)->update(array(
            'ver_sn' => $ver_sn
        ));

        return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }




}
