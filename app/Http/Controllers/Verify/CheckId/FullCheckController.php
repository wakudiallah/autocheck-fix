<?php

namespace App\Http\Controllers\Verify\CheckId;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;

class FullCheckController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {       

        $data  =  VehicleChecking::where('status', '26')
                    ->where('real_type_report_id', 'Full')
                    ->where('searching_by', 'NOT')
                    ->get();

        $data2  =  VehicleChecking::where('status', '26')
                    ->where('real_type_report_id', 'Full')
                    ->where('searching_by', 'NOT')
                    ->get();

        return view('verifier.check_id.full', compact('data', 'data2'));
    }



    public function store(){
        
    }


}
