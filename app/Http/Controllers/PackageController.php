<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ParameterPackage;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use Mail;
use Session;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = ParameterPackage::orderBy('id', 'DESC')->get();
        
        return view('admin.parameter.package.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $user             = Auth::user()->id;

        $data               =  new ParameterPackage;
        
        $data->total_report = $request->total_report;
        $data->total_harga = $request->total_price;
        $data->created_by   =  $user;
        $data->status       =  '1';
        
        $data->save();


        return redirect('/package')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParameterPackage::where('id',$id)->first();

        return view('admin.parameter.package.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $total_report =  $request->input('total_report');
        $total_harga =  $request->input('total_harga');

        $data = ParameterPackage::where('id',$id)->update(array('total_report' => $total_report, 'total_harga' => $total_harga));

         return redirect('package')->with(['success' => 'Data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterPackage::where('id',$id)->delete();   
        
        return redirect('package')->with(['success' => 'Data successfully deleted']);
    }
}
