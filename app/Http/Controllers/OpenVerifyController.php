<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;

class OpenVerifyController extends Controller
{
    
    public function __construct()

    {
        $this->middleware('auth');
    }


    public function open_vehicle_short_report_naza()
    {
        
        $data = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_sync', '1')->get();
        $data2 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_sync', '1')->get();

        $data3 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_sync', '1')->get();

        $verify_manual = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_sync', '1')->get();
        

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $brand2    =  Brand::get();
        $model    =  ParameterModel::get();
        $model2    =  ParameterModel::get();
       
        return view('verifier.open_vehicle.short_report_open_verify', compact('verify_manual','data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'brand2', 'model2'));

    }

    


    public function open_vehicle_full_report()
    {
        $data = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'US')->get();
        $data2 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'US')->get();

        $data3 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'US')->get();

        $verify_manual = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'US')->get();
        

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $brand2    =  Brand::get();
        $model    =  ParameterModel::get();
        $model2    =  ParameterModel::get();

 
        return view('verifier.open_vehicle.full_report_open_verify', compact('verify_manual','data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'brand2', 'model2'));
    }



    public function open_vehicle_short_report_kastam()
    {
        
        $data = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'KA')->where('is_sync', '1')->where('not_found_id_carvx', '1')->get();
        $data2 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'KA')->where('is_sync', '1')->where('not_found_id_carvx', '1')->get();

        $data3 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'KA')->where('is_sync', '1')->where('not_found_id_carvx', '1')->get();

        $verify_manual = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'KA')->where('is_sync', '1')->where('not_found_id_carvx', '1')->get();
        

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $brand2    =  Brand::get();
        $model    =  ParameterModel::get();
        $model2    =  ParameterModel::get();


        return view('verifier.open_vehicle.kastam_short_report_open_verify', compact('verify_manual','data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'brand2', 'model2'));

    }






    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

