<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ParameterBranchKastam;
use App\Model\UserGroup;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = ParameterBranchKastam::get();
        $data2 = ParameterBranchKastam::get();

        return view('admin.parameter.branch.index', compact('data', 'data2'));
    }


    public function store(Request $request)
    {
        $user             = Auth::user()->id;

        $data             =  new ParameterBranchKastam;

        $data->branch      = $request->branch;
        $data->created_by =  $user;
        
        $data->save();


        $usergroup 			= new UserGroup;
        $usergroup->user_id	= 'KA_'.$request->branch;
        $usergroup->group_name	= 'KASTAM '.$request->branch;
        $usergroup->branch   = $data->id;
        $usergroup->created_by =  $user;
        
        $usergroup->save();


        return redirect()->back()->with(['success' => 'Data saved successfuly']);
    }

    public function edit($id)
    {
        $data = ParameterBranchKastam::where('id',$id)->first();

        return redirect()->back()->with(['success' => 'Data updated successfuly']);    
    }

    public function update(Request $request, $id)
    {
        $branch =  $request->input('branch');

        $data = ParameterBranchKastam::where('id',$id)->update(array('branch' => $branch ));

         return redirect()->back()->with(['success' => 'Branch data successfully updated']);
    }

    public function destroy($id)
    {
        ParameterBranchKastam::where('id',$id)->delete();   
        
        return redirect()->back()->with(['success' => 'Branch data successfully deleted']);

    }
}
