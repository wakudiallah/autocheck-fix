<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Model\HistoryBalance;
use App\Model\UserGroup;
use Response;
use App\User;

class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
         /*$data = UserGroup::get();
         $data2 = UserGroup::get();*/

	 $data = UserGroup::whereNotIn('group_name', ['Car Dealer Marii', 'KASTAM MARII'])->get();
         $data2 = UserGroup::whereNotIn('group_name', ['Car Dealer Marii', 'KASTAM MARII'])->get();

        return view('share.balance.index', compact('data', 'data2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function topup()
    {
        $user_id             = Auth::user()->user_group_id;

        $user_group_id         = UserGroup::where('user_id', $user_id)->first();

        

        return view('share.balance.topup', compact('user_group_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request, $id)
    {

        $user             = Auth::user()->id;
        $role_me  = Auth::user()->user_group_id;
        $branch_me             = Auth::user()->is_role_kastam;


        $last_balance = UserGroup::where('id',$id)->first();
        $last_balance_me = $last_balance->balance;
        $get_role_user =  $last_balance->user_id;

        $balance =  $request->input('balance');
        $total_balance = $balance + $last_balance_me;

        

        $data = UserGroup::where('id',$id)->update(array('balance' => $total_balance ));

        $getUserGroupId = UserGroup::where('id',$id)->first();
   

        $data             =  new HistoryBalance;

        $data->balance    = $total_balance;
        $data->debit    = $balance;
        $data->transaction_fee       = '0';
        $data->desc       = 'Top-up Balance';
        $data->created_by =  $user;
        $data->user_id     =  $getUserGroupId->user_id;
        $data->save();
        

        return redirect()->back()->with(['success' => 'Balance data successfully updated']);
    }



    public function storeold(Request $request, $id)
    {
        $user             = Auth::user()->id;


        $last_balance = UserGroup::where('id',$id)->first();
        $last_balance_me = $last_balance->balance;
        $get_role_user =  $last_balance->user_id;

        $balance =  $request->input('balance');
        $total_balance = $balance + $last_balance_me;

        

        $data = UserGroup::where('id',$id)->update(array('balance' => $total_balance ));



        $data             =  new HistoryBalance;

        $data->balance    = $total_balance;
        $data->debit    = $balance;
        $data->transaction_fee       = '0';
        $data->desc       = 'Top-up Balance';
        $data->created_by =  $user;
        $data->user_id     =  $get_role_user;
        
        $data->save();
        

        return redirect('balance')->with(['success' => 'Balance data successfully updated']);
    }



    public function history($id)
    {
        $getGroup = UserGroup::where('id', $id)
                    ->first();

        $data = HistoryBalance::where('user_id', $getGroup->user_id)
                ->orderBy('id', 'Desc')
                ->get();

        return view('share.balance_history.index', compact('data'));
    }



    public function index_extra()
    {
         
        $data = UserGroup::get();
        $data2 = UserGroup::get();

        return view('admin.balance.extra', compact('data', 'data2'));
    }


    public function index_full()
    {
         
        $data = UserGroup::get();
        $data2 = UserGroup::get();

        return view('admin.balance.full', compact('data', 'data2'));
    }


    public function index_half()
    {
         
        $data = UserGroup::get();
        $data2 = UserGroup::get();

        return view('admin.balance.half', compact('data', 'data2'));
    }



}
