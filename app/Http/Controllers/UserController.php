<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use App\Model\UserApiStatus;
use App\Model\ParameterTypeReport;
use App\Authorizable;
use App\Model\UserGroup;
use App\Model\UserGroupRelationUser;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Redirect;
use App\Model\ParameterBranchKastam;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role  = Auth::user()->role_id;

        if($role == "AD")
        {
            $data = User::orderBy('id', 'DESC')
                    ->whereNotIn('role_id',['KA', 'NA', 'US'] )
                    ->get();

            $role = Role::whereNotIn('id_role',['KA', 'NA', 'US'] )
                    ->get();

            $group = UserGroup::get();

            return view('admin.user.index', compact('data', 'role', 'group'));

        }
        else{

            return Redirect::back()->withErrors(['warning', 'Not Found']);
        }
    }

    public function update_status(Request $request)
    {   

      
        
        User::where('id', $request->id)
            ->update(array('status' => $request->status ));
 
    }


    public function index_group()
    {
        $role  = Auth::user()->user_group_id;
        $status_admin  = Auth::user()->is_status_admin_group;



        //if($status_admin == '1'){
            $data = User::where('user_group_id', $role)->orderBy('id', 'DESC')->get();

            $data_detail = User::where('user_group_id', $role)->orderBy('id', 'DESC')->get();
            
            $role = Role::get();
            $group = UserGroup::get();
            $branch = ParameterBranchKastam::get();



            return view('admin.user_group.index', compact('data','data_detail', 'role', 'group', 'branch'));
       
        /*}else{

            return redirect('/dashboard')->with(['warning' => 'Sory, Dont have access']);
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_groupx(Request $request)
    {
        $id_user = Auth::user()->id;
        $role  = Auth::user()->role_id;
        $user_group_id = Auth::user()->user_group_id;

        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->input('phone');
        $pass = bcrypt($password);


        $check_email = User::where('email', $email)->count();

        if($check_email >= '1'){

            return redirect('/user-group')->with(['warning' => 'Email  already exist']);

        } 

        $data             =  new User;

        $password =  $request->input('password');
        $passx     = bcrypt($password);
             
         $data->name       = $request->name;
         $data->email      = $request->email;
         $data->password   = $passx;
         $data->role_id    =  $role;
         $data->phone      =  $request->phone;
         $data->status     =  '1';
         $data->created_by =  $id_user;
         $data->user_group_id =  $user_group_id;
         $data->group_by =  $user_group_id;

        
        $data->save();

        return redirect('/user-group')->with(['success' => 'Data saved successfuly']);



    }

    public function store_group(Request $request)
    {
        $id_user = Auth::user()->id;
        $role  = Auth::user()->role_id;
        $user_group_id = Auth::user()->user_group_id;

        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->input('phone');
        $pass = bcrypt($password);


        $check_email = User::where('email', $email)->count();

        if($check_email >= '1'){

            return redirect('/user-group')->with(['warning' => 'Email  already exist']);

        } 

        $data             =  new User;

        $password =  $request->input('password');
        $passx     = bcrypt($password);
             
         $data->name       = $request->name;
         $data->email      = $request->email;
         $data->password   = $passx;
         $data->role_id    =  $role;
         $data->is_role_kastam     = $request->branch;
         $data->phone      =  $request->phone;
         $data->status     =  '1';
         $data->created_by =  $id_user;
         $data->user_group_id =  $user_group_id;
         $data->group_by =  $user_group_id;

        
        $data->save();

        return redirect('/user-group')->with(['success' => 'Data saved successfuly']);



    }


    public function store(Request $request)
    {
        $id_user = Auth::user()->id;

        $name = $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->phone;
        $role = $request->role;
        $pass = bcrypt($password);

        $check_email = User::where('email', $email)->count();
        $get_email = User::where('email', $email)->first();

        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('name', 'email','password', 'phone'),  function($message) use($use_email , $email_send, $email)
            {
                $message->from('autocheckmalaysia@gmail.com', 'Autocheck');
                $message->to($email)->subject('Registration');
            }); */

        

        if($check_email >= '1'){

            return redirect('/user')->with(['warning' => 'Email  already exist']);

        } 

         if($role == "NA"){

            $data             =  new User;
             
             $data->name       = $request->name;
             $data->email      = $request->email;
             $data->password   = $pass;
             $data->role_id    =  $request->role;
             $data->is_status_admin_group = $request->admin_status;
             $data->user_group_id = $request->user_group;
             $data->phone      =  $request->phone;
             $data->status     =  '1';
             $data->created_by =  $id_user;
            
            $data->save();

         }elseif($role == "KA"){

            $data             =  new User;
             
             $data->name       = $request->name;
             $data->email      = $request->email;
             $data->password   = $pass;
             $data->role_id    =  $request->role;
             $data->is_status_admin_group = $request->admin_status;
             $data->phone      =  $request->phone;
             $data->status     =  '1';
             $data->created_by =  $id_user;
            
            $data->save();

         }else{

             $data             =  new User;
             
             $data->name       = $request->name;
             $data->email      = $request->email;
             $data->password   = $pass;
             $data->role_id    =  $request->role;
             $data->phone      =  $request->phone;
             $data->status     =  '1';
             $data->created_by =  $id_user;
            
            $data->save();
        }

        $get_id = User::where('email', $email)->first();
        $is_id  = $get_id->id;

        $data          =  new DetailBuyer;
        $data->user_id = $is_id;
        $data->save();

        $data                       =  new HistoryUser;
        $data->user_id              = $is_id;
        $data->parameter_history_id = 'CREATE';
        $data->save();


        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('email_send', 'password', 'phone'),  function($message) use($use_email , $email_send)
            {
                $message->from($use_email, 'Autocheck');
                $message->to($email_send->email)->subject('Registration');
            });*/


        return redirect('/user')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id',$id)
            ->first();

        $role = Role::whereNotIn('id_role',['KA', 'NA', 'US'] )
                    ->get();

        $group = UserGroup::get();

        return view('admin.user.edit', compact('data', 'role'));
    }

    
    public function edit_group($id)
    {
        $data = User::where('id',$id)->first();
        $role = Role::get();

        return view('admin.user_group.edit', compact('data', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $name =  $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->input('phone');
        $role_id =  $request->input('role');

        $pass = bcrypt($password);


        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'password' => $pass, 'role_id' => $role_id, 'phone' => $phone));

         return redirect('user')->with(['success' => 'User data successfully updated']);
    }


    public function update_group(Request $request, $id)
    {

        $name =  $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->input('phone');
        $role_id =  $request->input('role');

        $pass = bcrypt($password);

        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'password' => $pass, 'phone' => $phone));

         return redirect('user-group')->with(['success' => 'User data successfully updated']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();   
        
        return redirect('user')->with(['success' => 'Data successfully deleted']);
    }


    public function destroy_group($id)
    {
        User::where('id',$id)->delete();   
        
        return redirect('user')->with(['success' => 'Data successfully deleted']);
    }

   public function user_short_report()
    {
        $data = User::where('id',$id)->get();

        return view('admin.user_group.edit', compact('data'));
    }



    /*public function user_full_report()
    {
        $data = User::where('id',$id)->get();

        return view('admin.user_group.edit', compact('data'));
    }



    public function user_extra_report()
    {
        $data = User::where('id',$id)->get();

        return view('admin.user_group.edit', compact('data'));
    }*/



    public function user_api()
    {
	
	    $data = UserApiStatus::get();
        $user_data = User::whereNotIn('role_id', ['AD','VER','MAN','MAR'])->get();
        $param_type_report_get = ParameterTypeReport::get();

        return view('admin.user_group.api.index', compact('data', 'user_data','param_type_report_get' ));
    }


    public function user_api_submit(Request $request)
    {

        $user =  $request->user;
        $type_report =  $request->type_report;


        $data                   = new UserApiStatus;
        $data->user_id          = $user;
        $data->type_report_id   = $type_report;
        $data->api_status       = '1';
        $data->status           = '1';
        $data->save();

        return redirect()->back()->with(['success' => 'Data successfully updated']);
    }


    public function user_api_change_status(Request $request)
    {
        $user = UserApiStatus::find($request->user_id);
        $user->status = $request->status;
        $user->save(); 
    }

     public function user_half_report()
    {

        $data = UserGroupRelationUser::orderBy('id', 'DESC')->where('type_report_id', '3')->get();
        $data_detail = UserGroupRelationUser::orderBy('id', 'DESC')->where('type_report_id', '3')->get();

        $role = Role::get();
        $group = UserGroup::where('type_report_id', '3')->where('status', '1')->orderBy('group_name', 'ASC')->get();




        return view('admin.user.user_creation.user_creation_half_report', compact('data', 'role', 'group', 'data_detail'));

    }



    public function user_full_report()
    {

        $data = UserGroupRelationUser::orderBy('id', 'DESC')->where('type_report_id', '4')->get();
        $data_detail = UserGroupRelationUser::orderBy('id', 'DESC')->where('type_report_id', '4')->get();

        $group = UserGroup::where('type_report_id', '4')->where('status', '1')->orderBy('group_name', 'ASC')->get();


        return view('admin.user.user_creation.user_creation_full_report', compact('data', 'group', 'data_detail'));

    }


    public function user_full_report_save(Request $request)
    {
        $id_user = Auth::user()->id;

        $name = $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->phone;
        $role = $request->role;
        $group = $request->group;

        $check_email = User::where('email', $email)->count();
        $get_email = User::where('email', $email)->first();

        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('name', 'email','password', 'phone'),  function($message) use($use_email , $email_send, $email)
            {
                $message->from('autocheckmalaysia@gmail.com', 'Autocheck');
                $message->to($email)->subject('Registration');
            }); */

        

        if($check_email >= '1'){

            return redirect('/user')->with(['warning' => 'Email  already exist']);

        } 

        

            $data             =  new User;
            $data->name       = $name;
            $data->email      = $email;
            $data->password   = bcrypt($password);
            $data->role_id    =  "KA";
            $data->phone      =  $phone;
            $data->status     =  '1';
            $data->created_by =  $id_user;
            $data->save();

            $get_id = User::where('email', $email)->first();
            $is_id  = $get_id->id;

            $user       = new UserGroupRelationUser;
            $user->user_id       = $is_id;
            $user->type_report_id     = "4";  //half report
            $user->user_group_id   = $group;
            $user->status    = '1';
            $user->save();
            
        
            $data          =  new DetailBuyer;
            $data->user_id = $is_id;
            $data->save();

            $data                       =  new HistoryUser;
            $data->user_id              = $is_id;
            $data->parameter_history_id = 'CREATE';
            $data->save();


        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('email_send', 'password', 'phone'),  function($message) use($use_email , $email_send)
            {
                $message->from($use_email, 'Autocheck');
                $message->to($email_send->email)->subject('Registration');
            });*/


        return redirect()->back()->with(['success' => 'Data successfully save']);
    }



    public function user_full_report_edit($id)
    {

        $data = User::where('id',$id)->first();
        $role = Role::get();
        $type_report = ParameterTypeReport::where('status', '1')->get();

        return view('admin.user.user_creation.edit_user_creation_full_report', compact('data', 'role', 'type_report'));

    }



    public function user_full_report_change_status(Request $request)
    {

        /*$group = UserGroupRelationUser::find($request->user_id);
        $group->status = $request->status;
        $group->save(); */

        $data = UserGroupRelationUser::where('user_id',$request->user_id)->update(array('status' => $request->status));


        $user = User::find($request->user_id);
        $user->status = $request->status;
        $user->save(); 
    }




    public function user_full_report_update($id, Request $request)
    {

        $name =  $request->name;
        $email =  $request->email;
        $phone =  $request->phone;
        $type_report = $request->type_report;
        

        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'phone' => $phone));

        $update = UserGroupRelationUser::where('user_id',$id)->update(array('type_report_id' => $type_report));

         return redirect('user/full-report')->with(['success' => 'Data successfully updated']);

    }


    public function user_extra_report()
    {

        $data = UserGroupRelationUser::orderBy('id', 'DESC')->where('type_report_id', '2')->get();
        $data_detail = UserGroupRelationUser::orderBy('id', 'DESC')->where('type_report_id', '2')->get();

        $group = UserGroup::where('type_report_id', '2')->where('status', '1')->orderBy('group_name', 'ASC')->get();


        return view('admin.user.user_creation.user_creation_extra_report', compact('data', 'data_detail', 'group'));

    }


    public function user_extra_report_save(Request $request)
    {
        $id_user = Auth::user()->id;

        $name = $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->phone;
        $role = $request->role;
        $group = $request->group;

        $check_email = User::where('email', $email)->count();
        $get_email = User::where('email', $email)->first();

        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('name', 'email','password', 'phone'),  function($message) use($use_email , $email_send, $email)
            {
                $message->from('autocheckmalaysia@gmail.com', 'Autocheck');
                $message->to($email)->subject('Registration');
            }); */

        

        if($check_email >= '1'){

            return redirect('/user')->with(['warning' => 'Email  already exist']);

        } 

        

            $data             =  new User;
            $data->name       = $name;
            $data->email      = $email;
            $data->password   = bcrypt($password);
            $data->role_id    =  "US";
            $data->phone      =  $phone;
            $data->status     =  '1';
            $data->created_by =  $id_user;
            $data->save();

            $get_id = User::where('email', $email)->first();
            $is_id  = $get_id->id;

            $user       = new UserGroupRelationUser;
            $user->user_id       = $is_id;
            $user->type_report_id     = "2";  //half report
            $user->user_group_id   = $group;
            $user->status    = '1';
            $user->save();
            
        
            $data          =  new DetailBuyer;
            $data->user_id = $is_id;
            $data->save();

            $data                       =  new HistoryUser;
            $data->user_id              = $is_id;
            $data->parameter_history_id = 'CREATE';
            $data->save();


        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('email_send', 'password', 'phone'),  function($message) use($use_email , $email_send)
            {
                $message->from($use_email, 'Autocheck');
                $message->to($email_send->email)->subject('Registration');
            });*/


        return redirect()->back()->with(['success' => 'Data successfully save']);
    }



    public function user_extra_report_edit($id)
    {

        $data = User::where('id',$id)->first();
        $role = Role::get();
        $type_report = ParameterTypeReport::where('status', '1')->get();

        return view('admin.user.user_creation.edit_user_creation_full_report', compact('data', 'role', 'type_report'));

    }



    public function user_extra_report_change_status(Request $request)
    {

        /*$group = UserGroupRelationUser::find($request->user_id);
        $group->status = $request->status;
        $group->save(); */

        $data = UserGroupRelationUser::where('user_id',$request->user_id)->update(array('status' => $request->status));


        $user = User::find($request->user_id);
        $user->status = $request->status;
        $user->save(); 
    }




    public function user_extra_report_update($id, Request $request)
    {

        $name =  $request->name;
        $email =  $request->email;
        $phone =  $request->phone;
        $type_report = $request->type_report;
        

        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'phone' => $phone));

        $update = UserGroupRelationUser::where('user_id',$id)->update(array('type_report_id' => $type_report));

         return redirect('user/full-report')->with(['success' => 'Data successfully updated']);

    }

    

}
