<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\RAccidentHistory;
use App\Model\RRecallHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use Redirect;


class SyncController extends Controller
{
    
	const USER_UID_HEADER = 'Carvx-User-Uid';

    private $url;
    private $uid = '34ll8i8hPxOY';
    private $key = 'fWU6b-pMs4DWTeSBJE4vcH3heskGEcXdrAFSBcZjV38yZld7DP4PKwXZ4co9lsJF';

    private $needSignature = true;
    private $raiseExceptions = false;
    private $isTest = false;



    public function __construct()

    {
        $this->middleware('auth');
    }




	


    


    


	


    public function sync_cadealer_autocheck3(Request $request) //short_report_all
    {
        try {

            $me    = Auth::user()->id;

            $sync = array_map(null, $request->id_vehicle, $request->id, $request->ids);


            /* Foreach */
            foreach ($sync as $val) {

            	$id_vehicle = $val[0];

                $getreport = ReportVehicle::where('id_vehicle', $val[0])->first();
                $reportId = $getreport->report_id;

                $get_searchid = VehicleApi::where('id_vehicle', $val[0])->first();


                $searchid = $get_searchid->search_id;
                $carid = $get_searchid->car_id;


                $options=array(

                    'raiseExceptions' => '1',
                    'isTest' => '1',
                    'needSignature'=>'0'
                );

                $url           = 'https://carvx.jp';
                $userUid       = 'Hhpj765XV4WX';
                $apiKey        = 'wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';

                //$sha256 = 'car_id1is_test1search_id'.$searchid.'wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';

                $sha256 = 'car_id0is_test1search_idHayhd!r8MTj!wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';

                $hashvalue=hash('sha256', $sha256);
                //dd($hashvalue);

                $client = new Client();
                $body = $client->request('GET', 'https://carvx.jp/api/v1/get-report', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Carvx-User-Uid' => 'Hhpj765XV4WX', 
                    //'Carvx-Signature'=> '61c8676233750a0eebffbe55c27416feca78a3cac636199e1a9e41f1a3287e88',
                    'Carvx-Api-Key' => 'wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva',
                    'raiseExceptions' => '1',
                    'isTest' => '1',
                    'needSignature'=>'0'
                    
                ],
                    'query' => [
                        'report_id' => $reportId
                        
                    ]
                ])->getBody()->getContents();



                $contents = (string) $body;
                $result = json_decode($contents, true);

                $count_result = count($result);

                //dd($result);
                /*$service = new CarvxService($url, $userUid, $hashvalue, $options);
               
                $result = $service->getReport($reportId, true);*/


                $is_ready       = $result["data"]["status"];
                $creation_date  = $result["data"]["creation_date"];
                $due_date       = $result["data"]["due_date"];
                //$data           = $result["data"]["status"];
                

                $data = ReportVehicle::where('id_vehicle', $val[0])->update(array(
                    'is_ready' => $is_ready,
                    'creation_date' => $creation_date,
                    'due_date' => $due_date
                ));


                if($is_ready == '6') //status reject carvx
                {
                    $status_chassis = '30';

                    $data                       =  new HistorySearchVehicle;
                    
                    $data->id_vehicle           = $val[0];
                    $data->parameter_history_id = "REJECT";
                    $data->user_id              =  $me;
                    
                    $data->save();
                }
                elseif($is_ready == '5'){ //complete
                    $status_chassis = '25';

                    $data                       =  new HistorySearchVehicle;
                    
                    $data->id_vehicle           = $val[0];
                    $data->parameter_history_id = "COMPLETE";
                    $data->user_id              =  $me;
                    
                    $data->save();

                }
                elseif($is_ready == '3'){ //new
                    $status_chassis = '20';
                }
                elseif($is_ready == '4'){ //inprogress
                    $status_chassis = '25';
                }
                else{
                    $status_chassis = $is_ready;
                }


                $data = VehicleChecking::where('id_vehicle',$val[0])->update(array(
                    'status' => $status_chassis,
                    'is_sync' => '1'
                ));

                
                /* --- status == 5 update data --- */
                if($is_ready == "5"){
                /* Update API */
                $api = VehicleApi::where('id_vehicle',$val[0])->update(array(
                    'vehicle' => $result["data"]["data"]["chassis_number"],
                    'country_origin' => "",
                    'brand' => $result["data"]["data"]["make"],
                    'model' => $result["data"]["data"]["model"],
                    'engine_number' => $result["data"]["data"]["engine"],
                    'cc' => $result["data"]["data"]["displacement"],
                    'fuel_type' => $result["data"]["data"]["fuel"],
                    'year_manufacture' => $result["data"]["data"]["manufacture_date"],
                    'registation_date' => $result["data"]["data"]["registration_date"]
                ));
                
                /*End update API */



                /*check match */

                $vehicle = $result["data"]["data"]["chassis_number"];
                $brand = $result["data"]["data"]["make"];
                $model = $result["data"]["data"]["model"];
                $engine_number = $result["data"]["data"]["engine"];
                $cc = $result["data"]["data"]["displacement"];
                $fuel_type = $result["data"]["data"]["fuel"];
                $year_manufacture = $result["data"]["data"]["manufacture_date"];
                $registation_date = $result["data"]["data"]["registration_date"];

                $get_vehicle = VehicleChecking::where('id_vehicle',$val[0])->first();

                $brand_kadealer = $get_vehicle->brand_id;
                $model_kadealer = $get_vehicle->model_id;
                $cc_kadealer = $get_vehicle->cc;
                $engine_number_kadealer = $get_vehicle->engine_number;
                $registered_kadealer = $get_vehicle->vehicle_registered_date;

                $brand_data = Brand::where('id', $brand_kadealer)->first();
                $model_data = ParameterModel::where('id', $model_kadealer)->first();

                $get_brand = $brand_data->brand;
                $get_model = $model_data->model;

                
                if($brand == $get_brand){
                    $brand_match = '1';
                }else{
                    $brand_match = '0';
                }


                if($model == $get_model){
                    $model_match = '1';
                }else{
                    $model_match = '0';
                }


                if($cc_kadealer == $cc){
                    $cc_match = '1';
                }else{
                    $cc_match = '0';
                }

                $registation_date =  date('m-Y ', strtotime($registation_date));
                $registered_kadealer =  date('m-Y ', strtotime($registered_kadealer));

                if($registered_kadealer == $registation_date){
                    $register_match = '1';
                }else{
                    $register_match = '0';
                }

                //check all match 
                if($brand_match == '1' AND $model_match == '1' AND $cc_match == '1' AND $register_match == '1'){
                    $status_match = '1';
                }else{
                    $status_match = '0';
                }


                $update_status_match = VehicleStatusMatch::where('id_vehicle',$val[0])->update(array(
                    'brand' => $brand_match,
                    'model' => $model_match,
                    'cc' => $cc_match,
                    //'fuel_type' => $status_chassis ->bynaza
                    'registation_date' => $register_match,
                    'status' => $status_match
                ));
                /* end check match */

                } /* End of status == 5 */

            }
            /* End Foreach */

            return Redirect::back()->with(['success' => 'Data updated successfuly and Please Checklist Need Signature in carvx System']);


        } catch (\Throwable $e) {

            return redirect()->back()->with(['warning' => 'Please UnCheck Need Signature in carvx System']); 
        }

    }



    public function sync_shortreport_kastam(Request $request)
    {
        

       try {

		

            $me =  Auth::user()->id;


            $sync = array_map(null, $request->id_vehicle, $request->id, $request->ids);

            /*$sync = VehicleChecking::whereIn('status',['20', '25'])->where('group_by','KA')->where('searching_by','API')->get();*/
		


            foreach ($sync as $val) {

                $id_vehicle = $val[0];


                $getreport = ReportVehicle::where('id_vehicle', $val[0])->first();		
               
		          $reportId = $getreport->report_id;

		
                /*$options=array(

                        'needSignature' => '0', 
                        'raiseExceptions' => '1',
                        'isTest' => '0'
                    );

                $url           = 'https://carvx.jp';
                $userUid       = 'h1Nigk43M3rP';
                $apiKey        = '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t';*/


               // $sha256 = 'car_id0is_test1search_idHayhd!r8MTj!wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';


                //$reportId = '20200812-705570e4';

                $client = new Client();
                $body = $client->request('GET', 'https://carvx.jp/api/v1/get-report', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Carvx-User-Uid' => 'h1Nigk43M3rP', 
                    'Carvx-Api-Key' => '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t',
                    'raiseExceptions' => '1',
                    'isTest' => '1',
                    'needSignature'=>'1'
                    
                ],
                    'query' => [
                        'report_id' => $reportId
                        
                    ]
                ])->getBody()->getContents();

                $contents = (string) $body;
                $result = json_decode($contents, true);

                $count_result = count($result);

                //dd($result);

                $is_ready       = $result["data"]["status"];
                $creation_date  = $result["data"]["creation_date"];
                $due_date       = $result["data"]["due_date"];


                $data = ReportVehicle::where('id_vehicle', $val[0])->update(array(
                        'is_ready' => $is_ready,
                        
                        'creation_date' => $creation_date,
                        'due_date' => $due_date
                ));



                if($is_ready == '6') //status reject carvx
                    {
                        $status_chassis = '30';

                        $data                       =  new HistorySearchVehicle;
                        
                        $data->id_vehicle           = $val[0];
                        $data->parameter_history_id = "REJECT";
                        $data->user_id              =  $me;
                        
                        $data->save();
                    }
                    elseif($is_ready == '5'){ //complete
                        $status_chassis = '40';

                        $data                       =  new HistorySearchVehicle;
                        
                        $data->id_vehicle           = $val[0];
                        $data->parameter_history_id = "COMPLETE";
                        $data->user_id              =  $me;
                        
                        $data->save();

                    }
                    elseif($is_ready == '3'){ //new
                        $status_chassis = '20';
                    }
                    elseif($is_ready == '4'){ //inprogress
                        $status_chassis = '25';
                    }
                    else{
                        $status_chassis = $is_ready;
                    }


                    $data = VehicleChecking::where('id_vehicle',$val[0])->update(array(
                        'status' => $status_chassis
                    ));

                    
                    /* --- status == 5 update data --- */
                    if($is_ready == "5"){
                    /* Update API */
                    $api = VehicleApi::where('id_vehicle',$val[0])->update(array(
                        'vehicle' => $result["data"]["data"]["chassis_number"],
                        'country_origin' => "",
                        'brand' => $result["data"]["data"]["make"],
                        'model' => $result["data"]["data"]["model"],
                        'engine_number' => $result["data"]["data"]["engine"],
                        'cc' => $result["data"]["data"]["displacement"],
                        'fuel_type' => $result["data"]["data"]["fuel"],
                        'year_manufacture' => $result["data"]["data"]["manufacture_date"],
                        'registation_date' => $result["data"]["data"]["registration_date"]
                    )); /*End update API */

                    

                    $datas = new VehicleApiKastam;
                    $datas->vehicle_id = $val[0];
                    $datas->api_from = 'carvx';
                    $datas->is_test = '1';
                    $datas->car_id = '';
                    $datas->search_id = '';
                    $datas->status = 'Complete';
                    $datas->request_by = '';
                    $datas->update_by = $me;

                    $datas->vehicle = $result["data"]["data"]["chassis_number"];
                    $datas->make = $result["data"]["data"]["make"];
                    $datas->model = $result["data"]["data"]["model"];
                    $datas->engine = $result["data"]["data"]["engine"];
                    $datas->body = $result["data"]["data"]["body"];
                    $datas->grade = $result["data"]["data"]["grade"];
                    $datas->manufactureDate = $result["data"]["data"]["manufacture_date"];
                    $datas->date_of_origin = $result["data"]["data"]["registration_date"];
                    $datas->drive = $result["data"]["data"]["drive"];
                    $datas->transmission = $result["data"]["data"]["transmission"];
                    $datas->displacement_cc = $result["data"]["data"]["displacement"];
                    $datas->average_market = $result["data"]["data"]["average_price"];
                    
                    $datas->save();    


                    $photos = $result["data"]["data"]["photos"];
                    $photo_implode = implode(", ", $photos);
                    
                    
                    $photos_encode = json_encode($photos);
                    //dd($photos);


                    $count_photos = count($photos);
               
                    for($i=0; $i<$count_photos; $i++){

                        $image64 = file_get_contents($photo_implode);


                        $datas = new VehicleApiKastamImage;
                        $datas->vehicle_id = $val[0];
                        $datas->image = $photo_implode;
                        $datas->base64 = 'data:image/jpg;base64,'.base64_encode($image64);
                        $datas->save();  
                    }
                

                }
            }


            return redirect()->back()->with(['success' => 'Data updated successfuly and Please Checklist Need Signature in carvx System']); 


        } catch (\Throwable $e) {

            return redirect()->back()->with(['warning' => 'Please UnCheck Need Signature in carvx System']); 
        }



    }



    public function sync(Request $request,$id ) //full report
    {


        $getreport = ReportVehicle::where('id_vehicle', $id)->first();
        $reportId = $getreport->report_id;

        $options=array(

                'needSignature' => '1', 
                'raiseExceptions' => '1',
                'isTest' => '0'
            );

        $url           = 'https://carvx.jp';
        $userUid       = '34ll8i8hPxOY';
        $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';
        
        $service = new CarvxService($url, $userUid, $apiKey, $options);
       
        $report = $service->getReport($reportId, true);


        $is_ready       = $report->status;
        $creation_date  = $report->creationDate;
        $due_date       = $report->dueDate;
        $data           = $report->data;

          
          
        //dd($report);
         //dd($report->data->vehicleAssessment->driverSeatEvaluation ); 
         //dd($report->data->vehicleDetails->chassisNumber); 
        //exit();

       

        ReportVehicle::where('id_vehicle',$id)->update(array(
            'is_ready' => $is_ready,
            //'is_ready' => '4',  
            'creation_date' => $creation_date,
            'due_date' => $due_date
            //'data' => $data
        ));

        if($is_ready == '6') //status reject carvx
        {
            $status_chassis = '30';
        }
        elseif($is_ready == '5'){ //complete
            $status_chassis = '40';


            $data                       =  new HistorySearchVehicle;
            
            //$data->vehicle              = $request->vehicle;
            $data->id_vehicle           = $id;
            $data->parameter_history_id = "Complete";
            $data->user_id              =  Auth::user()->id;
            
            $data->save();

        }
        elseif($is_ready == '3'){ //new
            $status_chassis = '20';
        }
        elseif($is_ready == '4'){ //inprogress
            $status_chassis = '25';

            $data                       =  new HistorySearchVehicle;
            
            //$data->vehicle              = $request->vehicle;
            $data->id_vehicle           = $id;
            $data->parameter_history_id = "In Progress";
            $data->user_id              =  Auth::user()->id;
            
            $data->save();
        }


        VehicleChecking::where('id_vehicle',$id)->update(array(
            'status' => $status_chassis
        ));



        
        /*if status ready == 5 update date */

        if($is_ready == '5'){

        $body_odometerhistory = json_encode($report->data->odometerHistory, true);
        $body_detailedhistory = json_encode($report->data->detailedHistory, true);
        $body_auctionhistory = json_encode($report->data->auctionHistory, true); 

        $body_recallhistory = json_encode($report->data->recallHistory, true); 

        //Accident History
        $body_accident_history_collision = json_encode($report->data->accidentHistory->collision, true); 
        $body_accident_malfunction = json_encode($report->data->accidentHistory->malfunction, true);

        $body_accident_theft = json_encode($report->data->accidentHistory->theft, true);

        $body_accident_fireDamage = json_encode($report->data->accidentHistory->fireDamage, true);

        $body_accident_waterDamage = json_encode($report->data->accidentHistory->waterDamage, true);

        $body_accident_hailDamage = json_encode($report->data->accidentHistory->hailDamage, true);
        //End Accident History
         

        $data_odometerhistory = json_decode($body_odometerhistory, true);
        $data_detailedhistory = json_decode($body_detailedhistory, true);
        $data_auctionhistory = json_decode($body_auctionhistory, true);

        $data_recallhistory = json_decode($body_recallhistory, true);

        //Accident History
        $data_accident_history_collision = json_decode($body_accident_history_collision, true);

        $data_accident_history_malfunction = json_decode($body_accident_malfunction, true);

        $data_accident_history_theft = json_decode($body_accident_theft, true);

        $data_accident_history_fireDamage = json_decode($body_accident_fireDamage, true);

        $data_accident_history_waterDamage = json_decode($body_accident_waterDamage, true);

        $data_accident_history_hailDamage = json_decode($body_accident_hailDamage, true);
        //End Accident History

        //Accident History
        $count_accident_history_collision=count($data_accident_history_collision);
        $count_accident_history_malfunction=count($data_accident_history_malfunction);

        $count_accident_history_theft=count($data_accident_history_theft);
        $count_accident_history_fireDamage=count($data_accident_history_fireDamage);
        $count_accident_history_waterDamage=count($data_accident_history_waterDamage);
        $count_accident_history_hailDamage=count($data_accident_history_hailDamage);
        //Accident History

        $count_detailedhistory=count($data_detailedhistory);
        $count_auctionhistory = count($data_auctionhistory);

        $count_auctionhistory = count($data_auctionhistory);

        $count_recallhistory = count($data_recallhistory);
        $count_odometerhistory = count($data_odometerhistory);
        
      

        for($i=0; $i<$count_recallhistory; $i++){
            $datas = new RRecallHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->date = $data_recallhistory[$i]["date"];
            $datas->source = $data_recallhistory[$i]["source"];
            

            $datas->affected_part = $data_recallhistory[$i]["affected_part"];
            $datas->details = $data_recallhistory[$i]["details"];
            
            $datas->save();
        }

         //Accident History
         //
         
        for($i=0; $i<$count_accident_history_hailDamage; $i++){
            $datas = new RAccidentHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->field = "hailDamage";
            $datas->date_reported = $data_accident_history_hailDamage[$i]["date"];
            $datas->source = $data_accident_history_hailDamage[$i]["source"];
            $datas->details = $data_accident_history_hailDamage[$i]["problem_scale"];
            $datas->airbag = $data_accident_history_hailDamage[$i]["airbag"];
            
            $datas->save();
        }

        for($i=0; $i<$count_accident_history_waterDamage; $i++){
            $datas = new RAccidentHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->field = "waterDamage";
            $datas->date_reported = $data_accident_history_waterDamage[$i]["date"];
            $datas->source = $data_accident_history_waterDamage[$i]["source"];
            $datas->details = $data_accident_history_waterDamage[$i]["problem_scale"];
            $datas->airbag = $data_accident_history_waterDamage[$i]["airbag"];
            
            $datas->save();
        }


        for($i=0; $i<$count_accident_history_fireDamage; $i++){
            $datas = new RAccidentHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->field = "fireDamage";
            $datas->date_reported = $data_accident_history_fireDamage[$i]["date"];
            $datas->data_source = $data_accident_history_fireDamage[$i]["source"];
            $datas->details = $data_accident_history_fireDamage[$i]["problem_scale"];
            $datas->airbag = $data_accident_history_fireDamage[$i]["airbag"];
            
            $datas->save();
        }

        for($i=0; $i<$count_accident_history_theft; $i++){
            $datas = new RAccidentHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->field = "theft";
            $datas->date_reported = $data_accident_history_theft[$i]["date"];
            $datas->data_source = $data_accident_history_theft[$i]["source"];
            $datas->details = $data_accident_history_theft[$i]["problem_scale"];
            $datas->airbag = $data_accident_history_theft[$i]["airbag"];
            
            $datas->save();
        }


        for($i=0; $i<$count_accident_history_malfunction; $i++){
            $datas = new RAccidentHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->field = "malfunction";
            $datas->date_reported = $data_accident_history_malfunction[$i]["date"];
            $datas->data_source = $data_accident_history_malfunction[$i]["source"];
            $datas->details = $data_accident_history_malfunction[$i]["problem_scale"];
            $datas->airbag = $data_accident_history_malfunction[$i]["airbag"];
            
            $datas->save();
        }


        for($i=0; $i<$count_accident_history_collision; $i++){
            $datas = new RAccidentHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->field = "collision";
            $datas->date_reported = $data_accident_history_collision[$i]["date"];
            $datas->data_source = $data_accident_history_collision[$i]["source"];
            $datas->details = $data_accident_history_collision[$i]["problem_scale"];
            $datas->airbag = $data_accident_history_collision[$i]["airbag"];
            
            $datas->save();
        }

         //Elnd Accident History


        for($i=0; $i<$count_auctionhistory; $i++){
            $datas = new RAuctionHistory;
            $datas->id_vehicle = $id;
            $datas->report_id = $reportId;
            $datas->date = $data_auctionhistory[$i]["date"];
            $datas->lotNumber = $data_auctionhistory[$i]["lot_number"];
            $datas->auction = $data_auctionhistory[$i]["auction"];
            $datas->make = $data_auctionhistory[$i]["make"];
            $datas->model = $data_auctionhistory[$i]["model"];
            $datas->registrationDate = $data_auctionhistory[$i]["registration_date"];
            $datas->mileage = $data_auctionhistory[$i]["mileage"];
            $datas->displacement = $data_auctionhistory[$i]["displacement"];
            $datas->transmission = $data_auctionhistory[$i]["transmission"];
            $datas->color = $data_auctionhistory[$i]["color"];
            $datas->body = $data_auctionhistory[$i]["body"];
            $datas->finalPrice = $data_auctionhistory[$i]["final_price"];
            $datas->result = $data_auctionhistory[$i]["result"];
            $datas->assessment = $data_auctionhistory[$i]["assessment"];
            //$datas->images = $data_auctionhistory[$i]["images"];
            $datas->save();
        }



        for($i=0; $i<$count_detailedhistory; $i++){
            $datas             =  new RDetailHistory;
            $datas->id_vehicle   = $id;
            $datas->report_id    = $reportId;
            $datas->mileage = $data_detailedhistory[$i]["mileage"];
            $datas->source = $data_detailedhistory[$i]["source"];
            $datas->date = $data_detailedhistory[$i]["date"];
            $datas->action = $data_detailedhistory[$i]["action"];
            $datas->location = $data_detailedhistory[$i]["location"];
            $datas->save();
        }


        for($i=0; $i<$count_odometerhistory; $i++){
            $datas             =  new ROdometerHistory;
            $datas->id_vehicle   = $id;
            $datas->report_id    = $reportId;
            $datas->mileage = $data_odometerhistory[$i]["mileage"];
            $datas->source = $data_odometerhistory[$i]["source"];
            $datas->date = $data_odometerhistory[$i]["date"];
            $datas->save();
        }

        

        

        $auctionImages = $report->data->auctionImages;
            $i = 0;
        foreach($auctionImages as $key) {
            $data             =  new RAuctionImages;
            $data->id_vehicle   = $id;
            $data->report_id    = $reportId;
            $data->image    = $report->data->auctionImages[$i++];
            $data->save();
        }



        $data             =  new RVehicleDetails;
        
        $data->id_vehicle       = $id;
        $data->report_id    = $reportId;
        $data->chassisNumber    = $report->data->vehicleDetails->chassisNumber;
        $data->make =  $report->data->vehicleDetails->make;
        $data->model =  $report->data->vehicleDetails->model;
        $data->grade =  $report->data->vehicleDetails->grade;
        $data->manufactureDate =  $report->data->vehicleDetails->manufactureDate;
        $data->body =  $report->data->vehicleDetails->body;
        $data->engine =  $report->data->vehicleDetails->engine;
        $data->drive =  $report->data->vehicleDetails->drive;
        $data->transmission =  $report->data->vehicleDetails->transmission;
        $data->save();

        $data             =  new RUsageHistory;
        
        $data->id_vehicle       = $id;
        $data->report_id    = $reportId;
        $data->contaminatedRegion    = $report->data->usageHistory->contaminatedRegion;
        $data->contaminationTest =  $report->data->usageHistory->contaminationTest;
        $data->commercialUsage =  $report->data->usageHistory->commercialUsage;
        $data->save();
        
        

        $data             =  new RVehicleAssessment;
        
        $data->id_vehicle       = $id;
        $data->report_id    = $reportId;

        $data->driverSeatPoints    = $report->data->vehicleAssessment->driverSeatPoints;
        $data->driverSeatEvaluation    = $report->data->vehicleAssessment->driverSeatEvaluation;
        $data->driverSeatGoalAverage =  $report->data->vehicleAssessment->driverSeatGoalAverage;
        $data->frontPassengerSeatPoints =  $report->data->vehicleAssessment->frontPassengerSeatPoints;
        $data->frontPassengerSeatEvaluation =  $report->data->vehicleAssessment->frontPassengerSeatEvaluation;
        $data->frontPassengerSeatGoalAverage =  $report->data->vehicleAssessment->frontPassengerSeatGoalAverage;
        $data->dryRoadStoppingDistance =  $report->data->vehicleAssessment->dryRoadStoppingDistance;
        $data->wetRoadStoppingDistance =  $report->data->vehicleAssessment->wetRoadStoppingDistance;
        $data->safetyGrade =  $report->data->vehicleAssessment->safetyGrade;
        $data->save();

        

        $data             =  new RVehicleSpecification;
        
        $data->id_vehicle       = $id;
        $data->report_id    = $reportId;
        $data->firstGearRatio    = $report->data->vehicleSpecification->firstGearRatio;
        $data->secondGearRatio =  $report->data->vehicleSpecification->secondGearRatio;
        $data->thirdGearRatio =  $report->data->vehicleSpecification->thirdGearRatio;
        $data->fourthGearRatio =  $report->data->vehicleSpecification->fourthGearRatio;
        $data->fifthGearRatio =  $report->data->vehicleSpecification->fifthGearRatio;
        $data->sixthGearRatio =  $report->data->vehicleSpecification->sixthGearRatio;
        $data->additionalNotes =  $report->data->vehicleSpecification->additionalNotes;
        $data->airbagPosition =  $report->data->vehicleSpecification->airbagPosition;
        $data->bodyRearOverhang =  $report->data->vehicleSpecification->bodyRearOverhang;
        $data->bodyType =  $report->data->vehicleSpecification->bodyType;
        $data->chassisNumberEmbossingPosition =  $report->data->vehicleSpecification->chassisNumberEmbossingPosition;
        $data->classificationCode =  $report->data->vehicleSpecification->classificationCode;
        $data->cylinders =  $report->data->vehicleSpecification->cylinders;
        $data->displacement =  $report->data->vehicleSpecification->displacement;
        $data->electricEngineType =  $report->data->vehicleSpecification->electricEngineType;
        $data->electricEngineMaximumOutput =  $report->data->vehicleSpecification->electricEngineMaximumOutput;
        $data->electricEngineMaximumTorque =  $report->data->vehicleSpecification->electricEngineMaximumTorque;
        $data->electricEnginePower =  $report->data->vehicleSpecification->electricEnginePower;
        $data->engineMaximumPower =  $report->data->vehicleSpecification->engineMaximumPower;
        $data->engineMaximumTorque =  $report->data->vehicleSpecification->engineMaximumTorque;
        $data->engineModel =  $report->data->vehicleSpecification->engineModel;
        $data->frameType =  $report->data->vehicleSpecification->frameType;
        $data->frontShaftWeight =  $report->data->vehicleSpecification->frontShaftWeight;
        $data->frontShockAbsorberType =  $report->data->vehicleSpecification->frontShockAbsorberType;
        $data->frontStabilizerType =  $report->data->vehicleSpecification->frontStabilizerType;
        $data->frontTiresSize =  $report->data->vehicleSpecification->frontTiresSize;
        $data->frontTread =  $report->data->vehicleSpecification->frontTread;
        $data->fuelConsumption =  $report->data->vehicleSpecification->fuelConsumption;
        $data->fuelTankEquipment =  $report->data->vehicleSpecification->fuelTankEquipment;
        $data->gradeData =  $report->data->vehicleSpecification->gradeData;
        $data->height =  $report->data->vehicleSpecification->height;
        $data->length =  $report->data->vehicleSpecification->length;
        $data->mainBrakesType =  $report->data->vehicleSpecification->mainBrakesType;
        $data->dataMake =  $report->data->vehicleSpecification->dataMake;
        $data->maximumSpeed =  $report->data->vehicleSpecification->maximumSpeed;
        $data->minimumGroundClearance =  $report->data->vehicleSpecification->minimumGroundClearance;
        $data->minimumTurningRadius =  $report->data->vehicleSpecification->minimumTurningRadius;
        $data->modelData =  $report->data->vehicleSpecification->modelData;
        $data->modelCode =  $report->data->vehicleSpecification->modelCode;
        $data->mufflersNumber =  $report->data->vehicleSpecification->mufflersNumber;
        $data->rearShaftWeight =  $report->data->vehicleSpecification->rearShaftWeight;
        $data->rearShockAbsorberType =  $report->data->vehicleSpecification->rearShockAbsorberType;
        $data->rearStabilizerType =  $report->data->vehicleSpecification->rearStabilizerType;
        $data->rearTiresSize =  $report->data->vehicleSpecification->rearTiresSize;
        $data->rearTread =  $report->data->vehicleSpecification->rearTread;
        $data->reverseRatio =  $report->data->vehicleSpecification->reverseRatio;
        $data->ridingCapacity =  $report->data->vehicleSpecification->ridingCapacity;
        $data->sideBrakesType =  $report->data->vehicleSpecification->sideBrakesType;
        $data->specificationCode =  $report->data->vehicleSpecification->specificationCode;
        $data->stoppingDistance =  $report->data->vehicleSpecification->stoppingDistance;
        $data->transmissionType =  $report->data->vehicleSpecification->transmissionType;
        $data->weight =  $report->data->vehicleSpecification->weight;
        $data->wheelAlignment =  $report->data->vehicleSpecification->wheelAlignment;

        $data->wheelbase =  $report->data->vehicleSpecification->wheelbase;

        $data->width =  $report->data->vehicleSpecification->width;
        $data->save();


        $data             =  new RSummary;
        
        $data->id_vehicle       = $id;
        $data->report_id    = $reportId;
        $data->registered    = $report->data->summary->registered;
        $data->accident =  $report->data->summary->accident;
        $data->odometer =  $report->data->summary->odometer;
        $data->recall    = $report->data->summary->recall;
        $data->safetyGrade =  $report->data->summary->safetyGrade;
        $data->averagePrice =  $report->data->summary->averagePrice;
        $data->buyback    = $report->data->summary->buyback;
        $data->contaminationRisk =  $report->data->summary->contaminationRisk;
        $data->fillingTime =  $report->data->summary->fillingTime;
        $data->save();
        
        } /* End of is_ready == 5 */

        return redirect()->back()->with(['success' => 'Data saved successfuly']);



           



    }

    

}

