<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ParameterTypeVehicle;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use Mail;
use Session;

class TypeVehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = ParameterTypeVehicle::orderBy('id', 'DESC')->get();
        
        return view('admin.parameter.type_vehicle.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user             = Auth::user()->id;

        $data               =  new ParameterTypeVehicle;
        
        $data->type_vehicle = $request->type;
        $data->created_by   =  $user;
        $data->status       =  '1';
        
        $data->save();


        return redirect('/type-vehicle')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParameterTypeVehicle::where('id',$id)->first();

        return view('admin.parameter.type_vehicle.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $type =  $request->input('type');

        $data = ParameterTypeVehicle::where('id',$id)->update(array('type_vehicle' => $type, 'updated_by' => $user));

         return redirect('type-vehicle')->with(['success' => 'Data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterTypeVehicle::where('id',$id)->delete();   
        
        return redirect('type-vehicle')->with(['success' => 'Data successfully deleted']);
    }
}
