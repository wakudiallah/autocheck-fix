<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterEmail;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\Model\BatchSent;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use Excel;
use Illuminate\Support\Facades\Mail;

class SentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()

    {
        $this->middleware('auth');
    }

    public function index()
    {
        $me  = Auth::user()->role_id;

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::where('status', 1)->get();
        $model    =  ParameterModel::where('status', 1)->get();

        if($me == 'VER'){
            $data = VehicleChecking::where('is_sent', '0')->where('searching_by', 'NOT')->orderBy('id', 'DESC')->whereIn('status', ['10'])->get();
            $data2 = VehicleChecking::where('is_sent', '0')->where('searching_by', 'NOT')->orderBy('id', 'DESC')->whereIn('status', ['10'])->get();
    

        }elseif($me == 'AD'){

            $data = VehicleChecking::where('is_sent', '1')->where('searching_by', 'NOT')->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('is_sent', '1')->orderBy('id', 'DESC')->get();

        }

        return view('verifier.sent.sent', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model'));

        
    }


    public function batch(){

        $data_batch = BatchSent::orderBy('id', 'DESC')->get();

        $table1 = BatchSent::orderBy('id', 'DESC')->get();
        $table2 = BatchSent::orderBy('id', 'DESC')->get();

        return view('verifier.batch.index', compact('data_batch', 'table1', 'table2'));
    }


    public function sent_vehicle_kastam()
    {
        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::where('status', 1)->get();
        $model    =  ParameterModel::where('status', 1)->get();

        $data = VehicleChecking::where('is_sent', '0')->where('searching_by', 'NOT')->whereNotIn('status', ['20','30','40'])->where('group_by', 'KA')->orderBy('id', 'DESC')->get();
        $data2 = VehicleChecking::where('is_sent', '0')->where('searching_by', 'NOT')->whereNotIn('status', ['20','30','40'])->where('group_by', 'KA')->orderBy('id', 'DESC')->get();


        return view('verifier.sent.sent_kastam', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reject_kastam(Request $request, $id)
    {
        $me    = Auth::user()->id;
        $remark = $request->remark;

        $user_group_me = Auth::user()->user_group_id;



        /*$pra = VehicleChecking::where('id_vehicle', $id)->update([
                "status"              => '30'
            ]);*/

        //get balance
        $get_balance = UserGroup::where('user_id', 'T')->first();


        //get price
        $get_price = ParameterFee::where('fee_for', '22')->first();
        
        //update balance
        /*var balance now */
        $var_balance = $get_balance->balance;

        dd( $var_balance);
        exit();

        /*var fee price */
        //$update_balance = UserGroup
        //update history

        $data          = new HistorySearchVehicle;
        $data->id_vehicle =  $id;
        $data->parameter_history_id   = "REJECT";
        $data->user_id  = $me;
        $data->remark  = $remark;
        $data->save();


        $data          = new UserGroup;



        return redirect('sent-vehicle-kastam')->with(['success' => 'Data successfully updated']);
    }


    public function reject_kadealer(Request $request,  $id)
    {
        $me    = Auth::user()->id;
        $remark = $request->remark;
        $vh = $request->vh;


        $user_group_me = Auth::user()->user_group_id;

        

        //get_id_create 
        $get_id_create = VehicleChecking::where('id_vehicle', $id)->first();

        $var_group_id_create = $get_id_create->company_name;


         //get balance
        //$get_balance = UserGroup::where('user_id', $var_group_id_create)->first();

        $get_balance = UserGroup::where('branch', '1')->first();

        //get price
        $get_price = ParameterFee::where('fee_for', '22')->first();
        
        //---- update balance
        /*var balance now */
        $var_balance = $get_balance->balance;

        /*var fee price */
        $var_fee =  $get_price->fee;

        /*calculate balance */
        $calculate = $var_balance+$var_fee;


        //update balance now 
        $update_balance = UserGroup::where('user_id', $var_group_id_create)->update([
                "balance"    => $calculate
            ]);

        
        //add history balance
        $data          = new HistoryBalance;
        $data->id_vehicle =  $id;
        $data->balance   = $calculate;
        $data->debit  = $var_fee;
        $data->desc  = 'Not Found Vehicle';
        $data->created_by  = $me;
        $data->user_id  = $var_group_id_create;

        $data->save();

        //add history search
        $data          = new HistorySearchVehicle;
        $data->id_vehicle =  $id;
        $data->vehicle = $vh;
        $data->parameter_history_id = 'NOT FOUND';
        $data->user_id = $me;

        $data->save();



        $pra = VehicleChecking::where('id_vehicle', $id)->update([
                "status"              => '30'
            ]);


        

        /*$data          = new HistorySearchVehicle;
        $data->id_vehicle =  $id;
        $data->parameter_history_id   = "REJECT";
        $data->user_id  = $me;
        $data->remark  = $remark;
        $data->save();*/




        return redirect('sent-vehicle')->with(['success' => 'Data successfully updated']);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate_and_sent(Request $request)
    {
        $me    = Auth::user()->id;

        $item = array_map(null, $request->id, $request->ci, $request->ids, $request->ktp);
        $hash  = str_random(20);
        $today2  = date('Y-m-d H:i:s'); 
        $today = date('Ymd-Hi');
        $format = "AC-".$today;

        $partner = $request->partner;
        $account = $request->account_autocheck;


            
        /*batch */
        $batch          = new BatchSent;
        $batch->unique_sent =  $format;
        $batch->created_by    = $me;
        $batch->partner = $partner;
        $batch->save();
        /*end batch */


        foreach($item as $val) {
            
            $pra = VehicleChecking::where('id_vehicle',$val[0])->update([
                "is_sent"              => '1',
                "hash"               => $hash,
                "unique_sent"        => $format,
                "date_sent"          => $today2,
                "status"          => '25',
                "sent_by"         => $me,
                "is_sync"           => '1'
            ]);
        }


        //Dia Download
        $current = date('d-m-Y');

        set_time_limit(0);
        ini_set('memory_limit', '1G');
        
        $excel = Excel::create('Application Form '.$current, function($excel) use( $item,$hash, $me, $partner, $account) {

            $excel->setTitle('Report Autocheck');

            // Chain the setters
            $excel->setCreator('Autocheck')->setCompany('Autocheck.com.my');

            $excel->sheet('form', function($sheet) use( $item, $hash, $me, $partner, $account) {
                 $sheet->setColumnFormat(array(
   
                'D' => '0'
            ));

            $assessments = VehicleChecking::orderBy('id', 'desc')->where('hash', $hash)->get();

                $i = 1;
                    foreach($assessments as $product) {
                     $data[] = array(
                        $i++,
                        $product->vehicle,
                        '',
                        '',
                        ''
                    );

                    $sheet->cell("A{$i}:E{$i}", function($cell) {

                    $cell->setBackground('#ffffff')
                        ->setFontColor('#000000')
                        ->setBorder( 'thin','thin','thin','thin');
                    });
                }

                $current = date('Y-m-d ');

                $sheet->fromArray($data, null, 'A1', false, false);

                    $ir = $i-1;
                    $is = $i+1;
                   
                  $range = "A1:F{$ir}";
                $sheet->setBorder($range, 'thin','thin','thin','thin');

                $headings = array('No', 'Chassis No.','Registration No.','AP No.', 'Remarks');
                
                $sheet->prependRow(1, $headings);

                    $sheet->prependRow(1, ["申　請　用　紙"]);
                    $sheet->mergeCells("A1:F1");


                    $sheet->prependRow(2, [""]);
                    $sheet->mergeCells("B2:F2");


                    $sheet->prependRow(3, ["下記車両について、登録事項等証明書・現在記録及び保存記録の申請、及びその英文翻訳・発送業務を依頼します。 "]);
                    $sheet->mergeCells("A3:F3");


                       
                    $date_created = array('申請日 :', '03.02.2019');
                    $sheet->prependRow(4, $date_created);
                    $sheet->mergeCells("B2:F2");

                    $from = array('御社名 :', 'Naza Motor Trading Sdn Bhd', '', '', '合計台数');
                    $sheet->prependRow(5, $from);
                    $sheet->mergeCells("B2:F2");

                    $alamat = array('御住所 :', 'Naza Automall, Lot 1, Jalan 51A/221, 46100 Petaling Jaya, Selangor');
                    $sheet->prependRow(6, $alamat);
                    $sheet->mergeCells("B2:F2");

                    $from = array('ご担当者 :', 'Muhammad Aizzuddin Jaafar', '', '', '1台');
                    $sheet->prependRow(7, $from);
                    $sheet->mergeCells("B2:F2");

                    $tel = array('TEL :', '+603-7953 8888');
                    $sheet->prependRow(8, $tel);
                    $sheet->mergeCells("B2:F2");

                    $fax = array('FAX :', '+603-7954 6885');
                    $sheet->prependRow(9, $fax);
                    $sheet->mergeCells("B2:F2");

                    $sheet->prependRow(10, [""]);
                    $sheet->mergeCells("B2:F2");


                    $sheet->cell("A1:E1", function($cell) {
                       
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });




                    $sheet->setSize('E1', 25, 50);

                    $sheet->cell("A3:E3", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setAlignment('center')
                            ->setValignment('center');
                    });

                    $sheet->cell("E6:E9", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell("E5:E5", function($cell) {
                       
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                            ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->setSize('A1', 10, 18);
                    $sheet->setSize('B1', 35, 18);
                    $sheet->setSize('C1', 25, 18);
                    $sheet->setSize('D1', 25, 18);
                    $sheet->setSize('E1', 25, 18); 

                    
                    /* Bold */
                    $sheet->cell("A11", function($cell) {
                        
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    
                    
                     $sheet->cell("B11", function($cell) {
                        // change header color
                      
                        $cell->setBackground('#f4e877')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                      $sheet->cell("C11", function($cell) {
                        // change header color
                      
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                       $sheet->cell("D11", function($cell) {
                        // change header color
                      
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                        $sheet->cell("E11", function($cell) {
                        // change header color
                      
                        $cell->setBackground('#ffffff')
                            ->setFontColor('#000000')
                            ->setFontWeight('bold')
                            ->setAlignment('center')
                            ->setValignment('center')
                          ->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                        /* End Bold */
                          
                    
                
                });

                /* Get Email */
                
                $get_email_to = ParameterEmail::where('status', '1')->get();
                $get_email_from = ParameterEmail::latest('id')->where('status', '1')->first();
                $get_email_cc = ParameterEmail::latest('id')->where('status', '1')->first();

                $assessments2 = VehicleChecking::orderBy('id', 'desc')->where('hash', $hash)->get();

                $vehc_count = VehicleChecking::orderBy('id', 'desc')->where('hash', $hash)->count();

                /* End Get Email */

            $get_user_submit = User::where('id', $me)->first();
            $user_submit =  $get_user_submit->name;

            $today  = date('d F Y');

            if($partner == "contact@carvx.jp"){
                $account_is = "$account";
            }else{
                $account_is = "";
            }


            /*$dataq = array('today'=>$today, 'me' => $user_submit, 'assessments2' => $assessments2, 'vehc_count' => $vehc_count, 'partner' => $partner, 'account_is' => $account_is);

            Mail::send('email.manual_send', $dataq, function($message) use ($partner){
                 $message->to($partner, 'Partner Autocheck Malaysia')->subject('Autocheck Malaysia');
                 $message->cc(['wakudiallah05@gmail.com', 'autocheckmalaysia@gmail.com']);
                 $message->from('autocheckmalaysia@gmail.com','Autocheck Malaysia');
              });*/
              
      

            /* return redirect('/sent-vehicle')->with(['update' => 'Assessment data successfully downloaded']);
              })->download('xlsx');*/

           return redirect('/sent-vehicle')->with(['update' => 'Vehicle data successfully downloaded']);
              })->download('xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = VehicleChecking::where('id',$id)->update(array('is_sent' => '0'));

        return redirect('sent-vehicle')->with(['success' => 'Data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
