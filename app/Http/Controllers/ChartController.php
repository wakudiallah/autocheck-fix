<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use DB;

class ChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $monthly_vehicle = DB::table('vehicle_checkings')
        ->select(DB::raw('count(vehicle_checkings.id) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        //->wherein('vehicle_checkings.stage', ['W2', 'W4', 'W100' ]) 
        ->where('vehicle_checkings.status', '40')
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->get();


            $list_vehicle_complete = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_vehicle [] = 0; // if not found, store as 0
               }
            }


        $monthly_reject = DB::table('vehicle_checkings')
        ->select(DB::raw('count(vehicle_checkings.id) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('vehicle_checkings.status', '40') 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
         ->get();


            $list_vehicle_reject = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_reject as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_vehicle_reject [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_vehicle_reject [] = 0; // if not found, store as 0
               }
            }


            /* NOT API */

        $monthly_not_api = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.searching_by) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->wherein('vehicle_checkings.searching_by', ['NOT']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->get();


            $list_not_api = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_not_api as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_not_api [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_not_api [] = 0; // if not found, store as 0
               }
            }
        

        return view('share.chart.index', compact('country', 'fuel', 'supplier', 'type', 'brand', 'vehicle', 'model'))
            ->with('list_vehicle_complete',json_encode($list_vehicle_complete, JSON_NUMERIC_CHECK))
            ->with('list_vehicle_reject',json_encode($list_vehicle_reject, JSON_NUMERIC_CHECK))
            ->with('list_not_api',json_encode($list_not_api, JSON_NUMERIC_CHECK))
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
