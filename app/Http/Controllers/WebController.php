<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carvx\CarvxService;
use Response;
use Carvx\Models\AbstractModel;
use App\Model\VehicleApi;
use App\Model\ParameterFee;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\Share\WController;
use Auth;
use Session;

class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    const USER_UID_HEADER = 'Carvx-User-Uid';

    private $url;
    private $uid = '34ll8i8hPxOY';
    private $key = 'fWU6b-pMs4DWTeSBJE4vcH3heskGEcXdrAFSBcZjV38yZld7DP4PKwXZ4co9lsJF';

    private $needSignature = true;
    private $raiseExceptions = false;
    private $isTest = false;




    public function __construct()
    {
        $this->needSignature = config('carvx_extrareport.needSignature');
        $this->raiseExceptions = config('carvx_extrareport.raiseExceptions');
        $this->isTest = config('carvx_extrareport.isTest');
        $this->url = config('carvx_extrareport.url');
        $this->userUid = config('carvx_extrareport.userUid');
        $this->apiKey = config('carvx_extrareport.apiKey');

        /*$this->parseOptions(
            [
                'needSignature' => 'is_bool',
                'raiseExceptions' => 'is_bool',
                'isTest' => 'is_bool',
            ],
            $options
        );


        $option = ['','']*/


    }
    
    public function url()
    {
        
        return redirect('/public');
    }

    
    public function index()
    {
        
        return view('web.index');
    }

      public function package()
    {
        $package = ParameterPackage::get();

        return view('web.select_package', compact('package'));
    }

     public function sample_report()
    {
        
        return view('web.ample_report');
    }


    public function how_to_buy()
    {
        
        return view('web.how_to_buy');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $vin = $request->vehicle;

        /*check 5 first character coz must add W*/
        $check_vin = substr($vin, 0, 5);


        $goto_dummyW = new WController();
        $goto_dummyW->index($request);

        $chassisNumber = Session::get('chassisNumber');


        
        $options=array(
            'needSignature' => $this->needSignature, 
            'raiseExceptions' => $this->raiseExceptions,
            'isTest' => $this->isTest
        );
        $id_vehicle     = Uuid::uuid4()->tostring();

        //$url           = 'https://carvx.jp/api/v1/create-search';
        $url           = $this->url;
        $userUid       = $this->userUid;
        $apiKey        = $this->apiKey;
        

        $service    = new CarvxService($url, $userUid, $apiKey, $options);
        $search     = $service->createSearch($chassisNumber);
        

        $fee_extrareport = ParameterFee::where('role_id', 'US')
                ->first();
    

        if(!empty($search->cars[0]->chassisNumber)){

        /*$data                   =  new VehicleApi;
        $data->id_vehicle       =$id_vehicle;
        $data->vehicle          =$search->cars[0]->chassisNumber;
        $data->country_origin   ='';
        $data->brand            =$search->cars[0]->make;
        $data->model            =$search->cars[0]->model;
        $data->engine_number    =$search->cars[0]->engine;
        $data->cc               ='';
        $data->fuel_type        ='';
        $data->year_manufacture =$search->cars[0]->manufactureDate;
        $data->registation_date ='';
        $data->status           ='complete';
        $data->request_by       ='1';
        $data->save();*/

       // $sign = 'car_id1is_test0search_idAhxu6wieyWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';

        //$hashedPassword = hash('sha256', $sign);

            /*$me = Auth::user()->role_id;

            if($me == 'NA'){ //check if Group  login to admin
                return redirect('/')->with(['warning' => 'Sorry, Your Account already as group account']);
            }
            else{

                return view('web.search_result', compact('search'));
            }*/



            return view('web.search_result', compact('search', 'fee_extrareport'));
             
        }
        else{
            return redirect('/')->with(['warning' => 'Sorry, Vehicle Not Found']);
        }



        //var_dump($report);
        //exit();

         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
