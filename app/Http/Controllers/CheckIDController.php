<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use App\Model\HistorySearchVehicle;
use App\Model\ReportVehicle;
use App\Model\VehicleStatusMatch;



class CheckIDController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }


     public function get_sync_halfreport()
    {
        
        
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_check_id', '1')
            ->whereNull('is_sync')
            ->get();

        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_check_id', '1')
            ->whereNull('is_sync')
            ->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('is_check_id', '1')
            ->whereNull('is_sync')
            ->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.tasklist.index', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        
    }


    public function get_sync_fullreport()
    {
       
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->whereNull('not_found_id_carvx')->get();

        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->whereNull('not_found_id_carvx')->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->whereNull('not_found_id_carvx')->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();
        
        return view('verifier.tasklist.short_report_kastam', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }


    public function get_sync_extrareport()
    {
        
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();
        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.tasklist.index_kastam', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        
    }


    



    


    




    public function submit_id_full_report(Request $request, $id)
    {
	$engine = $request->engine;

	$form= new ReportVehicle();
        $form->id_vehicle = $id;;
        $form->report_id = $engine;
        $form->is_ready = "3";
        $form->data = "1";
        $form->save();

	VehicleChecking::where('id_vehicle',$id)->update(array('is_check_id' => '1', 'is_sync' => '0'));

	return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }


    public function submit_id_extra_report(Request $request, $id)
    {
	$engine = $request->engine;

	$form= new ReportVehicle();
        $form->id_vehicle = $id;;
        $form->report_id = $engine;
        $form->is_ready = "3";
        $form->data = "1";
        $form->save();

	return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }	


}

