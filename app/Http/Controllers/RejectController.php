<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\ParameterEmail;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\Model\BatchSent;
use App\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use Excel;
use Illuminate\Support\Facades\Mail;

class RejectController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
    	$me  = Auth::user()->role_id;
    	$group_me  = Auth::user()->user_group_id;
    	$role_cadealer = Auth::user()->role_id;

	   $is_role_kastam = Auth::user()->is_role_kastam;

       $mygroup = Auth::user()->real_user_group_id;

    	if($me == 'VER'){

    		$data = VehicleChecking::where('status', '30')->orderBy('id', 'DESC')->get();
    	}
    	elseif($role_cadealer == 'NA'){

    		$data = VehicleChecking::where('status', '30')
                ->where('group_by', $mygroup )
                ->orderBy('id', 'DESC')
                ->get();

    	}
    	elseif($me == 'KA' OR $me == 'US'){
    		$data = VehicleChecking::where('status', '30')
                ->where('group_by', $mygroup )
                ->orderBy('id', 'DESC')
                ->get();
    	}

    	return view('share.reject.index', compact('data'));
    	
    }
}
