<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class VehicleVerifiedHalf extends Model
{
    //vehicle_verified_halfs

    protected $table    = 'vehicle_verified_halfs';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;



    public function half_to_co() {
        return $this->belongsTo('App\Model\ParameterCountryOrigin', 'country_origin','id'); 
    }


}
