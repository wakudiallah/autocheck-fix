<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class UserApiStatus extends Model
{
    protected $table    = 'user_api_statuses';

    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    
    public function user_api_status_to_parameter_type_report() {
        return $this->belongsTo('App\Model\ParameterTypeReport', 'type_report_id','id'); 
    }


    public function user_api_status_to_user() {
        return $this->belongsTo('App\User', 'user_id','id'); 
    }
}
