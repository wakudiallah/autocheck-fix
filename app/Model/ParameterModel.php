<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class ParameterModel extends Model
{
    protected $table 	= 'parameter_models';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	public function par_model() {
		return $this->belongsTo('App\Model\Brand','brand_id','id');	
		//Postcode dipunyai city
	}
}
