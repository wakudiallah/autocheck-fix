<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class VehicleChecking extends Model
{
    protected $table 	= 'vehicle_checkings';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


	public function supplier() {
        return $this->belongsTo('App\Model\ParameterSupplier', 'supplier_id','id'); 
    }

    public function type() {
        return $this->belongsTo('App\Model\ParameterTypeVehicle', 'type_id','id'); 
    }

    public function co() {
        return $this->belongsTo('App\Model\ParameterCountryOrigin', 'country_origin_id','id'); 
    }

    public function brand() {
        return $this->belongsTo('App\Model\Brand', 'brand_id','id'); 
    }

    public function model_brand() {
        return $this->belongsTo('App\Model\ParameterModel', 'model_id','id'); 
    }

    public function fuel() {
        return $this->belongsTo('App\Model\ParameterFuel', 'fuel_id','id'); 
    }

    public function request_by() {
        return $this->belongsTo('App\User', 'created_by', 'id'); 
    }


    public function status_vehicle() {
        return $this->belongsTo('App\Model\VehicleStatusMatch', 'id_vehicle','id_vehicle'); 
    }

    public function vehicle_api() {
        return $this->belongsTo('App\Model\VehicleApi', 'id_vehicle','id_vehicle'); 
    }

    public function vehicle_manual() {
        return $this->belongsTo('App\Model\VehicleManual', 'id_vehicle','id_vehicle'); 
    }

    public function status_carvx() {
        return $this->belongsTo('App\Model\ReportVehicle', 'id_vehicle','id_vehicle')->latest(); 
    }

    //get Name Group
    public function request_from() {
        return $this->belongsTo('App\User', 'group_by', 'role_id'); 
    }

    //get verifier name
    public function name_verifier() {
        return $this->belongsTo('App\User', 'updated_by', 'id'); 
    }

    public function group() {
        return $this->belongsTo('App\Model\UserGroup', 'group_by', 'user_id'); 
    }

    public function group_company() {
        return $this->belongsTo('App\Model\UserGroup', 'company_name', 'user_id'); 
    }


    public function vehiclechecking_to_status() {
        return $this->belongsTo('App\Model\ParameterStatus', 'status', 'code'); 
    }


    public function vehiclehalf() {
        return $this->belongsTo('App\Model\VehicleVerifiedHalf', 'id_vehicle', 'id_vehicle'); 
    }


    public function vehiclefull() {
        return $this->belongsTo('App\Model\VehicleApiKastam', 'id_vehicle', 'vehicle_id'); 
    }




}
