<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Support\Facades\Route;
use Request;

class UserGroup extends Model
{
    protected $table 	= 'user_groups';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	protected $fillable = [
        'group_name', 'user_id'
    ];

    public function supplier() {
        return $this->belongsTo('App\Model\ParameterSupplier', 'supplier_id','id'); 
    }
    
	public function total_group_user() {
        return $this->hasMany('App\User', 'user_group_id','user_id');    
    }

	public function count_jan() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '01')->whereYear('created_at', date("Y"));   
    }

    public function count_feb() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '02')->whereYear('created_at', date("Y"));   
    }


    public function count_mar() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '03')->whereYear('created_at', date("Y"));   
    }


    public function count_apr() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '04')->whereYear('created_at', date("Y"));   
    }


    public function count_may() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '05')->whereYear('created_at', date("Y"));   
    }

    public function count_jun() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '06')->whereYear('created_at', date("Y"));   
    }


    public function count_jul() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '07')->whereYear('created_at', date("Y"));   
    }


    public function count_aug() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '08')->whereYear('created_at', date("Y"));   
    }


    public function count_sep() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '09')->whereYear('created_at', date("Y"));   
    }

    public function count_oct() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '10')->whereYear('created_at', date("Y"));   
    }

    public function count_nov() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '11')->whereYear('created_at', date("Y"));   
    }

    public function count_des() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '12')->whereYear('created_at', date("Y"));   
    }




	/* Car Dealer */
    public function count_jan_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '01')->whereYear('created_at', date("Y"));   
    }

    public function count_feb_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '02')->whereYear('created_at', date("Y"));   
    }


    public function count_mar_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '03')->whereYear('created_at', date("Y"));   
    }


    public function count_apr_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '04')->whereYear('created_at', date("Y"));   
    }


    public function count_may_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '05')->whereYear('created_at', date("Y"));   
    }

    public function count_jun_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '06')->whereYear('created_at', date("Y"));   
    }


    public function count_jul_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '07')->whereYear('created_at', date("Y"));   
    }


    public function count_aug_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '08')->whereYear('created_at', date("Y"));   
    }


    public function count_sep_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '09')->whereYear('created_at', date("Y"));   
    }

    public function count_oct_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '10')->whereYear('created_at', date("Y"));   
    }

    public function count_nov_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '11')->whereYear('created_at', date("Y"));   
    }

    public function count_des_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','user_id')->whereMonth('created_at', '12')->whereYear('created_at', date("Y"));   
    }

	
	

    //get By Year request//
    public function count_jan_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '01')->whereYear('created_at', Request::segment(2));   
    }

    public function count_feb_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '02')->whereYear('created_at', Request::segment(2));    
    }


    public function count_mar_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '03')->whereYear('created_at', Request::segment(2));    
    }


    public function count_apr_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '04')->whereYear('created_at', Request::segment(2));    
    }


    public function count_may_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '05')->whereYear('created_at', Request::segment(2));    
    }

    public function count_jun_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '06')->whereYear('created_at', Request::segment(2));    
    }


    public function count_jul_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '07')->whereYear('created_at', Request::segment(2));    
    }


    public function count_aug_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '08')->whereYear('created_at', Request::segment(2));    
    }


    public function count_sep_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '09')->whereYear('created_at', Request::segment(2));    
    }

    public function count_oct_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '10')->whereYear('created_at', Request::segment(2));    
    }

    public function count_nov_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '11')->whereYear('created_at', Request::segment(2));    
    }

    public function count_des_by_year_kastam() {
        return $this->hasMany('App\Model\VehicleChecking', 'company_name','branch')->whereMonth('created_at', '12')->whereYear('created_at', Request::segment(2));    
    }
    //get By Year request//



    //get request by Car Dealer //
    /* Car Dealer */
    public function count_jan_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '01')->whereYear('created_at', Request::segment(2));    
    }

    public function count_feb_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '02')->whereYear('created_at', Request::segment(2));    
    }


    public function count_mar_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '03')->whereYear('created_at', Request::segment(2));    
    }


    public function count_apr_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '04')->whereYear('created_at', Request::segment(2));   
    }


    public function count_may_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '05')->whereYear('created_at', Request::segment(2));    
    }

    public function count_jun_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '06')->whereYear('created_at', Request::segment(2));    
    }


    public function count_jul_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '07')->whereYear('created_at', Request::segment(2));    
    }


    public function count_aug_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '08')->whereYear('created_at', Request::segment(2));   
    }


    public function count_sep_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '09')->whereYear('created_at', Request::segment(2));    
    }

    public function count_oct_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '10')->whereYear('created_at', Request::segment(2));    
    }

    public function count_nov_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '11')->whereYear('created_at', Request::segment(2));    
    }

    public function count_des_by_year_car_dealer() {
        return $this->hasMany('App\Model\VehicleChecking', 'group_by','user_id')->whereMonth('created_at', '12')->whereYear('created_at', Request::segment(2));    
    }

    //end get request by Car Dealer//



    public function user_group_type_report() {
        return $this->belongsTo('App\Model\UserGroupTypeReport', 'id', 'user_group_id'); 
    }
   
}
