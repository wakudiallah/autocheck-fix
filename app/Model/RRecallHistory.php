<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class RRecallHistory extends Model
{
    protected $table 	= 'r_recall_histories';

	protected $guarded = ["id"]; 
}
