<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class UserGroupRelationUser extends Model
{
    protected $table    = 'user_group_relation_users';


    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;


    public function user_group_relation_user_to_parameter_type_report() {

        return $this->belongsTo('App\Model\ParameterTypeReport','type_report_id', 'id_type_report'); 

    }


    public function user_group_relation_user_to_user() {

        return $this->belongsTo('App\User','user_id', 'id'); 

    }


}

