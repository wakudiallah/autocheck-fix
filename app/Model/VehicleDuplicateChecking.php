<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class VehicleDuplicateChecking extends Model
{
    protected $table    = 'vehicle_duplicate_checkings';


    use SoftDeletes;

    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;
}
