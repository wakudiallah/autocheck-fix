<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class VehiclePast extends Model
{
    protected $table 	= 'vehicle_pasts';


	protected $guarded = ["id"]; 


	public function modelpast() {
        return $this->belongsTo('App\Model\ParameterModel', 'ModelId','id'); 
    }

	public function userpast() {
        return $this->belongsTo('App\User', 'UserId','id'); 
    }

}
