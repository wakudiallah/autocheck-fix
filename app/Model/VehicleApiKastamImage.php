<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class VehicleApiKastamImage extends Model
{
    protected $table 	= 'vehicle_api_kastam_images';



	protected $guarded = ["id"]; 
	public $timestamps = true;
}
