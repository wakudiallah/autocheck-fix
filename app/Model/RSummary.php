<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use DB;

class RSummary extends Model
{
    protected $table 	= 'r_summaries';

	protected $guarded = ["id"]; 
}
