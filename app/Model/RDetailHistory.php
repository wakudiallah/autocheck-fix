<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class RDetailHistory extends Model
{
    
	protected $table 	= 'r_detail_histories';


	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
