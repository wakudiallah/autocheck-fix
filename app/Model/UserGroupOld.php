<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Support\Facades\Route;
use Request;

class UserGroupOld extends Model
{
    
    protected $table 	= 'user_group_olds';


    protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;



	public function count_jan() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '01')->whereYear('CreationTime', date("Y"));   
    }

    public function count_feb() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '02')->whereYear('CreationTime', date("Y"));   
    }


    public function count_mar() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '03')->whereYear('CreationTime', date("Y"));   
    }


    public function count_apr() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '04')->whereYear('CreationTime', date("Y"));   
    }


    public function count_may() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '05')->whereYear('CreationTime', date("Y"));   
    }

    public function count_jun() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '06')->whereYear('CreationTime', date("Y"));   
    }


    public function count_jul() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '07')->whereYear('CreationTime', date("Y"));   
    }


    public function count_aug() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '08')->whereYear('CreationTime', date("Y"));   
    }


    public function count_sep() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '09')->whereYear('CreationTime', date("Y"));   
    }

    public function count_oct() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '10')->whereYear('CreationTime', date("Y"));   
    }

    public function count_nov() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '11')->whereYear('CreationTime', date("Y"));   
    }

    public function count_des() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '12')->whereYear('CreationTime', date("Y"));   
    }






    public function count_jan_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '01')->whereYear('CreationTime', Request::segment(2));   
    }

    public function count_feb_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '02')->whereYear('CreationTime', Request::segment(2));    
    }


    public function count_mar_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '03')->whereYear('CreationTime', Request::segment(2));    
    }


    public function count_apr_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '04')->whereYear('CreationTime', Request::segment(2));    
    }


    public function count_may_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '05')->whereYear('CreationTime', Request::segment(2));    
    }

    public function count_jun_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '06')->whereYear('CreationTime', Request::segment(2));    
    }


    public function count_jul_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '07')->whereYear('CreationTime', Request::segment(2));    
    }


    public function count_aug_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '08')->whereYear('CreationTime', Request::segment(2));    
    }


    public function count_sep_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '09')->whereYear('CreationTime', Request::segment(2));    
    }

    public function count_oct_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '10')->whereYear('CreationTime', Request::segment(2));    
    }

    public function count_nov_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '11')->whereYear('CreationTime', Request::segment(2));    
    }

    public function count_des_by_year() {
        return $this->hasMany('App\Model\VehiclePast', 'UserId', 'user_group_old_id')->whereMonth('CreationTime', '12')->whereYear('CreationTime', Request::segment(2));    
    }
    //get By Year request//

}

