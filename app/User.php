<?php

namespace App;


use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable

{

    use HasApiTokens, Notifiable;

    /**

     * The attributes that are mass assignable.

     *
     * @var array

     */

    protected $table    = 'users';


    /*use SoftDeletes;


    protected $guarded = ["id"]; 
    protected $dates   = ['deleted_at'];
    public $timestamps = true;*/



    protected $fillable = [
        'name', 'email', 'password', 'address', 'fax', 'company_name', 'company_description',  'role_id', 'phone'
    ];



    /**
     * The attributes that should be hidden for arrays

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];





    public function role() {

        return $this->belongsTo('App\Model\Role','role_id','id_role'); //id =  table user == model_id = table model has roles

    }



    public function detail_user() {

        return $this->belongsTo('App\Model\DetailBuyer', 'id','user_id'); //id =  table user == model_id = table model has roles

    }


    public function case() {
        return $this->belongsTo('App\Model\VehicleChecking', 'id','created_by')->latest();

    }


    public function history() {

        return $this->belongsTo('App\Model\HistoryUser', 'id','user_id')->latest(); 

    }



    public function user_group() {

        return $this->belongsTo('App\Model\UserGroup', 'user_group_id','user_id'); 

    }


    public function user_kartam() {

        return $this->belongsTo('App\Model\ParameterBranchKastam','is_role_kastam', 'id'); 

    }

	
	public function user_to_user_api_status() {

        	return $this->belongsTo('App\Model\UserApiStatus','id', 'user_id')->where('status','1'); 

    }

    public function user_relate_usergroup() {

        return $this->belongsTo('App\Model\UserGroup', 'real_user_group_id','user_id'); 

    }

}

