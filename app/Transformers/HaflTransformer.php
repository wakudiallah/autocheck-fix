<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehicleStatusMatch;


class HaflTransformer extends TransformerAbstract
{

	public function transform ($vin)
	{				
			
	    /* Param */
	    $id_vehicle     = Uuid::uuid4()->tostring();

	    $is_sent = '1';
            $api_from = 'CARVX';
            $marii = '0';
	    /* End Param */


			$options=array(
	            'needSignature' => '1', 
	            'raiseExceptions' => '1',
	            'isTest' => '1'
	        );

	        $url           = 'https://carvx.jp';
	        $userUid       = 'Hhpj765XV4WX';
	        $apiKey        = 'wk4PCEyk5cs_mFhddh25u7w1FT00n4HOzm5RUhGuSKligmYQWHfSXlxjWDdpxBva';


	        $service = new CarvxService($url, $userUid, $apiKey, $options);
	        $search = $service->createSearch($vin);



	        if(!empty($search->cars[0]->chassisNumber)){

		    $api_from = 'CARVX';

                    $data                   =  new VehicleApi;
                    $data->api_from         = $api_from;
                    //$data->istest           = $is_test; //
                    $data->search_id        = $search->uid; //
                    $data->car_id           = $search->cars[0]->carId; //
                    $data->id_vehicle       = $id_vehicle;
                    $data->vehicle          = $search->cars[0]->chassisNumber;
                    $data->country_origin   = '';
                    $data->brand            = $search->cars[0]->make;
                    $data->model            = $search->cars[0]->model;
                    $data->engine_number    = $search->cars[0]->engine;
                    $data->cc               = '';
                    $data->fuel_type        = '';
                    $data->year_manufacture = $search->cars[0]->manufactureDate;
                    $data->registation_date = '';
                    $data->status           = 'found';
                    $data->request_by       = $marii;
                    $data->save();



	             $carId      = $search->cars[0]->carId;
                     $searchId   = $search->uid;
                     $isTest     = "1";

	        }

	        //$reportId   = $service->createReport($searchId, $carId, $isTest);
        	/*$reportId   = "dummy";


	        $data                   =  new ReportVehicle;
            $data->id_vehicle       = $id_vehicle;
            $data->report_id        = $reportId;
            $data->save(); */


            


            	    $data                          =  new VehicleChecking;   
		    $data->id_vehicle              = $id_vehicle;
		    $data->vehicle                 = $vin;
		    $data->created_by              =  $marii;
		    $data->status                  =  '20';
		    $data->searching_by		       = 'API';
		    $data->is_sent 		           = '0';
		    $data->created_by		       = $marii;
		    $data->group_by                = 'NA';
		    $data->type_report	           = "3";

		    $data->brand_id	           = "1";  //dummy
		    $data->model_id	           = "1"; //dummy

		    $data->save();

	            
            //history search
			$data                       =  new HistorySearchVehicle;
			$data->vehicle              = $vin;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $marii;
			$data->save();


			//history user            
			$data                       =  new HistoryUser;
			$data->vehicle_id           = $vin;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $marii;
			$data->save();


			$data                   =  new VehicleStatusMatch;
            $data->id_vehicle = $id_vehicle;
            $data->vehicle = $vin;
            $data->brand = '0';
            $data->model = '0';
            /*$data->engine_number =  $match_engine_number;*/
            $data->status =  '0';
            $data->type_verify      = '0';
            $data->save();

	       

        	return[
			
				"data" => "S",
				"id"   => $id_vehicle,


			];

		
	}
}

