<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\ReportVehicle;


class ExtraTransformer extends TransformerAbstract
{

	public function transform ($chassisNumber)
	{				
			/* ============== Extra Report ============= */

			$options=array(
	            'needSignature' => '1', 
	            'raiseExceptions' => '1',
	            'isTest' => '1'
	        );

	        $url           = 'https://carvx.jp';
	        $userUid       = '34ll8i8hPxOY';
	        $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';



	        $service = new CarvxService($url, $userUid, $apiKey, $options);
	        $search = $service->createSearch($chassisNumber);

	        //https://carvx.jp/api/v1/create-search

	        $id_vehicle     = Uuid::uuid4()->tostring();


	        //$reportId   = $service->createReport($searchId, $carId, $isTest);
	        $reportId   = "dummy";


        	$data                   =  new ReportVehicle;
            $data->id_vehicle       = $id_vehicle;
            $data->report_id        = $reportId;
            $data->save();


            $is_sent = '1';
            $api_from = 'CARVX';
            $marii = '99';

            $data                   =  new VehicleApi;
            $data->api_from         = $api_from;
            $data->search_id        = $search->uid; //
            $data->car_id           = $search->cars[0]->carId; //
            $data->id_vehicle       = $id_vehicle;
            $data->vehicle          = $search->cars[0]->chassisNumber;
            $data->country_origin   = '';
            $data->brand            = $search->cars[0]->make;
            $data->model            = $search->cars[0]->model;
            $data->engine_number    = $search->cars[0]->engine;
            $data->cc               = '';
            $data->fuel_type        = '';
            $data->year_manufacture = $search->cars[0]->manufactureDate;
            $data->registation_date = '';
            $data->status           = 'found';
            $data->request_by       = $marii;
            $data->save();


            $data                        =  new VehicleChecking;
                
            $data->id_vehicle              = $id_vehicle;
            $data->vehicle                 = $chassisNumber;
            $data->real_type_report_id = 'Extra';
            $data->created_by              =  $marii;
            $data->status                  =  '20';
            $data->searching_by				= 'API';
            $data->is_sent 					= '1';
            $data->created_by				= '99';
            $data->group_by                 = 'US';
            
            $data->save();


	        if(!empty($search->cars[0]->chassisNumber)){

	        	return[
				
					"data" => "S",
					"id"   => $id_vehicle,

		            /*"chassis_number" => $search->cars[0]->chassisNumber,
		            "make" => $search->cars[0]->make,
		            "model" => $search->cars[0]->model,
		            "grade" => $search->cars[0]->grade,

		            "body" => $search->cars[0]->body,
		            "engine" => $search->cars[0]->engine,
		            "drive" => $search->cars[0]->drive,
		            "transmission" => $search->cars[0]->transmission,*/

		            //"manufacture_date" => $search->cars[0]->manufacture_date,

				];

	        }else{

	        	return[
				
					"data" => "P",
					"id"   => $id_vehicle,

		            /*"chassis_number" => $search->cars[0]->chassisNumber,
		            "make" => $search->cars[0]->make,
		            "model" => $search->cars[0]->model,
		            "grade" => $search->cars[0]->grade,
		            "body" => $search->cars[0]->body,
		            "engine" => $search->cars[0]->engine,
		            "drive" => $search->cars[0]->drive,
		            "transmission" => $search->cars[0]->transmission,*/

		            //"manufacture_date" => $search->cars[0]->manufacture_date,

				];


	        }
			
		

		
	}
}

