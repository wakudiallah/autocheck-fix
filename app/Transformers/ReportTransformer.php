<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use App\Model\VehicleApiKastamImage;
use League\Fractal\TransformerAbstract;
use App\Model\VehicleStatusMatch;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\ParameterSupplier;
use App\Model\RAuctionImages;
use App\Model\ParameterTypeVehicle;
use DB;
use Illuminate\Support\Facades\Auth;
use Mail;
use Session;
use App\Model\HistoryUser;
use App\Model\DetailBuyer;
use App\User;



class ReportTransformer extends TransformerAbstract
{
	public function transform ($mo)
	{
		
		if($mo->type_report == "2"){  //Extra report

			$id = $mo->id_vehicle;
			
			$data = VehicleChecking::where('id_vehicle', $id)
                ->latest()
                ->first();


            $raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->get();
	        $raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->get();
	        $raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->get();
	        $raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->get();
	        $raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->get();
	        $raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->get();


	        $check_raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->first();
	        $check_raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->first();
	        $check_raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->first();
	        $check_raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->first();
	        $check_raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->first();
	        $check_raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->first();

	        $recall = RRecallHistory::where('id_vehicle', $id)
	                ->get();

	        $summary = RSummary::where('id_vehicle', $id)
	                    ->latest('id')
	                    ->first();

	        $usagehistory = RUsageHistory::where('id_vehicle', $id)
	                ->first();
	                
	        $vehicleassessment = RVehicleAssessment::where('id_vehicle', $id)->latest('id')->first();
	        $vehicledetail = RVehicleDetails::where('id_vehicle', $id)->first();
	        $vehiclespesification = RVehicleSpecification::where('id_vehicle', $id)->first();
	        $auctionimage = RAuctionImages::where('id_vehicle', $id)->orderBy('id', 'DESC')->get();
	        $detailhistory = RDetailHistory::where('id_vehicle', $id)->get();
	        $auctionhistory = RAuctionHistory::where('id_vehicle', $id)->get();
	        $odometerhistory = ROdometerHistory::where('id_vehicle', $id)->get();
	        $vehicle = VehicleChecking::where('id_vehicle', $id)->first();
			

	        if(!empty($vehicledetail->manufactureDate)){

	        	$manufactureDate = $vehicledetail->manufactureDate;

	        }else{

	        	$manufactureDate = "";

	        }


	        if(!empty($vehicledetail->make)){

	        	$make = $vehicledetail->make;

	        }else{

	        	$make = "";

	        }


	        if(!empty($vehicledetail->model)){

	        	$model = $vehicledetail->model;

	        }else{

	        	$model = "";

	        }
                                     
            
            if(!empty($vehicledetail->body)){

            	$body = $vehicledetail->body;

            }else{

            	$body = "";
            }


            if(!empty($vehicledetail->grade)){

            	$grade = $vehicledetail->grade;

            }else{

            	$grade = "";

            }


            if(!empty($vehicledetail->engine)){

            	$engine = $vehicledetail->engine;

            }else{

            	$engine = "";

            }


            if(!empty($vehicledetail->drive)){

            	$drive = $vehicledetail->drive;

            }else{

            	$drive = "";

            }
                                      
                                      
            if(!empty($vehicledetail->transmission)){

            	$transmission = $vehicledetail->transmission;

            }else{

            	$transmission = "";

            }
            
            $result = json_decode($auctionimage, true);
            $count_result = count($result);



            for($i=0; $i<$count_result; $i++){

            	//dd($result[$i]["imagebit64"]);

            }

            //dd($result["data"]["imagebit64"]);


			return[
				
				'status' => 'C',
				'chassisNumber' => $data->vehicle,
				'manufactureDate' => $manufactureDate,
				'make' => $make,
				'model' => $model,
				'body' => $body,
				'grade' => $grade,
				'engine' => $engine,
				'drive' => $drive,
				'transmission' => $transmission,
				'collision_reported' => $raccident_collision,
				'malfunction_reported' => $raccident_malfunction,
				'theft_reported' => $raccident_theft,
				'fire_demage_reported' => $raccident_fireDamage,
				'water_demage_reported' => $raccident_waterDamage,
				'hail_demage_reported' => $raccident_hailDamage,
				'summary' => $summary,
				'odometer' => $odometerhistory,
				'usagehistory' => $usagehistory,
				'detailhistory' => $detailhistory,
				'recall' => $recall,
				'vehicleassessment' => $vehicleassessment,
				'vehiclespesification' => $vehiclespesification,
				'auctionhistory' => $auctionhistory,
				
            	'image' => $auctionimage,
				
			];

			//return \Response::json($data);



		}elseif($mo->type_report == '3'){ //Cardealer

			if($mo->searching_by == 'API'){
				$vehicleapi = VehicleApi::where('id_vehicle', $mo->id_vehicle)->first();

				$country_origin = ParameterCountryOrigin::addSelect('id','country_origin')->where('id', $mo->country_origin_id)->first();

				$fuel = ParameterFuel::addSelect('id','fuel')->where('id', $mo->fuel_id)->first();

				$regist_date =  date('F Y', strtotime($vehicleapi->registation_date));

				return[
					
					'status' => 'C',
					'chassisNumber' => $mo->vehicle,
					'country_origin' => $country_origin,
					'make' => $vehicleapi->brand,
					'model' => $vehicleapi->model,
					'engine' => $mo->engine_number,
					'displacement' => $vehicleapi->cc,
					'fuel' => $fuel,
					'year_manufacture' => $vehicleapi->year_manufacture,
					'first_registration_date' => $regist_date,  
					
				];


			}else{


				/*$vehiclemanual = VehicleManual::where('id_vehicle', $mo->id_vehicle)
					->latest()
					->first();*/

				$data = VehicleChecking::where('id_vehicle', $mo->id_vehicle)
                ->latest()
                ->first();


				$country_origin = ParameterCountryOrigin::addSelect('id','country_origin')->where('id', $mo->country_origin_id)->first();

				$y  = explode('-', $data->vehiclehalf->year_manufacture);
        				$year = $y[0];

        		$regist_date =  date('F Y ', strtotime($data->vehiclehalf->registation_date));

        		$fuel = ParameterFuel::addSelect('id','fuel')->where('id', $mo->fuel_id)->first();

				return[
					'data' => 'C',
					'chassisNumber' => $mo->vehicle,
					'country_origin' => $country_origin,
					'make' => $data->vehiclehalf->brand,
					'model' => $data->vehiclehalf->model,
					'engine' => $data->vehiclehalf->engine_number,
					'displacement' => $data->vehiclehalf->cc,
					'fuel' => $data->vehiclehalf->fuel_type,
					'year_manufacture' => $year,
					'first_registration_date' => $regist_date,
					
				];

			}

		

		}
		elseif($mo->type_report == "4" AND empty($mo->old_report)){   //naza report

			if($mo->searching_by == 'API'){

        		$api_kastam = VehicleApiKastam::where('vehicle_id', $mo->id_vehicle)->first();

        		$manufactureDatex =  date('d F Y ', strtotime($api_kastam->manufactureDate));

				$date_of_origin =  date('d F Y ', strtotime($api_kastam->date_of_origin));

			$image_api = VehicleApiKastamImage::where('vehicle_id', $mo->id_vehicle)->get();
		

				return[
					
					'data' => 'C',
					'chassisNumber' => $mo->vehicle,
					'make' => $api_kastam->make,
					'model' => $api_kastam->model,
					'engine' => $api_kastam->engine,
					'body' => $api_kastam->body,
					'grade' => $api_kastam->grade,
					'year_manufacture' => $manufactureDatex,
					'first_registration_date' => $date_of_origin,
					'drive' => $api_kastam->drive,
					'transmission' => $api_kastam->transmission,
					'displacement' => $api_kastam->displacement_cc,
					'average_market' => $api_kastam->average_market,
					
					'image' => DB::table('vehicle_api_kastam_images')->where('vehicle_id', $mo->id_vehicle)->get(),
								

				];

			}else{


				$api_kastam = VehicleApiKastam::where('vehicle_id', $mo->id_vehicle)->first();

				$manufactureDate =  date('d F Y ', strtotime($api_kastam->manufactureDate));

				$date_of_origin =  date('d F Y ', strtotime($api_kastam->date_of_origin));
				

				return[
					
					'status' => 'C',
					'chassisNumber' => $mo->vehicle,
					'make' => $api_kastam->make,
					'model' => $api_kastam->model,
					'engine' => $api_kastam->engine,
					'body' => $api_kastam->body,
					'grade' => $api_kastam->grade,
					'year_manufacture' => $manufactureDate,
					'first_registration_date' => $date_of_origin,
					'drive' => $api_kastam->drive,
					'transmission' => $api_kastam->transmission,
					'displacement' => $api_kastam->displacement_cc,


				];
			}
		}
		
	}
}

