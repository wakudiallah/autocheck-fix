<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;


class ExtraMnTransformer extends TransformerAbstract
{

	public function transform ($id_vehicle)
	{				

		/* ============== Extra Report ============= */
			
	        //$id_vehicle     = Uuid::uuid4()->tostring();
			$vehicleChecking = VehicleChecking::where('id_vehicle', $id_vehicle)->first();

			$marii = '0';

			$reportId = date("Ymd his");
 

	    	$data                   =  new ReportVehicle;
	    	$data->id_vehicle       = $id_vehicle;
	    	$data->report_id        = $reportId;
	    	$data->save();


		    /*$data                          =  new VehicleChecking;   
		    $data->id_vehicle              = $id_vehicle;
		    $data->vehicle                 = $vin;
		    $data->real_type_report_id = 'Extra';
		    $data->created_by              =  $marii;
		    $data->status                  =  '10';
		    $data->searching_by		       = 'NOT';
		    $data->is_sent 		           = '0';
		    $data->created_by		       = $marii;
		    $data->group_by                = 'US';
		    $data->type_report			   = "2";
		    $data->save();*/


		    $data = VehicleChecking::where('id_vehicle',$id_vehicle)->update(array(
                        'status' => '10',
                        'real_type_report_id' => "Extra",
                        'searching_by' => 'NOT',
                        'is_sent' => '0',
                        'group_by' => 'US',
                        'type_report' => '2' 
                    ));


			//history search
			$data                       =  new HistorySearchVehicle;
			$data->vehicle              = $vehicleChecking->vehicle;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $vehicleChecking->created_by;
			$data->save();


			//history user            
			$data                       =  new HistoryUser;
			$data->vehicle_id           = $vehicleChecking->vehicle;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $vehicleChecking->created_by;
			$data->save();


	        return[
				
				"status" => "S",
				"id" => $id_vehicle,

			];

	        
	}
}

