<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;


class FullTransformer extends TransformerAbstract
{

	public function transform ($chassisNumber)
	{				
			
		/* ==========   check vehicle for ready or not ========= */
			$id_vehicle     = Uuid::uuid4()->tostring();

			$options=array(
	            'needSignature' => '1', 
	            'raiseExceptions' => '1',
	            'isTest' => '1'
	        );

	        $url           = 'https://carvx.jp';
	        $userUid       = 'h1Nigk43M3rP';
	        $apiKey        = '4g7ifuecHSrEyeNktgNc9V-BnPMuzh03bCrimoCI-QmBCZFQiDLVYwe7u68Zco3t';



	        $service = new CarvxService($url, $userUid, $apiKey, $options);
	        $search = $service->createSearch($chassisNumber);


	        if(!empty($search->cars[0]->chassisNumber)){

	        	$carId      = $search->cars[0]->carId;
                $searchId   = $search->uid;
                $isTest     = "1";

	        }


	        //$reportId   = $service->createReport($searchId, $carId, $isTest);
	        $reportId   = "dummy";

	        /*
        	$data                   =  new ReportVehicle;
            $data->id_vehicle       = $id_vehicle;
            $data->report_id        = $reportId;
            $data->save();*/

            
            $is_sent = '1';
            $api_from = 'CARVX';
            $marii = '0';


            $data                        =  new VehicleChecking;
            $data->id_vehicle            = $id_vehicle;
            $data->vehicle               = $chassisNumber;
            $data->real_type_report_id = 'Full';
            $data->created_by            = $marii;
            $data->status                = '20';
            $data->searching_by			 = 'API';
            $data->is_sent 				 = '0';
            $data->created_by			 = $marii;
            $data->group_by              = 'KA';
            $data->type_report	         = "4";
	   

            $data->save();


            //history search
			$data                       =  new HistorySearchVehicle;
			$data->vehicle              = $chassisNumber;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $marii;
			$data->save();


			//history user            
			$data                       =  new HistoryUser;
			$data->vehicle_id           = $chassisNumber;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $marii;
			$data->save();

            
            /* =============   check vehicle for ready or not =========== */
	        if(!empty($search->cars[0]->chassisNumber)){ /* ==== Ready ===== */

	        	return[
				
					"data" => "S",
					"id"   => $id_vehicle,    

				];

	        }
			
		

		
	}
}

