<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use League\Fractal\TransformerAbstract;

class VehicleTransformer extends TransformerAbstract
{
	public function transform (VehicleChecking $mo)
	{
		
		if($mo->searching_by == 'API' AND $mo->company_name == "KA" OR $mo->company_name == "US"){

			$id = $mo->id_vehicle;

			$vehicleapi = VehicleApi::where('id_vehicle', $mo->id_vehicle)->first();

			$country_origin = ParameterCountryOrigin::addSelect('id','country_origin')->where('id', $mo->country_origin_id)->first();

			$fuel = ParameterFuel::addSelect('id','fuel')->where('id', $mo->fuel_id)->first();

			$regist_date =  date('F Y', strtotime($vehicleapi->registation_date));

			$vehicledetail = RVehicleDetails::where('id_vehicle', $id)->first();
        	$summary = RSummary::where('id_vehicle', $id)->latest('id')->first();

        	$check_raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->count();
	        $check_raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->count();
	        $check_raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->count();
	        $check_raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->count();
	        $check_raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->count();
	        $check_raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->count();


	        $raccident_collision = RAccidentHistory::where('field', 'collision')->where('id_vehicle', $id)->get();
	        $raccident_malfunction = RAccidentHistory::where('field', 'malfunction')->where('id_vehicle', $id)->get();
	        $raccident_theft = RAccidentHistory::where('field', 'theft')->where('id_vehicle', $id)->get();
	        $raccident_fireDamage = RAccidentHistory::where('field', 'fireDamage')->where('id_vehicle', $id)->get();
	        $raccident_waterDamage = RAccidentHistory::where('field', 'waterDamage')->where('id_vehicle', $id)->get();
	        $raccident_hailDamage = RAccidentHistory::where('field', 'hailDamage')->where('id_vehicle', $id)->get();


        	$odometerhistory = ROdometerHistory::where('id_vehicle', $id)->get();

        	$usagehistory = RUsageHistory::where('id_vehicle', $id)->first();

        	$detailhistory = RDetailHistory::where('id_vehicle', $id)->get();
        	$recall = RRecallHistory::where('id_vehicle', $id)->get();

        	$vehicleassessment = RVehicleAssessment::where('id_vehicle', $id)->latest('id')->first();

        	$vehiclespesification = RVehicleSpecification::where('id_vehicle', $id)->first();


        	$auctionhistory = RAuctionHistory::where('id_vehicle', $id)->get();


			return[
				
				'status' => 'full_report',
				'id' => $mo->id,
				/*'vehicle' => $mo->vehicle,
				'country_origin' => $country_origin,
				'brand' => $vehicleapi->brand,
				'model' => $vehicleapi->model,
				'engine_model' => $mo->engine_number,
				'cc' => $vehicleapi->cc,
				'fuel' => $fuel,
				'year_manufacture' => $vehicleapi->year_manufacture,
				'first_registration_date' => $regist_date,  //Month YY*/
				//'token'=> $user->api_token,


				'chassisNumber' => $vehicledetail->chassisNumber,
				'manufactureDate' => $vehicledetail->manufactureDate,
				'make' => $vehicledetail->make,
				'model' => $vehicledetail->model,
				'body' => $vehicledetail->body,
				'grade' => $vehicledetail->grade,
				'engine' => $vehicledetail->engine,
				'drive' => $vehicledetail->drive,
				'transmission' => $vehicledetail->transmission,

				
				'collision_reported' => $raccident_collision,				
				'malfunction_reported' => $raccident_malfunction,
				'theft_reported' => $raccident_theft,
				'fire_demage_reported' => $raccident_fireDamage,
				'water_demage_reported' => $raccident_waterDamage,
				'hail_demage_reported' => $raccident_hailDamage,


				/*'registered' => $summary->registered,
				'accident' => $summary->accident,
				'odometer' => $summary->odometer,
				'recall' => $summary->recall,
				'safetyGrade' => $summary->safetyGrade,
				'buyback' => $summary->buyback,
				'contaminationRisk' => $summary->contaminationRisk,
				'averagePrice' => $summary->averagePrice,*/

				'summary' => $summary,

				'odometer' => $odometerhistory,

				'usagehistory' => $usagehistory,

				'detailhistory' => $detailhistory,

				'recall' => $recall,

				'vehicleassessment' => $vehicleassessment,

				'vehiclespesification' => $vehiclespesification,

				'auctionhistory' => $auctionhistory,




			];

		}elseif($mo->group_by == 'NA'){ //Cardealer

			if($mo->searching_by == 'API'){
				$vehicleapi = VehicleApi::where('id_vehicle', $mo->id_vehicle)->first();

				$country_origin = ParameterCountryOrigin::addSelect('id','country_origin')->where('id', $mo->country_origin_id)->first();

				$fuel = ParameterFuel::addSelect('id','fuel')->where('id', $mo->fuel_id)->first();

				$regist_date =  date('F Y', strtotime($vehicleapi->registation_date));

				return[
					
					'status' => 'short_report1',

					'id' => $mo->id,
					'chassisNumber' => $mo->vehicle,
					'country_origin' => $country_origin,
					'make' => $vehicleapi->brand,
					'model' => $vehicleapi->model,
					'engine' => $mo->engine_number,
					'displacement' => $vehicleapi->cc,
					'fuel' => $fuel,
					'year_manufacture' => $vehicleapi->year_manufacture,
					'first_registration_date' => $regist_date,  //Month YY
					//'token'=> $user->api_token,
				];
			}else{


				$vehiclemanual = VehicleManual::where('id_vehicle', $mo->id_vehicle)->first();

				$country_origin = ParameterCountryOrigin::addSelect('id','country_origin')->where('id', $mo->country_origin_id)->first();

				$y  = explode('-', $mo->vehicle_manual->year_manufacture);
        				$year = $y[0];

        		$regist_date =  date('F Y ', strtotime($mo->vehicle_manual->registation_date));

        		$fuel = ParameterFuel::addSelect('id','fuel')->where('id', $mo->fuel_id)->first();

				return[
					'status' => 'short_report1',
					'id' => $mo->id,
					'chassisNumber' => $mo->vehicle,
					'country_origin' => $country_origin,
					'make' => $mo->brand->brand,
					'model' => $mo->model_brand->model,

					'engine' => $vehiclemanual->engine_number,

					'displacement' => $vehiclemanual->cc,
					'fuel' => $fuel,
					'year_manufacture' => $year,
					'first_registration_date' => $regist_date,
					
				];

			}

		}
		elseif($mo->group_by == "KA" AND empty($mo->old_report)){

			if($mo->searching_by == 'API'){

        		$api_kastam = VehicleApiKastam::where('vehicle_id', $mo->id_vehicle)->first();

        		$manufactureDatex =  date('d F Y ', strtotime($api_kastam->manufactureDate));

				$date_of_origin =  date('d F Y ', strtotime($api_kastam->date_of_origin));

				return[
					
					'status' => 'short_report2',

					'id'=> $mo->id,
					'chassisNumber' => $mo->vehicle,
					'make' => $api_kastam->make,
					'model' => $api_kastam->model,

					'engine' => $api_kastam->engine,
					'body' => $api_kastam->body,
					'grade' => $api_kastam->grade,
					'year_manufacture' => $manufactureDatex,
					'first_registration_date' => $date_of_origin,
					'drive' => $api_kastam->drive,
					'transmission' => $api_kastam->transmission,
					'displacement' => $api_kastam->displacement_cc,

				];

			}else{


				$api_kastam = VehicleApiKastam::where('vehicle_id', $mo->id_vehicle)->first();

				$manufactureDate =  date('d F Y ', strtotime($api_kastam->manufactureDate));

				$date_of_origin =  date('d F Y ', strtotime($api_kastam->date_of_origin));
				

				return[
					
					'status' => 'short_report2',

					'id'=> $mo->id,
					'chassisNumber' => $mo->vehicle,
					'make' => $api_kastam->make,
					'model' => $api_kastam->model,

					'engine' => $api_kastam->engine,
					'body' => $api_kastam->body,
					'grade' => $api_kastam->grade,
					'year_manufacture' => $manufactureDate,
					'first_registration_date' => $date_of_origin,
					'drive' => $api_kastam->drive,
					'transmission' => $api_kastam->transmission,
					'displacement' => $api_kastam->displacement_cc,


				];
			}
		}
		
	}
}
