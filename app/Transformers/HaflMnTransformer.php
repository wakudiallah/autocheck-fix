<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehicleStatusMatch;


class HaflMnTransformer extends TransformerAbstract
{

	public function transform ($id_vehicle)
	{				
			
		/* ==========   check vehicle for ready or not ========= */
			$vehicleChecking = VehicleChecking::where('id_vehicle', $id_vehicle)->first();

			//$id_vehicle     = Uuid::uuid4()->tostring();
			$marii = '0';

			$reportId = date("Ymd his");

	        $data                   =  new ReportVehicle;
	    	$data->id_vehicle       = $id_vehicle;
	    	$data->report_id        = $reportId;
	    	$data->save();


	    	/*$data                          =  new VehicleChecking;   
		    $data->id_vehicle              = $id_vehicle;
		    $data->vehicle                 = $vin;
		    $data->created_by              =  $marii;
		    $data->real_type_report_id = 'Half';
		    $data->status                  =  '10';
		    $data->searching_by		       = 'NOT';
		    $data->is_sent 		           = '0';
		    $data->created_by		       = $marii;
		    $data->group_by                = 'NA';
		    $data->type_report			   = "3";
		    $data->save();*/


		    $data = VehicleChecking::where('id_vehicle',$id_vehicle)->update(array(
                        'status' => '20',
                        'real_type_report_id' => "Half",
                        'searching_by' => 'NOT',
                        'is_sent' => '0',
                        'group_by' => 'NA',
                        'type_report' => '3' 
                    ));


			//history search
			$data                       =  new HistorySearchVehicle;
			$data->vehicle              = $vehicleChecking->vehicle;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $vehicleChecking->created_by;
			$data->save();


			//history user            
			$data                       =  new HistoryUser;
			$data->vehicle_id           = $vehicleChecking->vehicle;
			$data->id_vehicle           = $id_vehicle;
			$data->parameter_history_id = "SEARCH";
			$data->user_id              =  $vehicleChecking->created_by;
			$data->save();


			//status match
			$data                   =  new VehicleStatusMatch;
            $data->id_vehicle 		= $id_vehicle;
            $data->vehicle 			= $vehicleChecking->vehicle;
            $data->brand 			= '0';
            $data->model 			= '0';
            /*$data->engine_number =  $match_engine_number;*/
            $data->status 			= '0';
            $data->type_verify      = '0';
            $data->save();



            /* =============   check vehicle for ready or not =========== */
	       

	        	return[
				
					"data" => "S",
					"id"   => $id_vehicle,

		            

				];

	        
		

		
	}
}

