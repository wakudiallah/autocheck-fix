<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\RVehicleDetails;
use App\Model\RSummary;
use App\Model\RAccidentHistory;
use App\Model\ROdometerHistory;
use App\Model\RUsageHistory;
use App\Model\RDetailHistory;
use App\Model\RRecallHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionHistory;
use App\Model\VehicleApiKastam;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Mail;


class PartnerTransformer extends TransformerAbstract
{


	public function transform ($vin)
	{
		
		
			$options=array(
	            'needSignature' => '1', 
	            'raiseExceptions' => '1',
	            'isTest' => '1'
	        );

	        $url           = 'https://carvx.jp';
	        $userUid       = '34ll8i8hPxOY';
	        $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';



	        $service = new CarvxService($url, $userUid, $apiKey, $options);

	        $search = $service->createSearch($vin);

	       

	        if(!empty($search->cars[0]->chassisNumber)){

	        	
	        	$image = "https://carvx.jp/".$search->cars[0]->image;
	        	$image_base    = base64_encode(file_get_contents($image));


	        	return[
				
					"data" => $search,
					"uid" => $search->data[0]->uid,

		            "chassis_number" => $search->cars[0]->chassisNumber,
		            "make" => $search->cars[0]->make,
		            "model" => $search->cars[0]->model,
		            "grade" => $search->cars[0]->grade,

		            "body" => $search->cars[0]->body,
		            "engine" => $search->cars[0]->engine,
		            "drive" => $search->cars[0]->drive,
		            "transmission" => $search->cars[0]->transmission,
		            "image" => $image_base,
					

					//"manufacture_date" => $search->cars[0]->manufacture_date,

				];

	        }else{

	        	return[
				
					"data" => "Not Found",

		            /*"chassis_number" => $search->cars[0]->chassisNumber,
		            "make" => $search->cars[0]->make,
		            "model" => $search->cars[0]->model,
		            "grade" => $search->cars[0]->grade,
		            
		            "body" => $search->cars[0]->body,
		            "engine" => $search->cars[0]->engine,
		            "drive" => $search->cars[0]->drive,
		            "transmission" => $search->cars[0]->transmission,*/

		            //"manufacture_date" => $search->cars[0]->manufacture_date,

				];


	        }
			
		

		
	}
}

