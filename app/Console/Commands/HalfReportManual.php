<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleChecking;
use App\Model\VehicleVerifiedHalf;
use App\Model\Brand;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use DB;

class HalfReportManual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:migratemn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migration Half Report to New Table (Manual)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       

        $migrate = VehicleChecking::where('group_by', 'NA')
                    ->whereNull('deleted_at')
                    ->where('searching_by', 'NOT')
                    //->where('id_vehicle', '6538b143-f4ab-4391-91c6-91fff0c006e7')
                    ->get();


        foreach($migrate as $data){  

           $brand_get = Brand::where('id', $data->brand_id)
                        ->first();

           $model_get = ParameterModel::where('id', $data->model_id)
                        ->first();

            $fuel_get = ParameterFuel::where('id', $data->fuel_id)
                        ->first();

            $get_vehicle_api = VehicleManual::where('id_vehicle', $data->id_vehicle)
                            ->latest()
                            ->first();
            

            if(empty($get_vehicle_api->engine_number)){
                $engine_number = Null;
            }else{
                $engine_number = $get_vehicle_api->engine_number;
            }


            if(empty($get_vehicle_api->cc)){
                $cc = Null;
            }else{
                $cc = $get_vehicle_api->cc;
            }


            if(empty($get_vehicle_api->year_manufacture)){
                $year_manufacture = Null;
            }else{
                $year_manufacture = $get_vehicle_api->year_manufacture;
            }


            if(empty($get_vehicle_api->registation_date)){
                $registation_date = Null;
            }else{
                $registation_date = $get_vehicle_api->registation_date;
            }


            $today  = date("HisdmY", strtotime($data->created_at));


            DB::table('vehicle_verified_halfs')
            ->insert([
                
                "id_vehicle" =>   $data->id_vehicle, 
                "ver_sn" =>  'HX'.$today,                                
                "partner" =>   'carvx',
                "vehicle" =>   $data->vehicle,
                "country_origin"=>   $data->country_origin_id,
                "brand" =>   $brand_get->brand,
                "model"  => $model_get->model,
                "engine_number"  =>  $engine_number, //api
                "cc"  => $cc, //api
                "fuel_type"  =>  $fuel_get->fuel,
                "year_manufacture"  =>  $year_manufacture,  //api
                "registation_date"  =>  $registation_date,  //api
                "status"  => Null, 
                "request_by"  =>  Null,
                "updated_by"  =>  Null,
                "created_at"  =>  $data->created_at
                  
    
            ]);
        }  

    }
}
