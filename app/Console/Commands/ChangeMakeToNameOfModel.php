<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleApiKastamImage;
use App\Model\VehicleApiKastam;
use App\Model\VehicleChecking;
use App\Model\Brand;
use DB;

class ChangeMakeToNameOfModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:makeidtoname';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Make ID to Name Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $affected = DB::table('vehicle_api_kastams')
              ->where('make', '23')
              ->update(['make' => 'LEXUS']);


        $affected = DB::table('vehicle_api_kastams')
              ->where('make', '28')
              ->update(['make' => 'MERCEDES-BENZ']);


        $affected = DB::table('vehicle_api_kastams')
              ->where('make', '22')
              ->update(['make' => 'LAND ROVER']);


        $affected = DB::table('vehicle_api_kastams')
              ->where('make', '4')
              ->update(['make' => 'BMW']);

        $affected = DB::table('vehicle_api_kastams')
              ->where('make', '3')
              ->update(['make' => 'BENTLEY']);

        $affected = DB::table('vehicle_api_kastams')
              ->where('make', '42')
              ->update(['make' => 'TOYOTA']);

        $affected = DB::table('vehicle_api_kastams')
              ->where('make', '35')
              ->update(['make' => 'PORSCHE']);




        //try {
                        
            /*$get_vehicle = DB::table('vehicle_api_kastams')
            ->join('brands', 'brands.id', '=', 'vehicle_api_kastams.make')
            ->select('brands.brand as brand', 'brands.id as id', 'vehicle_api_kastams.make')
            ->get();


    
            foreach($get_vehicle as $data){
                    if($data->make == 23){



                            VehicleApiKastam::where('make',23)->update(array('make' => $data->brand ));
                    }
                    
                    

            }*/

       /* } catch (\Exception $e) {

            die("The file doesn't exist");
            \Log::warning("cannot change image working done");
        }*/
    }
}
