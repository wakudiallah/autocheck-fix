<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleChecking;

class UpdateRealTypeReportIdVehicleChecking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:update_type_report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Real Type Report Id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $update = VehicleChecking::get();

        foreach($update as $data){

               $data_type_report = VehicleChecking::join('users', 'users.id', '=', 'vehicle_checkings.created_by')
                                                    ->join('user_groups', 'user_groups.user_id', '=', 'users.real_user_group_id')
                                                    ->join('user_group_type_reports', 'user_group_type_reports.user_group_id', '=', 'user_groups.id')
                                                    ->where('vehicle_checkings.id', $data->id)
                                                    //->whereNotNull('users.real_user_group_id')
                                                    //->whereNotNull('user_groups.user_id')
                                                    ->first();

                if(!empty($data_type_report->type_report_id)){

                    $is_type_report = $data_type_report->type_report_id;

                }else{

                    $is_type_report = Null;
                }


                if(!empty($data_type_report->real_user_group_id)){

                    $is_company_name = $data_type_report->real_user_group_id;

                }else{

                    $is_company_name = Null;

                }


                VehicleChecking::where('id', $data->id)->update(array('real_type_report_id' =>  $is_type_report, 'company_name' => $is_company_name));

        }
    }
}
