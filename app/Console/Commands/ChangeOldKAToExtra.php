<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleChecking;
use App\Model\VehicleVerifiedHalf;
use App\Model\Brand;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use DB;

class ChangeOldKAToExtra extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:changeold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change old to real type report Extra';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $migrate = VehicleChecking::where('old_report', '1')
                    ->whereNull('deleted_at')
                    ->get();

        foreach($migrate as $data){ 

            VehicleChecking::where('old_report', '1')->update(array('real_type_report_id' => "Extra" ));
            
        }



    }
}
