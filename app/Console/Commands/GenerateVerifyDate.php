<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleChecking;
use App\Model\HistorySearchVehicle;

class GenerateVerifyDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:verify_date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $update = VehicleChecking::get();

        foreach($update as $data){

            $get_latest_history = HistorySearchVehicle::where('id_vehicle', $data->id_vehicle)->latest('id')->first();

            VehicleChecking::where('id', $data->id)->update(array('verify_date' =>  $get_latest_history->created_at));

        }


    }
}
