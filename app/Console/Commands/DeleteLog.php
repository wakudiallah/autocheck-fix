<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeleteLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:deletelog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Autocheck Delete Log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $src =  config('ac.backup_log_src');
        $hk = config('ac.log_housekeeping');


            try {
                
                shell_exec("find $src* -mtime +$hk -exec rm {} +");

                \Log::info("Cronjob: Autocheck delete log file - working fine");


              } catch (\Exception $e) {
                   
                   die("The file doesn't exist");
                   \Log::warning("Cronjob: Autocheck delete log file - file doesn't exist / hard disk is full");
              }
        
    }
}
