<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleChecking;
use App\Model\VehicleVerifiedHalf;
use App\Model\Brand;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleManual;
use DB;

class HalfReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:migrateapi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migration Half Report to New Table (API)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        

        $migrate = VehicleChecking::where('group_by', 'NA')
                    ->whereNull('deleted_at')
                    ->where('searching_by', 'API')
                    //->where('id_vehicle', '38c08a93-2811-4d67-8b89-d32ce6083223')
                    ->get();


        foreach($migrate as $data){  

           $brand_get = Brand::where('id', $data->brand_id)
                        ->first();

           $model_get = ParameterModel::where('id', $data->model_id)
                        ->first();

            $fuel_get = ParameterFuel::where('id', $data->fuel_id)
                        ->first();

            $get_vehicle_api = VehicleApi::where('id_vehicle', $data->id_vehicle)
                            ->latest()
                            ->first();

            if(empty($fuel_get->fuel)){
                $fuel = Null;
            }else{
                $fuel = $fuel_get->fuel;
            }

            $today  = date("HisdmY", strtotime($data->created_at));


            DB::table('vehicle_verified_halfs')
            ->insert([
                
                "id_vehicle" =>   $data->id_vehicle, 
                "ver_sn" =>  'HX'.$today,                                
                "partner" =>   'carvx',
                "vehicle" =>   $data->vehicle,
                "country_origin"=>   $data->country_origin_id,
                "brand" =>   $brand_get->brand,
                "model"  => $model_get->model,
                "engine_number"  =>  $get_vehicle_api->engine_number, //api
                "cc"  => $get_vehicle_api->cc, //api
                "fuel_type"  =>  $fuel,
                "year_manufacture"  =>  $get_vehicle_api->year_manufacture,  //api
                "registation_date"  =>  $get_vehicle_api->registation_date,  //api
                "status"  => Null, 
                "request_by"  =>  Null,
                "updated_by"  =>  Null,
                "created_at"  =>  $data->created_at
                  
    
            ]);
        }  

        
    }
}
