<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LogRotation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:logrotation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Autoocheck Log Rotation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $date = date('m-y');

        $yesterday = date('Y-m-d',strtotime("-1 days"));
        $log = 'laravel.log';

        $src =  config('ac.backup_log_src').$log;
        $dest = config('ac.backup_log_src')."laravel-".$yesterday.".log";


        try {
          

            shell_exec("cp $src $dest");
            shell_exec("rm $src");

            shell_exec("chown -R apache.apache $dest");


            \Log::info("Cronjob: DCA Log Rotation - working fine");
            shell_exec("chown -R apache.apache $src");

        } catch (\Exception $e) {

            \Log::warning("Cronjob: DCA Log Rotation - file doesn't exist / hard disk is full");

        }


    }
}
