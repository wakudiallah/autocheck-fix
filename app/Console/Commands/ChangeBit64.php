<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\RAuctionImages;

class ChangeBit64 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:64';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change image to bit64';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            

            $get_image = RAuctionImages::get();

            foreach($get_image as $get_image){

                $b64image = base64_encode(file_get_contents($get_image->image));

                $data = RAuctionImages::where('id',$get_image->id)->update(array('imagebit64' => $b64image ));

                
            }

            \Log::info("Change image working done");


        } catch (\Exception $e) {
               
               die("The file doesn't exist");
               \Log::warning("cannot change image working done");
        }
    }
}
