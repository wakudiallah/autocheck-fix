<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\UserGroup;
use App\User;

class UpdateRealUserGroupId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:update_user_group_id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Real Group User Id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $update = User::get();

        foreach($update as $data){

                /*update is_role_kastam*/
                if($data->email == "k1@gmail.com"){
                    
                    User::where('email', 'k1@gmail.com')->update(array('is_role_kastam' => "99" ));

                }elseif($data->email == "k2@gmail.com"){
                    
                    User::where('email', 'k2@gmail.com')->update(array('is_role_kastam' => "99" ));

                }elseif($data->email == "k3@gmail.com"){

                    User::where('email', 'k3@gmail.com')->update(array('is_role_kastam' => "99" ));

                }



                if(!empty($data->is_role_kastam)){

                    $user_group_ka = UserGroup::join('users', 'users.is_role_kastam', '=', 'user_groups.branch')
                                ->whereNotNull('branch')
                                ->where('branch', $data->is_role_kastam)
                                ->first();
                    $is_user_kastam = $user_group_ka->is_role_kastam;


                    $user = User::where('is_role_kastam', $is_user_kastam)
                        ->update(array('real_user_group_id' => $user_group_ka->user_id ));
       

                }



                /*update CarDealer*/

                if($data->user_group_id == "T"){

                    User::where('user_group_id', 'T')->update(array('user_group_id' => "Car_Dealer_Marii" ));

                }



                if($data->role_id == "NA"){

                    

 

                    $user = User::where('user_group_id', $data->user_group_id)->update(array('real_user_group_id' => $data->user_group_id ));

                }
                
                
        }

    }
}
