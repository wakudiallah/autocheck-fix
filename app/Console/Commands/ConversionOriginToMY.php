<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleApiKastamImage;
use App\Model\VehicleApiKastam;
use App\Model\VehicleChecking;
use App\Model\Brand;
use DB;


class ConversionOriginToMY extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:convertringgit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert Original to Ringgit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //try {      

                  $get = VehicleApiKastam::whereNotNull('average_market')
                        ->whereNull('deleted_at')
                        ->whereIn('id', [13])
                        ->select('id', 'average_market')
                        ->get();



                    foreach($get as $data){

                        if(!empty($data->average_market)){

                            $endpoint = 'live';
                            $access_key = '1b0fd13e42ae1c5932e70779ae0a24ff';

                            // Initialize CURL:
                            $ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'');
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            // Store the data:
                            $json = curl_exec($ch);
                            curl_close($ch);

                            // Decode JSON response:
                            $exchangeRates = json_decode($json, true);

                            $count_country= count($exchangeRates);

                            if(!empty($data->average_market)){
                                $price = $data->average_market;    
                            }else{
                                $price = 0;
                            }
                            



                            $currency_usdyen = $exchangeRates["quotes"]["USDJPY"];
                            $currency_usdmyr = $exchangeRates["quotes"]["USDMYR"];

                            //dd($currency_usdmyr);
                            //$yen = ;

                            $priceusd = $price / $currency_usdyen;
                            $pricemyr = $priceusd * $currency_usdmyr;

                            $rate =  $pricemyr / $price;

                            $rete6 = round($rate, 6);

                            $today = date("Y-m-d");

                        
                            VehicleApiKastam::where('id',$data->id)->update(array('rm_convert' => $pricemyr, 'date_convert' => $today, 'rate' => $rete6)); 

                        }

                  
            }

            \Log::info("Change image working done");


        /*} catch (\Exception $e) {
               
               die("The file doesn't exist");
               \Log::warning("cannot change image working done");
        }*/


    }
}
