<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\VehicleChecking;

class OldCaseToNewStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ac:updatestatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Status Old Chassis to New Status New Chassis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        //Open Verify 

        $openVerify = VehicleChecking::where('status', '25')
                    ->whereNull('deleted_at')
                    ->where('is_sent', '1')
                    ->where('is_check_id', '1')
                    ->where('is_sync', '1')
                    ->get();


        foreach($openVerify as $data){ 

                VehicleChecking::where('id', $data->id)->update(array('status' => "28", "real_type_report_id" => "Full"));      
                     
        }



        $checkId = VehicleChecking::whereNull('is_check_id')
                    ->whereNull('deleted_at')
                    ->where('status', '25')
                    ->where('is_sent', '1')
                    ->where('searching_by', 'NOT')
                    ->where('type_report', '4')
                    ->get();


        foreach($checkId as $data){ 

                VehicleChecking::where('id', $data->id)
                                ->update(array('status' => "26", "real_type_report_id" => "Full" ));
           

        }


        $SentManual = VehicleChecking::whereNull('deleted_at')
                    ->where('status', '26')
                    ->where('searching_by', 'API')
                    ->get();


        foreach($SentManual as $data){ 

                VehicleChecking::where('id', $data->id)
                                ->update(array('status' => "20", "real_type_report_id" => "Full" ));
           
        }



        $EditToFullReport = VehicleChecking::whereNull('deleted_at')
                    ->where('status', '26')
                    ->where('type_report', '4')
                    ->get();


        foreach($EditToFullReport as $data){ 

                VehicleChecking::where('id', $data->id)
                                ->update(array('status' => "26", "real_type_report_id" => "Full" ));
           
        }




    }
}
