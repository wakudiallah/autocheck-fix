@extends('admin.layout.template')

@section('content')


        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Searching {{config('autocheck.full_report')}}</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">
              <div class="col-lg-12 mb-4">
                
                <!-- Input & Button Groups -->
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Search</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      
                      <form id="search" action="{{url('search-vehicle')}}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="input-group mb-5">
                          <input type="text" class="form-control form-control-lg" placeholder="Vehicles"  name="vehicle" required="">

                          <div class="input-group-append">

                            <input type="Submit" class="btn btn-white" value="Search">
                          </div>
                        </div>
                        
                      </form>
                    </li>
                    
                      
                  </ul>

                </div>
                <!-- / Input & Button Groups -->
              </div>

              
            </div>


            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Status</th>
                                  <th>Created at</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($vehicle as $data)
                            <tr>
                              <td>{{$i++}}</td>
                              <td>{{$data->vehicle}}</td>
                              <td>
                                

                                @if($data->status == 40)
                                  
                                  <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>

                                @elseif($data->status == 20 OR 

                                $data->status == 10)
                                  
                                  <i href="#" class="card-post__category badge badge-pill badge-warning">New</i>

                                
                                @else
                                   <i href="#" class="card-post__category badge badge-pill badge-success">In Progress</i>
                                @endif
                              </td>
                              <td>
                                <?php  $tarikh =  date('m/d/Y ', strtotime($data->created_at)); ?>
                                
                                {{$tarikh}}
                              </td>
                            </tr>
                            @endforeach
                              
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>


@endsection


@push('js')

<<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>
  

@endpush