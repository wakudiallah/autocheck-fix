@extends('admin.layout.template')

@section('content')

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Detail Vehicle ( {{config('autocheck.extra_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">

                  <div class="main-content-container container-fluid px-4">
                                        
                    <div class="row">
                      <div class="col-lg-8 mb-4">
                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h3 class="page-title">{{$data->vehicle}}</h3>
                          </div>
                          <ul class="list-group list-group-flush">
                            
                            <li class="list-group-item p-3">
                              
                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">directions_car_filled</i>
                                <h5 class="mb-1"> Ver Number :</h5>
                              </span>
                              <strong class="text-muted d-block my-2 mb-4">

                              	</strong>


                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">visibility</i>
                                <h5 class="mb-1"> Detail Requested :</h5>
                              </span>

                               
                              <strong class="text-muted d-block my-2 mb-4"><i>Requested By</i> : {{$data->request_by->name}} | <i>Email </i> : {{$data->request_by->email}} | <i>Group By </i> : {{$data->request_by->user_relate_usergroup->group_name}} </strong>


                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">event</i>
                                <h5 class="mb-1"> Date Verify :</h5>
                              </span>
                              <?php $tarikh =  date('d F Y ', strtotime($data->updated_at)); ?>
                              <strong class="text-muted d-block my-2 mb-4">{{$tarikh}}</strong>
                              <!-- / Small Outline Buttons -->
                            </li>
                            
                          </ul>
                        </div>

                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">VEHICLE DETAILS</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              
                              <table class="table mb-0">
                                
                                <tbody>
                                  <tr>
                                    <td><b>Vin / Chassis</b></td>
                                    <td>:</td>
                                    <td>{{$data->vehicle}}</td>

                                    <td></td>
                                    <td><b>Title Information</b></td>
                                    <td>:</td>
                                    <td>
                                      @if(!empty($summary->registered == '1'))
                                        Deregistered to Export
                                      @else

                                      
                                      @endif
                                    </td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Manufacture date</b></td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->manufactureDate))
                                      {{$vehicledetail->manufactureDate}}
                                       @endif
                                    </td>

                                    <td></td>
                                    <td><b>Accident / Repair </b></td>
                                    <td>:</td>
                                    <td>@if($summary->accident == '1')
                                        No Problem
                                      @else
                                        Problem Found
                                      @endif
                                    </td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Make</b></td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->make))
                                      {{$vehicledetail->make}}
                                        @endif
                                    </td>


                                    <td></td>
                                    <td><b>Odometer rollback</b></td>
                                    <td>:</td>
                                    <td>@if($summary->odometer == '1')
                                        No Problem
                                      @else
                                        Problem Found
                                      @endif
                                    </td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Model </b></td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->model))
                                      {{$vehicledetail->model}}
                                        @endif
                                    </td>

                                    <td></td>
                                    <td><b>Manufacturer recall</b></td>
                                    <td>:</td>
                                    <td>@if($summary->recall == "1")
                                        No Problem
                                      @else
                                        Problem Found
                                      @endif
                                    </td>

                                  </tr>
                                  <tr>
                                    <td><b>Body</b> </td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->body))
                                      {{$vehicledetail->body}}
                                        @endif
                                    </td>
                                    
                                    <td></td>
                                    <td><b>Safety grade</b></td>
                                    <td>:</td>
                                    <td>@if($summary->safetyGrade == "1")
                                        5 Star
                                      @else
                                        < 5 Star
                                      @endif
                                    </td>
                                  </tr>
                                  
                                  <tr>
                                    <td><b>Grade </b></td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->grade))
                                      {{$vehicledetail->grade}}
                                      @endif
                                    </td>


                                    <td></td>
                                    <td><b>Contamination risk</b></td>
                                    <td>:</td>
                                    <td>@if($summary->contaminationRisk == "1")
                                        No Problem
                                      @else
                                        Problem Found
                                      @endif
                                    </td>
                                    
                                  </tr>

                                 
                                  
                                  <tr>
                                    <td><b>Engine </b></td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->engine))
                                      {{$vehicledetail->engine}}
                                      @endif
                                    </td>


                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                  </tr>

                                  <tr>
                                    <td><b>Drive</b> </td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->drive))
                                      {{$vehicledetail->drive}}
                                        @endif
                                    </td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                  </tr>

                                  <tr>
                                    <td><b>Transmission</b> </td>
                                    <td>:</td>
                                    <td>@if(!empty($vehicledetail->transmission))
                                      {{$vehicledetail->transmission}}
                                      @endif
                                    </td>
                                    
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                  </tr>

                                </tbody>
                              </table>

                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">ACCIDENT / REPAIR HISTORY</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              
                              <table width="100%" class="table table-striped" style="font-size: 15px;">
                                <thead>
                                <tr>
                                  <td width="10%"><b>Problem type</b></td>
                                  <td width="20%"><b>Reported</b></td>
                                  <td width="10%"><b>Date reported</b></td>
                                  <td width="10%"><b>Data source</b></td>
                                  <td width="10%"><b>Details</b></td>
                                  <td width="5%"><b>Airbag</b></td>
                                </tr>
                                </thead>
                                <tr>
                                  <td>Collision</td>
                                  <td>
                                    @if(empty($check_raccident_collision->date_reported))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
                                    @else
                                    <img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
                                    @endif
                                  </td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                @foreach($raccident_collision as $data)
                                <tr>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>{{$data->date_reported}}</td>
                                  <td>{{$data->data_source}}</td>
                                  <td>{{$data->details}}</td>
                                  <td>{{$data->airbag}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                  <td>Malfunction</td>
                                  <td>
                                    @if(empty($check_raccident_malfunction->date_reported))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
                                    @else
                                    <img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
                                    @endif
                                  </td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                @foreach($raccident_malfunction as $data)
                                <tr>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>{{$data->date_reported}}</td>
                                  <td>{{$data->data_source}}</td>
                                  <td>{{$data->details}}</td>
                                  <td>{{$data->airbag}}</td>
                                </tr>
                                @endforeach
                                
                                <tr>
                                  <td>Theft</td>
                                  <td>
                                    @if(empty($check_raccident_theft->date_reported))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
                                    @else
                                    <img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
                                    @endif
                                  </td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                @foreach($raccident_theft as $data)
                                <tr>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>{{$data->date_reported}}</td>
                                  <td>{{$data->data_source}}</td>
                                  <td>{{$data->details}}</td>
                                  <td>{{$data->airbag}}</td>
                                </tr>
                                @endforeach

                                <tr>
                                  <td>Fire damage</td>
                                  <td>
                                    @if(empty($check_raccident_fireDamage->date_reported))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
                                    @else
                                    <img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
                                    @endif
                                  </td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>

                                @foreach($raccident_fireDamage as $data)
                                <tr>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>{{$data->date_reported}}</td>
                                  <td>{{$data->data_source}}</td>
                                  <td>{{$data->details}}</td>
                                  <td>{{$data->airbag}}</td>
                                </tr>
                                @endforeach


                                <tr>
                                  <td>Water damage</td>
                                  <td>
                                    @if(empty($check_raccident_waterDamage->date_reported))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
                                    @else
                                    <img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
                                    @endif
                                  </td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>

                                @foreach($raccident_waterDamage as $data)
                                <tr>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>{{$data->date_reported}}</td>
                                  <td>{{$data->data_source}}</td>
                                  <td>{{$data->details}}</td>
                                  <td>{{$data->airbag}}</td>
                                </tr>
                                @endforeach

                                <tr>
                                  <td>Hail damage</td>
                                  <td>
                                    @if(empty($check_raccident_hailDamage->date_reported))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
                                    @else
                                    <img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
                                    @endif
                                  </td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>

                                @foreach($raccident_hailDamage as $data)
                                <tr>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>{{$data->date_reported}}</td>
                                  <td>{{$data->data_source}}</td>
                                  <td>{{$data->details}}</td>
                                  <td>{{$data->airbag}}</td>
                                </tr>
                                @endforeach

                              </table>

                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">ODOMETER READINGS HISTORY</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <table width="100%" class="table table-striped" style="font-size: 15px;">
                                <thead>
                                <tr>
                                  <td><b>Date reported</b></td>
                                  <td><b>Data source</b></td>
                                  <td><b>Odometer reading (Km)</b></td>
                                </tr>
                                </thead>
                                @foreach($odometerhistory as $data)
                                <tr>
                                  <td>{{$data->date}}</td>
                                  <td>{{$data->source}}</td>
                                  <td>{{$data->mileage}}</td>
                                </tr>
                                @endforeach
                                      
                              </table>
                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">USE HISTORY</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <table width="100%" class="table table-striped" style="font-size: 15px;">
                                <thead>
                                <tr>
                                  <td><b>Use in the contaminated regions</b><sup>4</sup></td>
                                  <td><b>Radioactive contamination test fail</b><sup>5</sup></td>
                                  <td><b>Commercial use</b></td>
                                </tr>
                                </thead>
                                <tr>
                                  
                                  <td>
                                    @if(!empty($usagehistory->contaminatedRegion == '0'))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">
                                    &nbsp;&nbsp;Not reported 
                                    @endif
                                  </td>
                                  <td>
                                    @if(!empty($usagehistory->contaminationTest == '0'))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">
                                    &nbsp;&nbsp;Not reported
                                    @endif
                                  </td>
                                  <td>
                                    @if(!empty($usagehistory->commercialUsage == '0'))
                                    <img src="{{asset('images/check.jpg')}}" width="20" height="20">
                                    &nbsp;&nbsp;Not reported
                                    @endif
                                  </td>
                                </tr>
                                      
                              </table>
                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">DETAILED HISTORY</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <table width="100%" class="table table-striped" style="font-size: 15px;">
                                <thead>
                                <tr>
                                  <td><b>Event date</b></td>
                                  <td><b>Location</b></td>
                                  <td><b>Odometer reading (Km)</b></td>
                                  <td><b>Data source</b></td>
                                  <td><b>Details</b></td>
                                </tr>
                                </thead>
                                @foreach($detailhistory as $data)
                                <tr>
                                  <td>
                                    {{$data->date}}
                                  </td>
                                  <td>
                                    {{$data->location}}
                                  </td>
                                  <td>
                                    {{$data->mileage}}
                                  </td>
                                  <td>
                                    {{$data->source}}
                                  </td>
                                  <td>
                                    {{$data->action}}
                                  </td>
                                </tr>
                                @endforeach
                                      
                              </table>
                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">MANUFACTURER RECALL HISTORY</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <table width="100%" class="table table-striped" style="font-size: 15px;" >
                                <thead>
                                <tr>
                                  <td><b>Date reported</b></td>
                                  <td><b>Data source</b></td>
                                  <td><b>Affected part</b></td>
                                  <td><b>Details</b></td>
                                </tr>
                                </thead>
                                @foreach($recall as $data)
                                <tr>
                                  <td>{{$data->date}}</td>
                                  <td>{{$data->source}}</td>
                                  <td>{{$data->affected_part}}</td>
                                  <td>{{$data->details}}</td>
                                </tr>
                                @endforeach
                                      
                              </table>
                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">ODOMETER READINGS HISTORY</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <table width="100%" class="table table-striped" style="font-size: 15px;">
                              <tr>
                                <td colspan="6"><b>Overall Collision Safety Ratings</b></td>
                              </tr>
                              <tr>
                                <td colspan="3" align="center"><b>Driver's seat</b></td>
                                <td colspan="3" align="center"><b>Front passenger's seat</b></td>
                              </tr>

                              <tr>
                                <td>Points </td>
                                <td>Evaluation</td>
                                <td>Goal average</td>
                                <td>Points </td>
                                <td>Evaluation</td>
                                <td>Goal average</td>
                              </tr>

                              <tr>
                                
                                <td>{{$vehicleassessment->driverSeatPoints}}</td>
                                <td><img src="{{asset('images/start_full.png')}}" width="60" height="20"></td>
                                <td>{{$vehicleassessment->driverSeatGoalAverage}}%</td>

                                <td>{{$vehicleassessment->frontPassengerSeatPoints}}</td>
                                <td><img src="{{asset('images/start_full.png')}}" width="60" height="20"></td>
                                <td>{{$vehicleassessment->frontPassengerSeatGoalAverage}}%</td>
                              </tr>

                              <tr>
                                <td colspan="6">
                                <p>* In order to accurately differentiate between the evaluations of different vehicles, a standard is set based on current technology. Up to 6 points out of 12 is given level 1 and the rest of the range is divided up into equal parts, which are respectively assigned to level 2 (more than 6 points but 7.5 or less), level 3 (more than 7.5 points but 9 or less), level 4 (more than 9 points but 10.5 or less) or level 5 (more than 10.5 points).</p>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="6">
                                <p><b>Braking performance tests</b><sup>7</sup></p>
                                </td>
                              </tr>
                              <tr>
                                <td>Dry road</td>
                                <td colspan="4"><img src="{{asset('images/report/car1.png')}}" width="300" height="50"></td>
                                <td>{{$vehicleassessment->dryRoadStoppingDistance}} m</td>
                              </tr>
                              <tr>
                                <td>Wet road</td>
                                <td colspan="4"><img src="{{asset('images/report/car2.png')}}" width="300" height="50"></td>
                                <td>{{$vehicleassessment->wetRoadStoppingDistance}} m</td>
                              </tr>
                                    
                            </table>
                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">VEHICLE SPECIFICATION</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            
                            <li class="list-group-item px-3">
                              <div class="table-responsive">
                              <table width="100%" class="table table-striped" style="font-size: 15px;" >
                                <tr>
                                  <td><b>1st gear ratio </b></td>
                                  <td>{{$vehiclespesification->firstGearRatio}}</td>
                                  <td></td>
                                  <td><b>2nd gear ratio </b></td>
                                  <td>{{$vehiclespesification->secondGearRatio}}</td>
                                </tr>
                                <tr>
                                  <td><b>3rd gear ratio  </b></td>
                                  <td>{{$vehiclespesification->thirdGearRatio}}</td>
                                  <td></td>
                                  <td><b>4th gear ratio</b></td>
                                  <td>{{$vehiclespesification->fourthGearRatio}}</td>
                                </tr>
                                <tr>
                                  <td><b>5th gear ratio</b></td>
                                  <td>{{$vehiclespesification->fifthGearRatio}}</td>
                                  <td></td>
                                  <td><b>6th gear ratio</b></td>
                                  <td>{{$vehiclespesification->sixthGearRatio}}</td>
                                </tr>
                                <tr>
                                  <td><b>Additional notes</b></td>
                                  <td>{{$vehiclespesification->additionalNotes}}</td>
                                  <td></td>
                                  <td><b>Airbag position, capacity</b></td>
                                  <td>{{$vehiclespesification->airbagPosition}}</td>
                                </tr>
                                <tr>
                                  <td><b>Body rear overhang </b></td>
                                  <td>{{$vehiclespesification->bodyRearOverhang}}</td>
                                  <td></td>
                                  <td><b>Body type</b></td>
                                  <td>{{$vehiclespesification->bodyType}}</td>
                                </tr>

                                <tr>
                                  <td><b>Chassis number embossing position </b></td>
                                  <td>{{$vehiclespesification->chassisNumberEmbossingPosition}}</td>
                                  <td></td>
                                  <td><b>Classification code</b></td>
                                  <td>{{$vehiclespesification->classificationCode}}</td>
                                </tr>
                                <tr>
                                  <td><b>Cylinders </b></td>
                                  <td>{{$vehiclespesification->cylinders}}</td>
                                  <td></td>
                                  <td><b>Displacement</b></td>
                                  <td>{{$vehiclespesification->displacement}}</td>
                                </tr>
                                <tr>
                                  <td><b>Electric engine type </b></td>
                                  <td>{{$vehiclespesification->electricEngineType}}</td>
                                  <td></td>
                                  <td><b>Electric engine maximum output</b></td>
                                  <td>{{$vehiclespesification->electricEngineMaximumOutput}}</td>
                                </tr>
                                <tr>
                                  <td><b>Electric engine maximum torque</b></td>
                                  <td>{{$vehiclespesification->electricEngineMaximumTorque}}</td>
                                  <td></td>
                                  <td><b>Electric engine power</b></td>
                                  <td>{{$vehiclespesification->electricEnginePower}}</td>
                                </tr>
                                <tr>
                                  <td><b>Engine maximum power</b></td>
                                  <td>{{$vehiclespesification->engineMaximumPower}}</td>
                                  <td></td>
                                  <td><b>Engine maximum torque</b></td>
                                  <td>{{$vehiclespesification->engineMaximumTorque}}</td>
                                </tr>
                                <tr>
                                  <td><b>Engine model </b></td>
                                  <td>{{$vehiclespesification->engineModel}}</td>
                                  <td></td>
                                  <td><b>Frame type</b></td>
                                  <td>{{$vehiclespesification->frameType}}</td>
                                </tr>
                                <tr>
                                  <td><b>Front shaft weight</b></td>
                                  <td>{{$vehiclespesification->frontShaftWeight}}</td>
                                  <td></td>
                                  <td><b>Front shock absorber type</b></td>
                                  <td>{{$vehiclespesification->frontShockAbsorberType}}</td>
                                </tr>
                                <tr>
                                  <td><b>Front stabilizer type</b></td>
                                  <td>{{$vehiclespesification->frontStabilizerType}}</td>
                                  <td></td>
                                  <td><b>Front tires size</b></td>
                                  <td>{{$vehiclespesification->frontTiresSize}}</td>
                                </tr>
                                <tr>
                                  <td><b>Front tread </b></td>
                                  <td>{{$vehiclespesification->frontTread}}</td>
                                  <td></td>
                                  <td><b>Fuel consumption</b></td>
                                  <td>{{$vehiclespesification->fuelConsumption}}</td>
                                </tr>
                                <tr>
                                  <td><b>Fuel tank equipment</b></td>
                                  <td>{{$vehiclespesification->fuelTankEquipment}}</td>
                                  <td></td>
                                  <td><b>Grade</b></td>
                                  <td>{{$vehiclespesification->gradeData}}</td>
                                </tr>
                                <tr>
                                  <td><b>Height</b></td>
                                  <td>{{$vehiclespesification->height}}</td>
                                  <td></td>
                                  <td><b>Length</b></td>
                                  <td>{{$vehiclespesification->length}}</td>
                                </tr>
                                <tr>
                                  <td><b>Main brakes type</b></td>
                                  <td>{{$vehiclespesification->mainBrakesType}}</td>
                                  <td></td>
                                  <td><b>Make</b></td>
                                  <td>{{$vehiclespesification->dataMake}}</td>
                                </tr>
                                <tr>
                                  <td><b>Maximum speed</b></td>
                                  <td>{{$vehiclespesification->maximumSpeed}}</td>
                                  <td></td>
                                  <td><b>Minimum ground clearance</b></td>
                                  <td>{{$vehiclespesification->minimumGroundClearance}}</td>
                                </tr>
                                <tr>
                                  <td><b>Minimum turning radius</b></td>
                                  <td>{{$vehiclespesification->minimumTurningRadius}}</td>
                                  <td></td>
                                  <td><b>Model</b></td>
                                  <td>{{$vehiclespesification->modelData}}</td>
                                </tr>

                                <tr>
                                  <td><b>Model code</b></td>
                                  <td>{{$vehiclespesification->modelCode}}</td>
                                  <td></td>
                                  <td><b>Mufflers number</b></td>
                                  <td>{{$vehiclespesification->mufflersNumber}}</td>
                                </tr>

                                <tr>
                                  <td><b>Rear shaft weight </b></td>
                                  <td>{{$vehiclespesification->rearShaftWeight}}</td>
                                  <td></td>
                                  <td><b>Rear shock absorber type</b></td>
                                  <td>{{$vehiclespesification->rearShockAbsorberType}}</td>
                                </tr>
                                <tr>
                                  <td><b>Rear stabilizer type </b></td>
                                  <td>{{$vehiclespesification->rearStabilizerType}}</td>
                                  <td></td>
                                  <td><b>Rear tires size</b></td>
                                  <td>{{$vehiclespesification->rearTiresSize}}</td>
                                </tr>
                                <tr>
                                  <td><b>Rear tread</b></td>
                                  <td>{{$vehiclespesification->rearTread}}</td>
                                  <td></td>
                                  <td><b>Reverse ratio</b></td>
                                  <td>{{$vehiclespesification->reverseRatio}}</td>
                                </tr>
                                <tr>
                                  <td><b>Riding capacity</b></td>
                                  <td>{{$vehiclespesification->ridingCapacity}}</td>
                                  <td></td>
                                  <td><b>Side brakes type</b></td>
                                  <td>{{$vehiclespesification->sideBrakesType}}</td>
                                </tr>
                                <tr>
                                  <td><b>Specification code </b></td>
                                  <td>{{$vehiclespesification->specificationCode}}</td>
                                  <td></td>
                                  <td><b>Stopping distance</b></td>
                                  <td>{{$vehiclespesification->stoppingDistance}}</td>
                                </tr>
                                <tr>
                                  <td><b>Transmission type</b></td>
                                  <td>{{$vehiclespesification->transmissionType}}</td>
                                  <td></td>
                                  <td><b>Weight</b></td>
                                  <td>{{$vehiclespesification->weight}}</td>
                                </tr>
                                <tr>
                                  <td><b>Wheel alignment </b></td>
                                  <td>{{$vehiclespesification->wheelAlignment}}</td>
                                  <td></td>
                                  <td><b>Wheelbase</b></td>
                                  <td>{{$vehiclespesification->wheelbase}}</td>
                                </tr>
                                <tr>
                                  <td><b>Width</b></td>
                                  <td>{{$vehiclespesification->width}}</td>
                                  <td></td>
                                  <td><b></b></td>
                                  <td></td>
                                </tr>
                                
                                      
                              </table>
                              </div>
                            </li>
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h4 class="page-title">AUCTION DATA</h4>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <table width="100%" class="table table-striped" style="font-size: 15px;" >
    
                                @foreach($auctionhistory as $data)
                                <tr>
                                  <td>Date</td>
                                  <td>{{$data->date}}</td>
                                  <td></td>
                                  <td>Lot#</td>
                                  <td>{{$data->lotNumber}}</td>
                                </tr>
                                <tr>
                                  <td>Auction name</td>
                                  <td>{{$data->auction}}</td>
                                  <td></td>
                                  <td>Region</td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>Make</td>
                                  <td>{{$data->make}}</td>
                                  <td></td>
                                  <td>Model</td>
                                  <td>{{$data->model}}</td>
                                </tr>
                                <tr>
                                  <td>Reg. year</td>
                                  <td>{{$data->registrationDate}}</td>
                                  <td></td>
                                  <td>Mileage (km)</td>
                                  <td>{{$data->mileage}}</td>
                                </tr>
                                <tr>
                                  <td>Displacement (cc)</td>
                                  <td>{{$data->displacement}}</td>
                                  <td></td>
                                  <td>Transmission</td>
                                  <td>{{$data->transmission}}</td>
                                </tr>

                                <tr>
                                  <td>Color</td>
                                  <td>{{$data->color}}</td>
                                  <td></td>
                                  <td>Model code</td>
                                  <td>{{$data->body}}</td>
                                </tr>
                                <tr>
                                  <td>Result</td>
                                  <td>{{$data->result}}</td>
                                  <td></td>
                                  <td>Auction grade</td>
                                  <td>{{$data->assessment}}</td>
                                </tr>
                                <tr>
                                  <td>Problem type</td>
                                  <td></td>
                                  <td></td>
                                  <td>Problem scale</td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>Contaminated</td>
                                  <td>{{$data->assessment}}</td>
                                  <td></td>
                                  <td>Airbag</td>
                                  <td></td>
                                </tr>
                                @endforeach
                                      
                              </table>
                            </li>
                          </ul>
                        </div>




                      </div>




                      <div class="col-lg-4 mb-4">
                        <!-- QR Code -->
                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h6 class="m-0">QR Code</h6>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <!-- Progress Bars -->
                              <div class="mb-1" align="center">
                                

                                <?php $url = url('FullReport/Report/'.$data->id_vehicle); ?>
                                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(250)->generate(url($url))) !!} ">
                                
                              </div>
                              
                              <div class="row">
                                <div class="mx-auto mb-4">
                                  
                                  <a href="{{url('ExtraReport/report/'.$data->id_vehicle)}}" target="_blank">
                                      <button class="btn btn-lg btn-success" type="button">Print Vehicle Report</button>
                                  </a>

                                </div>
                              </div>
                              <!-- / Progress Bars -->
                            </li>
                            
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h6 class="m-0">Image</h6>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <!-- Progress Bars -->
                              <div class="mb-1" align="center">
                                

                                @foreach($auctionimage as $data)
    
                                  <img src="{{$data->image}}" alt="{{$data->image}}" class="img-fluid" style="width: 100%; height: 70%;" >
                                  
                                @endforeach 
                              </div>
                              
                              <div class="row">
                               
                              </div>
                              <!-- / Progress Bars -->
                            </li>
                            
                          </ul>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  
                 
              



            </div>
          </div>

        
       

@endsection


@push('js')


@endpush
