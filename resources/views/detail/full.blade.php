@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>


        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Detail Vehicle ( {{config('autocheck.full_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">


                  <div class="main-content-container container-fluid px-4">
                                        
                    <div class="row">
                      <div class="col-lg-8 mb-4">
                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h3 class="page-title">{{$data->vehicle}}</h3>
                          </div>
                          <ul class="list-group list-group-flush">
                            
                            <li class="list-group-item p-3">
                              
                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">directions_car_filled</i>
                                <h5 class="mb-1"> Ver Number :</h5>
                              </span>
                              <strong class="text-muted d-block my-2 mb-4">

                              	</strong>


                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">visibility</i>
                                <h5 class="mb-1"> Detail Requested :</h5>
                              </span>

                               
                              <strong class="text-muted d-block my-2 mb-4"><i>Requested By</i> : {{$data->request_by->name}} | <i>Email </i> : {{$data->request_by->email}} | <i>Group By </i> : {{$data->request_by->user_relate_usergroup->group_name}} </strong>


                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">event</i>
                                <h5 class="mb-1"> Date Verify :</h5>
                              </span>
                              <?php $tarikh =  date('d F Y ', strtotime($data->updated_at)); ?>
                              <strong class="text-muted d-block my-2 mb-4">{{$tarikh}}</strong>
                              <!-- / Small Outline Buttons -->
                            </li>
                            
                          </ul>
                        </div>

                        <div class="card card-small mb-4">
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              
                              <table class="table mb-0">
                                
                                <tbody>
                                  <tr>
                                    <td><b>Vin / Chassis</b></td>
                                    <td>{{$data->vehicle}}</td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Make / Brand</b></td>
                                    <td>{{$data->vehiclefull->make}}</td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Model / Variance</b></td>
                                    <td>{{$data->vehiclefull->model}}</td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Engine Code / Engine Number</b> </td>
                                    <td>{{$data->vehiclefull->engine}}</td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Body</b> </td>
                                    <td>{{$data->vehiclefull->body}}</td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Car Grade </b></td>
                                    <td>{{$data->vehiclefull->grade}}</td>
                                    
                                  </tr>

                                  @php
									$manufactureDate =  date('d F Y ', strtotime($data->vehiclefull->manufactureDate));

									$date_of_origin =  date('d F Y ', strtotime($data->vehiclefull->date_of_origin));
								@endphp 
                                  
                                  <tr>
                                    <td><b>Date Of Manufacture </b></td>
                                    <td>{{$data->vehiclefull->manufactureDate}}</td>
                                    
                                  </tr>

                                  <tr>
                                    <td><b>Date of original registration</b> </td>
                                    <td>{{$date_of_origin}}</td>
                                    
                                  </tr>

                                  <tr>
                                    <td><b>Drive</b> </td>
                                    <td>{{$data->vehiclefull->drive}}</td>
                                    
                                  </tr>

                                  <tr>
                                    <td><b>Transmission</b> </td>
                                    <td>{{$data->vehiclefull->transmission}}</td>
                                    
                                  </tr>

                                  <tr>
                                    <td><b>Displacement (cc)</b> </td>
                                    <td>{{$data->vehiclefull->displacement_cc}}</td>
                                    
                                  </tr>

                                  @php

                                  $tot_yen  = number_format($data->vehiclefull->average_market, 0, ',', ',');

                                  @endphp
                                  <tr>

                                    <td><b>Average Market Price</b> </td>
                                    <td>&#165; {{$tot_yen}}</td>
                                    
                                    
                                  </tr>

                                  <tr>
                                    <td><b></b> </td>
                                    <td>RM @if(!empty($data->vehiclefull->rm_convert))
                                      {{$data->vehiclefull->rm_convert}}
                                      @endif
                                    </td>
                                  </tr>

                                </tbody>
                              </table>

                            </li>
                          </ul>
                        </div>

                      </div>




                      <div class="col-lg-4 mb-4">
                        <!-- QR Code -->
                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h6 class="m-0">QR Code</h6>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <!-- Progress Bars -->
                              <div class="mb-1" align="center">
                                

                                <?php $url = url('FullReport/Report/'.$data->id_vehicle); ?>
                                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(250)->generate(url($url))) !!} ">
                                
                              </div>
                              
                              <div class="row">
                                <div class="mx-auto mb-4">
                                  
                                  <a href="{{url('FullReport/Report/'.$data->id_vehicle)}}" target="_blank">
                                      <button class="btn btn-lg btn-success" type="button">Print Vehicle Report</button>
                                  </a>

                                </div>
                              </div>
                              <!-- / Progress Bars -->
                            </li>
                            
                          </ul>
                        </div>


                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h6 class="m-0">Image</h6>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <!-- Progress Bars -->
                              <div class="mb-1" align="center">
                                
                              	@php
                              		$image = DB::table('vehicle_api_kastam_images')
                              				->where('vehicle_id', $data->id_vehicle)
                              				->get();
                              	@endphp
                               	

                                @foreach($image as $image)
                                	@if(!empty($image->image))
                                	<p style="text-align:center;"><img src="{{$image->image}}" width="85%" align="center" style="margin-top: 20px"> </p><br>
                                	@endif
                                @endforeach
                              </div>
                              
                              <div class="row">
                               
                              </div>
                              <!-- / Progress Bars -->
                            </li>
                            
                          </ul>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  
                 
              



            </div>
          </div>

        
       

@endsection


@push('js')


@endpush
