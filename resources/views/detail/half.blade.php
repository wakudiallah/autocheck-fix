@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>


        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Detail Vehicle ( {{config('autocheck.half_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">


                  
                  
                  <div class="main-content-container container-fluid px-4">
                                        
                    <div class="row">
                      <div class="col-lg-8 mb-4">
                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h3 class="page-title">{{$data->vehicle}}</h3>
                          </div>
                          <ul class="list-group list-group-flush">
                            
                            <li class="list-group-item p-3">
                              
                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">directions_car_filled</i>
                                <h5 class="mb-1"> Ver NumberX :</h5>
                              </span>
                              <strong class="text-muted d-block my-2 mb-4">@if(!empty($data->vehiclehalf->ver_sn))
                                {{$data->vehiclehalf->ver_sn}}
                              @endif
                                </strong>


                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">visibility</i>
                                <h5 class="mb-1"> Detail Requested :</h5>
                              </span>

                               
                              <strong class="text-muted d-block my-2 mb-4"><i>Requested By</i> : {{$data->request_by->name}} | <i>Email </i> : {{$data->request_by->email}} | <i>Group By </i> : {{$data->request_by->user_relate_usergroup->group_name}} </strong>


                              <span class="d-flex mb-1">
                                <i class="material-icons mr-3">event</i>
                                <h5 class="mb-1"> Date Verify :</h5>
                              </span>
                              <?php $tarikh =  date('d F Y ', strtotime($data->updated_at)); ?>
                              <strong class="text-muted d-block my-2 mb-4">{{$tarikh}}</strong>
                              <!-- / Small Outline Buttons -->
                            </li>
                            
                          </ul>
                        </div>

                        <div class="card card-small mb-4">
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              
                              <table class="table mb-0">
                                
                                <tbody>
                                  <tr>
                                    <td><b>Country Origin</b></td>
                                    <td>@if(!empty($data->co->country_origin))
                                        {{$data->co->country_origin}}
                                        @endif
                                    </td>
                                    <td><span class="material-icons">check_circle</span>
                                    </td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Make / Brand</b></td>
                                    <td>{{$data->vehiclehalf->brand}}</td>
                                    <td><span class="material-icons">check_circle</span></td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Model / Variance</b></td>
                                    <td>{{$data->vehiclehalf->model}}</td>
                                    <td><span class="material-icons">check_circle</span></td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Engine Number / Engine Model</b> </td>
                                    <td>{{$data->vehiclehalf->engine_number}}</td>
                                    <td><span class="material-icons">check_circle</span></td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Cubic Capacity (CC)</b> </td>
                                    <td>{{$data->vehiclehalf->cc}}</td>
                                    <td><span class="material-icons">check_circle</span></td>
                                    
                                  </tr>
                                  <tr>
                                    <td><b>Fuel Type </b></td>
                                    <td>{{$data->vehiclehalf->fuel_type}}</td>
                                    <td><span class="material-icons">check_circle</span></td>
                                    
                                  </tr>
                                  <?php 
            
                                    $y         = explode('-', $data->vehiclehalf->year_manufacture);
                                      $year = $y[0];
                                      
                                      ?>
                                  <tr>
                                    <td><b>Year of Manufacture </b></td>
                                    <td>{{$year}}</td>
                                    <td><span class="material-icons">check_circle</span></td>
                                    
                                  </tr>

                                  <?php 
            
                                    $regist_date =  date('F Y ', strtotime($data->vehiclehalf->registation_date)); 
                                    
                                    ?>

                                  <tr>
                                    <td><b>First Registration Date</b> </td>
                                    <td>{{$regist_date}}</td>
                                    <td><span class="material-icons">check_circle</span></td>
                                    
                                  </tr>
                                </tbody>
                              </table>

                            </li>
                          </ul>
                        </div>

                      </div>




                      <div class="col-lg-4 mb-4">
                        <!-- QR Code -->
                        <div class="card card-small mb-4">
                          <div class="card-header border-bottom">
                            <h6 class="m-0">QR Code</h6>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3">
                              <!-- Progress Bars -->
                              <div class="mb-1" align="center">
                                

                                <?php $url = url('HalfReport/Report/'.$data->id_vehicle); ?>
                                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(250)->generate(url($url))) !!} ">
                                
                              </div>
                              
                              <div class="row">
                                <div class="mx-auto mb-4">
                                  
                                  <a href="{{url('HalfReport/Report/'.$data->id_vehicle)}}" target="_blank">
                                      <button class="btn btn-lg btn-success" type="button">Print Vehicle Report</button>
                                  </a>

                                </div>
                              </div>
                              <!-- / Progress Bars -->
                            </li>
                            
                          </ul>
                        </div>
                        <!-- / Sliders & Progress Bars -->
                        
                      </div>
                    </div>
                  </div>
                  
                 
              



            </div>
          </div>

        
       

@endsection


@push('js')


@endpush
