<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <!--[if !mso]><!-- -->
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel="stylesheet">
    <!-- <![endif]-->

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <title>Art Insko</title>

    <style type="text/css">
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }
        /* ----------- responsivity ----------- */

        @media only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }
            .main-section-header {
                font-size: 28px !important;
            }
            .show {
                display: block !important;
            }
            .hide {
                display: none !important;
            }
            .align-center {
                text-align: center !important;
            }
            .no-bg {
                background: none !important;
            }
            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }
            .container580 {
                width: 400px !important;
            }
            .main-button {
                width: 220px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }
            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }
            .main-section-header {
                font-size: 26px !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 280px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 280px !important;
            }
            .container590 {
                width: 280px !important;
            }
            .container580 {
                width: 260px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }
        }
    </style>
    <!-- [if gte mso 9]><style type=”text/css”>
        body {
        font-family: arial, sans-serif!important;
        }
        </style>
    <![endif]-->
</head>


<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <!-- pre-header -->
    <table style="display:none!important;">
        <tr>
            <td>
                <div style="overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;">
                    Pre-header for the newsletter template
                </div>
            </td>
        </tr>
    </table>
    <!-- pre-header end -->
    <!-- header -->


   

    


    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">

        <tr>
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">

                            <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                                <tr>
                                    <td align="center" height="70" style="height:70px;">
                                        <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 100px;" src="{{asset('images/img/core-img/logo.png')}}" alt="" /></a>
                                    </td>

                                </tr>

                                <!--<tr>
                                    <td align="center">
                                        <table width="360 " border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590 hide">
                                            <tr>
                                                <td width="120" align="center" style="font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                                    <a href="" style="color: #312c32; text-decoration: none;">MEN</a>
                                                </td>
                                                <td width="120" align="center" style="font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                                    <a href="" style="color: #312c32; text-decoration: none;">WOMEN</a>
                                                </td>
                                                <td width="120" align="center" style="font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                                    <a href="" style="color: #312c32; text-decoration: none;">BLOG</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>-->
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <!-- end header -->

    <!-- big image section -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">

        <tr>
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">
                    <tr>

                        <td align="center" class="section-img">
                            <a href="https://www.art.insko.my" target="_blank" style=" border-style: none !important; display: block; border: 0 !important;"><img src="
                                {{asset('images/slider/Presentation0.jpg')}}" style="display: block; width: 590px;" width="590" border="0" alt="" /></a>

                        </td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" style="color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;" class="main-header">


                            <div style="line-height: 35px">

                                <!-- NEW IN <span style="color: #5caad2;">NOVEMBER</span> -->

                            </div>
                        </td>
                    </tr>

                    

                    

                    

                   


                </table>

            </td>
        </tr>

        <tr class="hide">
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>
        <tr>
            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end section -->

   

    <!-- Invite friends -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">
        <tr>
            <td align="center">
                <table border="0" width="200" align="center" cellpadding="0" cellspacing="0" bgcolor="333333" style="border: 3px solid #333333; ">
                    <tr>
                        <td align="center" style="color: #ffffff; font-size: 15px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;  line-height: 32px; background-color: #1A81B5">
                            CONGRATULATIONS 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td align="center">
                <table border="0" width="590" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 3px solid #333; ">

                    <tr>
                        <td height="45" style="font-size: 45px; line-height: 45px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="540" align="center" cellpadding="0" cellspacing="0" class="container580">

                                <tr>

                                    <td width="30">&nbsp;</td>
                                    <td align="center" style="color: #333333; font-size: 18px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 32px;">

                                        <div style="line-height: 40px">
                                             <span style="color: #5caad2; text-decoration: none;font-size: 30px"> <b>-- $email_send->email --</b></span> 

                                        <div style="line-height: 32px">
                                            You get <span style="color: #5caad2; text-decoration: none;"> 15% </span> discount for ordering Video Creation Service

                                            
                                        </div>
                                    </td>
                                    <td width="30">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="30">&nbsp;</td>
                                    <td align="center">
                                        <table border="0" align="center" width="160" cellpadding="0" cellspacing="0" bgcolor="5caad2" style="">

                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 26px;">


                                                    <div style="line-height: 26px;">
                                                        <a href="art.insko.my" class="btn btn-danger" style="color: #ffffff; text-decoration: none;">Lets Check</a>

                                                        
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                        </table>
                                    </td>
                                    <td width="30">&nbsp;</td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="45" style="font-size: 45px; line-height: 45px;">&nbsp;</td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr class="hide">
            <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
        </tr>
        <tr>
            <td height="40" style="font-size: 64px; line-height: 40px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end section ====== -->

    <!--  50% image -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">
        <tr>
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tr>
                        <td>
                            <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">


                                <tr>
                                    <td align="center">
                                        <a href="" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/logo3.jpg')}}" style="display: block; width: 280px;" width="280" border="0" alt="" /></a>
                                    </td>
                                </tr>
                            </table>

                            <table border="0" width="5" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tr>
                                    <td width="5" height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>
                            </table>

                            <table border="0" width="260" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                

                                <tr>
                                    <td align="left">
                                        <table border="0" align="left" cellpadding="0" cellspacing="0" class="container590">
                                            <tr>
                                                <td align="center">
                                                    <table align="center" width="40" border="0" cellpadding="0" cellspacing="0" bgcolor="eeeeee">
                                                        <tr>
                                                            <td height="2" style="font-size: 2px; line-height: 2px;"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="15" style="font-size: 12px; line-height: 12px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td align="left" style="color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;" class="align-center">


                                        <div style="line-height: 24px">

                                             Video can have a big impact on your bottom line. A majority of consumers say that video has influenced their decision to buy. So try to make a promotion video with us.

                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>

                                

                            </table>

                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr>
            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end section -->

    <!-- 2 Column Blog Layout-->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">

        <tr>
            <td align="center">

                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tr>
                        <td align="center" style="color: #343434; font-size: 22px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 2px; line-height: 35px;" class="main-header">


                            <div style="line-height: 35px">

                                RECOMMENDED <span style="color: #5caad2;">FOR YOU</span>

                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="40" align="center" cellpadding="0" cellspacing="0" bgcolor="eeeeee">
                                <tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                    </tr>

                    

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td>
                            <table border="0" width="260" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td align="center">

                                        
                                        <a href="https://www.youtube.com/watch?v=UoQdazQj1pI" target="_blank" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/email1.jpg')}}" style="display: block; width: 260px;" width="260" border="0" alt="" /></a>
                                        

                                    </td>
                                </tr>

                                <tr>
                                    <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                </tr>

                               

                            </table>



                            <table border="0" width="5" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tr>
                                    <td width="5" height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td>
                                </tr>
                            </table>

                            <table border="0" width="260" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td align="center">

                                        

                                        <a href="https://www.youtube.com/watch?v=Lzjl-Die7OE" target="_blank" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/email2.jpg')}}" style="display: block; width: 260px;" width="260" border="0" alt="" /></a>

                                        <!-- <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/email2.jpg')}}" style="display: block; width: 260px;" width="260" border="0" alt="" /></a> -->


                                    </td>
                                </tr>

                                <tr>
                                    <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                </tr>

                               

                            </table>

                            
                           
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table border="0" width="260" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td align="center">

                                        

                                        <a href="https://www.youtube.com/watch?v=OcwUC9AiUH4" target="_blank" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/email3.jpg')}}" style="display: block; width: 260px;" width="260" border="0" alt="" /></a>

                                        <!-- <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/email3.jpg')}}" style="display: block; width: 260px;" width="260" border="0" alt="" /></a> -->


                                    </td>
                                </tr>

                                <tr>
                                    <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                </tr>

                               

                            </table>



                            <table border="0" width="5" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tr>
                                    <td width="5" height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td>
                                </tr>
                            </table>

                            <table border="0" width="260" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td align="center">

                                        <a href="https://www.youtube.com/watch?v=GfpvL32MZD8" target="_blank" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/email4.jpg')}}" style="display: block; width: 260px;" width="260" border="0" alt="" /></a>

                                        <!-- <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal4" style=" border-style: none !important; border: 0 !important;"><img src="{{asset('images/email4.jpg')}}" style="display: block; width: 260px;" width="260" border="0" alt="" /></a> -->


                                    </td>
                                </tr>

                                <tr>
                                    <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                </tr>

                               

                            </table>

                            
                           
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr class="hide">
            <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
        </tr>
        <tr>
            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end section -->

    <!-- contact section -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">

        <tr class="hide">
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>
        <tr>
            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
        </tr>

        <tr>
            <td height="60" style="border-top: 1px solid #e0e0e0;font-size: 60px; line-height: 60px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590 bg_color">

                    <tr>
                        <td>
                            <table border="0" width="300" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <!-- logo -->
                                    <td align="left">
                                        <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="80" border="0" style="display: block; width: 80px;" src="{{asset('images/img/core-img/logo.png')}}" alt="" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td align="left" style="color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 23px;" class="text_color" width="100">
                                        <div style="color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                            Email us: <br/> <a href="mailto:" style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">admin@art.insko.my</a>
                                            <br>
                                            <a href="mailto:" style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">wakudiallah05@gmail.com</a><br>
                                            <a href="mailto:" style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">novrizaleka@gmail.com</a>

                                        </div>
                                    </td>

                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                    <td align="right" style="color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 23px;" class="text_color">
                                        <div style="color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                            Phone / WA us: <br/> <i style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">+60 16 382 4356</i>
                                            <br>
                                            <i style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">+60 14 244 9179</i>
                                            
                                        </div>
                                    </td>
                                </tr>

                            </table>

                            

                            <table border="0" width="100" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td class="hide" height="45" style="font-size: 45px; line-height: 45px;">&nbsp;</td>
                                </tr>



                                <tr>
                                    <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <table border="0" align="right" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <a href="https://www.instagram.com/artinsko/?hl=id" target="_blank" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="{{asset('images/ig.png')}}" alt=""></a>
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>
                                                    <a href="art.insko.my" target="_blank" style="display: block; border-style: none !important; border: 0 !important;"><img width="24" border="0" style="display: block;" src="{{asset('images/globe.jpg')}}" alt=""></a>
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td height="60" style="font-size: 60px; line-height: 60px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end section -->

    <!-- footer ====== -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">

                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tr>
                        <td>
                            <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tr>
                                    <td align="left" style="color: #aaaaaa; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <div style="line-height: 24px;">

                                            <span style="color: #333333;">Video Creation Service</span>

                                        </div>
                                    </td>
                                </tr>
                            </table>

                            <table border="0" align="left" width="5" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tr>
                                    <td height="20" width="5" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>
                            </table>

                            <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">
                                                    <a style="font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #5caad2; text-decoration: none;font-weight:bold;" href="art.insko.my">www.art.insko.my</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp; </td>
        </tr>

    </table>
    <!-- end footer ====== -->

</body>

</html>
