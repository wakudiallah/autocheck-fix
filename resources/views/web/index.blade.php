@extends('web.layout2.template_local')

@section('content')


<style>
    .modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -100 !important;
    background-color: #000;
}
</style>


    @guest

    @else
    <!-- Menu -->
    <div class="inner-head overlap">
        <div style="background: url({{asset('web2/img/parallax1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div>  
        <div class="container">
            
            <div class="follow_widget widget">
                
                <div class="social_widget">
                    <a href="{{url('/my-account/report')}}" title=""><i class="fa fa-list"></i></a> <!-- report --> 
                    <a href="{{url('/my-account/invoice')}}" title=""><i class="fa fa-money"></i></a><!-- Invoice -->
                    <a href="{{url('/my-account/profile')}}" title=""><i class="fa fa-user"></i></a> <!-- Profile -->
                </div>
            </div><!-- Follow Widget -->
        </div>
    </div>
    <!-- End Menu -->
    
    @endguest


<section class="box-slider-search">


            <div class="container">


                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('warning'))
                <div class="alert alert-warning alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('info'))
                <div class="alert alert-info alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    Please check the form below for errors
                </div>
                @endif    



                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="z-index: 50 !important;">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <img src="{{asset('images/VIN Search.jpg')}}" class="img-responsive">
            </div>
          </div>
        </div>

                <h1 class="nocontent outline">--- Search form  ---</h1>
                <div class="row">
                    <div class="col-md-7">
                          <div class="box_slider">
                            <h2 style="text-align: center !important; margin-bottom: 70px !important;"><b style="color: #8a4b4b">autocheck report</b> <br> is ESSENTIAL when shopping for  used imported car</h2>
                          </div>
                        <div id="rev_slider-wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin-bottom: 40px !important">
                            <div class="tp-banner-container">
                                <div class="tp-banner" >
                                    <ul>                        
                                        <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" 
                                            data-saveperformance="on"  data-title="Ken Burns Slide">
                                            <!-- MAIN IMAGE -->
                                            <img src="{{asset('images/slider.jpg')}}"  alt="2" data-lazyload="{{asset('images/slider.jpg')}}" 
                                                 data-bgposition="right top" data-kenburns="off" data-duration="12000" 
                                                 data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" 
                                                 data-bgpositionend="center bottom">

                                             <!--     
                                            <div class="tp-caption tentered_white_huge lft tp-resizeme" 
                                                 data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600"
                                                 data-y="130" data-hoffset="0" data-x="center"
                                                 style="">
                                                <img alt="" src="{{asset('web2/img/4.png')}}" style="width: 110px; height: 110px;">
                                            </div>
                                            <div class="tp-caption tentered_white_huge lft tp-resizeme" 
                                                 data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600"
                                                 data-y="272" data-hoffset="0" data-x="center"
                                                 style="color: #fff; text-transform: uppercase; font-size: 40px; letter-spacing: 6px;
                                                 font-weight: 400;">
                                                
                                            </div>
                                            <div class="tp-caption tentered_white_huge lfb tp-resizeme" data-endspeed="300" 
                                                 data-easing="Power4.easeOut" data-start="800" data-speed="600" data-y="320" 
                                                 data-hoffset="0" data-x="center"
                                                 style="color: #fff; font-size: 13px; text-transform: uppercase; letter-spacing: 10px;">
                                                <i class="fa fa-map-marker"> </i> 
                                            </div>
                                            <div class="tp-caption tentered_white_huge lft tp-resizeme" 
                                                 data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600"
                                                 data-y="365" data-hoffset="0" data-x="center"
                                                 style="color: #fff; text-transform: uppercase; font-size: 40px; letter-spacing: 6px;
                                                 font-family: Montserrat; font-weight: 400;">
                                               
                                            </div> -->
                                            

                                        </li> 


                                        
                                    </ul>
                                    <div class="tp-bannertimer"></div>
                                    <div>

                                    </div>
                                </div>
                            </div>
                        </div><!-- END REVOLUTION SLIDER -->

                        <div class="box_slider">
                          <h4 style="text-align: center !important"><b>KNOW THE HISTORY: don’t risk buying a car with hidden defects</b> </h4>
                          <ul>
                            <li>Has an accident or stolen being reported for this vehicle?</li>
                            <li>Has it been damaged by flood, fire, earth quake or other natural disasters?</li>
                            <li>Odometer discrepancies (possible rollback)?</li>
                            <li>Has the vehicle ever been recalled?</li>
                            <li>The usage of the vehicle (taxi, hire and drive, rental etc)?</li>
                            <li>Is there a risk of radioactive contamination?</li>
                      </ul>
                        </div>

                    </div>

                    <style type="text/css">
                        .horizontal-search form {
                                padding: 10px 12px;
                                background-color: #FF2929 !important;
                            }
                    </style>
                    
                    <div class="col-md-5 box_search" style="background-color: #FF2929">
                        <div class="horizontal-search v-f-p"> 
                            <div class="search-form"> 
                                <h1 class="fsearch-title hidden-xs">
                                    <i class="fa fa-search"></i><span>CHECK CAR HISTORY NOW</span>
                                </h1>

                                
                                <div class="search_widget widget">

                                  <div class="box_text">
                                    <h3>Check details of your car <b>NOW</b> <br>
                                      <b>Free</b> vehicle search</p> 
                                    </h3>

                                  </div>

                                <style type="text/css">
                                    #banner {
                                      display: block;
                                      text-align: center;
                                      margin-top: 80px;
                                    }

                                    .images {
                                      display: inline-block;
                                      max-width: 20%;
                                      margin: 0 2.5%;
                                    }


                                    .tombol:hover {
                                        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
                                    }


                                    .tombol2:hover {
                                        color: red;
                                    }

                                </style>


                                <div id="banner" style="cursor: pointer;">
                                    <div class="images tombol">
                                        <img src="{{asset('images/flag/japan.jpg')}}" class="img-responsive" alt="jp" style="width:100%" name="flag" id="title-format-0" onclick= "document.forms[0].elements['country'].value = 'jp'">  
                                    </div>

                                    <div class="images tombol">
                                        <img src="{{asset('images/flag/uk.jpg')}}" class="img-responsive" alt="uk" style="width:100%" name="flag" id="title-format-1" onclick= "document.forms[0].elements['country'].value = 'uk'">
                                    </div>

                                    

                                </div>

                                

                                    <form method="POST" class="register-form" id="register-form" action="{{url('/check-vehicle-web')}}">
                                
                                        {{ csrf_field() }}
                                        
                                        <input type="hidden" name="country" id="country" value="jp" />

                                        <input type="text" placeholder="VIN / CHASSIS NUMBER" name="vehicle" style="font-size: 20pt !important" />

                                        <input type="submit" name="" value="Search" class="btn btn-primary btn-lg" style="background-color:  #995151; color: #FFFFFF; margin-top:20px !important;font-size: 20pt">
                                    </form>


                                    <a data-toggle="modal" data-target=".bd-example-modal-lg" style="margin-bottom: 100px; cursor: pointer">
                                      <p class="tombol2">Where can I find VIN/ Chassis number?</p>
                                    </a>

                                    


                                    <div class="box_text" style="margin-top: 100px">

                                    <h4 style="margin-top: 40px !important">
                                    Malaysia authorities are doing cross reference with our reports.
                                    <br>
                                     We gives you details of used imported car past history together with average market value (if available) from the origin countries. <br> </h3>

                                      <h4 style="margin-top: 40px !important">
                                       Improve your chances of buying a great car
                                     </h4>
                                       
                                      <ul>
                                         <li>Get access to comprehensive report</li>
                                         <li>Do not compromise your family safety</li>
                                         <li>Value for money</li>
                                       </ul>

                                      </div>

                                    </div>
                                     
                                </div><!-- Search Widget -->

                                

                                 
                            </div><!-- Services Sec -->

                        </div>

                        
                    </div>
    


                </div>
            </div>
        </section>





        

        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="faq-sec">
                            <div class="heading4">
                                <h2>VEHICLE HISTORY REPORT CHECKS FOR</h2>
                                
                            </div>
                            <br>
                            <div class="service-circle-sec">
                                <div class="row">
                                    <div class="col-md-3">
                                        <p align="center">
                                            <img src="{{asset('images/index/accid.jpg')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Accident</h4>
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <p align="center"><img src="{{asset('images/index/stolen1.jpg')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Stolen</h4></p>
                                    </div>
                                    <div class="col-md-3">
                                        <p align="center"><img src="{{asset('images/index/demage2.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Damaged</h4></p>
                                    </div>
                                    <div class="col-md-3">
                                        <p align="center"><img src="{{asset('images/index/Contamination.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Contamination</h4></p>
                                    </div>
                                </div>
                            </div>

                            <div class="service-circle-sec">
                                <div class="row">
                                    <div class="col-md-3">
                                        <p align="center"><img src="{{asset('images/index/odorometer.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Odometer Rollback</h4></p>
                                    </div>
                                    <div class="col-md-3">
                                        <p align="center"><img src="{{asset('images/index/sales.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Market Price</h4></p>
                                    </div>
                                    <div class="col-md-3">
                                        <p align="center"><img src="{{asset('images/index/rating.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Safety Ratings</h4></p>
                                    </div>
                                    <div class="col-md-3">
                                        <p align="center"><img src="{{asset('images/index/specifi.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Specification</h4></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


@endsection

@push('js')
    <script type="text/javascript">
        var img1 = document.getElementById('title-format-0');
        var img2 = document.getElementById('title-format-1');
        var country = document.getElementById('country');
        
        img2.style.filter = 'blur(3px)';

        img1.onclick = function(){
            img2.style.filter = 'blur(3px)';
            img1.style.filter = 'blur(0px)';
            country.value = "jp";
        }

        img2.onclick = function(){
           img1.style.filter = 'blur(3px)';
           img2.style.filter = 'blur(0px)';
           country.value = "uk";
        }        

    </script>
@endpush