<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Autocheck</title>
    <meta name="description" content="">
    <meta name="keywords" content="A high-quality &amp; Autocheck, Check Kereta, Semak, Car Import Checking, Car Checking, Vehicle, VIN, Chassis Number, Chassis, Kereta Malaysia, Malaysia Automotif, Malaysian Car.">

    <!-- <base href="/autocheck/public/"> -->
        <!-- Basic Styles -->

    <base href="/autocheck/public/">
    <!-- Basic Styles -->
        

    <!-- Styles -->
    <link rel="stylesheet" href="/web2/css/bootstrap.min.css" type="text/css" /><!-- Bootstrap -->
    <link rel="stylesheet" href="/web2/fonts/font-awesome/css/font-awesome.min.css" type="text/css" /><!-- Icons -->
    <link rel="stylesheet" href="/web2/fonts/themify-icons/themify-icons.css" type="text/css" /><!-- Icons -->
    <link rel="stylesheet" href="/web2/css/owl.carousel.css" type="text/css" /><!-- Owl Carousal-->
    <link rel="stylesheet" href="/web2/css/price-range.css" type="text/css" /><!-- Owl Carousal -->

    <link rel="stylesheet" href="/web2/css/style.css" type="text/css" /><!-- Style -->  
    <link rel="stylesheet" href="/web2/css/responsive.css" type="text/css" /><!-- Responsive -->    
    <link rel="stylesheet" href="/web2/css/colors.css" type="text/css" /><!-- color --> 

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="/web2/js/rs-plugin/css/settings.css">

    <!-- custom -->
    <link rel="stylesheet" type="text/css" href="/web2/css/custom.css">

    <!-- Favicon-->
    <link rel="shortcut icon" href="/images/fav.ico">

    <style type="text/css">
        .prg{
            color: #ffffff; 
            padding-top: 50px !important;
            padding-left: 50px !important;
            padding-right: 50px !important;
        }
    </style>

</head>
<body>

    @include('sweetalert::alert')

    <!-- /.preloader -->
    <div id="preloader"></div>
    <div class="theme-layout">

        <!--<div class="account-popup-sec">
            
            <div class="account-popup-area">
                <div class="account-popup">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="account-user">
                                <div class="logo">
                                    <a href="#" title="">
                                        
                                        <img src="{{asset('web/img/logobx.png')}}" alt="">
                                    </a>
                                </div>
                                <form>
                                    <h1>Login Form</h1>
                                    <div class="field">
                                        <input type="text" placeholder="Username" />
                                    </div>
                                    <div class="field">
                                        <input type="password" placeholder="Password" />
                                    </div>
                                    <div class="field">
                                        <input type="submit" value="LOGIN" class="flat-btn" />
                                    </div>
                                </form>
                                <i>OR</i>
                                <span>LOGIN WITH</span>
                                <ul class="social-btns">
                                    <li></li>
                                    <li></li>
                                    <li></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="registration-sec">
                                <h1>SIGNUP Form</h1>
                                <form>
                                    <div class="field">
                                        <input type="text" placeholder="Your Name" />
                                    </div>
                                    <div class="field">
                                        <input type="text" placeholder="Your Email" />
                                    </div>
                                    <div class="field">
                                        <input type="password" placeholder="Type Password" />
                                    </div>
                                    <div class="field">
                                        <input type="password" placeholder="Retype Password" />
                                    </div>                                   
                                    <label>
                                        <input type="checkbox" /> By Clicking on this You are agree with our <a href="#" title="">Terms & Condition</a>
                                    </label>
                                    <input type="submit" value="Singup Now" class="flat-btn" />
                                </form>
                            </div>
                        </div>
                    </div>
                    <span class="close-popup"><i class="fa fa-close"></i></span>
                </div>
            </div>
        </div> --> <!-- Account Popup Sec -->

         <header class="simple-header for-sticky white">
            <!-- 
            <div class="top-bar">
                <div class="container">
                    <ul class="contact-item">
                        <li><i class="fa fa-envelope-o"></i>yourcompnay@email.com</li>
                        <li><i class="fa fa-mobile"></i>+1 333 44 555 / +1 333 44 500</li>
                    </ul>
                     <div class="choose-language">
                        <a href="#" title="">FR</a>
                        <a href="#" title="">DE</a>
                        <a href="#" title="">EN</a>
                        <a href="#" title="">jp</a>
                    </div> 
                </div>
            </div> --><!-- Top bar -->
            <div class="menu">
                <div class="container">
                    <div class="logo">
                        <a href="{{url('/')}}" title="">
                            <img src="{{asset('web/img/logobx.png')}}" alt="" width="160%" height="100%">
                        </a>
                    </div><!-- LOGO -->


                    @guest
                    <a href="{{url('login')}}" title="">
                        <div class="popup-client">
                            <span><i class="fa fa-user"></i>Login / Register</span>
                        </div>
                    </a>
                    @else

                    <a href="{{ route('logout') }}" title="">
                        <div class="popup-client" style="margin-left: 15px !important;">
                            <span style="background-color: #ff2929"><i class="fa fa-sign-out"></i> Logout </span>
                        </div>
                    </a>

                    <a href="{{url('/')}}" title="">
                        <div class="popup-client" style="margin-left: 15px !important">
                            <span><i class="fa fa-user"></i>{{Auth::user()->name}}</span>
                        </div>
                    </a>

                    @endguest



                    <span class="menu-toggle"><i class="fa fa-bars"></i></span>
                    <nav>  
                        <h1 class="nocontent outline">--- Main Navigation ---</h1>
                       
                        <ul>
                            <li><a href="{{url('/')}}" title="">HOME</a></li>

                            <li><a href="{{url('sample_report')}}" title="">VIEW SAMPLE REPORT</a></li>

                            <li><a href="{{url('how-to-buy')}}" title="">HOW TO BUY</a></li>


                           <!--  <li class="menu-item-has-children">
                                <a href="#" title="">ABOUT US</a>
                                <ul>
                                    <li><a href="blog.html" title="">Blog Page</a></li>
                                    <li><a href="blog-sidebar.html" title="">Blog left Sidebar</a></li>
                                    <li><a href="single.html" title="">Single Post</a></li>
                                    <li><a href="single-left-sidebar.html" title="">Single Left sidebar</a></li>
                                    <li><a href="single-no-sidebar.html" title="">Single No Sidebar</a></li>
                                </ul>
                            </li> -->

                            <!-- <li><a href="#" title="">ABOUT US</a></li> -->

                            <!-- <li class="menu-item-has-children">
                                <a href="#" title="">ABOUT US</a>
                                <ul>
                                    <li><a href="blog.html" title="">Blog Page</a></li>
                                    <li><a href="blog-sidebar.html" title="">Blog left Sidebar</a></li>
                                    <li><a href="single.html" title="">Single Post</a></li>
                                    <li><a href="single-left-sidebar.html" title="">Single Left sidebar</a></li>
                                    <li><a href="single-no-sidebar.html" title="">Single No Sidebar</a></li>
                                </ul>
                            </li> -->

                            <!-- <li><a href="#" title="">FAQ</a></li> -->
                        </ul>
                    </nav>

                </div>
            </div>
        </header>       
        
        

        @yield('content')
  

        <section class="block">
            <div style="background: url('web2/img/call-to-action-bg.jpg') repeat scroll 50% 422.28px transparent; background-attachment: fixed;" class="parallax scrolly-invisible  blackish"></div><!-- PARALLAX BACKGROUND IMAGE -->  
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="vehiculs-text-bar">
                            <h3>CHECK  <span> CAR STORY </span>NOW ! </h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        

        <footer class="light-footer">
            <section class="top-line">
                <div style="background: url('web2/img/footer.jpg') repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax whitish"></div><!-- PARALLAX BACKGROUND IMAGE --> 
                <div class="container">
                    <div class="row">   

                        <div class="col-md-4 column">
                            <div class="about_widget widget">
                                <div class="heading1">
                                    <h2><span>AUTOCHECK</span>.MARII.MY</h2>
                                </div><!-- heading -->

                                <!-- <span><i class="fa fa-envelope"></i>yourcompany@gmail.com</span>
                                <span><i class="fa fa-phone"></i>0888 (29999996)</span>
                                <span><i class="fa fa-location-arrow"></i>1234 Tokyo shibuia , WI 54115</span> -->
                                
                            </div>
                        </div>

                        <div class="col-md-4 column">
                            <div class="links_widget widget">
                                <div class="heading1">
                                    
                                </div><!-- heading -->
                                <ul>
                                    <li><a href="{{url('/')}}" title=""><i class="fa fa-angle-right"></i> Home</a></li>
                                    <li><a href="{{url('sample_report')}}" title=""><i class="fa fa-angle-right"></i> View Sample Report</a></li>
                                    <li><a href="{{url('how-to-buy')}}" title=""><i class="fa fa-angle-right"></i>How To Buy</a></li> 
                                    
                                     
                                </ul>
                            </div>
                        </div>

                       
                        <div class="col-md-4 column">
                            <div class="subscribe_widget widget">
                                <div class="heading1">
                                    <h2><span>Subscribe</span> Us</h2>
                                </div><!-- heading -->
                                
                                <form>
                                    <label><input type="text" placeholder="TYPE YOUR EMAIL" /></label>
                                    <button type="submit" class="flat-btn"><i class="ti ti-email"></i></button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <div class="bottom-line">
                <div class="container">
                    <span>Copyright All Right Reserved <?php echo date("Y");?> <a href="#" title="">Autocheck</a></span>
                    <ul>
                       
                       <li><a title="" href="{{url('/')}}">HOME</a></li>
                        <li><a title="" href="{{url('sample_report')}}">VIEW SAMPLE REPORT</a></li>
                        <li><a title="" href="{{url('how-to-buy')}}">HOW TO BUY</a></li>
                       
                        
                    </ul>
                </div>
            </div>
            <a href="#" class="scrollToTop"><i class="ti ti-arrow-circle-up"></i></a>
        </footer>

    </div>

    <!-- Script -->
    <script type="text/javascript" src="/web2/js/modernizr.js"></script><!-- Modernizer -->
    <script type="text/javascript" src="/web2/js/jquery-1.10.2.min.js"></script><!-- Jquery -->
    <script type="text/javascript" src="/web2/js/bootstrap.min.js"></script><!-- Bootstrap -->
    <script type="text/javascript" src="/web2/js/owl.carousel.min.js"></script><!-- Owl Carousal -->
    <script type="text/javascript" src="/web2/js/html5lightbox.js"></script><!-- HTML -->
    <script type="text/javascript" src="/web2/js/scrolly.js"></script><!-- Parallax -->
    <script type="text/javascript" src="/web2/js/price-range.js"></script><!-- Parallax -->
    <script type="text/javascript" src="/web2/js/script.js"></script><!-- Script -->

    <script src="/web2/js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>  

    <script src="/web2/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    @stack('js')


    <!-- <script src="{{asset(web2/js/rs-plugin/js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
    <script src="{{asset(web2/js/rs-plugin/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>!-->

    <script type="text/javascript">
        $(document).ready(function () {
            "use strict";
            jQuery('.tp-banner').show().revolution({
                dottedOverlay: "none",
                delay: 16000,
                startwidth: 1170,
                startheight: 700,
                hideThumbs: 200,
                thumbWidth: 100,
                thumbHeight: 50,
                thumbAmount: 5,
                navigationType: "bullet",
                navigationArrows: "solo",
                navigationStyle: "preview1",
                touchenabled: "on",
                onHoverStop: "on",
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
                parallax: "mouse",
                parallaxBgFreeze: "on",
                parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                keyboardNavigation: "off",
                navigationHAlign: "center",
                navigationVAlign: "bottom",
                navigationHOffset: 0,
                navigationVOffset: 20,
                soloArrowLeftHalign: "left",
                soloArrowLeftValign: "center",
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,
                soloArrowRightHalign: "right",
                soloArrowRightValign: "center",
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,
                shadow: 0,
                fullWidth: "on",
                fullScreen: "off",
                spinner: "spinner4",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                forceFullWidth: "off",
                hideThumbsOnMobile: "off",
                hideNavDelayOnMobile: 1500,
                hideBulletsOnMobile: "off",
                hideArrowsOnMobile: "off",
                hideThumbsUnderResolution: 0,
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                videoJsPath: "rs-plugin/video{{asset('web2/js/",
                fullScreenOffsetContainer: ""
            });

            $(".carousel-prop").owlCarousel({
                autoplay: true,
                autoplayTimeout: 3000,
                smartSpeed: 2000,
                loop: true,
                dots: true,
                nav: false,
                items: 4,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: false
                    },
                    600: {
                        items: 2,
                        nav: false
                    },
                    1000: {
                        items: 3,
                        nav: true,
                        loop: false
                    }
                }
            });

            $(".carousel").owlCarousel({
                autoplay: true,
                autoplayTimeout: 3000,
                smartSpeed: 2000,
                loop: false,
                dots: false,
                nav: true,
                margin: 0,
                items: 3
            });

            $(".carousel-client").owlCarousel({
                autoplay: true,
                autoplayTimeout: 3000,
                smartSpeed: 2000,
                loop: false,
                dots: false,
                nav: true,
                margin: 30,
                items: 5,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: true
                    },
                    1000: {
                        items: 5,
                        nav: true,
                        loop: false
                    }
                }
            });

        });
    </script>
</body>
</html>
