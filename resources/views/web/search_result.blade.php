@extends('web.layout2.template_local')

@section('content')

<style>
    .modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -100 !important;
    background-color: #000;
}
</style>


<div class="inner-head overlap">
    <div style="background: url(web2/img/parallax1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div>  
    <div class="container">
        <div class="inner-content">
            <span><i class="ti ti-home"></i></span>
            <h2>Search Vehicle </h2>
            <ul>
                <li><a href="{{url('/')}}" title="">HOME</a></li>
                <li><a href="#" title="">SEARCH VEHICLE</a></li>
            </ul>
        </div>
    </div>
</div>

<section class="block">
            <div class="container agnet-prop">
                
                <div class="row"> 
                   

                    <div class="col-md-4 column hidden-xs" align="center">
                        <h2>Sample Report</h2>

                        <a type="button" data-toggle="modal" data-target=".bd-example-modal-sm1">
                                     
                            <img src="{{asset('images/report/report_sample_searching.jpg')}}" alt="" class="img-responsive" />

                        </a>

                        
                    </div>

                    <div class="col-md-8 column">
                        <div class="heading4">
                            <h2>CONGRATULATIONS! <br>
                                YOUR CAR RECORDS FOUND</h2> 
                        </div>

                        <div class="submit-content">
                            <form id="new_post" name="new_post" method="post" class="vehicul-form" role="form" action="{{url('/pay-chassis')}}">

                                {{ csrf_field() }}

                                <input type="hidden" id="title" class="form-control" value="{{$search->cars[0]->chassisNumber}}" name="chassis" required="">

                                <input type="hidden" id="title" class="form-control" value="{{$search->cars[0]->chassisNumber}}" name="vehicle" required="">

                                <input type="hidden" name="carid" value="{{$search->cars[0]->carId}}"  readonly="" />


                                <div class="control-group">
                                    <div class="group-title">VEHICLE DETAILS - CHASSIS :{{$search->cars[0]->make}}</div>
                                    <div class="group-container row">

                                        <div class="col-md-4">
                                            
                                            <img src="{{url('https://carvx.jp/'.$search->cars[0]->image)}}" class="img-responsive" style="margin-bottom: 40px !important">

                                        </div>

                                        <div class="col-md-8">
                                            <div class="col-md-6">
                                                <div class="form-group s-prop-title">
                                                    <label for="title">Brand</label>
                                                    <input type="text" id="title" class="form-control" value="{{$search->cars[0]->make}}" name="brand" required="">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group s-prop-area">
                                                    <label for="area">Body </label>
                                                    <input type="text" id="area" class="form-control" value="{{$search->cars[0]->body}}" name="body">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group s-prop-title">
                                                    <label for="title">Model</label>
                                                    <input type="text" id="title" class="form-control" value="{{$search->cars[0]->model}}" name="model" required="">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group s-prop-area">
                                                    <label for="area">Engine Model </label>
                                                    <input type="text" id="area" class="form-control" value="{{$search->cars[0]->engine}}" name="engine">
                                                </div>
                                            </div>

                                        </div>


                                        
                                            
                                            <div class="col-md-4">
                                                <div class="form-group s-prop-status">
                                                    <label>Grade</label>
                                                    <input type="text" id="area" class="form-control" value="{{$search->cars[0]->grade}}" name="area">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                <div class="form-group s-prop-status">
                                                    <label>Transmision</label>
                                                    
                                                    <input type="text" id="area" class="form-control" value="{{$search->cars[0]->transmission}}" name="transmision"> 
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group s-prop-status">
                                                    <label>Drive</label>
                                                    <input type="text" id="area" class="form-control" value="{{$search->cars[0]->drive}}" name="drive">
                                                </div>
                                            </div>

                                            
                                        
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="group-title">Buy Report</div>
                                    <div class="group-container row">
                                        <div class="col-md-12">
                                            <div id="upload-container">
                                                <div id="aaiu-upload-container">
                                                    <div class="col-md-4">
                                                        <img src="{{asset('images/search_car.jpg')}}" width="100%" height="10%" class="hidden-xs img-responsive">
                                                    </div>

                                                    <div class="col-md-8">
                                                        <h2>RM {{$fee_extrareport->fee}}</h2>
                                                        
                                                        @guest

                                                        <a href="{{url('login')}}" class="btn btn-lg flat-btn" style="margin-right: 20px">Login
                                                        </a>

                                                            

                                                        
                                                        @else
                                                        
                                                        <input type="submit" class="btn btn-lg flat-btn" id="vehicul_submit" value="Buy Vehicle" style="margin-right: 20px">
                                                        @endguest


                                                        <div>


                                                        <a class="btn btn-lg flat-btn" id="showmenu" style="background-color: #d34e4e !important">
                                                            Search Other Vehicle
                                                        </a>

                                                        </div>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                

                            </form>

                            <div class="control-group menu" style="display: none; background-color: #ffffff">
                                    <div class="group-title">Search Other Vehicle</div>
                                    <div class="group-container row">
                                        <div class="col-md-12">
                                            <div id="upload-container">
                                                <div id="aaiu-upload-container">
                                                    
                                                    <form method="POST" class="register-form" id="register-form" action="{{url('/check-vehicle-web')}}">
                                                    {{ csrf_field() }}

                                                    <div class="col-md-8">
                                                        <input type="text" id="area" class="form-control col-lg" value="" name="vehicle" placeholder="VIN/CHASSIS NUMBER">
                                                    </div>

                                                    <div class="col-md-4">

                                                        <input type="submit" class="btn btn-lg flat-btn" id="vehicul_submit" value="Search" style="margin-right: 20px">
                                                    <div>

                                                    </form>
                                                    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 



                        </div>
                    </div>
                </div>
            </div> 


            <div class="modal fade bd-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="z-index: 50 !important;">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h1 class="modal-title" id="exampleModalLabel">Sample Report </h1>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" align="center">
                   <img src="{{asset('images/report/report_sample_searching.jpg')}}" alt="" class="img-responsive" />
                     
                </div>
                <div class="modal-footer">
                  
                  
                </div>


              </div>
            </div>
          </div>
        </section>

                    
@endsection


@push('js')
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#showmenu').click(function() {
                    $('.menu').toggle("slide");
            });
        });
    </script>

@endpush