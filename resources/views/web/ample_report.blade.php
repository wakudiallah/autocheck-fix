@extends('web.layout2.template')

@section('content')


		<div class="inner-head overlap">
		    <div style="background: url(web2/img/parallax1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!- PARALLAX BACKGROUND IMAGE ->   
		    <div class="container">
		        <div class="inner-content">
		            <span><i class="ti ti-home"></i></span>
		            <h2>Sample Report</h2>
		            <ul>
		                <li><a href="{{url('/')}}" title="">HOME</a></li>
		                <li><a href="#" title="">SAMPLE REPORT</a></li>
		            </ul>
		        </div>
		    </div>
		</div>


 <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">	
                        <div class="row">
                            
                            
                            <aside class="col-md-4 column">
                                
                                
                                <div class="category_widget widget">
                                    <div class="heading2">
                                        <h2>Categories</h2>
                                    </div>
                                    <ul>
                                        <li><a href="#" title="">VEHICLE DETAILS</a> </li>
                                        <li><a href="#" title="">ACCIDENT / REPAIR HISTORY</a></li>
                                        <li><a href="#" title="">ODOMETER READINGS HISTORY</a></li>
                                        <li><a href="#" title="">USE HISTORY</a></li>
                                        <li><a href="#" title="">DETAILED HISTORY</a></li>
                                        <li><a href="#" title="">MANUFACTURER RECALL HISTORY</a></li>
                                        <li><a href="#" title="">VEHICLE ASSESSMENT</a></li>
                                        <li><a href="#" title="">VEHICLE SPECIFICATION</a></li>
                                        <li><a href="#" title="">AUCTION DATA</a></li>
                                        <li><a href="#" title="">PHOTOS AND AUCTION SHEETS</a></li>
                                    </ul>
                                </div><!-- Category Widget -->
                            </aside>
                            
                             <div class="col-md-8 column">
                                <div class="single-post-sec">
                                    <div class="blog-post">
                                        <div class="post-thumb">
                                            <img src="{{url('images/report/report_sample_searching_full.jpg')}}" alt="" />
                                            <div class="post-detail">
                                                
                                                <div class="post-admin">
                                                    <a href="#" title=""> 
                                                        <h3>Sample Report</h3>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>


                                       
                                        <blockquote>
                                            <p>Voulpat dolor sit amet, consectetuer adipiscing elit. Sed diam nonummy nibh euismod tincidunt ut laoreet</p>
                                            <span>Directed By : <a href="#" title="">Jonathan Doe</a></span>
                                        </blockquote>
                                        
                                        <div class="post-tags">
                                            <span><i class="fa fa-tags"></i> Tags :</span>
                                            <a href="#" title="">Admin</a>,
                                            <a href="#" title="">Web Design</a>,
                                            <a href="#" title="">Numberstheme</a>,
                                            <a href="#" title="">Cool</a>,
                                            <a href="#" title="">Responsive</a>,
                                            <a href="#" title="">Hello</a>,
                                            <a href="#" title="">Parallax</a>,
                                        </div>
                                    </div><!-- Blog Post -->
                                </div><!-- Blog POst Sec -->
                            </div>
                            
                        </div>
                      

                </div>
            </div>
        </section>

@endsection
