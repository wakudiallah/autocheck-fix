<!--Header_section-->
    <header id="header_outer">
        <div class="container">
            <div class="header_section">
                <div class="logo"><a href="javascript:void(0)"><img src="{{asset('web/img/logobx.png')}}" alt=""></a></div>
                <nav class="nav" id="nav">
                    <ul class="toggle">
                        <li><a href="#top_content">Home</a></li>
                        <li><a href="#service">Services</a></li>
                        <!-- <li><a href="#client_outer">Clients</a></li>-->
                        <!-- <li><a href="#team">Team</a></li>-->
                        <li><a href="#contact">Contact</a></li>
                        @guest
                        <li><a href="{{url('login')}}">Login</a></li>
                        <li><a href="{{url('register')}}">Register</a></li>
                        @else
                        <li><a href="{{url('dashboard')}}">{{Auth::user()->name}}</a></li>
            
                        @endguest
                    </ul>
                    <ul class="">
                        <li><a href="#top_content">Home</a></li>
                        <li><a href="#service">Services</a></li>
                        <!--<li><a href="#client_outer">Clients</a></li>-->
                        <!-- <li><a href="#team">Team</a></li> -->
                        <li><a href="#contact">Contact</a></li>
                        @guest
                        <!-- <li><a href="{{url('login')}}">Login</a></li>
                        <li><a href="{{url('register')}}" class="card-post__category badge badge-pill badge-warning">Register</a></li> -->
                        
                            <a href="{{url('login')}}">
                                <button type="button" class="mb-2 btn btn-primary mr-2">Login</button>
                            </a>
                       
                            <a href="{{url('register')}}">
                                <button type="button" class="mb-2 btn btn-primary mr-2">Register</button>
                            </a>
                        @else
                        <li>
                            <a href="{{url('dashboard')}}">

                            <button type="button" class="mb-2 btn btn-primary mr-2">My Account</button>

                            </a>
                        </li>
            
                        @endguest
                    </ul>
                </nav>
                <a class="res-nav_click animated wobble wow" href="javascript:void(0)"><i class="fa-bars"></i></a> </div>
        </div>
    </header>
    <!--Header_section-->