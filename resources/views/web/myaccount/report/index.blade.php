@extends('web.layout2.template_local')

@section('content')


<style>
    .modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -100 !important;
    background-color: #000;
}



.horizontal-search form {
    padding: 10px 12px;
    background-color: #404cad !important;
}

.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

</style>


<!-- Menu -->
     <div class="inner-head overlap">
        <div style="background: url({{asset('web2/img/parallax1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div>  
        <div class="container">
            
            <div class="follow_widget widget">
                
                <div class="social_widget">
                    <a class="active" href="{{url('/my-account/report')}}" title=""><i class="fa fa-list"></i></a> <!-- report --> 
                    <a href="{{url('/my-account/invoice')}}" title=""><i class="fa fa-money"></i></a><!-- Invoice -->
                    <a href="{{url('/my-account/profile')}}" title=""><i class="fa fa-user"></i></a> <!-- Profile -->
                </div>
            </div><!-- Follow Widget -->
        </div>
    </div>
    <!-- End Menu -->

<section class="box-slider-search">

          <div class="container">


                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('warning'))
                <div class="alert alert-warning alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('info'))
                <div class="alert alert-info alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    Please check the form below for errors
                </div>
                @endif




                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="z-index: 50 !important;">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <img src="{{asset('images/vehicle location.jpg')}}" class="img-responsive">
            </div>
          </div>
        </div>

                <h1 class="nocontent outline">--- Search form  ---</h1>
                <div class="row">
                    <div class="col-md-12">
                          <div class="box_slider">
                            <h2 style="text-align: center !important; "><b style="color: #8a4b4b">Report</b> <br> </h2>
                          </div>
                    </div>
                </div>

                
            </div>
        </section>


        <section class="box-slider-search">
            <div class="container">
                <h1 class="nocontent outline">--- Search form  ---</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="horizontal-search v-f-p"> 
                            <div class="search-form"> 
                                <h1 class="fsearch-title">
                                    <i class="fa fa-search"></i><span>Report</span>
                                </h1>

                                <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Chassis</th>
                                  <th>Request Date</th>
                                  <th>Brand</th>
                                  <th>Model</th>
                                  <th>Status</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                             @foreach($get_vehicle as $data) 
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}} </td>
                                  <td>
                                    <?php $tarikh =  date('d-m-Y ', strtotime($data->created_at)); ?> {{$tarikh}}
                                  </td>
                                  
                                  <td>
                                    @if($data->searching_by == "NOT")
                                        @if(!empty($data->brand->brand))
                                        {{$data->brand->brand}}
                                        @endif
                                    @else
                                        @if(!empty($data->vehicle_api->brand))
                                        {{$data->vehicle_api->brand}}
                                        @endif
                                    @endif
                                  </td>
                                  <td>  

                                    @if($data->searching_by == "NOT")
                                        @if(!empty($data->model_brand->model))
                                        {{$data->model_brand->model}}
                                        @endif
                                    @else 
                                        @if(!empty($data->vehicle_api->model))
                                        {{$data->vehicle_api->model}}
                                        
                                        @endif
                                    @endif
                                  </td>

                                  <td align="center"><!-- status -->


                                    @if($data->status == '10' OR $data->status == '20')
                                    <i class="badge badge-pill badge-primary bg-primary">New</i><!-- New Not -->
                                    @endif


                                    @if($data->status == '25')
                                    <i class="badge badge-pill badge-primary bg-primary">Proccess</i><!-- New Not -->
                                    @endif


                                    @if($data->status == '26' AND $data->payment_status == '0')
                                    <i class="badge badge-pill badge-primary bg-primary">To Pay</i> <!-- New API -->
                                    @endif


                                    @if($data->status == '26' AND $data->payment_status == '1')
                                    <i class="badge badge-pill badge-primary bg-primary">Pay</i> 

                                    <i class="badge badge-pill badge-primary bg-primary">In Process</i> 
                                    @endif


                                    @if($data->status == '40')
                                    <i href="#" class="badge badge-pill badge-primary bg-primary">Complete</i>
                                    @endif

                                    @if($data->status == '30')
                                    <i href="#" class="badge badge-pill badge-primary bg-primary">Not Found</i>
                                    @endif
                                  </td><!-- end status -->


                                  <td align="center"> <!-- action -->
                                    
                                    
                                    @if($data->is_send == '1' AND $data->brand_id != 'NULL') <!-- manual dan brand != 0 -->

                                    <a href="{{url('report-autocheck/'.$data->id_vehicle)}}" target="_blank">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="fa fa-download"></i> Pay
                                      </button>
                                    </a>

                                    @endif




                                    @if($data->status == '40')
                                        @if($data->searching_by == 'API')
                                            <a href="{{url('report-autocheck/'.$data->id_vehicle)}}" target="_blank">
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                <i class="fa fa-download"></i> 
                                              </button>
                                            </a>
                                        @elseif($data->searching_by == 'NOT')
                                            

                                          <a href="{{ url('/') }}/report/manual/{{$data->full_report}}" target="_blank">
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                <i class="fa fa-download"></i> Manual
                                              </button>
                                            </a>
                                        @endif

                                    @endif


                                    @if($data->status == '26' AND $data->payment_status == '0')

                                    <a href="{{url('payment-mn/'.$data->id_vehicle)}}" target="_blank">
                                      <button type="button" class="mb-2 btn btn-sm btn-warning mr-1"> 
                                        <i class="fa fa-money"></i> To Pay
                                      </button>
                                    </a>

                                    @endif

                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="fa fa-history"></i>
                                      </button>
                                    </a>
                                  </td><!-- end action -->
                              </tr>
                            @endforeach 
                          </tbody>
                          
                      </table>
                            </div><!-- Services Sec -->

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>

        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="faq-sec">
                            <div class="heading4">
                                <h2>Check Before Buying </h2>
                                
                            </div>
                            <br>
                            <div class="service-circle-sec">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/accid.jpg')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Accident</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/stolen1.jpg')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Stolen</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/demage2.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Damaged</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/Contamination.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Contamination</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="service-circle-sec">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/odorometer.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Odometer Rollback</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/sales.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Market Price</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/rating.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Safety Ratings</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="{{asset('images/index/specifi.png')}}" alt="" class="img-responsive rounded-circle center" width="50%" height="50%">
                                        <h4 style="text-align: center ">Specification</h4>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


           

@endsection


@push('js')

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush

