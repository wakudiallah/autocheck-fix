@extends('web.layout2.template_local')

@section('content')


<style>
    .modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -100 !important;
    background-color: #000;
}


.horizontal-search form {
    padding: 10px 12px;
    background-color: #404cad !important;
}

.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

</style>



<!-- Menu -->
    <div class="inner-head overlap">
        <div style="background: url({{asset('web2/img/parallax1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div>  
        <div class="container">
            
            <div class="follow_widget widget">
                
                <div class="social_widget">
                    <a href="{{url('/my-account/report')}}" title=""><i class="fa fa-list"></i></a> <!-- report --> 
                    <a href="{{url('/my-account/invoice')}}" title=""><i class="fa fa-money"></i></a><!-- Invoice -->
                    <a href="{{url('/my-account/profile')}}" title=""><i class="fa fa-user"></i></a> <!-- Profile -->
                </div>
            </div><!-- Follow Widget -->
        </div>
    </div>
    <!-- End Menu -->

    <section class="box-slider-search">
      <div class="container">

        <div class="row">
            <div class="col-md-12">
                  <div class="box_slider">
                    <h2 style="text-align: center !important; "><b style="color: #8a4b4b">Profile</b> <br> </h2>
                  </div>
            </div>
        </div>

        
        <div class="row">
              <div class="col-lg-4">
                <!-- 
                  <div class="user-info">
                    <div class="profile-pic">@if(empty(Auth::user()->picture)) <img src="{{asset('images/avatar.png')}}" width="110"/> @else<img src="{{asset('images/profile/'.Auth::user()->picture)}}" width="110"/>@endif
                      <div class="layer">
                        <div class="loader"></div>
                      </div><a class="image-wrapper" href="#">
                        
                       <form id="uploadform"  action="{{url('save/profile/'.Auth::user()->id)}}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}
                              
                              <input type="file" name="fileToUpload" id="changePicture" class="hidden-input test form-control" onchange='this.form.submit()'>

                              <label class="edit glyphicon glyphicon-pencil" for="changePicture" type="file" title="Change picture"><i class="material-icons">edit</i></label>

                          </form>
                          </a>


                    </div>
                    <div class="username">
                      
                    </div>
                  </div>
                -->

                <div class="card card-small mb-4 pt-3" style="margin-top: 90px">
                  <div class="card-header border-bottom text-center">
                    

                    <div class="mb-3 mx-auto">
                        


                      <!--<img class="rounded-circle" src="images/avatars/0.jpg" alt="User Avatar" width="110"> -->
                    </div>
                    <h4 class="mb-0">{{Auth::user()->name}}</h4>
                    <span class="text-muted d-block mb-2">{{Auth::user()->email}}</span>
                    
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-4">
                      <div class="progress-wrapper">
                        <strong class="text-muted d-block mb-2">Workload</strong>
                        <div class="progress progress-sm">
                          <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="74" aria-valuemin="0" aria-valuemax="100" style="width: 74%;">
                            <span class="progress-value">74%</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item p-4">
                      <strong class="text-muted d-block mb-2">Description</strong>
                      <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="First Name" value="{{$data->name}}" disabled=""> 
                              </div>

                                                            

                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="text" class="form-control" id="feLastName" placeholder="Last Name" value="{{$data->email}}" readonly=""> 
                              </div>
                            </div>


                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">IC / Passport</label>
                                @if(!empty($data->detail_user->ic))
                                <input type="text" class="form-control" id="feFirstName" placeholder="IC / Passport" value="{{$data->detail_user->ic}}" disabled=""> 
                                @else
                                <input type="text" class="form-control" id="feFirstName" placeholder="IC / Passport" value="" disabled=""> 
                                @endif
                              </div>

                              <div class="form-group col-md-6">
                                <label for="feFirstName">Gender</label>
                                @if(!empty($data->detail_user->gender))

                                  @if($data->detail_user->gender == 1)
                                    <input type="text" class="form-control"  placeholder="Gender" value="Male" disabled=""> 
                                  @else
                                    <input type="text" class="form-control"  placeholder="Gender" value="Female" disabled="">
                                  @endif

                                @else
                                <input type="text" class="form-control" id="feFirstName" placeholder="Gender" value="" disabled=""> 
                                @endif
                              </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Home Address</label>
                                @if(!empty($data->detail_user->home_address))
                                <textarea class="form-control" name="feDescription" rows="5" readonly="">{{$data->detail_user->home_address}}</textarea>
                                @else
                                <textarea class="form-control" name="feDescription" rows="4" readonly=""></textarea>
                                @endif
                              </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Mailing Address</label>
                                @if(!empty($data->detail_user->mailing_address))
                                <textarea class="form-control" name="feDescription" rows="4" readonly="">{{$data->detail_user->mailing_address}}</textarea>
                                @else
                                <textarea class="form-control" name="feDescription" rows="5" readonly=""></textarea>
                                @endif
                              </div>
                            </div>


                            

                            <div class="form-row">
                              <div class="form-group col-md-2">
                                <label for="feInputCity">Postcode</label>
                                @if(!empty($data->detail_user->postcode))
                                <input type="text" class="form-control" id="feInputCity" value="{{$data->detail_user->postcode}}" readonly="">
                                @else
                                <input type="text" class="form-control" id="feInputCity" value="" placeholder="Postcode" readonly="">
                                @endif
                              </div>
                              <div class="form-group col-md-4">
                                <label for="feInputState">State</label>
                                @if(!empty($data->detail_user->bandar))
                                <input type="text" class="form-control" id="inputZip" readonly value="{{$data->detail_user->bandar}}"> 
                                @else
                                <input type="text" class="form-control" id="inputZip" placeholder="State" readonly=""> 
                                @endif
                              </div>
                              <div class="form-group col-md-6">
                                <label for="inputZip">Country</label>

                                @if(!empty($data->detail_user->postcode))
                                <input type="text" class="form-control" readonly id="inputZip" value="{{$data->detail_user->negara}}">  
                                @else
                                <input type="text" class="form-control" id="inputZip" placeholder="Country" readonly=""> 
                                @endif
                              </div>
                            </div>

                            <hr>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="feFirstName">Bank Accout</label>
                                  @if(!empty($data->detail_user->account_bank))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="" disabled=""> 
                                  @endif
                                </div>


                                <div class="form-group col-md-6">
                                  <label for="feFirstName">Bank Accout Number</label>
                                  @if(!empty($data->detail_user->account_bank_number))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank_number}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="" disabled=""> 
                                  @endif
                                </div>

                            </div>


                            <div class="form-row">
                              <div class="form-group col-md-4">
                                  <label for="feFirstName">Total Last Transaction</label>
                                  @if(!empty($data->detail_user->amount_last_transactions))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Total" value="" disabled=""> 
                                  @endif
                                </div>

                                <div class="form-group col-md-4">
                                  <label for="feFirstName">Status Transaction</label>
                                  @if(!empty($data->detail_user->status_transactions))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Status" value="" disabled=""> 
                                  @endif
                                </div>

                                <div class="form-group col-md-4">
                                  <label for="feFirstName">Date Last Transaction</label>
                                  @if(!empty($data->detail_user->account_bank))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->date_last_transactions}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Date" value="" disabled=""> 
                                  @endif
                                </div>
                            </div>


                            

                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

      </div>
    </section>


      


           

@endsection


@push('js')

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush

