@extends('web.layout2.template_local')

@section('content')


<style>
    .modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -100 !important;
    background-color: #000;
}



.horizontal-search form {
    padding: 10px 12px;
    background-color: #404cad !important;
}

.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

</style>


<!-- Menu -->
     <div class="inner-head overlap">
        <div style="background: url({{asset('web2/img/parallax1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div>  
        <div class="container">
            
            <div class="follow_widget widget">
                
                <div class="social_widget">
                    <a href="{{url('/my-account/report')}}" title=""><i class="fa fa-list"></i></a> <!-- report --> 
                    <a href="{{url('/my-account/invoice')}}" title=""><i class="fa fa-money"></i></a><!-- Invoice -->
                    <a href="{{url('/my-account/profile')}}" title=""><i class="fa fa-user"></i></a> <!-- Profile -->
                </div>
            </div><!-- Follow Widget -->
        </div>
    </div>
    <!-- End Menu -->

<section class="box-slider-search">

          <div class="container">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('warning'))
                <div class="alert alert-warning alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($message = Session::get('info'))
                <div class="alert alert-info alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif


                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    Please check the form below for errors
                </div>
                @endif


                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="z-index: 50 !important;">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <img src="{{asset('images/vehicle location.jpg')}}" class="img-responsive">
            </div>
          </div>
        </div>

                <h1 class="nocontent outline">--- Search form  ---</h1>
                <div class="row">
                    <div class="col-md-12">
                          <div class="box_slider">
                            <h2 style="text-align: center !important; "><b style="color: #8a4b4b">Invoice</b> <br> </h2>
                          </div>
                        

                    </div>
                </div>

                
            </div>
        </section>


        <section class="box-slider-search">
            <div class="container">
                <h1 class="nocontent outline">--- Search form  ---</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="horizontal-search v-f-p"> 
                            <div class="search-form"> 
                                <h1 class="fsearch-title">
                                    <i class="fa fa-search"></i><span>Invoice</span>
                                </h1>

                                <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th align="center">Vehicle</th>
                                  <th align="center">Payment Date</th>
                                  <th align="center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                              @foreach($get_vehicle as $data) 
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  <td>
                                    <?php $tarikh =  date('d-m-Y ', strtotime($data->payment_date)); ?> 
                                    {{$tarikh}}
                                  </td>
                                  
                                  <td align="center">
                                    
                                    
                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1{{$data->id}}">
                                      <i class="material-icons">details</i>
                                    </button>

                                  </td>
                              </tr>
                              @endforeach 
                              
                          </tbody>
                          
                      </table>
                            </div><!-- Services Sec -->

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>

        

        @foreach($get_vehicle as $data)
        <div class="modal fade bd-example-modal-sm1{{$data->id}}" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Chassis Detail </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                  <!-- Body content -->
                  <tr>
                    <td class="content-cell">
                      <div class="f-fallback">
                        
                        <table class="attributes" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                          <tr>
                            <td class="attributes_content">
                              <table width="100%" cellpadding="0" cellspacing="0" role="presentation">
                                <tr>
                                  <td class="attributes_item">
                                    <span class="f-fallback"> Chassis : <b>{{$data->vehicle}}</b>, </span>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="attributes_item">
                                    <span class="f-fallback"><?php $tarikh =  date('d-m-Y ', strtotime($data->created_at)); ?>
                                        Submite Date: {{$tarikh}}
                                    </span>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="attributes_item">
                                    <span class="f-fallback"><?php $tarikh =  date('d-m-Y ', strtotime($data->payment_date)); ?>
                                        Payment Date: {{$tarikh}}
                                    </span>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <!-- Action -->
                        <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                          <tr>
                            <td align="center">
                                
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                                <tr>


                                  <form id="new_post" name="new_post" method="post" class="vehicul-form" role="form" action="{{url('/pay-chassis')}}">

                                {{ csrf_field() }}



                                </form>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <table class="purchase" width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <h3>Qty Chassis</h3>
                            </td>
                            <td>
                              <h3 class="align-right">RM</h3>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">
                              <table class="purchase_content" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                  <th class="purchase_heading" align="left">
                                    <p class="f-fallback"></p>
                                  </th>
                                  <th class="purchase_heading" align="right">
                                    <p class="f-fallback"></p>
                                  </th>
                                </tr>
                                <tr>
                                  <td width="80%" class="purchase_item"><span class="f-fallback">1</span></td>
                                  <td class="align-right" width="20%" class="purchase_item"><span class="f-fallback">{{$get_price->fee}},00</span></td>
                                </tr>


                               
                                
                              </table>
                            </td>
                          </tr>
                        </table>
                        
                        
                        <!-- Sub copy -->
                        <table class="body-sub" role="presentation">
                          <tr>
                            <td>
                              <p class="f-fallback sub"></p>
                              <p class="f-fallback sub"></p>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        @endforeach
           

@endsection


@push('js')

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush

