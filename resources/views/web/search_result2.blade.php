@extends('web.layout.template')

@section('content')

    <style type="text/css">
        .card {
  /* Add shadows to create the "card" effect */
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
          transition: 0.3s;
          padding: 50px !important;
        }

        /* On mouse-over, add a deeper shadow */
        .card:hover {
          box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        /* Add some padding inside the card container */
        .container {
          padding: 2px 16px;
        }

        /* Add some padding inside the card container */
        h3 {
          color: #f56eab !important;
          font-weight: bold !important;
            text-decoration: underline  !important;
        }

        @media screen and (min-width: 1366px) {
            .price {
                margin-left:20px !important;
            }
        }

        @media only screen and (max-width: 767px)
            h2 {
                font-size: 40px !important;
            }

        @media (max-width: 768px) {
          .col-xs-12.text-right, .col-xs-12.text-left {
                  text-align: center;
           } 
        }

    </style>

    <!-- Top Content -->
    <section id="top_content" class="top_cont_outer">
        <div class="container">
            <div class="top_content">
                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <div class="">
                            <div class="card-body">
                                <div class="service-list">

                                    <h3>SAMPLE REPORT</h3>

                                    <img src="{{asset('images/report_parts.jpg')}}"  style="width:4200px;height:1000px;" class="img-thumbnail">
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-lg-8 col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <h3>CONGRATULATIONS! </h3>
                                <h2> YOUR CAR RECORDS FOUND</h2>
                                <h2>{{$search->cars[0]->chassisNumber }}</h2>

                                <div class="work_section" style="padding-top: 20px !important">
                                    <div class="row">
                                        

                                        <div class="col-lg-8 col-sm-8 ">
                                            <div class="service-list col-lg-6">
                                                <div class="service-list-col1"> <i class="icon-doc"></i> </div>
                                                <div class="service-list-col2">
                                                    <p>Brand :</p> <h3><b><u>{{$search->cars[0]->make}}</u></b></h3>

                                                </div>
                                            </div>

                                            <div class="service-list col-lg-6">
                                                <div class="service-list-col1"> <i class="icon-doc"></i> </div>
                                                <div class="service-list-col2">
                                                    <p>Body :</p> <h3>{{$search->cars[0]->body}}</h3>

                                                </div>
                                            </div>


                                            <div class="service-list col-lg-6">
                                                <div class="service-list-col1"> <i class="icon-doc"></i> </div>
                                                <div class="service-list-col2">
                                                    <p>Model :</p> <h3>{{$search->cars[0]->model}}</h3>

                                                </div>
                                            </div>

                                            <div class="service-list col-lg-6">
                                                <div class="service-list-col1"> <i class="icon-doc"></i> </div>
                                                <div class="service-list-col2">
                                                    <p>Engine Model :</p> <h3>{{$search->cars[0]->engine}}</h3>

                                                </div>
                                            </div>

                                            <div class="service-list col-lg-6">
                                                <div class="service-list-col1"> <i class="icon-doc"></i> </div>
                                                <div class="service-list-col2">
                                                    <p>Grade :</p> <h3>{{$search->cars[0]->grade}}</h3>

                                                </div>
                                            </div>

                                            <div class="service-list col-lg-6">
                                                <div class="service-list-col1"> <i class="icon-doc"></i> </div>
                                                <div class="service-list-col2">
                                                    <p>Transmision :</p> <h3>{{$search->cars[0]->transmission}}</h3>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-4 col-sm-4 ">
                                            <img src="{{asset('images/car_sample.jpg')}}" class="img-responsive" style="margin-bottom: 40px !important">

                                            <div class="service-list col-lg-12">
                                                <div class="service-list-col1"> <i class="icon-doc"></i> </div>
                                                <div class="service-list-col2">
                                                    <p>Drive :</p> <h3>{{$search->cars[0]->drive}}</h3>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>

                        <div class="card" style="margin-top: 20px !important">
                        <div class="card-body">


                            <div>
                                <div class="row">
                                    <div class="col-lg-4 col-xs-12"> <!-- image -->
                                            <img src="{{asset('images/search_car.jpg')}}" width="100%" height="10%">
                                    </div> <!-- end image -->

                                    <div class="col-lg-8 col-xs-12">
                                        <div class="row">
                                            <h2 style="margin-bottom: -20px !important; margin-left: -327px !important; color: #f56eab !important" class="price">RM 250</h2>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-xs-12"> <!--Button  Login / Buy-->

                                                @guest

                                                <form method="POST" class="register-form" id="register-form" action="{{url('/login-buyer')}}">

                                                    {{ csrf_field() }}


                                                    <input type="hidden" name="searchid"  value="{{$search->uid}}" readonly="" />

                                                    <input type="hidden" name="carid" value="{{$search->cars[0]->carId}}"  readonly="" />


                                                <div class="work_bottom"> 
                                                    <button type="submit" class="btn input-btn" align="left" style="margin-left:-73px !important">Login To Buy Report</button>
                                                </div>

                                                </form>

                                                @else


                                                    <form method="POST" class="register-form" id="register-form" action="{{url('/pay-chassis')}}">

                                                    {{ csrf_field() }}

                                                    <input type="hidden" name="searchid"  value="{{$search->uid}}" readonly="" />

                                                    <input type="hidden" name="carid" value="{{$search->cars[0]->chassisNumber}}"  readonly="" />

                                                    <div class="work_bottom"> 
                                                        <button type="submit" class="btn input-btn" align="left" style="margin-left:-73px !important">Buy</button>
                                                    </div>
                                                     

                                                </form>

                                                @endif





                                            </div> <!-- End Button Login /  Buy -->

                                            <div class="col-lg-4 col-xs-12" id="showmenu">
                                                <div class="work_bottom"> 
                                                    <a  class="contact_btn" style="margin-left:-20px !important; cursor: pointer !important">
                                                        Other Chassis <i class="icon-doc"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                            </div>  

                            <div class="menu"  style="display: none; margin-top: 0px">
                                    
                                    <h2>Check Car History Now</h2>                                
                                    <form method="POST" class="register-form" id="register-form" action="{{url('/check-vehicle-web')}}">

                                        {{ csrf_field() }}

                                    <div class="form-group">
                                        <input type="text" name="vehicle" class="form-control input-text" id="name" placeholder="Enter Chassis Number" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                                        <div class="validation"></div>
                                    </div>

                                   
                                    <button type="submit" class="btn input-btn">Find Detail</button>

                                    </div>

                                    </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

                
            </div>


        </div>
    </section>
    <!-- End top Content -->



    <div class="c-logo-part">
        <!--c-logo-part-start-->
        <div class="container">
            <ul class="delay-06s animated  bounce wow">
                <li><a href="javascript:void(0)"><img src="{{asset('web/img/c-liogo1.png')}}" alt=""></a></li>
                <li><a href="javascript:void(0)"><img src="{{asset('web/img/c-liogo2.png')}}" alt=""></a></li>
                <li><a href="javascript:void(0)"><img src="{{asset('web/img/c-liogo3.png')}}" alt=""></a></li>
                <li><a href="javascript:void(0)"><img src="{{asset('web/img/c-liogo5.png')}}" alt=""></a></li>
            </ul>
        </div>
    </div>
    <!--c-logo-part-end-->
    
    <!--twitter-feed-end-->
    <footer class="footer_section" id="contact">
        <div class="container">
            <section class="main-section contact" id="contact">
                <div class="contact_section">
                    <h2>Contact Us</h2>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="contact_block">
                                <div class="contact_block_icon rollIn animated wow"><span><i class="fa-home"></i></span></div>
                                <span> 308 Negra Arroyo Lane, <br>
              Albuquerque, NM, 87104 </span> </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="contact_block">
                                <div class="contact_block_icon icon2 rollIn animated wow"><span><i class="fa-phone"></i></span></div>
                                <span> 1-800-BOO-YAHH </span> </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="contact_block">
                                <div class="contact_block_icon icon3 rollIn animated wow"><span><i class="fa-pencil"></i></span></div>
                                <span> <a href="mailto:hello@butterfly.com"> hello@butterfly.com</a> </span> </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 wow fadeInLeft">
                        <div class="contact-info-box address clearfix">
                            <h3>Don’t be shy. Say hello!</h3>
                            <p>Accusantium quam, aliquam ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper. Accusantium quam, aliquam ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper.</p>
                            <p>Accusantium quam, aliquam ultricies eget tempor id, aliquam eget nibh et. Maecen aliquampor id.</p>
                        </div>
                        <ul class="social-link">
                            <li class="twitter animated bounceIn wow delay-02s"><a href="javascript:void(0)"><i class="fa-twitter"></i></a></li>
                            <li class="facebook animated bounceIn wow delay-03s"><a href="javascript:void(0)"><i class="fa-facebook"></i></a></li>
                            <li class="pinterest animated bounceIn wow delay-04s"><a href="javascript:void(0)"><i class="fa-pinterest"></i></a></li>
                            <li class="gplus animated bounceIn wow delay-05s"><a href="javascript:void(0)"><i class="fa-google-plus"></i></a></li>
                            <li class="dribbble animated bounceIn wow delay-06s"><a href="javascript:void(0)"><i class="fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 wow fadeInUp delay-06s">
                        <div class="form">
                            <div id="sendmessage">Your message has been sent. Thank you!</div>
                            <div id="errormessage"></div>
                            <form action="" method="post" role="form" class="contactForm">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control input-text" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input-text" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-text" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                    <div class="validation"></div>
                                </div>

                                <button type="submit" class="btn input-btn">SEND MESSAGE</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="container">
            <div class="footer_bottom">
                <span>© Butterfly Theme</span>
                <div class="credits">
                    <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Butterfly
          -->
                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>
            </div>
        </div>
    </footer>

@endsection

@push('js')
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#showmenu').click(function() {
                    $('.menu').toggle("slide");
            });
        });
    </script>

@endpush