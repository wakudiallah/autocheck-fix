@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Not Found ( {{config('autocheck.full_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
		                <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>
                                  <th>Date Verify</th>
                                  <th>Request From</th>
                                  <th>History</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  <td>{{$data->created_at}}</td>
                                  <td>{{$data->updated_at}}</td>
                                  <td>
                                  	{{$data->request_by->name}}

                                  	@if(!empty($data->request_from->user_relate_usergroup->group_name))
                                    <b style="color: red">- {{$data->request_from->user_relate_usergroup->group_name}}</b>
                                    @endif

                                  </td>
                                  <!-- Action -->
                                 
                                  <!-- History -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>
                                  <!-- End of History -->



                                  <!-- End History Search -->
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
		                  </table>
		                </div>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>



            

@endsection


@push('js')

  


    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>



    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />



    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
