@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Checked</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
		                <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Verification Serial Number</th>
                                  <th>Date Request</th>
                                  @if($me == "VER")
                                  <th>Sync</th>
                                  @endif
                                  <th >Status</th>
                                  
                                  <th width="20%" align="center">Action</th>
                                  
                                  
                                    @if($me == "VER" OR $me == "KA")
                                    <th>Full Report</th>
                                    <th>Req From</th>
                                    @endif
                                  

                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  

                                  <td>
                                    @if(!empty($data->status_vehicle->ver_sn))
                                    {{$data->status_vehicle->ver_sn}}
                                    @endif
                                    
                                  </td>
                                  <td>{{$data->created_at}}</td>

                                  <!-- Sync -->
                                  @if($me == "VER")
                                  <td align="center">
                                    @if(!empty($data->status_carvx->report_id))
                                    @if($data->status_carvx->is_ready != 6 AND $data->status_carvx->is_ready != 5) <!-- 6 cancel or 5 complete -->
                                    

                                    <!-- if KA sync to autocheckmalaysia3 -->
                                    @if($data->group_by != "KA")
                                    <a href="{{url('sync_autocheck3/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        Sync group
                                      </button>
                                    </a>
                                    @else
                                    

                                    <a href="{{url('sync/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        Sync 
                                      </button>
                                    </a>

                                    @endif
                                    <!-- end sync -->

                                    @endif
                                    @endif


                                    <!-- status match API -->
                                    @if(!empty($data->status_vehicle->status == "1") AND $data->searching_by == "API" AND $data->status == "40")
                                    <img src="{{asset('images/check.png')}}" width="20" height="20" align="center">

                                    @elseif(!empty($data->status_vehicle->status == "0") AND $data->searching_by == "API" AND $data->status == "40")

                                    <img src="{{asset('images/not.png')}}" width="20" height="20" align="center">
                                    @endif
                                    <!-- End status match API -->

                                  </td>
                                    @endif
                                  <!-- End Sync -->



                                  <!-- Status -->
                                  <td align="center">
                                      
                                    
                                      @if($data->status == '40')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>
                                      
                                      @elseif(($data->status == '40') AND ($data->searching_by == 'API') AND ($data->group_by == "KA"))
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>

                                      @elseif($data->status == '30')
                                      <i  class="card-post__category badge badge-pill badge-danger">Reject</i>
                                      @elseif($data->status == '10' OR $data->status == '20')
                                      <i  class="card-post__category badge badge-pill badge-warning">New</i>
                                      @elseif($data->status == '25' AND $data->status_vehicle->status == 0)
                                      <i  class="card-post__category badge badge-pill badge-success">In Progress</i>

                                      @elseif(($data->status == '40') AND !empty($data->full_report))
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>
                                      
                                      @endif
                                    
                                      
                                      @if($me == 'VER')
                                      @if($data->searching_by == 'NOT')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                      @endif
                                  </td>
                                  <!-- End of Status -->

                                  

                                  <!-- End of API Status -->


                                  <!-- Action -->
                                  <td width="20%" align="center">

                                      <!-- download complete manual -->
                                        @if($data->status == '40' AND $data->searching_by == 'NOT')
                                            
                                      @endif

                                      

                                      <!-- End download complete manual -->

                                      @if(($data->searching_by == 'API') AND ($data->status == '40' AND $data->group_by != 'KA') AND !empty($data->status_vehicle->status == "1"))
                                      <a href="{{url('report-autocheck/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>

                                      @elseif(($data->searching_by == 'NOT') AND ($data->status == '40' AND $data->group_by != 'KA') AND !empty($data->status_vehicle->status == "1"))

                                      <a href="{{url('report-autocheck-mn/'.$data->id_vehicle)}}" target="_blank">
                                                <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                  <i class="material-icons">save_alt</i>
                                                </button>
                                              </a>
                                      

                                      <!-- Action Print --> 
                                       <!-- <a href="{{url('report-autocheck/'.$data->id_vehicle)}}">
                                        <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                          <i class="material-icons">print</i>
                                        </button>
                                      </a> -->
                                      @endif
                                      <!-- End action print -->


                                      
                                    <!-- ==== Modal Detail ==== -->
                                    

                                    @if($data->status == "30" OR $data->status == "40")
                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".modal-detail-complete{{$data->id}}">
                                        <i class="material-icons">details</i> 
                                    </button>

                                    @elseif(($data->status == "10" OR $data->status == "20") OR ($data->group_by == "KA"))

                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1{{$data->id}}">
                                        <i class="material-icons">details</i>
                                    </button>

                                    @endif

                                    <!-- ==== End Modal Detail ==== -->


                                    
                                    <!-- Action upload manual Kastam -->
                                    @if($me == "VER")
                                      @if($data->searching_by == "NOT" AND $data->full_report != "NULL" AND $data->group_by == "KA" AND $data->status != "30" AND  $data->status != "40")
                                      
                                      <button type="button" class="mb-2 btn btn-sm btn-success mr-1" data-toggle="modal" data-target=".bd-example-modal-sm3{{$data->id}}">
                                          <i class="material-icons">cloud_upload</i>
                                        </button>
                                      
                                      @endif
                                    @endif
                                    <!-- End Action upload manual Kastam -->



                                      
                                    <!-- action verify -->
                                    @if($me == "VER")

                                      @if($data->searching_by == 'NOT' AND $data->is_sent == '1')
                                      <!--   For verify data / match or not -->
                                      <!-- <a href="{{url('verify-vehicle/'.$data->id_vehicle)}}"> -->

                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  data-toggle="modal" data-target=".bd-example-modal-sm2{{$data->id_vehicle}}">
                                        <i class="material-icons">details</i>
                                      </button>

                                      <!-- </a> -->

                                      @endif

                                      @if($data->status_vehicle->status == "0" AND $data->searching_by == "API")

                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  data-toggle="modal" data-target=".bd-example-modal-sm2{{$data->id_vehicle}}">
                                        <i class="material-icons">details</i>
                                      </button> 

                                      @endif
                                      
                                    @endif
                                  
                                  </td> <!-- end action verify -->


                                  <!-- Full Report -->
                                  @if($me == "VER" OR $me == "KA")
                                  <td align="center">
                                    @if($data->status == "40" AND $data->group_by == "KA")

                                    @if($data->searching_by == "API")
                                    <a href="{{url('report-autocheck-kastam-api/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>
                                    @elseif($data->searching_by == "NOT")
                                      

                                      @if(!empty($data->status_carvx->id_vehicle))
                                        <!-- Manual Data -->
                                        <a href="{{url('report-autocheck-kastam-api-mn/'.$data->id_vehicle)}}" target="_blank">
                                          <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                            <i class="material-icons">save_alt</i>
                                          </button>
                                        </a>

                                        <!-- End Manual Data -->

                                          @else

                                      <a href="{{ url('/') }}/report-autocheck-kastam-mn/{{$data->id_vehicle}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>

                                      @endif

                                    @endif
                                    @endif

                                  </td>

                                  <td>

                                    @if(!empty($data->request_from->role->desc))
                                    <p style="color: red">{{$data->request_from->role->desc}}</p>
                                    @endif
                                  </td>
                                  @endif
                                  <!-- End Full Report -->


                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>

                                  <!-- End History Search -->
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
		      </table>
		</div>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
       
        
          <!-- ======== Modal Detail Complete === -->
          @foreach($data2 as $data)
          <div class="modal fade modal-detail-complete{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail Complete </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}} </b></h2>
                        </div>

                        <hr>

                        <!-- Not Kastam and complete API Detail -->
                        @if(($data->status == '40' AND $data->searching_by == 'API') AND ($data->group_by != 'KA'))
                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}}</h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand </label>
                          @if(!empty($data->vehicle_api->brand))
                          <h3>{{$data->vehicle_api->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($data->vehicle_api->model))
                          <h3>{{$data->vehicle_api->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->vehicle_api->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$data->vehicle_api->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$data->vehicle_api->registation_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($data->fuel->fuel))
                            <h3>{{$data->fuel->fuel}}</h3>
                            @endif
                          </div>



                          <!-- Not Kastam and complete Manual Detail -->
                          @elseif(($data->status == '40' AND $data->searching_by == 'NOT') AND ($data->group_by != 'KA'))




                        <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}} </h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand </label>
                          @if(!empty($vehicle_manual->manual_brand->brand))
                          <h3>{{$vehicle_manual->manual_brand->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($vehicle_manual->manual_model_brand->model))
                          <h3>{{$vehicle_manual->manual_model_brand->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$vehicle_manual->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$vehicle_manual->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$vehicle_manual->registation_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($vehicle_manual->manual_fuel->fuel))
                            <h3>{{$vehicle_manual->manual_fuel->fuel}}</h3>
                            @endif
                          </div>

                      @endif

                      </div>

                  </div>
                <div class="modal-footer">
                  
                  
                </div>


              </div>
            </div>
          </div>

          @endforeach
          <!-- ======== Modal Detail Complete === -->



          <!-- ======== Modal Detail Not Complete === -->
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm1{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail Not complete</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                   
                      
                      <!-- get from API and complete / KADEALER-->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}} </b></h2>
                        </div>

                        <hr>

                      
                      </div>
                      <!-- end get from API -->
  
                </div>
                <div class="modal-footer">
                  
                  
                </div>


              </div>
            </div>
          </div>
          @endforeach
          <!-- ======= End Modal Detail Complete ======= -->
          


          <!-- Modal Upload -->

          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm3{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Upload {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('upload-report-vehicle/'.$data->id_vehicle) }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                      


                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <input type="file" name="file" class="form-control">

                          <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        </div>

                        <hr>

                      </div>

                      <div class="form-row">
                        <input type="submit" name="submit" class="btn btn-sm btn-primary">
                      </div>
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach

          <!-- End Modal upload -->


          
            

@endsection


@push('js')

  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    


    @foreach($data3 as $data2)
    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker2{{$data2->id}}" ).datepicker();
     });
    </script>
    @endforeach


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
