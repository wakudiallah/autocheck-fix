<!DOCTYPE html>

<html>

<head>

	<title>Report {{$vehicle->vehicle}}</title>

	<!-- Latest compiled and minified CSS -->


	<link href="font/Raleway/Raleway-Light" rel="stylesheet">

	<style type="text/css">
		

		.table_x {
		    background: white;
		    border: 1px solid #c3c3c3;
		    border-radius: 5px;
		    width: 50%;
		    border-spacing: 0px;
		    border-collapse: separate;
		}
		.table_x th {
		    background: #c6c6c6;
		}

		.m50{
			margin-top: 20px !important;
		}

		.m50 h3{
			font-weight: bold !important;
		}

		sup{
			color: red !important;
		}

	</style>



</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->


	<table width="100%" style="margin-top: -40px !important; margin-bottom: 50px !important;">
		<tr>
			<td width="40%" align="center" style="margin-left: 0px !important">
				<br><br><br>
				
				<img src="{{asset('images/auto.jpg')}}" style="width:150px;height:70px; text-align:center"/><br>
				
			</td>
			<td width="30%">
				<!-- <p style="line-height: normal; margin-top: -50px !important; font-size: 10px !important; font-family: 'Raleway', sans-serif;" ><b>Verification Serial Number</b><br>{{$vehicle->status_vehicle->ver_sn}}</p> -->
				
			</td>
			<td width="30%" style="text-align: right; margin-top: 40px !important">
				
				<?php $role_me = Auth::user()->role_id; ?>

				@if($role_me == "VER")
				<img src="{{asset('images/logo_miti.jpg')}}" style="width:150px;height:70px; margin-top: -50px !important"/>
				@endif

				<!-- <img src="{{asset('images/logo_miti_only.png')}}" style="width:150px; height:70px; margin-top: -50px !important"/>-->
				
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px"><b>VEHICLE DETAILS</b></h3></td>
		</tr>
	</table>


	<table width="100%" style="font-size: 15px;" >
		<tr>
			<td>
				<table>

					<tr>
						<td><b>Chassis number </b><sup>1</sup></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<!-- <td>ANH20</td>-->
						<td>
							@if(!empty($vehicledetail->chassisNumber))
							{{$vehicledetail->chassisNumber}}
							@endif
						</td> 

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td><b>Title information</b><sup>2</sup></td>
						<td><img src="{{asset('images/report/key.png')}}" width="30" height="30"></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td><b>
						@if(!empty($summary->registered == '1'))
							Deregistered to Export
						@else

						
						@endif
						</b>
						</td>
						<td>
							@if(!empty($summary->registered == '1'))
								<img src="{{asset('images/check.jpg')}}" width="20" height="20">
							@else
								<img src="{{asset('images/not.jpg')}}" width="20" height="20">
							@endif
						</td>
					</tr>
					<tr>
						<td><b>Manufacture date</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->manufactureDate}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td><b>Accident / Repair  </b></td>
						<td><img src="{{asset('images/report/car.png')}}" width="30" height="30"></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td><b>
							@if($summary->accident == '1')
								No Problem
							@else
								Problem Found
							@endif
						</b></td>
						<td>
							@if($summary->accident == '1')
								<img src="{{asset('images/check.jpg')}}" width="20" height="20">
							@else
								<img src="{{asset('images/not.jpg')}}" width="20" height="20">
							@endif
						</td>
					</tr>
					
					<tr>
						<td><b>Make</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->make}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td><b>Odometer rollback</b></td>
						<td><img src="{{asset('images/report/odometer.png')}}" width="30" height="30"></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td><b>
							@if($summary->odometer == '1')
								No Problem
							@else
								Problem Found
							@endif
						</b></td>
						<td>
							@if($summary->odometer == '1')
								<img src="{{asset('images/check.jpg')}}" width="20" height="20">
							@else
								<img src="{{asset('images/not.jpg')}}" width="20" height="20">
							@endif
						</td>
					</tr>
					<tr>
						<td><b>Model</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->model}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td><b>Manufacturer recall </b></td>
						<td><img src="{{asset('images/report/manufacture.png')}}" width="30" height="30"></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td><b>
							@if($summary->recall == "1")
								No Problem
							@else
								Problem Found
							@endif
						</b></td>
						<td>
							@if($summary->recall == '1')
								<img src="{{asset('images/check.jpg')}}" width="20" height="20">
							@else
								<img src="{{asset('images/not.jpg')}}" width="20" height="20">
							@endif
						</td>
					</tr>
					<tr>
						<td><b>Body</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->body}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td><b>Safety grade</b><sup>3</sup></td>
						<td><img src="{{asset('images/report/safety.png')}}" width="40" height="30"></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td><b>
							@if($summary->safetyGrade == '1')
								<img src="{{asset('images/start_full.png')}}" width="50" height="20">
							@else
								<img src="{{asset('images/start_not_full.png')}}" width="50" height="20">
							@endif
						</b></td>
						<td>
							@if($summary->safetyGrade == '1')
								<img src="{{asset('images/check.jpg')}}" width="20" height="20">
							@else
								<img src="{{asset('images/not.jpg')}}" width="20" height="20">
							@endif
						</td>
					</tr>
					<tr>
						<td><b>Grade</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->grade}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td><b>Contamination risk</b></td>
						<td><img src="{{asset('images/report/contamination.png')}}" width="30" height="30"></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td><b>
							
							@if($summary->contaminationRisk == '1')
								No Problem
							@else
								Problem Found
							@endif
							<b>
						</td>
						<td>
							@if($summary->contaminationRisk == '1')
								<img src="{{asset('images/check.jpg')}}" width="20" height="20">
							@else
								<img src="{{asset('images/not.jpg')}}" width="20" height="20">
							@endif
						</td>
					</tr>
					<tr>
						<td><b>Engine</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->engine}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td></td>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><b>Drive</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->drive}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td> </td>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><b>Transmission</b></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>{{$vehicledetail->transmission}}</td>

						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

						<td></td>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
			

		</tr>
	</table>

	<!-- Conversi Currency -->

	<?php 

            $endpoint = 'live';
            $access_key = '1b0fd13e42ae1c5932e70779ae0a24ff';

            // Initialize CURL:
            $ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Store the data:
            $json = curl_exec($ch);
            curl_close($ch);

            // Decode JSON response:
            $exchangeRates = json_decode($json, true);

            $count_country= count($exchangeRates);

            $price = $summary->averagePrice;

            $currency_usdyen = $exchangeRates["quotes"]["USDJPY"];
            $currency_usdmyr = $exchangeRates["quotes"]["USDMYR"];
            //$yen = ;

            $priceusd = $price / $currency_usdyen;
            $pricemyr = $priceusd * $currency_usdmyr;

            //echo "price usd" . $priceusd ."=="; 
            //echo "price myr" . $pricemyr;
	?>

	<!-- End Conversi Currency -->

	<table style="margin-top: 60px">
		<tr>
			<td colspan="3">
				<p><b>This vehicle does not qualify for Buyback Guarantee</b></p>
			</td>
			<td colspan="2">
				<p><b>Average Market Price</b></p>
			</td>
		</tr>

		<tr>
			<td>
				<img src="{{asset('images/report/tameng.jpg')}}" width="8%" height="10%">
			</td>
			<td colspan="2">
				<p>Unfortunately, this vehicle does not qualify for our
Buyback Guarantee program.</p>
			</td>
			<td>
				<img src="{{asset('images/report/yen.jpg')}}" width="10%" height="10%">
			</td>
			<td>
				 <?php $tot_balance  = number_format($summary->averagePrice, 0, ',', ',');

				 	$tot_myr  = number_format($pricemyr, 0, ',', ',');
                  ?>
				<h2>Y<b> {{$tot_balance}}</b></h2>

				<h2>RM<b> {{$tot_myr}}</b></h2>
			</td>
		</tr>
	</table>

	
	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">ACCIDENT / REPAIR HISTORY</h3></td>
		</tr>
	</table>

	<table width="100%" class="table table-striped" style="font-size: 15px;">
		<thead>
		<tr>
			<td width="10%"><b>Problem type</b></td>
			<td width="20%"><b>Reported</b></td>
			<td width="10%"><b>Date reported</b></td>
			<td width="10%"><b>Data source</b></td>
			<td width="10%"><b>Details</b></td>
			<td width="5%"><b>Airbag</b></td>
		</tr>
		</thead>
		<tr>
			<td>Collision</td>
			<td>
				@if(empty($check_raccident_collision->date_reported))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
				@else
				<img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
				@endif
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td></td>
			<td></td>
		</tr>
		@foreach($raccident_collision as $data)
		<tr>
			<td>-</td>
			<td>-</td>
			<td>{{$data->date_reported}}</td>
			<td>{{$data->data_source}}</td>
			<td>{{$data->details}}</td>
			<td>{{$data->airbag}}</td>
		</tr>
		@endforeach
		<tr>
			<td>Malfunction</td>
			<td>
				@if(empty($check_raccident_malfunction->date_reported))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
				@else
				<img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
				@endif
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td></td>
			<td></td>
		</tr>
		@foreach($raccident_malfunction as $data)
		<tr>
			<td>-</td>
			<td>-</td>
			<td>{{$data->date_reported}}</td>
			<td>{{$data->data_source}}</td>
			<td>{{$data->details}}</td>
			<td>{{$data->airbag}}</td>
		</tr>
		@endforeach
		
		<tr>
			<td>Theft</td>
			<td>
				@if(empty($check_raccident_theft->date_reported))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
				@else
				<img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
				@endif
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td></td>
			<td></td>
		</tr>
		@foreach($raccident_theft as $data)
		<tr>
			<td>-</td>
			<td>-</td>
			<td>{{$data->date_reported}}</td>
			<td>{{$data->data_source}}</td>
			<td>{{$data->details}}</td>
			<td>{{$data->airbag}}</td>
		</tr>
		@endforeach

		<tr>
			<td>Fire damage</td>
			<td>
				@if(empty($check_raccident_fireDamage->date_reported))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
				@else
				<img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
				@endif
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td></td>
			<td></td>
		</tr>

		@foreach($raccident_fireDamage as $data)
		<tr>
			<td>-</td>
			<td>-</td>
			<td>{{$data->date_reported}}</td>
			<td>{{$data->data_source}}</td>
			<td>{{$data->details}}</td>
			<td>{{$data->airbag}}</td>
		</tr>
		@endforeach


		<tr>
			<td>Water damage</td>
			<td>
				@if(empty($check_raccident_waterDamage->date_reported))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
				@else
				<img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
				@endif
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td></td>
			<td></td>
		</tr>

		@foreach($raccident_waterDamage as $data)
		<tr>
			<td>-</td>
			<td>-</td>
			<td>{{$data->date_reported}}</td>
			<td>{{$data->data_source}}</td>
			<td>{{$data->details}}</td>
			<td>{{$data->airbag}}</td>
		</tr>
		@endforeach

		<tr>
			<td>Hail damage</td>
			<td>
				@if(empty($check_raccident_hailDamage->date_reported))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">&nbsp;&nbsp;Not reported
				@else
				<img src="{{asset('images/warning.png')}}" width="20" height="20">&nbsp;&nbsp;Reported
				@endif
			</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td></td>
			<td></td>
		</tr>

		@foreach($raccident_hailDamage as $data)
		<tr>
			<td>-</td>
			<td>-</td>
			<td>{{$data->date_reported}}</td>
			<td>{{$data->data_source}}</td>
			<td>{{$data->details}}</td>
			<td>{{$data->airbag}}</td>
		</tr>
		@endforeach

	</table>

	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">ODOMETER READINGS HISTORY</h3></td>
		</tr>
	</table>


	<table width="100%" class="table table-striped" style="font-size: 15px;">
		<thead>
		<tr>
			<td><b>Date reported</b></td>
			<td><b>Data source</b></td>
			<td><b>Odometer reading (Km)</b></td>
		</tr>
		</thead>
		@foreach($odometerhistory as $data)
		<tr>
			<td>{{$data->date}}</td>
			<td>{{$data->source}}</td>
			<td>{{$data->mileage}}</td>
		</tr>
		@endforeach
					
	</table>

	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px"><b>USE HISTORY</b></h3></td>
		</tr>
	</table>


	<table width="100%" class="table table-striped" style="font-size: 15px;">
		<thead>
		<tr>
			<td><b>Use in the contaminated regions</b><sup>4</sup></td>
			<td><b>Radioactive contamination test fail</b><sup>5</sup></td>
			<td><b>Commercial use</b></td>
		</tr>
		</thead>
		<tr>
			
			<td>
				@if(!empty($usagehistory->contaminatedRegion == '0'))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">
				&nbsp;&nbsp;Not reported 
				@endif
			</td>
			<td>
				@if(!empty($usagehistory->contaminationTest == '0'))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">
				&nbsp;&nbsp;Not reported
				@endif
			</td>
			<td>
				@if(!empty($usagehistory->commercialUsage == '0'))
				<img src="{{asset('images/check.jpg')}}" width="20" height="20">
				&nbsp;&nbsp;Not reported
				@endif
			</td>
		</tr>
					
	</table>


	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">DETAILED HISTORY</h3></td>
		</tr>
	</table>

	<table width="100%" class="table table-striped" style="font-size: 15px;">
		<thead>
		<tr>
			<td><b>Event date</b></td>
			<td><b>Location</b></td>
			<td><b>Odometer reading (Km)</b></td>
			<td><b>Data source</b></td>
			<td><b>Details</b></td>
		</tr>
		</thead>
		@foreach($detailhistory as $data)
		<tr>
			<td>
				{{$data->date}}
			</td>
			<td>
				{{$data->location}}
			</td>
			<td>
				{{$data->mileage}}
			</td>
			<td>
				{{$data->source}}
			</td>
			<td>
				{{$data->action}}
			</td>
		</tr>
		@endforeach
					
	</table>

	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">MANUFACTURER RECALL HISTORY</h3></td>
		</tr>
	</table>

	<table width="100%" class="table table-striped" style="font-size: 15px;" >
		<thead>
		<tr>
			<td><b>Date reported</b></td>
			<td><b>Data source</b></td>
			<td><b>Affected part</b></td>
			<td><b>Details</b></td>
		</tr>
		</thead>
		@foreach($recall as $data)
		<tr>
			<td>{{$data->date}}</td>
			<td>{{$data->source}}</td>
			<td>{{$data->affected_part}}</td>
			<td>{{$data->details}}</td>
		</tr>
		@endforeach
					
	</table>

	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">VEHICLE ASSESSMENT</h3><sup>6</sup></td>
		</tr>
	</table>

	<table width="100%" class="table table-striped" style="font-size: 15px;">
		<tr>
			<td colspan="6"><b>Overall Collision Safety Ratings</b></td>
		</tr>
		<tr>
			<td colspan="3" align="center"><b>Driver's seat</b></td>
			<td colspan="3" align="center"><b>Front passenger's seat</b></td>
		</tr>

		<tr>
			<td>Points </td>
			<td>Evaluation</td>
			<td>Goal average</td>
			<td>Points </td>
			<td>Evaluation</td>
			<td>Goal average</td>
		</tr>

		<tr>
			
			<td>{{$vehicleassessment->driverSeatPoints}}</td>
			<td><img src="{{asset('images/start_full.png')}}" width="60" height="20"></td>
			<td>{{$vehicleassessment->driverSeatGoalAverage}}%</td>

			<td>{{$vehicleassessment->frontPassengerSeatPoints}}</td>
			<td><img src="{{asset('images/start_full.png')}}" width="60" height="20"></td>
			<td>{{$vehicleassessment->frontPassengerSeatGoalAverage}}%</td>
		</tr>

		<tr>
			<td colspan="6">
			<p>* In order to accurately differentiate between the evaluations of different vehicles, a standard is set based on current technology. Up to 6 points out of 12 is given level 1 and the rest of the range is divided up into equal parts, which are respectively assigned to level 2 (more than 6 points but 7.5 or less), level 3 (more than 7.5 points but 9 or less), level 4 (more than 9 points but 10.5 or less) or level 5 (more than 10.5 points).</p>
			</td>
		</tr>
		<tr>
			<td colspan="6">
			<p><b>Braking performance tests</b><sup>7</sup></p>
			</td>
		</tr>
		<tr>
			<td>Dry road</td>
			<td colspan="4"><img src="{{asset('images/report/car1.png')}}" width="300" height="50"></td>
			<td>{{$vehicleassessment->dryRoadStoppingDistance}} m</td>
		</tr>
		<tr>
			<td>Wet road</td>
			<td colspan="4"><img src="{{asset('images/report/car2.png')}}" width="300" height="50"></td>
			<td>{{$vehicleassessment->wetRoadStoppingDistance}} m</td>
		</tr>
					
	</table>


	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">VEHICLE SPECIFICATION</h3></td>
		</tr>
	</table>

	<table width="100%" class="table table-striped" style="font-size: 15px;" >
		<tr>
			<td><b>1st gear ratio </b></td>
			<td>{{$vehiclespesification->firstGearRatio}}</td>
			<td></td>
			<td><b>2nd gear ratio </b></td>
			<td>{{$vehiclespesification->secondGearRatio}}</td>
		</tr>
		<tr>
			<td><b>3rd gear ratio  </b></td>
			<td>{{$vehiclespesification->thirdGearRatio}}</td>
			<td></td>
			<td><b>4th gear ratio</b></td>
			<td>{{$vehiclespesification->fourthGearRatio}}</td>
		</tr>
		<tr>
			<td><b>5th gear ratio</b></td>
			<td>{{$vehiclespesification->fifthGearRatio}}</td>
			<td></td>
			<td><b>6th gear ratio</b></td>
			<td>{{$vehiclespesification->sixthGearRatio}}</td>
		</tr>
		<tr>
			<td><b>Additional notes</b></td>
			<td>{{$vehiclespesification->additionalNotes}}</td>
			<td></td>
			<td><b>Airbag position, capacity</b></td>
			<td>{{$vehiclespesification->airbagPosition}}</td>
		</tr>
		<tr>
			<td><b>Body rear overhang </b></td>
			<td>{{$vehiclespesification->bodyRearOverhang}}</td>
			<td></td>
			<td><b>Body type</b></td>
			<td>{{$vehiclespesification->bodyType}}</td>
		</tr>

		<tr>
			<td><b>Chassis number embossing position </b></td>
			<td>{{$vehiclespesification->chassisNumberEmbossingPosition}}</td>
			<td></td>
			<td><b>Classification code</b></td>
			<td>{{$vehiclespesification->classificationCode}}</td>
		</tr>
		<tr>
			<td><b>Cylinders </b></td>
			<td>{{$vehiclespesification->cylinders}}</td>
			<td></td>
			<td><b>Displacement</b></td>
			<td>{{$vehiclespesification->displacement}}</td>
		</tr>
		<tr>
			<td><b>Electric engine type </b></td>
			<td>{{$vehiclespesification->electricEngineType}}</td>
			<td></td>
			<td><b>Electric engine maximum output</b></td>
			<td>{{$vehiclespesification->electricEngineMaximumOutput}}</td>
		</tr>
		<tr>
			<td><b>Electric engine maximum torque</b></td>
			<td>{{$vehiclespesification->electricEngineMaximumTorque}}</td>
			<td></td>
			<td><b>Electric engine power</b></td>
			<td>{{$vehiclespesification->electricEnginePower}}</td>
		</tr>
		<tr>
			<td><b>Engine maximum power</b></td>
			<td>{{$vehiclespesification->engineMaximumPower}}</td>
			<td></td>
			<td><b>Engine maximum torque</b></td>
			<td>{{$vehiclespesification->engineMaximumTorque}}</td>
		</tr>
		<tr>
			<td><b>Engine model </b></td>
			<td>{{$vehiclespesification->engineModel}}</td>
			<td></td>
			<td><b>Frame type</b></td>
			<td>{{$vehiclespesification->frameType}}</td>
		</tr>
		<tr>
			<td><b>Front shaft weight</b></td>
			<td>{{$vehiclespesification->frontShaftWeight}}</td>
			<td></td>
			<td><b>Front shock absorber type</b></td>
			<td>{{$vehiclespesification->frontShockAbsorberType}}</td>
		</tr>
		<tr>
			<td><b>Front stabilizer type</b></td>
			<td>{{$vehiclespesification->frontStabilizerType}}</td>
			<td></td>
			<td><b>Front tires size</b></td>
			<td>{{$vehiclespesification->frontTiresSize}}</td>
		</tr>
		<tr>
			<td><b>Front tread </b></td>
			<td>{{$vehiclespesification->frontTread}}</td>
			<td></td>
			<td><b>Fuel consumption</b></td>
			<td>{{$vehiclespesification->fuelConsumption}}</td>
		</tr>
		<tr>
			<td><b>Fuel tank equipment</b></td>
			<td>{{$vehiclespesification->fuelTankEquipment}}</td>
			<td></td>
			<td><b>Grade</b></td>
			<td>{{$vehiclespesification->gradeData}}</td>
		</tr>
		<tr>
			<td><b>Height</b></td>
			<td>{{$vehiclespesification->height}}</td>
			<td></td>
			<td><b>Length</b></td>
			<td>{{$vehiclespesification->length}}</td>
		</tr>
		<tr>
			<td><b>Main brakes type</b></td>
			<td>{{$vehiclespesification->mainBrakesType}}</td>
			<td></td>
			<td><b>Make</b></td>
			<td>{{$vehiclespesification->dataMake}}</td>
		</tr>
		<tr>
			<td><b>Maximum speed</b></td>
			<td>{{$vehiclespesification->maximumSpeed}}</td>
			<td></td>
			<td><b>Minimum ground clearance</b></td>
			<td>{{$vehiclespesification->minimumGroundClearance}}</td>
		</tr>
		<tr>
			<td><b>Minimum turning radius</b></td>
			<td>{{$vehiclespesification->minimumTurningRadius}}</td>
			<td></td>
			<td><b>Model</b></td>
			<td>{{$vehiclespesification->modelData}}</td>
		</tr>

		<tr>
			<td><b>Model code</b></td>
			<td>{{$vehiclespesification->modelCode}}</td>
			<td></td>
			<td><b>Mufflers number</b></td>
			<td>{{$vehiclespesification->mufflersNumber}}</td>
		</tr>

		<tr>
			<td><b>Rear shaft weight </b></td>
			<td>{{$vehiclespesification->rearShaftWeight}}</td>
			<td></td>
			<td><b>Rear shock absorber type</b></td>
			<td>{{$vehiclespesification->rearShockAbsorberType}}</td>
		</tr>
		<tr>
			<td><b>Rear stabilizer type </b></td>
			<td>{{$vehiclespesification->rearStabilizerType}}</td>
			<td></td>
			<td><b>Rear tires size</b></td>
			<td>{{$vehiclespesification->rearTiresSize}}</td>
		</tr>
		<tr>
			<td><b>Rear tread</b></td>
			<td>{{$vehiclespesification->rearTread}}</td>
			<td></td>
			<td><b>Reverse ratio</b></td>
			<td>{{$vehiclespesification->reverseRatio}}</td>
		</tr>
		<tr>
			<td><b>Riding capacity</b></td>
			<td>{{$vehiclespesification->ridingCapacity}}</td>
			<td></td>
			<td><b>Side brakes type</b></td>
			<td>{{$vehiclespesification->sideBrakesType}}</td>
		</tr>
		<tr>
			<td><b>Specification code </b></td>
			<td>{{$vehiclespesification->specificationCode}}</td>
			<td></td>
			<td><b>Stopping distance</b></td>
			<td>{{$vehiclespesification->stoppingDistance}}</td>
		</tr>
		<tr>
			<td><b>Transmission type</b></td>
			<td>{{$vehiclespesification->transmissionType}}</td>
			<td></td>
			<td><b>Weight</b></td>
			<td>{{$vehiclespesification->weight}}</td>
		</tr>
		<tr>
			<td><b>Wheel alignment </b></td>
			<td>{{$vehiclespesification->wheelAlignment}}</td>
			<td></td>
			<td><b>Wheelbase</b></td>
			<td>{{$vehiclespesification->wheelbase}}</td>
		</tr>
		<tr>
			<td><b>Width</b></td>
			<td>{{$vehiclespesification->width}}</td>
			<td></td>
			<td><b></b></td>
			<td></td>
		</tr>
		
					
	</table>

	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">AUCTION DATA</h3></td>
		</tr>
	</table>


	<table width="100%" class="table table-striped" style="font-size: 15px;" >
		
		@foreach($auctionhistory as $data)
		<tr>
			<td>Date</td>
			<td>{{$data->date}}</td>
			<td></td>
			<td>Lot#</td>
			<td>{{$data->lotNumber}}</td>
		</tr>
		<tr>
			<td>Auction name</td>
			<td>{{$data->auction}}</td>
			<td></td>
			<td>Region</td>
			<td></td>
		</tr>
		<tr>
			<td>Make</td>
			<td>{{$data->make}}</td>
			<td></td>
			<td>Model</td>
			<td>{{$data->model}}</td>
		</tr>
		<tr>
			<td>Reg. year</td>
			<td>{{$data->registrationDate}}</td>
			<td></td>
			<td>Mileage (km)</td>
			<td>{{$data->mileage}}</td>
		</tr>
		<tr>
			<td>Displacement (cc)</td>
			<td>{{$data->displacement}}</td>
			<td></td>
			<td>Transmission</td>
			<td>{{$data->transmission}}</td>
		</tr>

		<tr>
			<td>Color</td>
			<td>{{$data->color}}</td>
			<td></td>
			<td>Model code</td>
			<td>{{$data->body}}</td>
		</tr>
		<tr>
			<td>Result</td>
			<td>{{$data->result}}</td>
			<td></td>
			<td>Auction grade</td>
			<td>{{$data->assessment}}</td>
		</tr>
		<tr>
			<td>Problem type</td>
			<td></td>
			<td></td>
			<td>Problem scale</td>
			<td></td>
		</tr>
		<tr>
			<td>Contaminated</td>
			<td>{{$data->assessment}}</td>
			<td></td>
			<td>Airbag</td>
			<td></td>
		</tr>
		@endforeach
					
	</table>


	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">PHOTOS AND AUCTION SHEETS</h3></td>
		</tr>
	</table>  


	<table width="100%"  style="font-size: 15px;" >
		
		@foreach($auctionimage as $data)
		
			<img src="{{$data->image}}" alt="{{$data->image}}" class="img-fluid" width="80%" align="center">
			
		@endforeach			
	</table>

	<!-- 
	<table width="100%" class="m50">
		<tr>
			<td align="left"><h3 style="font-size: 17px !important; background-color: #d65353; color: white; padding: 5px">GLOSSARY</h3></td>
		</tr>
	</table> -->

	<!-- 
	<table width="100%" class="table table-striped" style="font-size: 15px;" >
		
		<tr>
			<td>
				<p><sup>1</sup> <b>Chassis number</b> – a unique identification number of the vehicle in Japan (same as VIN in the USA or Europe)</p>
			</td>
		</tr>
		<tr>
			<td>
				<sup>2</sup> <b>Title Information: </b> <br> 
				Registered – qualified for driving in Japan<br>
				Deregistered Temporarily – not qualified for driving in Japan, usually a temporary title during the ownership change <br>
				Deregistered Completely – not qualified for driving in Japan, the vehicle is determined to be scrapped <br>
				Deregistered to Export – not qualified for driving in Japan , the vehicle is determined to be exported <br>
			</td>
		</tr>

		<tr>
			<td>
				<sup>3</sup> <b> Determining the overall collision safety performance evaluation </b> – For the driver's seat, the results of the full-wrap frontal collision test, offset frontal collision test, and side collision test are added together and evaluated to 6 different levels. For the Frontal passenger's seat, the results of the full-wrap frontal collision test and the side collision test (results for the driver's or the front passenger's seat are used) are added together and evaluated to 6 different levels. <br><br>

				Regular vehicle inspection – All vehicles in Japan must undergo regular vehicle inspections (shaken). New cars need to be tested after three years, and then vehicles must be tested every two years thereafter. A vehicle inspection (shaken) is compulsory for all vehicles with an engine size over 250cc. It ensures that all vehicles on the road are properly maintained and safe to drive. The test also checks that vehicles have not been illegally modified; if they are found to have been modified, they are not allowed on the road.

			</td>
		</tr>

		<tr>
			<td>
				<sup>4</sup> <b> Use in the contaminated regions </b> – The Fukushima Daiichi nuclear disaster was a catastrophic failure at the Fukushima I Nuclear Power Plant on 11 March 2011, resulting in a meltdown of three of the plant's six nuclear reactors.

			</td>
		</tr>

		<tr>
			<td>
				<sup>5</sup> <b>  Radioactive contamination test </b>– radioactive contamination inspection that was started in July 2011 as a preventive measure for exporting contaminated vehicles from Japan. The inspection is being conducted since in all sea ports of Japan under the supervision of The Japan Harbor Transportation Association (JHTA). <br><br>

				MLIT – Ministry of Land, Infrastructure, Transport and Tourism

			</td>
		</tr>

		<tr>
			<td>
				<sup>6</sup> <b>   Japan New Car Assessment Program</b> – the Ministry of Land, Infrastructure, Transport and Tourism (MLIT) and the National Agency for Automotive Safety & Victims' Aid (NASVA) have taken measures for safety, one of which is to assess commercially available vehicles through a variety of safety performance tests and release the resulting information compiled into the "New Car Assessment Program". The objective of Japan New Car Assessment Program is to increase the use of safe automobiles by providing an environment in which users can easily select such vehicles. This also promotes the development of safer vehicles by automobile manufacturers. Neck injury protection for rear-end collision performance test , rear seat passenger's protection for frontal collision performance test, rear passenger's seat belt usability evaluation test and seat belt reminder for passengers evaluation test are started in FY2009.
 				<br>


			</td>
		</tr>

		<tr>
			<td>
			<sup>7</sup><b>Braking Performance Tests</b> – Braking performance is determined by the shortness of the distance in which a vehicle can stop and the stability of the vehicle at the time of braking. This test is performed under wet and dry road conditions for a vehicle which has both a driver and a front passenger. The distance it takes for the vehicle to stop and the stability of the vehicle at the time of braking is evaluated for when the vehicle is stopped abruptly while traveling at a speed of 100km/h. The stopping distance and vehicle speed have been measured by using GPS since FY2009.
			</td>
		</tr>
		
	</table> -->


</body>
</html>