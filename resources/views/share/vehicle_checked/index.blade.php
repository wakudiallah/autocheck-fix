@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Checked XX</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
		                <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered"  width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>
                                 
                                  <th >Status</th>
                                  
                                  <th width="20%" align="center">Action</th>
                                  
                                  
                                    @if($me == "VER" OR $me == "KA")
                                    <th>Full Report</th>
                                    <th>Req From</th>
                                    @endif
                                  

                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  
                                  <td>{{$data->created_at}}</td>

                                  <!-- Status -->
                                  <td align="center">
                                      @if($data->status == '40')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>
                                      
                                      
                                      @endif
                                  </td>
                                  <!-- End of Status -->


                                  <!-- Action -->
                                  <td width="20%" align="center">

                                    <a href="{{url('ExtraReport/detail/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"> 
                                          <i class="material-icons">details</i>
                                        </button>
                                      </a>


                                      <!-- download complete manual -->
                                        @if($data->status == '40' AND $data->searching_by == 'NOT')
                                            
                                      @endif


                                      <!-- End download complete manual -->
                                      @if($data->searching_by == 'API')
                                      <a href="{{url('report-autocheck/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>

                                      @elseif($data->searching_by == 'NOT')

                                      <a href="{{url('report-autocheck-mn/'.$data->id_vehicle)}}" target="_blank">
                                                <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                  <i class="material-icons">save_alt</i>
                                                </button>
                                              </a>
                                      
                                      @endif
                                      <!-- End action print -->


                                      
                                    <!-- ==== Modal Detail ==== -->
                                    

                                    @if($data->status == "30" OR $data->status == "40")
                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".modal-detail-complete{{$data->id}}">
                                        <i class="material-icons">details</i> 
                                    </button>

                                   

                                    @endif

                                    <!-- ==== End Modal Detail ==== -->


                              
                                  </td> <!-- end action verify -->


                                  <!-- Full Report -->
                                  @if($me == "VER" OR $me == "KA")
                                  <td align="center">
                                    @if($data->status == "40" AND $data->group_by == "KA")

                                    @if($data->searching_by == "API")
                                    <a href="{{url('report-carvx/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>
                                    @else
                                      
                                      <a href="{{ url('/') }}/report/manual/{{$data->full_report}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>

                                    @endif
                                    @endif

                                  </td>

                                  <td>

                                    @if(!empty($data->request_from->role->desc))
                                    <p style="color: red">{{$data->request_from->role->desc}}</p>
                                    @endif
                                  </td>
                                  @endif
                                  <!-- End Full Report -->


                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>

                                  </td>

                                  <!-- End History Search -->
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
                      </table>
		                </div>		

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
        <!-- Modal update by API -->
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm2{{$data->id_vehicle}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Match  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <form method="POST" class="register-form" id="register-form" action="{{url('verify-vehicle-verifier-api/'.$data->id_vehicle)}}">
                        {{ csrf_field() }}

                        @if(!empty($data->vehicle_api->api_from))
                          <input type="hidden" name="api_from" value="{{$data->vehicle_api->api_from}}" class="">
                        @endif

                        <input type="hidden" name="vehicle" value="{{$data->vehicle}}" class="">
                        <input type="hidden" name="created_by" value="{{$data->created_by}}" class="">
                      

                      <div class="form-group col-md-12">
                        <h2><b>{{$data->vehicle}}</b></h2>
                      </div>

                      <hr>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Supplier</label>
                        @if(!empty($data->supplier->supplier))
                        <h3>{{$data->supplier->supplier}}</h3>
                        @endif
                      </div>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Type</label>
                        @if(!empty($data->type->type_vehicle))
                        <h3>{{$data->type->type_vehicle}}</h3>
                        @endif
                      </div>

                      <hr>

                      <div class="form-row">                        
                        <label for="feDescription">Make / Brand </label>

                      </div>


                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->brand)){{$data->vehicle_api->brand}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>
                          <div class="form-group col-md-5">
                            
                            <select name="brand_verify" class="form-control is-valid" id="brand_value_edit{{$data->id}}" required>
                              <option value="" selected disabled hidden>- Please Select -</option>
                              @foreach($brand as $databrand)
                              <option value="{{$databrand->id}}" {{ $data->brand_id == $databrand->id ? 'selected' : '' }} >{{$databrand->brand}}</option>
                              @endforeach

                          </select>
                            <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if(!empty($data->status_vehicle->brand))
                              @if($data->status_vehicle->brand == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                              @endif
                            </div>
                          </div>
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Model / Variance </label>
                      </div>

                      <div class="form-row">
                          
                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->model)){{$data->vehicle_api->model}}@endif" disabled="">
                            
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <select id="model_value_edit{{$data->id}}" name="model_verify" class="form-control is-valid"  data-show-subtext="true" data-live-search="true">
                              <option value="" selected disabled hidden>- Please Select -</option>
                              
                              @foreach($model as $datamodel)
                              
                               <option value="{{ $datamodel->id }}|{{$datamodel->brand_id}}" {{ $data->model_id == $datamodel->id ? 'selected' : '' }}>{{$datamodel->model}}</option>

                              @endforeach
                            </select> 

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if(!empty($data->status_vehicle->model))
                              @if($data->status_vehicle->model == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                              @endif
                            </div>
                          </div>
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Engine Number / Engine Model </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->engine_number)){{$data->vehicle_api->engine_number}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->engine_number}}" required name="engine_number_verify"> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Cubic Capacity (CC) </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->cc)){{$data->vehicle_api->cc}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->cc}}" required name="cc_verify"> 
                            <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if(!empty($data->status_vehicle->cc))
                                @if($data->status_vehicle->cc == '1')
                                  <h3>OK <i class="material-icons">thumb_up</i></h3>
                                @else
                                  <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                                @endif
                              @endif
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Fuel Type </label>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->fuel_type)){{$data->vehicle_api->fuel_type}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif 
                            </div>
                          </div>
                          
                          <div class="form-group col-md-5">
                              <select id="feInputState" name="fuel_verify" class="form-control is-valid">
                              @foreach($fuel as $datafuel)
                              
                               <option value="{{ $datafuel->id }}" {{ $data->fuel_id == $datafuel->id ? 'selected' : '' }}>{{$datafuel->fuel}}</option>

                              @endforeach
                            </select>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Year of Manufacture </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->year_manufacture)){{$data->vehicle_api->year_manufacture}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input id="bday-month" name="year_verify" class="form-control is-valid" type="month" name="year_verify" value="" disabled> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>

                      
                      <div class="form-row">                        
                        <label for="feDescription">First Registration Date</label>
                      </div>

                      <div class="form-row">

                        <?php $tarikh_from_kadealer =  date('m/d/Y ', strtotime($data->vehicle_registered_date)); ?>

                        @if(!empty($data->vehicle_api->registation_date))

                        <?php $tarikh_from_api =  date('m/d/Y ', strtotime($data->vehicle_api->registation_date)); ?>

                        @endif

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->registation_date)){{$tarikh_from_api}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>


                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid"  value="{{$tarikh_from_kadealer}}" name="registration_date_verify" id="datepicker{{$data->id}}"> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if(!empty($data->status_vehicle->registation_date))
                              @if($data->status_vehicle->registation_date == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                              @endif
                            </div>
                          </div>
                      </div>

                      @if(empty($data->vehicle_api->api_from)) <!-- if not from carvx -->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Report From</label>
                          <select name="api_from" class="form-control" >
                            <option value="RYO">RYOCHI</option>
                            <option value="HPI">HPI</option>
                            <option value="OTHER">Others</option>
                          </select>
                        </div>
                      </div>
                      @endif <!-- end if not from carvx -->

                      @if(!empty($data->status_vehicle->status))
                      @if($data->status_vehicle->status == "0")<!-- if not match -->
                      
                      <!-- Setakat ini remark belum dulu 
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Remark</label>
                          <textarea name="remark" class="form-control is-valid" rows="5" placeholder="Remark"></textarea>
                        </div>
                      </div> -->
                      <!-- end if not match -->
                      

                      <!-- <div class="form-row">
                        <div class="form-group col-md-6"></div>
                        <div class="form-group col-md-6">
                          <div class="radio">
                            <label><input type="radio" name="yes" value="yes" checked>Approve</label>
                          </div>
                          <div class="radio">
                            <label><input type="radio" name="yes" value="no">Reject</label>
                          </div>
                        </div>
                      </div> -->
                      @endif
                      @endif
                   
                   
                </div>
                <div class="modal-footer">
                  
                  <button type="submit" class="btn btn-accent">Verify</button>

                </div>
              </form>

              </div>
            </div>
          </div>
          @endforeach
        
          <!-- ======== Modal Detail Complete === -->
          @foreach($data2 as $data)
          <div class="modal fade modal-detail-complete{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail Complete </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}} </b></h2>
                        </div>

                        <hr>

                        <!-- Not Kastam and complete API Detail -->
                        @if(($data->status == '40' AND $data->searching_by == 'API') AND ($data->group_by != 'KA'))
                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}}</h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand </label>
                          @if(!empty($data->vehicle_api->brand))
                          <h3>{{$data->vehicle_api->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($data->vehicle_api->model))
                          <h3>{{$data->vehicle_api->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->vehicle_api->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$data->vehicle_api->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$data->vehicle_api->registation_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($data->fuel->fuel))
                            <h3>{{$data->fuel->fuel}}</h3>
                            @endif
                          </div>



                          <!-- Not Kastam and complete Manual Detail -->
                          @elseif(($data->status == '40' AND $data->searching_by == 'NOT') AND ($data->group_by != 'KA'))




                        <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}} </h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand </label>
                          @if(!empty($vehicle_manual->manual_brand->brand))
                          <h3>{{$vehicle_manual->manual_brand->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($vehicle_manual->manual_model_brand->model))
                          <h3>{{$vehicle_manual->manual_model_brand->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3></h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3></h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3></h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($vehicle_manual->manual_fuel->fuel))
                            <h3>{{$vehicle_manual->manual_fuel->fuel}}</h3>
                            @endif
                          </div>

                      @endif

                      </div>

                  </div>
                <div class="modal-footer">
                  
                  
                </div>


              </div>
            </div>
          </div>

          @endforeach
          <!-- ======== Modal Detail Complete === -->




          
            

@endsection


@push('js')


    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
