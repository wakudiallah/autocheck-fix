@extends('admin.layout.template')

@section('content')

    
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Checked ( {{config('autocheck.half_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
 

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      
			             <div class="table table-responsive">
                      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Verification Serial Number</th>
                                  <th>Date Request</th>
                                  
                                  <th>Status</th>
                                  
                                  <th>Date Verify</th>
                                  
                                  <th>Report</th>

                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)

                            
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>
                                    <a href="{{url('HalfReport/detail/'.$data->id_vehicle)}}" target="_blank">{{$data->vehicle}}</a>             
                                  </td>
                                  

                                  <td>
                                    @if(!empty($data->vehiclehalf->ver_sn))
                                    {{$data->vehiclehalf->ver_sn}}
                                    @endif
                                    
                                  </td>
                                  <td>{{$data->created_at}}</td>

                                  <!-- API Status -->
                                  <td>
                                    
                                      @if($data->status == 40)
                                        <i  class="card-post__category badge badge-pill badge-success">Complete</i>
                                      @endif
                                  </td>

                                  <!-- End of API Status -->

                                  <td>
                                    {{$data->updated_at}}
                                  </td>
                                  


                                  <!-- Full Report -->

                                  <td align="center">

                                    <a href="{{url('HalfReport/Report/'.$data->id_vehicle)}}" target="_blank">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="material-icons">save_alt</i>
                                      </button>
                                    </a>

                                    <a href="{{url('HalfReport/detail/'.$data->id_vehicle)}}" target="_blank">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"> 
                                        <i class="material-icons">details</i>
                                      </button>
                                    </a>

                                    
                                  </td>

                                  
                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>

                                  <!-- End History Search -->
                              </tr>

                            @endforeach
                              
                          </tbody>
                          
                      </table>
			             </div>


                      </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
    
 
            

@endsection


@push('js')

  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    


    @foreach($data3 as $data2)
    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker2{{$data2->id}}" ).datepicker();
     });
    </script>
    @endforeach


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
