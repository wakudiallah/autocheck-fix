<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
      <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>

      <style type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"></style>
      <style type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css"></style>

    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('images/fav.ico')}}">


    <title>Autocheck</title>
  </head>
  <body>

    <h1>Balance</h1>
    
    <div class="table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Topup</th>
              <th scope="col">Balance (RM)</th>
              <th scope="col">Fee (RM)</th>
              <th scope="col">Desc</th>
              <th scope="col">Created By</th>
              <th scope="col">Date</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
            @foreach($data as $data)

            <tr>
              <?php 
                $balance = number_format($data->balance, 2, '.', '');
                $transaction_fee = number_format($data->credit, 2, '.', '');
              ?>
              <td scope="row">{{$i++}}</td>
              <td>
                {{$data->debit}}
              
              </td>
              <td>RM {{$balance}}</td>
              <td>RM {{$transaction_fee}}</td>
              <td>{{$data->desc}}</td>
              <td>{{$data->topup_by->name}}</td>
              <td>{{$data->created_at}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>


  
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>

   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>
  </body>
</html>