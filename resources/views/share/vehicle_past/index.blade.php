@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Past </h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
		              <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>
                                  <th>Date Verify</th>
				                  <th>Request From</th>
                                  <th>Status</th>
                                  <th width="20%" align="center">Action</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($vehicle as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle_id_number}}</td>
                                  <td>{{$data->CreationTime}}</td>
                                  <td>{{$data->VerifiedDate}}</td>
                                    <td>
                                        @if(!empty($data->userpast->name))
                                        	{{$data->userpast->name}}
                                        @else
                                        @endif 
                                    </td>
                                  <!-- Status -->
                                  <td align="center">
                                    {{$data->Status}}
                                  </td>
                                  <!-- End of Status -->



                                  <!-- Action -->
                                  <td width="20%" align="center">

                                    
                                    <!-- === Download === -->

                                    <a href="{{url('report-short-past/'.$data->Id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>
                                    <!-- === End  Download === -->

                                      
                                    <!-- ==== Modal Detail ==== -->
                                   
                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1{{$data->id}}">
                                        <i class="material-icons">details</i>
                                      </button>

                                    <!-- ==== End Modal Detail ==== -->


                                  
                                  </td> <!-- end action verify -->


                          

                                  <!-- End History Search -->
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
            		      </table>
            		</div>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
       
          
          
          <!-- ======== Modal Detail === -->
          @foreach($vehicle as $data)
          <div class="modal fade bd-example-modal-sm1{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                   
                      <!-- get from API and complete / KADEALER-->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->VehicleIdNumber}}</b></h2>
                        </div>

                        <hr>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          
                          <h3>{{$data->Supplier}}</h3>
                          
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if($data->VehicleTypeId == 1)
                          <h3>Car</h3>
                          @elseif($data->VehicleTypeId == 2)
                          <h3>Vehicle</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if($data->CountryId == '6')
                          <h3>Japan</h3>
                          @elseif($data->CountryId == '7')
                          <h3>Japan</h3>
                          @elseif($data->CountryId == '8')
                          <h3>Netherland</h3>
                          @elseif($data->CountryId == '9')
                          <h3>Germany</h3>
                          @elseif($data->CountryId == '10')
                          <h3>United States Of America</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          @if(!empty($data->modelpast->par_model->brand))
                          <h3>{{$data->modelpast->par_model->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                              @if(!empty($data->modelpast->id))
                          <h3>{{$data->modelpast->model}}</h3>
                              @endif
                          
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->EngineNumber}}</h3>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feInputCity">Engine Capacity (cc)</label>
                          <h3>{{$data->EngineCapacity}}</h3> 
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feInputCity">Vehicle Registered Date</label>
                          <h3>
                            <?php $tarikh =  date('F-Y ', strtotime($data->RegisterDate)); ?>
                           {{$tarikh}}
                          </h3>
                        </div>

                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            
                            <h3>
                              @if($data->FuelId == "4")
                                Petrol
                              @endif
                            </h3>
                            
                          </div>

                      </div>
                      <!-- end get from API -->


                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>


              </div>
            </div>
          </div>
          @endforeach
          <!-- ======= End Modal Detail ======= -->



          
            

@endsection


@push('js')

  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
