@extends('admin.layout.template')

@section('content')

      <link href="{{ asset('css/pict.css') }}" rel="stylesheet" />

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Account</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            


            <div class="row">
              <div class="col-lg-4">

                  <div class="user-info">
                    <div class="profile-pic">@if(empty(Auth::user()->picture)) <img src="{{asset('images/avatar.png')}}" width="110"/> @else<img src="{{asset('images/profile/'.Auth::user()->picture)}}" width="110"/>@endif
                      <div class="layer">
                        <div class="loader"></div>
                      </div><a class="image-wrapper" href="#">
                        
                       <form id="uploadform"  action="{{url('save/profile/'.Auth::user()->id)}}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}
                              
                              <input type="file" name="fileToUpload" id="changePicture" class="hidden-input test form-control" onchange='this.form.submit()'>

                              <label class="edit glyphicon glyphicon-pencil" for="changePicture" type="file" title="Change picture"><i class="material-icons">edit</i></label>

                          </form>
                          </a>


                    </div>
                    <div class="username">
                      
                    </div>
                  </div>

                <div class="card card-small mb-4 pt-3" style="margin-top: 90px">
                  <div class="card-header border-bottom text-center">
                    

                    <div class="mb-3 mx-auto">
                        


                      <!--<img class="rounded-circle" src="images/avatars/0.jpg" alt="User Avatar" width="110"> -->
                    </div>
                    <h4 class="mb-0">{{Auth::user()->name}}</h4>
                    <span class="text-muted d-block mb-2">{{Auth::user()->email}}</span>
                    
                  </div>
                  <ul class="list-group list-group-flush">
                    <!-- 
                    <li class="list-group-item px-4">
                      <div class="progress-wrapper">
                        <strong class="text-muted d-block mb-2">Workload</strong>
                        <div class="progress progress-sm">
                          <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="74" aria-valuemin="0" aria-valuemax="100" style="width: 74%;">
                            <span class="progress-value">74%</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    
                    <li class="list-group-item p-4">
                      <strong class="text-muted d-block mb-2">Description</strong>
                      <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?</span>
                    </li>
                  -->
                  </ul>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Account Details</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="First Name" value="{{$data->name}}" disabled=""> 
                              </div>

                                                            

                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="text" class="form-control" id="feLastName" placeholder="Last Name" value="{{$data->email}}" readonly=""> 
                              </div>
                            </div>


                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feFirstName">IC / Passport</label>
                                @if(!empty($data->detail_user->ic))
                                <input type="text" class="form-control" id="feFirstName" placeholder="IC / Passport" value="{{$data->detail_user->ic}}" disabled=""> 
                                @else
                                <input type="text" class="form-control" id="feFirstName" placeholder="IC / Passport" value="" disabled=""> 
                                @endif
                              </div>

                              <div class="form-group col-md-6">
                                <label for="feFirstName">Gender</label>
                                @if(!empty($data->detail_user->gender))

                                  @if($data->detail_user->gender == 1)
                                    <input type="text" class="form-control"  placeholder="Gender" value="Male" disabled=""> 
                                  @else
                                    <input type="text" class="form-control"  placeholder="Gender" value="Female" disabled="">
                                  @endif

                                @else
                                <input type="text" class="form-control" id="feFirstName" placeholder="Gender" value="" disabled=""> 
                                @endif
                              </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Home Address</label>
                                @if(!empty($data->detail_user->home_address))
                                <textarea class="form-control" name="feDescription" rows="5" readonly="">{{$data->detail_user->home_address}}</textarea>
                                @else
                                <textarea class="form-control" name="feDescription" rows="4" readonly=""></textarea>
                                @endif
                              </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feDescription">Mailing Address</label>
                                @if(!empty($data->detail_user->mailing_address))
                                <textarea class="form-control" name="feDescription" rows="4" readonly="">{{$data->detail_user->mailing_address}}</textarea>
                                @else
                                <textarea class="form-control" name="feDescription" rows="5" readonly=""></textarea>
                                @endif
                              </div>
                            </div>


                            

                            <div class="form-row">
                              <div class="form-group col-md-2">
                                <label for="feInputCity">Postcode</label>
                                @if(!empty($data->detail_user->postcode))
                                <input type="text" class="form-control" id="feInputCity" value="{{$data->detail_user->postcode}}" readonly="">
                                @else
                                <input type="text" class="form-control" id="feInputCity" value="" placeholder="Postcode" readonly="">
                                @endif
                              </div>
                              <div class="form-group col-md-4">
                                <label for="feInputState">State</label>
                                @if(!empty($data->detail_user->bandar))
                                <input type="text" class="form-control" id="inputZip" readonly value="{{$data->detail_user->bandar}}"> 
                                @else
                                <input type="text" class="form-control" id="inputZip" placeholder="State" readonly=""> 
                                @endif
                              </div>
                              <div class="form-group col-md-6">
                                <label for="inputZip">Country</label>

                                @if(!empty($data->detail_user->postcode))
                                <input type="text" class="form-control" readonly id="inputZip" value="{{$data->detail_user->negara}}">  
                                @else
                                <input type="text" class="form-control" id="inputZip" placeholder="Country" readonly=""> 
                                @endif
                              </div>
                            </div>

                            <hr>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="feFirstName">Bank Accout</label>
                                  @if(!empty($data->detail_user->account_bank))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="" disabled=""> 
                                  @endif
                                </div>


                                <div class="form-group col-md-6">
                                  <label for="feFirstName">Bank Accout Number</label>
                                  @if(!empty($data->detail_user->account_bank_number))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank_number}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="" disabled=""> 
                                  @endif
                                </div>

                            </div>


                            <div class="form-row">
                              <div class="form-group col-md-4">
                                  <label for="feFirstName">Total Last Transaction</label>
                                  @if(!empty($data->detail_user->amount_last_transactions))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Total" value="" disabled=""> 
                                  @endif
                                </div>

                                <div class="form-group col-md-4">
                                  <label for="feFirstName">Status Transaction</label>
                                  @if(!empty($data->detail_user->status_transactions))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->account_bank}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Status" value="" disabled=""> 
                                  @endif
                                </div>

                                <div class="form-group col-md-4">
                                  <label for="feFirstName">Date Last Transaction</label>
                                  @if(!empty($data->detail_user->account_bank))
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Bank" value="{{$data->detail_user->date_last_transactions}}" disabled=""> 
                                  @else
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Date" value="" disabled=""> 
                                  @endif
                                </div>
                            </div>


                            <button type="button" class="btn btn-accent" data-toggle="modal" data-target=".bd-example-modal-lg">Update Account</button>

                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>


          </div>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
          <form method="POST" class="register-form" id="register-form" action="{{url('/update-account/'.$data->id)}}">
              {{ csrf_field() }}
            

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="feDescription">IC / Passport</label>
                @if(!empty($data->detail_user->ic))
                <input type="text" class="form-control" id="feFirstName" placeholder="IC / Passport" value="{{$data->detail_user->ic}}" required=""> 
                @else
                <input type="text" class="form-control" id="feFirstName" placeholder="IC / Passport" value="" required=""> 
                @endif
              </div>

              <div class="form-group col-md-12">
                <label for="feDescription">Gender</label>
                
                @if(!empty($data->detail_user->gender))
                  @if($data->detail_user->gender == 1)
                  <select placeholder="Gender" name="gender" class="form-control">
                    <option value="1" selected>Male</option>
                    <option value="2">Female</option>
                  </select>
                  @else
                    <select placeholder="Gender" name="gender" class="form-control">
                      <option value="1">Male</option>
                      <option value="2" selected>Female</option>
                    </select>
                  @endif

                @else
                <select placeholder="Gender" name="gender" class="form-control">
                  <option value="1">Male</option>
                  <option value="2">Female</option>
                </select>
                @endif

              </div>

            </div>
            

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="feDescription">Home Address </label>
                @if(!empty($data->detail_user->home_address))
                <textarea class="form-control" rows="5" name="home_address" required="">{{$data->detail_user->home_address}}</textarea>
                @else
                <textarea class="form-control" rows="4" name="home_address" required=""></textarea>
                @endif
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="feDescription">Mailing Address</label>
                @if(!empty($data->detail_user->mailing_address))
                <textarea class="form-control" rows="4" name="mailing_address" required="">{{$data->detail_user->mailing_address}}</textarea>
                @else
                <textarea class="form-control" rows="5" name="mailing_address" required=""></textarea>
                @endif
              </div>
            </div>


            

            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="feInputCity">Postcode</label>
                @if(!empty($data->detail_user->postcode))
                <input type="text" class="form-control" id="postcode" value="{{$data->detail_user->postcode}}" name="postcode" required="">
                @else
                <input type="text" class="form-control" id="postcode" onkeypress="return isNumberKey(event)" value="" placeholder="Postcode" name="postcode" required="">
                @endif
              </div>
              <div class="form-group col-md-4">
                <label for="feInputState">State</label>
                @if(!empty($data->detail_user->bandar))
                <input type="text" class="form-control" id="city" value="{{$data->detail_user->bandar}}" name="state" required=""> 
                @else
                <input type="text" class="form-control" id="city" placeholder="State" name="state" required=""> 
                @endif
              </div>
              <div class="form-group col-md-6">
                <label for="inputZip">Country</label>

                @if(!empty($data->detail_user->negara))
                <input type="text" class="form-control" id="country" value="{{$data->detail_user->negara}}" name="contry" required="">  
                @else
                <input type="text" class="form-control" id="negara" placeholder="Country" name="contry" required=""> 
                @endif
              </div>
            </div>

            <hr>

            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="feFirstName">Bank Accout</label>
                  @if(!empty($data->detail_user->account_bank))
                  <input type="text" class="form-control"  placeholder="Bank" value="{{$data->detail_user->account_bank}}" name="bank_account" required=""> 
                  @else
                  <input type="text" class="form-control" placeholder="Bank" value="" name="bank_account" required=""> 
                  @endif
                </div>


                <div class="form-group col-md-6">
                  <label for="feFirstName">Bank Accout Number</label>
                  @if(!empty($data->detail_user->account_bank_number))
                  <input type="text" class="form-control"  placeholder="Bank" value="{{$data->detail_user->account_bank_number}}" name="bank_account_number" required="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"> 
                  @else
                  <input type="text" class="form-control"  placeholder="Bank Account Number" value=""  name="bank_account_number" required="" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"> 
                  @endif
                </div>

            </div>



             
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" value="Save Changes" name="" class="btn btn-primary">
        
      </div>

      </form> 


    </div>
  </div>
</div>




@endsection


@push('js')

 <script type="text/javascript">
    $( "#postcode" ).change(function() {
        var postcode = $('#postcode').val();
        $.ajax({
        url: "<?php  print url('/'); ?>/postcode/"+postcode,
        dataType: 'json',
        data: {
        },
        success: function (data, status) {
            jQuery.each(data, function (k) {
                $("#city").val(data[k].post_office );
                $("#state").val(data[k].state.state_name );
                $("#country").val("Malaysia");
                });
            }
        });
    });
</script>


<script type="text/javascript">
        function test() {
            var form = document.getElementById('uploadform');
            form.submit();
        };
    </script>
  

@endpush
