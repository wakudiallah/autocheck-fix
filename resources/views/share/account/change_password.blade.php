@extends('admin.layout.template')

@section('content')

      <link href="{{ asset('css/pict.css') }}" rel="stylesheet" />

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Change Password</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            


            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-lg-6">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Change Password</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">

                          <form method="POST" class="register-form" id="register-form" action="{{url('/update-password/'.$data->id)}}">
              

              {{ csrf_field() }}
                          
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">New Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password" value="" name="password" maxlength="12" required> 
                              </div>

                                                            

                              <div class="form-group col-md-12">
                                <label for="feLastName">Confirm New Password</label>
                                <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password" value="" name="conf" maxlength="12" required> 
                              </div>
                            </div>
                              <div class="form-group col-md-12">
                            <span id='message'></span>
                          </div>
                           
                            <input type="submit" value="Submit" name="" class="btn btn-accent">
                            

                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>


            <div class="col-md-3"></div>


          </div>







@endsection


@push('js')

 <script type="text/javascript">
    $( "#postcode" ).change(function() {
        var postcode = $('#postcode').val();
        $.ajax({
        url: "<?php  print url('/'); ?>/postcode/"+postcode,
        dataType: 'json',
        data: {
        },
        success: function (data, status) {
            jQuery.each(data, function (k) {
                $("#city").val(data[k].post_office );
                $("#state").val(data[k].state.state_name );
                $("#country").val("Malaysia");
                });
            }
        });
    });
</script>


<script type="text/javascript">
        function test() {
            var form = document.getElementById('uploadform');
            form.submit();
        };
    </script>


    <script type="text/javascript">
        $('#password, #confirm_password').on('keyup', function () {
          if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
          } else 
            $('#message').html('Not Matching').css('color', 'red');
        });
    </script>
  

@endpush