@extends('admin.layout.template_dashboard')

@section('content')

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4 mb-3 border-bottom">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <h3 class="page-title">Vehicle Chart</h3>
      </div>
    </div>
    <!-- End Page Header -->

        
    <!-- Chart -->
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Vehicle Chart</h6>
          </div>
          <div class="card-body pt-0">

              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>


      <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Income Chart</h6>
          </div>
          <div class="card-body pt-0">
              <div id="container_line" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
              
          </div>
        </div>
      </div>


    </div>


    <!-- End Chart -->

    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      
      <div class="col-lg-7 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">User Group Chart</h6>
          </div>
          <div class="card-body pt-0">

              <canvas id="myChart" style="min-width: 310px; height: 400px; margin: 0 auto"></canvas>

          </div>
        </div>
      </div>

      <!-- End Users Stats -->
      <!-- Users By Device Stats -->
      <div class="col-lg-5 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">Member Activity</h6>
          </div>
          <div class="card-body d-flex py-0 table-responsive">
            <table class="table table-striped table-bordered table-responsive" id="example" style="font-size: 11px !important">
                  <thead>
                    <tr>
                      <th>Member</th>
                      <th>Activity</th>
                      <th>Date Activity</th>
                      <th>Case</th>
                      <th>St Verify</th>
                      <th>History</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                    
                  </tbody>
                </table>
          </div>
          <div class="card-footer border-top">
            <div class="row">
              
              <div class="col">
                
              </div>
              <div class="col text-right view-report">
                <a href="#">Full report &rarr;</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Users By Device Stats -->
      
    </div>



  </div>


@endsection


@push('js')

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');
    });
  </script>


   <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

  <script type="text/javascript">

    $(function () { 
        var data_vehicle_complete = <?php echo $list_vehicle_complete; ?>;
        var data_vehicle_reject = <?php echo $list_vehicle_reject; ?>;
        var data_not_api = <?php echo $list_not_api; ?>;


        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Vehicle'
            },
            subtitle: {
                text: (new Date()).getFullYear()
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Vehicle (vehicle)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} app</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: 'Total Vehicle Complete',
                data: data_vehicle_complete
                

            }, {
                name: 'Total Vehicle Reject',
                data: data_vehicle_reject

            

            }, {
                name: 'Total By Manual',
                data: data_not_api

            }]
        });

        });
</script>



@endpush