<!DOCTYPE html>
<html lang="en">
<head>
    <title>Autocheck</title>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<base href="/autocheck/public/">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/vendor/animate/animate.css')}}">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assetlogin/css/main.css')}}">
<!--===============================================================================================-->

 <!-- Favicon-->
    <link rel="shortcut icon" href="/images/fav.ico">

    <style type="text/css">
        .center {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 50%;
        }
    </style>
</head>
<body>
    

        @include('sweetalert::alert')

    <div class="limiter">
        

        <div class="container-login100">
            
            <div class="wrap-login100">
                <a href="{{url('/')}}">
                <img src="{{asset('images/logoautocheck.png')}}" width="60%" height="60%" style=" margin:0 auto;" class="center">
                </a>

                <form class="login100-form validate-form" method="POST" action="{{ url('login') }}" aria-label="{{ __('Login') }}">

                    @csrf

                    <span class="login100-form-title p-b-26" style="color: #4db3a5; margin-bottom: 50px !important">
                        Log in 
                    </span>
                    

                    <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                        <input class="input100{{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" id="email" name="email" value="{{ old('email') }}" required autofocus>
                        <span class="focus-input100" data-placeholder="Email" style="color: #4db3a5;"></span>


                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <input  id="password" class="input100{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required>
                        <span class="focus-input100" data-placeholder="Password" style="color: #4db3a5;"></span>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" style="background-color: #4db3a5; "> Login
                            </button>
                        </div>
                    </div>

                    <div class="text-center p-t-30">
                        
                        <button type=""></button>
                        <a class="txt2" href="{{url('/password/reset')}}" >
                            Forgot my password
                        </a>
                    </div>

                     
                    <div class="text-center">
                        <span class="txt1">
                           Don’t have an account? 
                        </span>

                        <button type=""></button>
                        
                        <a class="txt2" href="{{url('/register-user')}}" >
                            Sign Up
                        </a>
                        
                    </div>

                </form>
            </div>
        </div>
    </div>
    

    <div id="dropDownSelect1"></div>
    
<!--===============================================================================================-->
    <script src="{{asset('assetlogin/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assetlogin/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assetlogin/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('assetlogin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assetlogin/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assetlogin/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('assetlogin/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assetlogin/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('assetlogin/js/main.js')}}"></script>

</body>
</html>
