@extends('web.layout2.template_local')

@section('content')


		<div class="inner-head overlap">
		    <div style="background: url(web2/img/parallax1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div>
		    <div class="container">
		        <div class="inner-content">
		            <span><i class="ti ti-home"></i></span>
		            <h2>Register </h2>
		            <ul>
		                <li><a href="#" title="">HOME</a></li>
		                <li><a href="#" title="">Register </a></li>
		            </ul>
		        </div>
		    </div>
		</div>

		<section class="block">
            <div class="container agnet-prop">
                <div class="row">                    
                    

                    <div class="col-md-12 column">
                        <div class="heading4">
                            <h2>Register</h2> 
                        </div>

                        <div class="submit-content">
                            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" >
                                    @csrf

                                <div class="control-group">
                                    <div class="group-title"></div>
                                    <div class="group-container row">                                       
                                        <div class="col-md-12">
                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group s-profile-title">
                                                        <label for="title">Name&nbsp;*</label>
                                                        <input id="title" class="form-control" value="" name="name" required="" type="text">
                                                    </div>
                                                    <div class="form-group s-profile-position">
                                                        <label for="position">Email&nbsp;*</label>
                                                        <input id="position" class="form-control" value="" name="email" type="text" required="">
                                                    </div>
                                                    <div class="form-group s-profile-email">
                                                        <label for="email">Address</label>
                                                        <input id="email" class="form-control" value="" name="address"  type="text">
                                                    </div>

                                                </div>

                                                <div class="col-md-6">


                                                    <div class="form-group s-profile-phone">
                                                        <label for="phone">Phone&nbsp;*</label>
                                                        <input id="phone" class="form-control" value="" name="phone" required type="text" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12">
                                                    </div>
                                                    <div class="form-group s-profile-mobile">
                                                        <label for="mobile">Fax</label>
                                                        <input id="mobile" class="form-control" value="" name="fax" type="text" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12">
                                                    </div>
                                                    <div class="form-group s-profile-skype">
                                                        <label for="skype">Company Name</label>
                                                        <input id="skype" class="form-control" value="" name="company_name" type="text">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <label for="desc">Company Description</label>
                                            <textarea id="desc" class="form-control" name="company_description" rows="5" style="margin-bottom: 50px !important"></textarea>
                                        </div>

                                        
                                        <div class="col-md-6">
                                        	<div class="form-group s-profile-skype">
                                                <label for="skype">Password&nbsp;*</label>
                                                <input id="password" class="form-control" value="" name="password" type="password" required="" onkeyup='check();'>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        	<div class="form-group s-profile-skype">
                                                <label for="skype">Password Confirm&nbsp;*</label>
                                                <input id="confirm_password" class="form-control" value="" name="password_confirmation" type="password" required onkeyup='check();' required="">
                                            </div>
                                        </div>

                                        <div class="col-md-12" style="margin-top: 30px !important; margin-bottom: 30px !important">
                                        	<span id='message'></span>
                                    	</div>

                                        <div class="col-md-12">
                                        	<div class="form-group s-profile-skype">
	                                        	<label for="skype">Status*</label>
	                                        	<br>
	                                        	<input type="radio" name="status_buyer" value="US" checked="" required> User
	                                        	<br>
	                                        	<p>Please, choose this option, if you only need a single report to find out the background of the car you own (or intend to buy).</p>
												<input type="radio" name="status_buyer" value="NA" required> Car Dealer<br>
												<p>Please, choose this option, if you are a car dealer (exporter/importer/etc.) and are looking for an efficient tool to attract new customers and gain their trust & loyalty by getting listed on Autocheck website and offering them FREE reports.</p>
											</div>
										</div>

                                        <div class="col-md-12">
                                            <div class="submit">
                                                <input class="btn flat-btn" id="profile_submit" value="Register" type="submit">
                                            </div>
                                        </div>

                                    </div>
                                   
                                </div>


                            </form>
                            
                        </div>

                    </div>
                </div>
            </div> 
        </section>

@endsection


@push('js')
    
    <script type="text/javascript">
        $('#password, #confirm_password').on('keyup', function () {
          if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
          } else 
            $('#message').html('Not Matching').css('color', 'red');
        });
    </script>

@endpush
