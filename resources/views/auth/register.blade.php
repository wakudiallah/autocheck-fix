@extends('web.layout.template')

@section('content')


<!--Top_content-->
    <section id="top_content" class="top_cont_outer">
        <div class="top_cont_inner">
            <div class="container">
                <div class="top_content">
                    <div class="row">
                        <div class="col-lg-6 col-sm-8">
                            <div class="top_left_cont flipInY wow animated">
                                <h3>Autocheck XX</h3>
                                <h2>Register SS</h2>
                                <!-- <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}"> -->

                                <form method="POST" action="{{ url('register-buyer') }}" aria-label="{{ __('Register') }}" >
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} input-text" name="name" value="{{ old('name') }}" required autofocus placeholder="Name" style="color: black" @if (Session::has('name'))  value="{{ Session::get('name') }}" @endif>


                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-8">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} input-text" name="email" value="{{ old('email') }}" required placeholder="Email" style="color: black" @if (Session::has('email'))  value="{{ Session::get('email') }}" @endif>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="" class="col-md-4 col-form-label text-md-right">Phone</label>

                                        <div class="col-md-8">
                                            <input id="phone" type="text" class="form-control input-text" name="phone" value="" required placeholder="Phone" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12" style="color: black" @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>

                                        </div>
                                    </div>

                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-8">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-text" name="password" required placeholder="Password" style="color: black" onkeyup='check();' minlength="8">

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-8">
                                            <input id="confirm_password" type="password" class="form-control input-text" name="password_confirmation" required placeholder="Confirm Password" style="color: black" onkeyup='check();' min="8">
                                            <span id='message'></span>
                                        </div>
                                        
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            
                                            <button type="submit" class="btn input-btn"> 
                                                {{ __('Register') }}

                                            </button>


                                        </div>
                                    </div>
                                </form>
                                
                                <!-- 
                                <a href="#service" class="learn_more2">{{ __('Register') }}</a> </div> -->
                        </div>
                        <div class="col-lg-6 col-sm-4"> </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Top_content-->


    <!--Service-->
    <section id="service">
        <div class="container">
            <h2>Services</h2>
            <div class="service_area">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="service_block">
                            <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="fa-flash"></i></span> </div>
                            <h3 class="animated fadeInUp wow">Quick TurnAround</h3>
                            <p class="animated fadeInDown wow">Proin iaculis purus consequat sem cure digni. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service_block">
                            <div class="service_icon icon2  delay-03s animated wow zoomIn"> <span><i class="fa-comments"></i></span> </div>
                            <h3 class="animated fadeInUp wow">Friendly Support</h3>
                            <p class="animated fadeInDown wow">Proin iaculis purus consequat sem cure digni. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service_block">
                            <div class="service_icon icon3  delay-03s animated wow zoomIn"> <span><i class="fa-shield"></i></span> </div>
                            <h3 class="animated fadeInUp wow">top Security</h3>
                            <p class="animated fadeInDown wow">Proin iaculis purus consequat sem cure digni. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Service-->


    <section class="twitter-feed">
        <!--twitter-feed-->
        <div class="container  animated fadeInDown delay-07s wow">
            <div class="twitter_bird"><span><i class="fa-twitter"></i></span></div>
            <p> When you're the underdog, your only option is to make #waves if you want to succeed. How much <br> and how often should you be drinking coffee?</p>
            <span>About 28 mins ago</span> </div>
    </section>
    <!--twitter-feed-end-->
    <footer class="footer_section" id="contact">
        <div class="container">
            <section class="main-section contact" id="contact">
                <div class="contact_section">
                    <h2>Contact Us</h2>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="contact_block">
                                <div class="contact_block_icon rollIn animated wow"><span><i class="fa-home"></i></span></div>
                                <span> 308 Negra Arroyo Lane, <br>
              Albuquerque, NM, 87104 </span> </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="contact_block">
                                <div class="contact_block_icon icon2 rollIn animated wow"><span><i class="fa-phone"></i></span></div>
                                <span> 1-800-BOO-YAHH </span> </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="contact_block">
                                <div class="contact_block_icon icon3 rollIn animated wow"><span><i class="fa-pencil"></i></span></div>
                                <span> <a href="mailto:hello@butterfly.com"> hello@butterfly.com</a> </span> </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 wow fadeInLeft">
                        <div class="contact-info-box address clearfix">
                            <h3>Don’t be shy. Say hello!</h3>
                            <p>Accusantium quam, aliquam ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper. Accusantium quam, aliquam ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper.</p>
                            <p>Accusantium quam, aliquam ultricies eget tempor id, aliquam eget nibh et. Maecen aliquampor id.</p>
                        </div>
                        <ul class="social-link">
                            <li class="twitter animated bounceIn wow delay-02s"><a href="javascript:void(0)"><i class="fa-twitter"></i></a></li>
                            <li class="facebook animated bounceIn wow delay-03s"><a href="javascript:void(0)"><i class="fa-facebook"></i></a></li>
                            <li class="pinterest animated bounceIn wow delay-04s"><a href="javascript:void(0)"><i class="fa-pinterest"></i></a></li>
                            <li class="gplus animated bounceIn wow delay-05s"><a href="javascript:void(0)"><i class="fa-google-plus"></i></a></li>
                            <li class="dribbble animated bounceIn wow delay-06s"><a href="javascript:void(0)"><i class="fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 wow fadeInUp delay-06s">
                        <div class="form">
                            <div id="sendmessage">Your message has been sent. Thank you!</div>
                            <div id="errormessage"></div>
                            <form action="" method="post" role="form" class="contactForm">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control input-text" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input-text" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-text" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                    <div class="validation"></div>
                                </div>

                                <button type="submit" class="btn input-btn">SEND MESSAGE</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="container">
            <div class="footer_bottom">
                <span>© Butterfly Theme</span>
                <div class="credits">
                    <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Butterfly
          -->
                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>
            </div>
        </div>
    </footer>


@endsection



@push('js')
    
    <script type="text/javascript">
        $('#password, #confirm_password').on('keyup', function () {
          if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
          } else 
            $('#message').html('Not Matching').css('color', 'red');
        });
    </script>

@endpush
