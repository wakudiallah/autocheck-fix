@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

      <link rel="stylesheet" type="text/css"    href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Batch Sent</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                 
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      

                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th width="10%" align="center">No</th>
                                  <th>Batch</th>
                                  <th>Sender</th>
                                  <th>Type Report</th>
                                  <th width="20%">Detail</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i = 1; ?>
                            @foreach($data_batch as $data)
                              <tr>
                                  <td align="center">{{$i++}}</td>
                                  <td>{{$data->unique_sent}}</td>
                                  <td>@if(!empty($data->verify_sent_by->name))
                                    {{$data->verify_sent_by->name}}
                                    @endif
                                  </td>
                                  <td></td>
                                  <td align="center">
                                    <a href="#" class="mb-2 btn btn-primary mr-2 btn-sm" data-toggle="modal" data-target="#exampleModal{{$data->id}}">Detail</a>
                                  </td>
                                  
                              </tr>

                            @endforeach 
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>

          
          <!-- Modal -->
          @foreach($data_batch as $data)
            
            <div class="modal fade" id="exampleModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail {{$data->unique_sent}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <?php $detail_data_batch =  DB::table('vehicle_checkings')->where('unique_sent', $data->unique_sent)->get()?>

                    <div class="table-responsive">
                    <table id="example{{$data->id}}" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th>Vehicle</th>
                                  <th>request From</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i = 1; ?>
                            @foreach($detail_data_batch as $datax)
                              <tr>
                                  <td>{{$i++}}</td>
                                  <td>{{$datax->vehicle}}</td>
                                  <td>{{$datax->vehicle}}</td>
                                  
                              </tr>

                            @endforeach 
                          </tbody>
                          
                    </table>
                    </div>
                    
                  </div>
                  <div class="modal-footer">
                    
                      
                    
                  </div>
                </div>
              </div>
            </div>

          @endforeach

          <!-- Modal -->

@endsection


@push('js')

 
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  @foreach($table1 as $table1)
  <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example{{$table1->id}}').DataTable();
            } );

  </script>
  @endforeach

@endpush