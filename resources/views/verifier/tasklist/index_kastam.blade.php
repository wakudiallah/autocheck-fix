@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Sync Tasklist ( {{config('autocheck.extra_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <!-- <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/sync_all')}}" >
                      
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->


                      <!-- <input type="submit" value="Generate & Send Vehicle" class="mb-2 btn btn-primary mr-2 btn-lg" style="float: right;"> -->
			
		          <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered"  width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Verification Serial Number</th>
                                  <th>Date Request</th>
                                  @if($me == "VER")
                                  <th>Sync</th>
                                  @endif
                                  <th>Status</th>
                                  <th>
                                      @if($me == "VER")
                                      API Status
                                      @else
                                      Status Verify
                                      @endif
                                  </th>
                                  <th width="20%" align="center">Action</th>
                                  
                                  
                                    @if($me == "VER" OR $me == "KA")
                                    <!-- <th>Full Report</th>-->
                                    <th>Req From</th>
                                    @endif
                                  

                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  

                                  <td>
                                    @if(!empty($data->status_vehicle->ver_sn))
                                    {{$data->status_vehicle->ver_sn}}
                                    @endif
                                    
                                  </td>
                                  <td>{{$data->created_at}}</td>
                                  @if($me == "VER")
                                  <td>
                                    
                                    @if(!empty($data->status_carvx->report_id))
                                    @if($data->status_carvx->is_ready != 6 AND $data->status_carvx->is_ready != 5) <!-- 6 cancel or 5 complete -->
                                    

                                    <!-- if KA sync to autocheckmalaysia3 -->
                                    @if($data->group_by == "NA")
                                    <a href="{{url('sync_autocheck3/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"> 
                                        Sync group
                                      </button>
                                    </a>
                                    @else
                                    

                                    <a href="{{url('sync/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        Sync 
                                      </button>
                                    </a>

                                    @endif
                                    <!-- end sync -->


                                    @endif
                                    @endif
                                  </td>
                                    @endif

                                  <!-- Status -->
                                  <td align="center">
                                    
                                      @if($data->status == '40')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>
                                      @elseif($data->status == '10' OR $data->status == '20')
                                      <i  class="card-post__category badge badge-pill badge-warning">New</i>
                                      @elseif($data->status == '25')
                                      <i  class="card-post__category badge badge-pill badge-warning">In Progress</i>
                                      @elseif($data->status == '30')
                                      <i  class="card-post__category badge badge-pill badge-danger">Reject</i>
                                      @endif
                                    
                                      
                                      @if($me == 'VER')
                                      @if($data->status == '10')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                      @endif
                                  </td>
                                  <!-- End of Status -->

                                  <!-- API Status -->
                                  <td>
                                    @if(!empty($data->status_carvx->report_id))

                                      @if($data->status_carvx->is_ready == 3)
                                        <i  class="card-post__category badge badge-pill badge-info">New</i>
                                      @elseif($data->status_carvx->is_ready == 4)
                                        <i  class="card-post__category badge badge-pill badge-success"> In Progress</i>
                                      @elseif($data->status_carvx->is_ready == 5)
                                        <i  class="card-post__category badge badge-pill badge-primary">Complete</i>
                                      @elseif($data->status_carvx->is_ready == 6)
                                        <i  class="card-post__category badge badge-pill badge-danger">Cancel</i>
                                      @endif
                                    @endif
                                  </td>

                                  <!-- End of API Status -->


                                  <!-- Action -->
                                  <td width="20%" align="center">

                                    

                                      @if($data->status == '40' )
                                      <a href="{{url('report-autocheck/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>

                                    
                                     <a href="{{url('report-autocheck/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                        <i class="material-icons">print</i>
                                      </button>
                                    </a>
                                    @endif
                                  
                                  @if($data->group_by != "KA")
                                  <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1{{$data->id}}">
                                      <i class="material-icons">details</i>
                                    </button>
                                  @else
                                    @if($data->searching_by == "NOT")
                                    <!-- upload file -->
                                    <button type="button" class="mb-2 btn btn-sm btn-success mr-1" data-toggle="modal" data-target=".bd-example-modal-sm3{{$data->id}}">
                                        <i class="material-icons">cloud_upload</i>
                                      </button>
                                    <!-- end upload file -->
                                    @endif
                                  @endif



                                    

                                  @if($me == "VER")

                                    @if(($data->searching_by == 'NOT') AND ($data->is_sent == '1') AND ($data->group_by != 'KA'))
                                    <!--   For verify data / match or not (kadealer)-->
                                    
                                    <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  data-toggle="modal" data-target=".bd-example-modal-manual{{$data->id_vehicle}}">
                                      <i class="material-icons">details</i>
                                    </button>

                                    <!-- </a> -->

                                    <!-- ============ End Verify ========== -->
                                    @endif
                                    
                                  @endif

                                  </td>

                                  
                                  <!-- Full Report -->
                                  <!-- @if($me == "VER" OR $me == "KA")
                                  <td align="center"> 
                                    @if($data->status == "40")
                                    <a href="{{url('report-carvx/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>
                                      @endif
                                  </td>End Full Report -->

                                  <td>

                                    @if(!empty($data->request_from->role->desc))
                                    <p style="color: red">{{$data->request_from->role->desc}}</p>
                                    @endif
                                  </td>
                                  @endif 
                                  


                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>

                                    <input type="hidden"  name="id[]" value="{{$data->id_vehicle}}" class="friends" />

                                    <input type="hidden"  name="ci[]" value="{{$data->id}}">
                                    
                                    <input type="hidden"  name="ktp[]" value="{{$data->id_vehicle}}">

                                    <input type="hidden"  name="ids[]" value="{{$data->vehicle}}">


                                  </td>
                                  <!-- End History Search -->


                              </tr>
                            @endforeach
                              
                          </tbody>
                          
		      </table>
		</div>

                      </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-manual{{$data->id_vehicle}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Match  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" class="register-form" id="register-form" action="{{url('verify-vehicle-verifier-manual/'.$data->id_vehicle)}}">
                        {{ csrf_field() }}

                        @if(!empty($data->vehicle_api->api_from))
                          <input type="hidden" name="api_from" value="{{$data->vehicle_api->api_from}}" class="">
                        @endif

                        <input type="hidden" name="vehicle" value="{{$data->vehicle}}" class="">
                        <input type="hidden" name="created_by" value="{{$data->created_by}}" class="">
                      

                      <div class="form-group col-md-12">
                        <h2><b>{{$data->vehicle}}</b></h2>
                      </div>

                      <hr>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Supplier</label>
                        @if(!empty($data->supplier_id))
                        <h3>{{$data->supplier->supplier}}</h3>
                        @endif
                      </div>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Type</label>
                        @if(!empty($data->type->type_vehicle))
                        <h3>{{$data->type->type_vehicle}}</h3>
                        @endif
                      </div>

                      <hr>

                      <div class="form-row">                        
                        <label for="feDescription">Make / Brand </label>
                      </div>


                      <div class="form-row">

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->brand_id)){{$data->brand->brand}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              
                            </div>
                          </div>
                          <div class="form-group col-md-6">
                            
                            <select name="brand_verify" class="form-control is-valid" id="brand_value_edit{{$data->id}}" required>
                              <option value="" selected disabled hidden>- Please Select -</option>
                              @foreach($brand as $databrand)
                              <option value="{{$databrand->id}}">{{$databrand->brand}}</option>
                              @endforeach
                          </select>
                            <div class="valid-feedback">
                              Verifier
                            </div>
                          </div>
                          
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Model / Variance </label>
                      </div>

                      <div class="form-row">

                          
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->brand_id)){{$data->model_brand->model}}@endif" disabled="">
                            <div class="valid-feedback">
                              
                                
                            </div>
                          </div>
                          <div class="form-group col-md-6">
                            <select id="model_value_edit{{$data->id}}" name="model_verify" class="form-control is-valid">
                              <option value="" selected disabled hidden>- Please Select -</option>
                              
                              @foreach($model as $datamodel)
                              
                               <option value="{{ $datamodel->id }}|{{$datamodel->brand_id}}">{{$datamodel->model}}</option>

                              @endforeach
                            </select> 

                            <div class="valid-feedback">
                              verifier
                            </div>
                          </div>
                          
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Engine Number / Engine Model </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->engine_number)){{$data->engine_number}}@endif" disabled>
                            <div class="valid-feedback">
                              
                            </div>
                          </div>
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="@if(!empty($data->engine_number)){{$data->engine_number}}@endif" required name="engine_number_verify"> 
                            <div class="valid-feedback">
                              Verifier
                            </div>
                          </div>
                          
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Cubic Capacity (CC) </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->cc)){{$data->cc}}@endif" disabled>
                            <div class="valid-feedback">
                              
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="@if(!empty($data->cc)){{$data->cc}}@endif" required name="cc_verify"> 
                            <div class="valid-feedback">
                              Verifier
                            </div>
                          </div>
                          
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Fuel Type </label>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->fuel_id)){{$data->fuel->fuel}}
                            @endif" disabled>
                            <div class="valid-feedback">
                             

                            </div>
                          </div>
                          
                          <div class="form-group col-md-6">
                              <select id="feInputState" name="fuel_verify" class="form-control is-valid">
                              @foreach($fuel as $datafuel)
                              
                               <option value="{{ $datafuel->id }}">{{$datafuel->fuel}}</option>
                              @endforeach
                            </select>

                            <div class="valid-feedback">
                              Verifier
                            </div>
                          </div>
                          
                         
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Year of Manufacture </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="" disabled>
                            <div class="valid-feedback">
                              
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            <input id="bday-month" name="year_verify" class="form-control is-valid" type="month" name="year_verify" value="" required=""> 
                            <div class="valid-feedback">
                              Verifier
                            </div>
                          </div>
                          
                          
                      </div>

                      <div class="form-row">                        
                        <label for="feDescription">First Registration Date</label>
                      </div>

                      <div class="form-row">

                        <?php $tarikh =  date('m/d/Y ', strtotime($data->vehicle_registered_date)); ?>

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_registered_date)){{$tarikh}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              
                            </div>
                          </div>

                          

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid"  value="@if(!empty($data->vehicle_registered_date)){{$tarikh}}
                            @endif"  name="registration_date_verify" id="datepicker{{$data->id}}"> 
                            <div class="valid-feedback">Verifier
                            </div>
                          </div>
                          
                          
                      </div>

                      @if(empty($data->vehicle_api->api_from))
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Report From</label>
                          <select name="api_from" class="form-control" >
                            <option value="RYO">RYOCHI</option>
                            <option value="HPI">HPI</option>
                            <option value="OTHER">Others</option>
                          </select>
                        </div>
                      </div>
                      @endif

                      

                 
                   
                </div>
                <div class="modal-footer">
                  
                  <button type="submit" class="btn btn-accent">Verify</button>

                </div>
              </form>

              </div>
            </div>
          </div>
          @endforeach
        
          
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm1{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="">
                        {{ csrf_field() }}
                      


                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}}</b></h2>
                        </div>

                        <hr>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}}</h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          @if(!empty($data->brand->brand))
                          <h3>{{$data->brand->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($data->model_brand->model))
                          <h3>{{$data->model_brand->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$data->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$data->vehicle_registered_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($data->fuel->fuel))
                            <h3>{{$data->fuel->fuel}}</h3>
                            @endif
                          </div>

                      </div>
                      


                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach


          <!-- Modal Upload -->

          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm3{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Upload {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('upload-report-vehicle/'.$data->id_vehicle) }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                      


                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <input type="file" name="file" class="form-control" required="">

                          <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        </div>

                        <hr>

                      </div>

                      <div class="form-row" style="float: right;">
                        <input type="submit" name="submit" class="btn btn-sm btn-primary">
                      </div>
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach

          <!-- End Modal upload -->
            

@endsection


@push('js')

  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    

    @foreach($data3 as $data2)

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker2{{$data2->id}}" ).datepicker();
     });
    </script>


    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>
    @endforeach

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script> -->

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
