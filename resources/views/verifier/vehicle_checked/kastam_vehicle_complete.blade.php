@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Checked ( {{config("autocheck.full_report")}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
      
                      <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Verification Serial Number</th>
                                  <th>Date Request</th>
                                  
                                  <th >Status</th>
                                  <th>
                                      @if($me == "VER")
                                      API Status
                                      @else
                                      Status Verify
                                      @endif
                                  </th>
                                  <th>Date Verify</th>
                                  <!-- <th width="10%" align="center">Action</th> -->
                                  
                                  
                                    @if($me == "VER" OR $me == "KA")
                                    <th width="10%" align="center">Full Report</th>
                                    <th width="10%" align="center">Req From</th>
                                    @endif
                                  
                                  
                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)

                            
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  

                                  <td>
                                    @if(!empty($data->status_vehicle->ver_sn))
                                    {{$data->status_vehicle->ver_sn}}
                                    @endif
                                    
                                  </td>
                                  <td>{{$data->created_at}}</td>

                                  
                                  <!-- Status -->
                                  <td align="center">
                                    
                                      @if($data->status == '40')
                                      <i href="#" class="card-post__category badge badge-pill badge-success">Complete</i>

                                      

                                      @elseif(($data->status == '40') AND ($data->searching_by == 'API') AND ($data->group_by == "KA"))
                                      <i href="#" class="card-post__category badge badge-pill badge-success">Complete</i>

                                      @elseif($data->status == '10' OR $data->status == '20')
                                      <i  class="card-post__category badge badge-pill badge-warning">New</i>
                                      @elseif($data->status == '25' AND $data->status_vehicle->status == 0)
                                      <i  class="card-post__category badge badge-pill badge-primary">In Progress</i>
                                      @elseif($data->status == '30')
                                      <i  class="card-post__category badge badge-pill badge-danger">Reject</i>

                                      <!-- status kastam complete -->
                                      @elseif(($data->status == '40') AND (!empty($data->full_report)))
                                      <i  class="card-post__category badge badge-pill badge-success">Complete</i>
                                      <!-- end status kastam complete -->

                                      @endif


                                    
                                      
                                      @if($me == 'VER')
                                      @if($data->searching_by == 'NOT')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                      @endif
                                  </td>
                                  <!-- End of Status -->

                                  <!-- API Status -->
                                  <td>
                                    @if(!empty($data->status_carvx->report_id))

                                      @if($data->status_carvx->is_ready == 3)
                                        <i  class="card-post__category badge badge-pill badge-info">New</i>
                                      @elseif($data->status_carvx->is_ready == 4)
                                        <i  class="card-post__category badge badge-pill badge-success"> In Progress</i>
                                      @elseif($data->status_carvx->is_ready == 5)
                                        <i  class="card-post__category badge badge-pill badge-success">Complete</i>
                                      @elseif($data->status_carvx->is_ready == 6)
                                        <i  class="card-post__category badge badge-pill badge-danger">Cancel</i>
                                      @endif
                                    @endif
                                  </td>

                                  <!-- End of API Status -->

                                  <td>
                                    {{$data->updated_at}}
                                  </td>
                                  

                                  <!-- Full Report -->

                                
                                  @if($me == "VER" OR $me == "KA")
                                  <td align="center"> 
                                    
                                    @if($data->status == "40" AND $data->group_by == "KA")
                                        
                                        @if($data->searching_by == "API")

                                        <!-- API Data -->
                                        <a href="{{url('report-autocheck-kastam-api/'.$data->id_vehicle)}}" target="_blank">
                                            <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                              <i class="material-icons">save_alt</i>
                                            </button>
                                          </a>
                                        <!-- end API Data -->

                                        @elseif($data->searching_by == "NOT")

                                          @if(!empty($data->status_carvx->id_vehicle))
                                        <!-- Manual Data -->
                                        <a href="{{url('report-autocheck-kastam-api-mn/'.$data->id_vehicle)}}" target="_blank">
                                          <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                            <i class="material-icons">save_alt</i>API MN
                                          </button>
                                        </a>

                                        <!-- End Manual Data -->

                                          @else

                                            <a href="{{url('report-autocheck-kastam-mn/'.$data->id_vehicle)}}" target="_blank">
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                <i class="material-icons">save_alt</i>Data
                                              </button>
                                            </a>

                                          @endif
                                        @endif

                                    @endif


                                    <a href="{{url('HalfReport/detail/'.$data->id_vehicle)}}" target="_blank">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"> 
                                        <i class="material-icons">details</i>
                                      </button>
                                    </a>

                                  </td>

                                  <td>

                                    @if(!empty($data->request_from->role->desc))
                                    <p style="color: red">{{$data->request_from->role->desc}}</p>
                                    @endif
                                    @if(!empty($data->request_by->name))
                                    {{$data->request_by->name}}
                                    @endif
                                  </td>
                                  @endif
                                  <!-- End Full Report -->


                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>

                                  <!-- End History Search -->
                              </tr>

                            @endforeach
                              
                          </tbody>
                          
                      </table>


                      </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
        <!-- Modal update by API -->
        
          @foreach($data2 as $data)
          @if($data->group_by != "KA")
          <div class="modal fade bd-example-modal-sm2{{$data->id_vehicle}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Match  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <form method="POST" class="register-form" id="register-form" action="{{url('verify-vehicle-verifier-api/'.$data->id_vehicle)}}">
                        {{ csrf_field() }}

                        @if(!empty($data->vehicle_api->api_from))
                          <input type="hidden" name="api_from" value="{{$data->vehicle_api->api_from}}" class="">
                        @endif

                        <input type="hidden" name="vehicle" value="{{$data->vehicle}}" class="">
                        <input type="hidden" name="created_by" value="{{$data->created_by}}" class="">
                      

                      <div class="form-group col-md-12">
                        <h2><b>{{$data->vehicle}}</b></h2>
                      </div>

                      <hr>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Supplier</label>
                       
                      </div>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Type</label>
                        @if(!empty($data->type->type_vehicle))
                        <h3>{{$data->type->type_vehicle}}</h3>
                        @endif
                      </div>

                      <hr>

                      <div class="form-row">                        
                        <label for="feDescription">Make / Brand </label>

                      </div>


                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->brand)){{$data->vehicle_api->brand}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>
                          <div class="form-group col-md-5">
                            
                            <select name="brand_verify" class="form-control is-valid" id="brand_value_edit{{$data->id}}" required>
                              <option value="" selected disabled hidden>- Please Select -</option>
                              @foreach($brand as $databrand)
                              <option value="{{$databrand->id}}" {{ $data->brand_id == $databrand->id ? 'selected' : '' }} >{{$databrand->brand}}</option>
                              @endforeach

                          </select>
                            <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if($data->status_vehicle->brand == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Model / Variance </label>
                      </div>

                      <div class="form-row">
                          
                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->model)){{$data->vehicle_api->model}}@endif" disabled="">
                            
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <select id="model_value_edit{{$data->id}}" name="model_verify" class="form-control is-valid"  data-show-subtext="true" data-live-search="true">
                              <option value="" selected disabled hidden>- Please Select -</option>
                              
                              @foreach($model as $datamodel)
                              
                               <option value="{{ $datamodel->id }}|{{$datamodel->brand_id}}" {{ $data->model_id == $datamodel->id ? 'selected' : '' }}>{{$datamodel->model}}</option>

                              @endforeach
                            </select> 

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">

                              @if($data->status_vehicle->model == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Engine Number / Engine Model </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->engine_number)){{$data->vehicle_api->engine_number}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->engine_number}}" required name="engine_number_verify"> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Cubic Capacity (CC) </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->cc)){{$data->vehicle_api->cc}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->cc}}" required name="cc_verify"> 
                            <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if($data->status_vehicle->cc == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Fuel Type </label>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->fuel_type)){{$data->vehicle_api->fuel_type}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif 
                            </div>
                          </div>
                          
                          <div class="form-group col-md-5">
                              <select id="feInputState" name="fuel_verify" class="form-control is-valid">
                              @foreach($fuel as $datafuel)
                              
                               <option value="{{ $datafuel->id }}" {{ $data->fuel_id == $datafuel->id ? 'selected' : '' }}>{{$datafuel->fuel}}</option>

                              @endforeach
                            </select>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Year of Manufacture </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->year_manufacture)){{$data->vehicle_api->year_manufacture}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input id="bday-month" name="year_verify" class="form-control is-valid" type="month" name="year_verify" value="" disabled> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>

                      
                      <div class="form-row">                        
                        <label for="feDescription">First Registration Date</label>
                      </div>

                      <div class="form-row">

                        <?php $tarikh_from_kadealer =  date('m/d/Y ', strtotime($data->vehicle_registered_date)); ?>

                        @if(!empty($data->vehicle_api->registation_date))

                        <?php $tarikh_from_api =  date('m/d/Y ', strtotime($data->vehicle_api->registation_date)); ?>

                        @endif

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->registation_date)){{$tarikh_from_api}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>


                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid"  value="{{$tarikh_from_kadealer}}" name="registration_date_verify" id="datepicker{{$data->id}}"> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if($data->status_vehicle->registation_date == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                      </div>

                      @if(empty($data->vehicle_api->api_from)) <!-- if not from carvx -->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Report From</label>
                          <select name="api_from" class="form-control" >
                            <option value="RYO">RYOCHI</option>
                            <option value="HPI">HPI</option>
                            <option value="OTHER">Others</option>
                          </select>
                        </div>
                      </div>
                      @endif <!-- end if not from carvx -->

                      @if($data->status_vehicle->status == "0")<!-- if not match -->
                      
                      <!-- Setakat ini remark belum dulu 
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Remark</label>
                          <textarea name="remark" class="form-control is-valid" rows="5" placeholder="Remark"></textarea>
                        </div>
                      </div> -->
                      <!-- end if not match -->
                      

                      <!-- <div class="form-row">
                        <div class="form-group col-md-6"></div>
                        <div class="form-group col-md-6">
                          <div class="radio">
                            <label><input type="radio" name="yes" value="yes" checked>Approve</label>
                          </div>
                          <div class="radio">
                            <label><input type="radio" name="yes" value="no">Reject</label>
                          </div>
                        </div>
                      </div> -->
                      @endif
                   
                   
                </div>
                <div class="modal-footer">
                  
                  <button type="submit" class="btn btn-accent">Verify</button>

                </div>
              </form>

              </div>
            </div>
          </div>
          @endif
          @endforeach
        
          
          
          <!-- ======== Modal Detail === -->
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm1{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                   
                      @if(($data->status == '40') AND ($data->searching_by == 'API' ) AND ($data->group_by != 'KA'))
                      <!-- get from API and complete / KADEALER-->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}}</b></h2>
                        </div>

                        <hr>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                         
                          <h3>{{$data->supplier_id}}</h3>
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          @if(!empty($data->brand->brand))
                          <h3>{{$data->vehicle_api->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($data->model_brand->model))
                          <h3>{{$data->vehicle_api->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->vehicle_api->engine_number}}</h3>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feInputCity">Engine Capacity (cc)</label>
                          <h3>{{$data->vehicle_api->cc}}</h3> 
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feInputCity">Vehicle Registered Date</label>
                          <h3>{{$data->vehicle_api->registation_date}}</h3>
                        </div>

                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($data->fuel->fuel))
                            <h3>{{$data->fuel->fuel}}</h3>
                            @endif
                          </div>

                      </div>
                      <!-- end get from API -->


                      @elseif(($data->searching_by == 'NOT') AND ($data->group_by == 'KA'))
                      <!-- get from Manual / KADEALER -->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}}</b></h2>
                        </div>

                        <hr>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}}</h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          @if(!empty($data->brand->brand))
                          <h3>{{$data->brand->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($data->model_brand->model))
                          <h3>{{$data->model_brand->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$data->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$data->vehicle_registered_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($data->fuel->fuel))
                            <h3>{{$data->fuel->fuel}}</h3>
                            @endif
                          </div>

                      </div>
                      <!-- end get from manual -->

                      @elseif($data->group_by == 'KA')
                        <div class="form-row">
                          <div class="form-group col-md-12">
                            <h2><b>{{$data->vehicle}}</b></h2>
                          </div>

                          <hr>
                        </div>
                      @else

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}}</b></h2>
                        </div>

                        <hr>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}}</h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          @if(!empty($data->brand->brand))
                          <h3>{{$data->brand->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($data->model_brand->model))
                          <h3>{{$data->model_brand->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$data->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$data->vehicle_registered_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($data->fuel->fuel))
                            <h3>{{$data->fuel->fuel}}</h3>
                            @endif
                          </div>

                      </div>

                      @endif
                      


                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>


              </div>
            </div>
          </div>
          @endforeach
          <!-- ======= End Modal Detail ======= -->



          


          <!-- Modal Upload -->

          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm3{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Upload {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('upload-report-vehicle/'.$data->id_vehicle) }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                      


                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <input type="file" name="file" class="form-control">

                          <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        </div>

                        <hr>

                      </div>

                      <div class="form-row">
                        <input type="submit" name="submit" class="btn btn-sm btn-primary">
                      </div>
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach

          <!-- End Modal upload -->






          <!-- Modal Eye -->
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm3{{$data->id_vehicle}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Match  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                    <?php 
                    
                      $summary = DB::table('r_summaries')->where('id_vehicle', $data->id_vehicle)->get();
                      $usagehistory = DB::table('r_usage_histories')->where('id_vehicle', $data->id_vehicle)->first();
                      $vehicleassessment = DB::table('r_vehicle_assessments')->where('id_vehicle', $data->id_vehicle)->first();
                      $vehicledetail = DB::table('r_vehicle_details')->where('id_vehicle', $data->id_vehicle)->first();
                      $vehiclespesification = DB::table('r_vehicle_specifications')->where('id_vehicle', $data->id_vehicle)->first();
                      $auctionimage = DB::table('r_auction_images')->where('id_vehicle', $data->id_vehicle)->get();
                      $detailhistory = DB::table('r_detail_histories')->where('id_vehicle', $data->id_vehicle)->get();
                      $auctionhistory = DB::table('r_auction_histories')->where('id_vehicle', $data->id_vehicle)->get();
                      $odometerhistory = DB::table('r_odometer_histories')->where('id_vehicle', $data->id_vehicle)->get();
                      $vehicle = DB::table('vehicle_checkings')->where('id_vehicle', $data->id_vehicle)->first();

                    ?>

                   



                   
                </div>
                <div class="modal-footer">
                  
                  <button type="submit" class="btn btn-accent">Verify</button>

                </div>
              </form>

              </div>
            </div>
          </div>
          @endforeach
          <!-- End Modal eye -->
            

@endsection


@push('js')

  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    


    @foreach($data3 as $data2)
    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker2{{$data2->id}}" ).datepicker();
     });
    </script>
    @endforeach


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
