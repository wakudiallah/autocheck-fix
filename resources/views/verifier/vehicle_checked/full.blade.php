@extends('admin.layout.template')

@section('content')

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Checked ( {{config("autocheck.full_report")}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
      
                      <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Verification Serial Number</th>
                                  <th>Date Request</th>
                                  
                                  <th >Status</th>
                                  <th>
                                      @if($me == "VER")
                                      API Status
                                      @else
                                      Status Verify
                                      @endif
                                  </th>
                                  <th>Date Verify</th>
                                  <th width="10%" align="center">Full Report</th>
                                  <th width="10%" align="center">Req From</th><th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)

                            
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  

                                  <td>
                                    @if(!empty($data->status_vehicle->ver_sn))
                                    {{$data->status_vehicle->ver_sn}}
                                    @endif
                                    
                                  </td>
                                  <td>{{$data->created_at}}</td>

                                  
                                  <!-- Status -->
                                  <td align="center">
                                    
                                      @if($data->status == '40')
                                      <i href="#" class="card-post__category badge badge-pill badge-success">Complete</i>
                                      @endif


                                      @if($data->searching_by == 'NOT')
                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                  
                                  </td>
                                  <!-- End of Status -->

                                  <!-- API Status -->
                                  <td>
                                    @if(!empty($data->status_carvx->report_id))

                                      @if($data->status_carvx->is_ready == 3)
                                        <i  class="card-post__category badge badge-pill badge-info">New</i>
                                      @elseif($data->status_carvx->is_ready == 4)
                                        <i  class="card-post__category badge badge-pill badge-success"> In Progress</i>
                                      @elseif($data->status_carvx->is_ready == 5)
                                        <i  class="card-post__category badge badge-pill badge-success">Complete</i>
                                      @elseif($data->status_carvx->is_ready == 6)
                                        <i  class="card-post__category badge badge-pill badge-danger">Cancel</i>
                                      @endif
                                    @endif
                                  </td>

                                  <!-- End of API Status -->

                                  <td>
                                    {{$data->updated_at}}
                                  </td>
                                  

                                  <!-- Full Report -->
                                 
                                  <td align="center"> 

                                    <a href="{{url('FullReport/Report/'.$data->id_vehicle)}}" target="_blank">
                                            <button type="button" class="mb-2 btn btn-sm btn-success mr-1"> 
                                              <i class="material-icons">save_alt</i>
                                            </button>
                                          </a>
                                    
                                    @if($data->status == "40" AND $data->group_by == "KA")
                                        
                                        @if($data->searching_by == "API")

                                        <!-- API Data -->
                                        <a href="{{url('report-autocheck-kastam-api/'.$data->id_vehicle)}}" target="_blank">
                                            <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                              <i class="material-icons">save_alt</i>
                                            </button>
                                          </a>
                                        <!-- end API Data -->

                                        @elseif($data->searching_by == "NOT")

                                          @if(!empty($data->status_carvx->id_vehicle))
                                        <!-- Manual Data -->
                                        <a href="{{url('report-autocheck-kastam-api-mn/'.$data->id_vehicle)}}" target="_blank">
                                          <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                            <i class="material-icons">save_alt</i>API MN
                                          </button>
                                        </a>

                                        <!-- End Manual Data -->

                                          @else

                                            <a href="{{url('report-autocheck-kastam-mn/'.$data->id_vehicle)}}" target="_blank">
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                <i class="material-icons">save_alt</i>Data
                                              </button>
                                            </a>

                                          @endif
                                        @endif

                                    @endif


                                    <a href="{{url('FullReport/detail/'.$data->id_vehicle)}}" target="_blank">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"> 
                                        <i class="material-icons">details</i>
                                      </button>
                                    </a>

                                  </td>

                                  <td>
                                    @if(!empty($data->request_by->name))
                                       {{$data->request_by->name}} -
                                    @endif

                                    @if(!empty($data->group->group_name))
                                       <b style="color: red">{{$data->group->group_name}}</b>
                                    @endif
                                  </td>
                                  
                                  <!-- End Full Report -->


                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>

                                  <!-- End History Search -->
                              </tr>

                            @endforeach
                              
                          </tbody>
                          
                      </table>


                      </form>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>

      
            

@endsection


@push('js')

  


    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>



    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
