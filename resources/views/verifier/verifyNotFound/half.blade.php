@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">{{trans('system_autocheck.vehile_verify_not_found')}} ( {{config("autocheck.half_report")}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      

                    <div class="table-responsive">
                      <table id="example" class="table table-striped  table-bordered" cellspacing="0" width="100%" >
                          <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>Vehicle</th>
                                    <th>Date Request</th>
                                    <th>Status</th>
                                    <th>Req From</th>
                                    <th>History</th>
                                    <th width="10%" align="center">Action</th>
                                </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)

                            
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  <td>{{$data->created_at}}</td>

                                  <!-- Status -->
                                  <td align="center">
                                      @if($data->searching_by == 'NOT')
                                      <i  class="card-post__category badge badge-pill badge-success">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-success">API</i>
                                      @endif

                                      @if(!empty($data->vehiclechecking_to_status->desc))
                                      <i  class="card-post__category badge badge-pill badge-{{$data->vehiclechecking_to_status->badge}}">{{$data->vehiclechecking_to_status->desc}}</i>
                                      @endif
                                  </td>
                                  
                                   
                                  <td>
                                        @if(!empty($data->request_from->role->desc))
                                        <b style="color: red">{{$data->request_from->role->desc}}</b>
                                        @endif
                                       
                                        @if(!empty($data->request_from->user_relate_usergroup->group_name))
                                    	- {{$data->request_from->user_relate_usergroup->group_name}}
                                    	@endif
                                  </td>
                                
                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>
                                  <!-- End History Search -->

                                  <!-- Action -->
                                  <td width="10%" align="center">
                                  
                                    <button type="button" class="mb-2 btn btn-sm btn-primary mr-1" data-toggle="modal" data-target=".bd-example-modal-sm4{{$data->id}}">
                                      <i class="material-icons">highlight_off</i>
                                    </button>
                                  </td>


                              </tr>

                            @endforeach
                              
                          </tbody>
                          
                      </table>
                    </div>


                      </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
          


          <!-- Modal Upload -->

          @foreach($data2 as $data)

          <div class="modal fade bd-example-modal-sm4{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">{{trans('system_autocheck.vehile_verify_not_found')}}  {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('HalfReport/verifynotfound/store/'.$data->id_vehicle) }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                      
                        <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        <div class="form-group">
                            <label class="control-label">Remark</label>
                            <textarea id="" class="form-control" name="remark_notfound" rows="4" cols="50" placeholder="Remark"></textarea>
                        </div>

                      
                        <input type="submit" name="submit" class="btn btn-sm btn-primary" style="float: right;">
                      
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach

          <!-- End Modal upload -->


@endsection


@push('js')


    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
