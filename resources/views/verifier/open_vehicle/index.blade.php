@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>


        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Open Verify Vehicle</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      

		<div class="table table-responsive">
                      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>
                                  
                                  <th >Status</th>
                                  
                                  <th>Date Sent</th>
                                  <th width="10%" align="center">Action</th>
				  <th>Type Report</th>
                                  
                                    <th>Req From</th>
                                  
                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)

                            <!-- if status match yes -->
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  

                                  <td>{{$data->created_at}}</td>

                                  

                                  <!-- Status -->
                                  <td align="center">
                                    
					 @if(!empty($data->status_carvx->report_id))

                                      @if($data->status_carvx->is_ready == 3)
                                        <i  class="card-post__category badge badge-pill badge-info">New</i>
                                      @elseif($data->status_carvx->is_ready == 4)
                                        <i  class="card-post__category badge badge-pill badge-success"> In Progress</i>
                                      @elseif($data->status_carvx->is_ready == 5)
                                        <i  class="card-post__category badge badge-pill badge-success">Complete</i>
                                      @elseif($data->status_carvx->is_ready == 6)
                                        <i  class="card-post__category badge badge-pill badge-danger">Cancel</i>
                                      @endif
                                    @endif
					


                                      @if($data->status == '40' AND $data->status_vehicle->status == 1)
                                      <i href="#" class="card-post__category badge badge-pill badge-success">Complete</i>

                                      @elseif(($data->status == '40') AND ($data->searching_by == 'API') AND ($data->group_by == "KA"))
                                      <i href="#" class="card-post__category badge badge-pill badge-success">Complete</i>

                                      @elseif($data->status == '10' OR $data->status == '20')
                                      <i  class="card-post__category badge badge-pill badge-warning">New</i>
                                      @elseif($data->status == '25')
                                      <i  class="card-post__category badge badge-pill badge-primary">In Progress</i>
                                      @elseif($data->status == '30')
                                      <i  class="card-post__category badge badge-pill badge-danger">Reject</i>

                                      <!-- status kastam complete -->
                                      @elseif(($data->status == '40') AND (!empty($data->full_report)))
                                      <i  class="card-post__category badge badge-pill badge-success">Complete</i>
                                      <!-- end status kastam complete -->

                                      @endif


                                    
                                      
                                      @if($me == 'VER')
                                      @if($data->searching_by == 'NOT')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                      @endif
                                  </td>
                                  <!-- End of Status -->

                                  
                                  <!-- End of API Status -->
                                  <td>
                                    {{$data->updated_at}}
                                  </td>
                                  <!-- Action -->
                                  <td width="10%" align="center">

                                      @if($data->status == '40' AND $data->group_by != 'KA' AND !empty($data->status_vehicle->status == "1"))
                                      <a href="{{url('report-autocheck/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>
                                      

                                    <!-- Action Print --> 
                                     <a href="{{url('report-autocheck/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                        <i class="material-icons">print</i>
                                      </button>
                                    </a>
                                    @endif
                                    <!-- End action print -->


                                    
                                  <!-- ==== Modal Detail ==== -->
                                  <!-- Kalau sudah verifier get by API -->

                                  @if($data->group_by != "KA" AND $data->group_by != "US")
                                  <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1{{$data->id}}">
                                      <i class="material-icons">details</i>
                                  </button>
                                  @endif

                                  <!-- ==== End Modal Detail ==== -->


                                  
                                  <!-- Action upload manual Kastam -->
                                  @if($data->searching_by == "NOT" AND $data->full_report != "NULL" AND $data->group_by == "US" AND $data->status != "30" AND  $data->status != "40")
                                  
                                  <button type="button" class="mb-2 btn btn-sm btn-success mr-1" data-toggle="modal" data-target=".bd-example-modal-sm3{{$data->id}}">
                                      <i class="material-icons">cloud_upload</i>
                                    </button>
                                  
                                  @elseif($data->searching_by == "NOT" AND $data->full_report != "NULL" AND $data->group_by == "KA" AND $data->status != "30" AND  $data->status != "40")
                                    
                                    <!-- Manual Kastam -->
                                    <button type="button" class="mb-2 btn btn-sm btn-success mr-1" data-toggle="modal" data-target=".bd-example-modal-sm4{{$data->id}}">
                                      <i class="material-icons">code</i> Key in Manual
                                    </button>
                                    <!-- End Manual Kastam -->

                                  @endif
                                  <!-- End Action upload manual Kastam -->


                                  <!-- verify for manual button to modal -->
                                  @if($me == "VER")

                                  @if($data->searching_by == "NOT")
                                  
                                    @endif
                                  @endif

                                    <!-- end verify for manual button to modal -->




                                    
                                  <!-- action verify -->
                                  @if($me == "VER")

                                    @if(($data->searching_by == 'NOT') AND ($data->is_sent == '1') AND ($data->group_by != "KA"))
                                    <!--   For verify data / match or not -->
                                    <!-- <a href="{{url('verify-vehicle/'.$data->id_vehicle)}}"> -->

                                    

                                    <!-- <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  data-toggle="modal" data-target=".bd-example-modal-sm2{{$data->id_vehicle}}">
                                      <i class="material-icons">details</i>
                                    </button> -->

                                    @if($data->group_by != 'US')

					
                                    @endif

                                    <!-- </a> -->

                                    @endif

                                    @if(($data->searching_by == "API" && $data->group_by == "NA"))

                                    <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  data-toggle="modal" data-target=".bd-example-modal-sm2{{$data->id_vehicle}}">
                                      <i class="material-icons">details</i> 
                                    </button> 

                                    @endif
        
                                  @endif

		@if($data->searching_by == "NOT" && $data->group_by == "NA")

	<button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  data-toggle="modal" data-target=".bd-example-modal-sm-manual{{$data->id_vehicle}}">
                                      <i class="material-icons">details</i>Manual 
                                    </button>
		@endif
                                  
                                  </td> <!-- end action verify -->


                                  <!-- Full Report -->

                                 <!-- ==== Type Report ==== -->
				<td> 
				@if(!empty($data->type_report))
					@if($data->type_report == "3" )
					<i  class="card-post__category badge badge-pill badge-warning"> {{config('autocheck.half_report')}} </i>
					@elseif($data->type_report == "4" )
					<i  class="card-post__category badge badge-pill badge-danger"> {{config('autocheck.full_report')}} </i>
					@elseif($data->type_report == "2" )
					<i  class="card-post__category badge badge-pill badge-success"> {{config('autocheck.extra_report')}} </i>
					@endif
				@endif
				</td>
 				<!-- ==== End Type Report ==== -->
                                  

                                 
				<!-- Req From -->
                                  <td>

                                    @if(!empty($data->request_from->role->desc))
                                    <p style="color: red">{{$data->request_from->role->desc}}</p>
                                    @endif
				<p>{{$data->request_by->name}}</p>
                                  </td>
                                  <!-- End Full Report -->


                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>

                                  <!-- End History Search -->
                              </tr>
                            

                            @endforeach
                              
                          </tbody>
                          
                      </table>
		</div>


                      </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        
       


          <!-- Modal Manual -->
          

          @foreach($verify_manual as $data)
          <div class="modal fade bd-example-modal-sm-manual{{$data->id_vehicle}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Match (Manual)  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <form method="POST" class="register-form" id="register-form" action="{{url('verify-vehicle-verifier-manual/'.$data->id_vehicle)}}">
                        {{ csrf_field() }}

                        @if(!empty($data->vehicle_api->api_from))
                          <input type="hidden" name="api_from" value="{{$data->vehicle_api->api_from}}" class="">
                        @endif

                        <input type="hidden" name="vehicle" value="{{$data->vehicle}}" class="">
                        <input type="hidden" name="created_by" value="{{$data->created_by}}" class="">
                      

                      <div class="form-group col-md-12">
                        <h2><b>{{$data->vehicle}} </b></h2>
                      </div>

                      <hr>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Supplier</label>
                       
                      </div>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Type</label>
                        @if(!empty($data->type->type_vehicle))
                        <h3>{{$data->type->type_vehicle}}</h3>
                        @endif
                      </div>

                      <hr>

                      <div class="form-row">                        
                        <label for="feDescription">Make / Brand </label>

                      </div>


                      <div class="form-row">

                         
                           <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->brand->brand)){{$data->brand->brand}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                             <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            
                            <select name="brand_verify" class="form-control is-valid" id="brand_value_edit{{$data->id}}" required>
                              <option value="" selected disabled hidden>- Please Select -</option>
                              @foreach($brand as $databrand)
                              <option value="{{$databrand->id}}" {{ $data->brand_id == $databrand->id ? 'selected' : '' }} >{{$databrand->brand}}</option>
                              @endforeach

                          </select>
                           
                          </div>
                          
                          
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Model / Variance </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->model_brand->model)){{$data->model_brand->model}}@endif" disabled="">
                            
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>

                           <div class="form-group col-md-6">
                            <select id="model_value_edit{{$data->id}}" name="model_verify" class="form-control is-valid"  data-show-subtext="true" data-live-search="true">
                              <option value="" selected disabled hidden>- Please Select -</option>
                              
                              @foreach($model as $datamodel)
                              
                               <option value="{{ $datamodel->id }}|{{$datamodel->brand_id}}" {{ $data->model_id == $datamodel->id ? 'selected' : '' }}>{{$datamodel->model}}</option>

                              @endforeach
                            </select> 

                            
                          </div>

                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Engine Number / Engine Model </label>
                      </div>

                      <div class="form-row">

                         

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->engine_number)){{$data->engine_number}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                            
                          </div>

                           <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->engine_number}}" required name="engine_number_verify"> 
                            

                            

                          </div>

                          
                          
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Cubic Capacity (CC) </label>
                      </div>

                      <div class="form-row">

                        

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->cc)){{$data->cc}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>

                            <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->cc}}" required name="cc_verify"> 
                            
                          </div>

                          
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Fuel Type </label>
                      </div>

                      <div class="form-row">

                          

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->fuel->fuel)){{$data->fuel->fuel}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif 
                            </div>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>


                          <div class="form-group col-md-6">
                              <select id="feInputState" name="fuel_verify" class="form-control is-valid">
                              @foreach($fuel as $datafuel)
                              
                               <option value="{{ $datafuel->id }}" {{ $data->fuel_id == $datafuel->id ? 'selected' : '' }}>{{$datafuel->fuel}}</option>

                              @endforeach
                            </select>

                            
                          </div>
                          
                          
                          
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Year of Manufacture </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->year_manufacture)){{$data->vehicle_api->year_manufacture}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>

                          <div class="form-group col-md-6">
                            <input id="bday-month" name="year_verify" class="form-control is-valid" type="month" name="year_verify" value=""> 
                            
                          </div>

                            
                      </div>

                      
                      <div class="form-row">                        
                        <label for="feDescription">First Registration Date</label>
                      </div>

                      <div class="form-row">

                        <?php $tarikh_from_kadealer =  date('m/d/Y ', strtotime($data->vehicle_registered_date)); ?>

                        @if(!empty($data->vehicle_api->registation_date))

                        <?php $tarikh_from_api =  date('m/d/Y ', strtotime($data->vehicle_api->registation_date)); ?>

                        @endif

                          

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="{{$tarikh_from_kadealer}}" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>

                          </div>

                          <div class="form-group col-md-6">
                            <input type="text" class="form-control is-valid"  value="{{$tarikh_from_kadealer}}" name="registration_date_verify" id="datepicker{{$data->id}}"> 
                            
                          </div>


                          
                          
                          
                      </div>

                      @if(empty($data->vehicle_api->api_from)) <!-- if not from carvx -->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Report From</label>
                          <select name="api_from" class="form-control" >
                            <option value="RYO">RYOCHI</option>
                            <option value="HPI">HPI</option>
                            <option value="OTHER">Others</option>
                          </select>
                        </div>
                      </div>
                      @endif <!-- end if not from carvx -->

                      
                   
                   
                </div>
                <div class="modal-footer">
                  
                  <button type="submit" class="btn btn-accent">Verify</button>

                </div>
              </form>

              </div>
            </div>
          </div>
          @endforeach

          <!-- End Modal Manual -->

        
          


            <!-- Modal update by API -->
        
          @foreach($data2 as $data)
          @if($data->group_by != "US" AND $data->group_by != "KA")
          <div class="modal fade bd-example-modal-sm2{{$data->id_vehicle}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Match API  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <form method="POST" class="register-form" id="register-form" action="{{url('verify-vehicle-verifier-api/'.$data->id_vehicle)}}">
                        {{ csrf_field() }}

                        @if(!empty($data->vehicle_api->api_from))
                          <input type="hidden" name="api_from" value="{{$data->vehicle_api->api_from}}" class="">
                        @endif

                        <input type="hidden" name="vehicle" value="{{$data->vehicle}}" class="">
                        <input type="hidden" name="created_by" value="{{$data->created_by}}" class="">
                      

                      <div class="form-group col-md-12">
                        <h2><b>{{$data->vehicle}}</b></h2>
                      </div>

                      <hr>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Supplier</label>
                        @if(!empty($data->supplier_id))
                        <h3>{{$data->supplier->supplier}}</h3>
                        @endif
                      </div>

                      <div class="form-group col-md-12">
                        <label for="feDescription">Type</label>
                        @if(!empty($data->type->type_vehicle))
                        <h3>{{$data->type->type_vehicle}}</h3>
                        @endif
                      </div>

                      <hr>

                      <div class="form-row">                        
                        <label for="feDescription">Make / Brand </label>

                      </div>


                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->brand)){{$data->vehicle_api->brand}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>
                          <div class="form-group col-md-5">
                            
                            <select name="brand_verify" class="form-control is-valid" id="brand_value_carvx_api{{$data->id}}" required>
                              <option value="" selected disabled hidden>- Please Select -</option>
                              @foreach($brand as $databrand)
                              <option value="{{$databrand->id}}" {{ $data->brand_id == $databrand->id ? 'selected' : '' }} >{{$databrand->brand}}</option>
                              @endforeach

                          </select>
                            <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if($data->status_vehicle->brand == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Model / Variance </label>
                      </div>

                      <div class="form-row">
                          
                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->model)){{$data->vehicle_api->model}}@endif" disabled="">
                            
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <select id="model_value_carvx_api{{$data->id}}" name="model_verify" class="form-control is-valid"  data-show-subtext="true" data-live-search="true">
                              <option value="" selected disabled hidden>- Please Select -</option>
                              
                              @foreach($model as $datamodel)
                              
                               <option value="{{ $datamodel->id }}|{{$datamodel->brand_id}}" {{ $data->model_id == $datamodel->id ? 'selected' : '' }}>{{$datamodel->model}}</option>

                              @endforeach
                            </select> 

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">

                              @if($data->status_vehicle->model == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                        
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Engine Number / Engine Model </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->engine_number)){{$data->vehicle_api->engine_number}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->engine_number}}" required name="engine_number_verify"> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Cubic Capacity (CC) </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->cc)){{$data->vehicle_api->cc}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer01" value="{{$data->cc}}" required name="cc_verify"> 
                            <div class="valid-feedback">
                              {{$data->request_by->group_by}}
                            </div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if($data->status_vehicle->cc == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Fuel Type </label>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->fuel_type)){{$data->vehicle_api->fuel_type}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif 
                            </div>
                          </div>
                          
                          <div class="form-group col-md-5">
                              <select id="feInputState" name="fuel_verify" class="form-control is-valid">
                              @foreach($fuel as $datafuel)
                              
                               <option value="{{ $datafuel->id }}" {{ $data->fuel_id == $datafuel->id ? 'selected' : '' }}>{{$datafuel->fuel}}</option>

                              @endforeach
                            </select>

                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>


                      <div class="form-row">                        
                        <label for="feDescription">Year of Manufacture </label>
                      </div>

                      <div class="form-row">

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->year_manufacture)){{$data->vehicle_api->year_manufacture}}@endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>

                          <div class="form-group col-md-5">
                            <input id="bday-month" name="year_verify" class="form-control is-valid" type="month" name="year_verify" value="" disabled> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                            
                              <h3>OK <i class="material-icons">thumb_up</i></h3>
                            </div>
                          </div>
                      </div>

                      
                      <div class="form-row">                        
                        <label for="feDescription">First Registration Date</label>
                      </div>

                      <div class="form-row">

                        <?php $tarikh_from_kadealer =  date('m/d/Y ', strtotime($data->vehicle_registered_date)); ?>

                        @if(!empty($data->vehicle_api->registation_date))

                        <?php $tarikh_from_api =  date('m/d/Y ', strtotime($data->vehicle_api->registation_date)); ?>

                        @endif

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid" id="validationServer02" value="@if(!empty($data->vehicle_api->registation_date)){{$tarikh_from_api}}
                            @endif" disabled>
                            <div class="valid-feedback">
                              @if(!empty($data->vehicle_api->api_from))
                                {{$data->vehicle_api->api_from}} 
                              @endif
                            </div>
                          </div>


                          <div class="form-group col-md-5">
                            <input type="text" class="form-control is-valid"  value="{{$tarikh_from_kadealer}}" name="registration_date_verify" id="datepicker{{$data->id}}"> 
                            <div class="valid-feedback">{{$data->request_by->group_by}}</div>
                          </div>
                          
                          <div class="form-group col-md-2">
                            <div class="custom-control custom-checkbox mb-1">
                              @if($data->status_vehicle->registation_date == '1')
                                <h3>OK <i class="material-icons">thumb_up</i></h3>
                              @else
                                <h3 style="color: red">NOT <i class="material-icons">thumb_down</i></h3>
                              @endif
                            </div>
                          </div>
                      </div>

                      @if(empty($data->vehicle_api->api_from)) <!-- if not from carvx -->
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Report From</label>
                          <select name="api_from" class="form-control" >
                            <option value="RYO">RYOCHI</option>
                            <option value="HPI">HPI</option>
                            <option value="OTHER">Others</option>
                          </select>
                        </div>
                      </div>
                      @endif <!-- end if not from carvx -->

                      @if($data->status_vehicle->status == "0")<!-- if not match -->
                      
                      <!-- Setakat ini remark belum dulu 
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feLastName">Remark</label>
                          <textarea name="remark" class="form-control is-valid" rows="5" placeholder="Remark"></textarea>
                        </div>
                      </div> -->
                      <!-- end if not match -->
                      

                      <!-- <div class="form-row">
                        <div class="form-group col-md-6"></div>
                        <div class="form-group col-md-6">
                          <div class="radio">
                            <label><input type="radio" name="yes" value="yes" checked>Approve</label>
                          </div>
                          <div class="radio">
                            <label><input type="radio" name="yes" value="no">Reject</label>
                          </div>
                        </div>
                      </div> -->
                      @endif
                   
                   
                </div>
                <div class="modal-footer">
                  
                  <button type="submit" class="btn btn-accent">Verify</button>

                </div>
              </form>

              </div>
            </div>
          </div>
          @endif
          @endforeach

          <!-- End Modal update by API -->
          


            <!-- Modal Upload -->

          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm3{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Upload {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('upload-report-vehicle/'.$data->id_vehicle) }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                      


                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <input type="file" name="file" class="form-control">

                          <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        </div>

                        <hr>

                      </div>

                      <div class="form-row">
                        <input type="submit" name="submit" class="btn btn-sm btn-primary">
                      </div>
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach



          @foreach($data2 as $data)
          @if($data->group_by != "US")
          <div class="modal fade bd-example-modal-sm4{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Input ( {{config("autocheck.full_report")}} ) - {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('kastam-manual-short-report/'.$data->id_vehicle) }}" enctype='multipart/form-data' >
                        {{ csrf_field() }}
                      

                      <div class="form-row">


                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand*</label>

                          <select name="make" class="form-control" id="brand_valuex{{$data->id}}" required style="text-transform:uppercase">
                            <option value="" selected disabled hidden>- Please Select -</option>
                            @foreach($brand2 as  $brand )
                            <option value="{{$brand->id}}">{{$brand->brand}}</option>
                            @endforeach
                          </select>
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model*</label>
                          
                          <select id="model_valuex{{$data->id}}" name="model" class="form-control" required style="text-transform:uppercase">
                            <option value="" selected disabled hidden>- Please Select -</option>
                            @foreach($model2 as  $datamodel )
                            <option value="{{ $datamodel->id }}|{{$datamodel->brand_id}}">{{$datamodel->model}}</option>

                            @endforeach
                          </select>
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Engine Code / Engine Number*</label>
                          
                          <input type="text" name="engine" class="form-control" maxlength="100" placeholder="Engine Code / Engine Number"required style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Body</label>
                          
                          <input type="text" name="body" class="form-control" maxlength="100" placeholder="Body" style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Car Grade</label>
                          
                          <input type="text" name="grade" class="form-control" maxlength="100" placeholder="Car Grade"  style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Date Of Manufacture*</label>
                          
                          <input type="text" name="date_of_manufacture" class="form-control" maxlength="100" id="datepicker3{{$data->id}}" placeholder="Date Of Manufacture" required style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Date Of Original Registration*</label>
                          
                          <input type="text" name="date_of_original_registration" class="form-control" maxlength="100" id="datepicker4{{$data->id}}" placeholder="Date Of Original Registration" required style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Drive*</label>
                          
                          <input type="text" name="drive" class="form-control" maxlength="100" placeholder="Drive" required style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Transmission*</label>
                          
                          <input type="text" name="transmission" class="form-control" maxlength="100" placeholder="Transmission" required style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Displacement (cc)*</label>
                          
                          <input type="text" name="cc" class="form-control" maxlength="100" placeholder="Displacement (cc)" required style="text-transform:uppercase">
                          
                        </div>

                        
                        <div class="form-group col-md-12">
                          <label for="feDescription">Currency</label>
                          

                          <select name="currency" class="form-control" style="text-transform:uppercase">
                            <option value="" selected disabled hidden>- Please Select -</option>
                            <option value="UK">UK</option>
                            <option value="JP">JAPAN</option>
                            <option value="ZA">South Africa</option>
                          </select>
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Average Market Price</label>
                          
                          <input type="text" name="price" class="form-control" maxlength="100" placeholder="Average Market Price" style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">

                          <label for="feDescription">Images</label>

                          <div class="input-group control-group increment" >
                            

                            <input type="file" name="filename[]" class="form-control">
                            <div class="input-group-btn"> 
                              <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                            </div>
                          </div>
                          <div class="clone hide">
                            <div class="control-group input-group" style="margin-top:10px">
                              <input type="file" name="filename[]" class="form-control">
                              <div class="input-group-btn"> 
                                <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                              </div>
                            </div>
                          </div>
                        </div>

                        
                        

                        <div class="form-group col-md-12">
                            

                          <!-- <input type="file" name="file" class="form-control"> -->
                          <!-- <input type="file" class="form-control" name="filename[]" id="photo" accept=".png, .jpg, .jpeg" onchange="readFile(this);" multiple> -->

                          <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        </div>

                        <div id="status"></div>
                        <div id="photos" class="row"></div>

                        <hr>

                      </div>

                      <div class="form-row">
                        <input type="submit" name="submit" class="btn btn-sm btn-primary">
                      </div>
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>


          <div class="modal fade bd-example-modal-input-id{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Input ID Report Kastam Report {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('kastam-manual-short-report-submit-idreport/'.$data->id_vehicle) }}" enctype='multipart/form-data' >
                        {{ csrf_field() }}
                      

                      <div class="form-row">


                        <div class="form-group col-md-12">
                          <label for="feDescription">ID Report</label>

                          <input type="text" name="engine" class="form-control" maxlength="100" placeholder="ID Report" required>
                          
                        </div>

                        <input type="hidden" name="vehicle" class="form-control" maxlength="100" placeholder="Vehicle" required value="{{$data->id_vehicle}}">
                        

                        <div class="form-group col-md-12">
                            

                          <!-- <input type="file" name="file" class="form-control"> -->
                          <!-- <input type="file" class="form-control" name="filename[]" id="photo" accept=".png, .jpg, .jpeg" onchange="readFile(this);" multiple> -->

                          <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        </div>

                        <div id="status"></div>
                        <div id="photos" class="row"></div>

                        <hr>

                      </div>

                      <div class="form-row">
                        <input type="submit" name="submit" class="btn btn-sm btn-primary">
                      </div>
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>


          @endif
          @endforeach

          <!-- End Modal upload -->
            



            

@endsection


@push('js')

  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->


<script type="text/javascript">

    $(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

    });

</script>


<script>
    function readFile(input) {
    $("#status").html('Processing...');
    counter = input.files.length;
        for(x = 0; x<counter; x++){
            if (input.files && input.files[x]) {

                var reader = new FileReader();

                reader.onload = function (e) {
            $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="'+e.target.result+'" class="img-thumbnail"></div>');
                };

                reader.readAsDataURL(input.files[x]);
            }
    }
    if(counter == x){$("#status").html('');}
  }
</script>


    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    


    @foreach($data3 as $data2)
    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>


    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_valuex{{$data2->id}}').children().clone();
      
        $('#brand_valuex{{$data2->id}}').change(function() {
          $('#model_valuex{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_valuex{{$data2->id}}').append(this);
                 }
            });
          $('#model_valuex{{$data2->id}}').val('');
        });
    });
    </script>

	
    <!--=============  Verify Brand and Model API ==============-->
    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_carvx_api{{$data2->id}}').children().clone();
      
        $('#brand_value_carvx_api{{$data2->id}}').change(function() {
          $('#model_value_carvx_api{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_carvx_api{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_carvx_api{{$data2->id}}').val('');
        });
    });
    </script>
    <!--=============  End Verify Brand and Model API ==============-->




    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker2{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker3{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker4{{$data2->id}}" ).datepicker();
     });
    </script>
    @endforeach


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
