@extends('admin.layout.template')

@section('content')

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>


        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Open Vehicle Verify ( {{config('autocheck.full_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">
              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                     
                    <div class="table-responsive">
                      <table id="example" class="table table-striped  table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>
                                  <th >Status</th>
                                  <th>Date Verify</th>
                                  <th>Req From</th>
                                  <th width="10%" align="center">Action</th>
                                  
                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)

                            <!-- if status match yes -->
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  <td>{{$data->created_at}}</td>

                                  <!-- Status -->
                                  <td align="center">
                                    
                                      @if($data->searching_by == 'NOT')

                                      <i  class="card-post__category badge badge-pill badge-danger">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                  </td>
                                  <!-- End of Status -->

                                 
                                  <!-- End of API Status -->

                                  <td>
                                    {{$data->updated_at}}
                                  </td>
                                  <td>
                                    @if(!empty($data->request_by->name))
                                       {{$data->request_by->name}}
                                    @endif

                                    -

                                    @if(!empty($data->group->group_name))
                                      <b style="color: red">{{$data->group->group_name}}</b>
                                    @endif
                                  </td>

                                  <!-- Action -->
                                  <td width="10%" align="center">
      
                                    <!-- Manual Kastam -->
                                    <button type="button" class="mb-2 btn btn-sm btn-success mr-1" data-toggle="modal" data-target=".bd-example-modal-sm4{{$data->id}}">
                                      <i class="material-icons">code</i>
                                    </button>
                                    <!-- End Manual Kastam -->

                                  
                                  
                                  </td> <!-- end action verify -->

                                 

                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>

                                  <!-- End History Search -->
                              </tr>
                            

                            @endforeach
                              
                          </tbody>
                          
                      </table>
                    </div>


                      </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

     
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm4{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Verify {{config("autocheck.full_report")}} {{$data->vehicle}} </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="upload-file" id="register-form" action="{{ url('kastam-manual-short-report/'.$data->id_vehicle) }}" enctype='multipart/form-data' >
                        {{ csrf_field() }}
                      

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand*</label>

                          <select name="make" class="form-control" id="brand_valuex{{$data->id}}" required style="text-transform:uppercase">
                            <option value="" selected disabled hidden>- Please Select -</option>
                            @foreach($brand2 as  $brand )
                            <option value="{{$brand->id}}">{{$brand->brand}}</option>
                            @endforeach
                          </select>
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model*</label>
                          
                          <select id="model_valuex{{$data->id}}" name="model" class="form-control" required style="text-transform:uppercase">
                            <option value="" selected disabled hidden>- Please Select -</option>
                            @foreach($model2 as  $datamodel )
                            <option value="{{ $datamodel->id }}|{{$datamodel->brand_id}}">{{$datamodel->model}}</option>

                            @endforeach
                          </select>
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Engine Code / Engine Number*</label>
                          
                          <input type="text" name="engine" class="form-control" maxlength="100" placeholder="Engine Code / Engine Number" required style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Body</label>
                          
                          <input type="text" name="body" class="form-control" maxlength="100" placeholder="Body" style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Car Grade</label>
                          
                          <input type="text" name="grade" class="form-control" maxlength="100" placeholder="Car Grade" style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Date Of Manufacture*</label>
                          
                          <input type="text" name="date_of_manufacture" class="form-control" maxlength="100" id="datepicker3{{$data->id}}" placeholder="Date Of Manufacture" required style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Date Of Original Registration*</label>
                          
                          <input type="text" name="date_of_original_registration" class="form-control" maxlength="100" id="datepicker4{{$data->id}}" placeholder="Date Of Original Registration" required style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Drive*</label>
                          
                          <input type="text" name="drive" class="form-control" maxlength="100" placeholder="Drive" required style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Transmission*</label>
                          
                          <input type="text" name="transmission" class="form-control" maxlength="100" placeholder="Transmission" required style="text-transform:uppercase">
                          
                        </div>


                        <div class="form-group col-md-12">
                          <label for="feDescription">Displacement (cc)*</label>
                          
                          <input type="text" name="cc" class="form-control" maxlength="100" placeholder="Displacement (cc)" required style="text-transform:uppercase">
                          
                        </div>

                        
                        <div class="form-group col-md-12">
                          <label for="feDescription">Currency</label>
                          

                          <select name="currency" class="form-control" style="text-transform:uppercase">
                            <option value="" selected disabled hidden>- Please Select -</option>
                            <option value="UK">UK</option>
                          </select>
                          
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Average Market Price</label>
                          
                          <input type="text" name="price" class="form-control" maxlength="100" placeholder="Average Market Price" style="text-transform:uppercase">
                          
                        </div>

                        <div class="form-group col-md-12">

                          <label for="feDescription">Images</label>

                          <div class="input-group control-group increment" >
                            

                            <input type="file" name="filename[]" class="form-control">
                            <div class="input-group-btn"> 
                              <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                            </div>
                          </div>
                          <div class="clone hide">
                            <div class="control-group input-group" style="margin-top:10px">
                              <input type="file" name="filename[]" class="form-control">
                              <div class="input-group-btn"> 
                                <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                              </div>
                            </div>
                          </div>
                        </div>

                        
                        

                        <div class="form-group col-md-12">
                            

                          <!-- <input type="file" name="file" class="form-control"> -->
                          <!-- <input type="file" class="form-control" name="filename[]" id="photo" accept=".png, .jpg, .jpeg" onchange="readFile(this);" multiple> -->

                          <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                        </div>

                        <div id="status"></div>
                        <div id="photos" class="row"></div>

                        <hr>

                      </div>

                      <div class="form-row">
                        <input type="submit" name="submit" class="btn btn-sm btn-primary">
                      </div>
                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach

          <!-- End Modal upload -->
            



            

@endsection


@push('addjs')



  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->


<script type="text/javascript">

    $(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

    });

</script>


<script>
    function readFile(input) {
    $("#status").html('Processing...');
    counter = input.files.length;
        for(x = 0; x<counter; x++){
            if (input.files && input.files[x]) {

                var reader = new FileReader();

                reader.onload = function (e) {
            $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="'+e.target.result+'" class="img-thumbnail"></div>');
                };

                reader.readAsDataURL(input.files[x]);
            }
    }
    if(counter == x){$("#status").html('');}
  }
</script>


    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    


    @foreach($data3 as $data2)
    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>


    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_valuex{{$data2->id}}').children().clone();
      
        $('#brand_valuex{{$data2->id}}').change(function() {
          $('#model_valuex{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_valuex{{$data2->id}}').append(this);
                 }
            });
          $('#model_valuex{{$data2->id}}').val('');
        });
    });
    </script>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    
    <script>
    $(function() {
       $( "#datepicker{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker2{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker3{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker4{{$data2->id}}" ).datepicker();
     });
    </script>

    @endforeach


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
   


@endpush


@push('js')


 <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush
