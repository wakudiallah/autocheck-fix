@extends('admin.layout.template')

@section('content')

	<style type="text/css">
	       
       .form-step-by-step{
            margin-bottom: 50px !important;
            margin-top: 30px !important;
            margin-left: 40px;
            margin-right: 40px;
        }

		.stepwizard-step p {
		    margin-top: 10px;
		}
		.stepwizard-row {
		    display: table-row;
		}
		.stepwizard {
		    display: table;
		    width: 100%;
		    position: relative;
            margin-top: 35px;
            font-size: 20px;
		}

        .step-title{
            margin-bottom: 50px !important;;
        }

		.stepwizard-step button[disabled] {
		    opacity: 1 !important;
		    filter: alpha(opacity=100) !important;
		}
		.stepwizard-row:before {
		    top: 14px;
		    bottom: 0;
		    position: absolute;
		    content: " ";
		    width: 100%;
		    height: 1px;
		    background-color: #ccc;
		    z-order: 0;
		}
		.stepwizard-step {
		    display: table-cell;
		    text-align: center;
		    position: relative;
		}
		
        /*.btn-circle {
		    width: 30px;
		    height: 30px;
		    text-align: center;
		    padding: 6px 0;
		    font-size: 12px;
		    line-height: 1.428571429;
		    border-radius: 15px;
		}*/

        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 2px 0;
            font-size: 20px;
            line-height: 1.428571;
            border-radius: 30px;
        }

        .row-md-6{
            margin-bottom: 30px !important;
        }

        .red-color{
            color: #FF0033 !important;
        }

	</style>

    <style>
        .ui-datepicker-calendar {
            display: none;
        }
    </style>

    <!-- Upload file Lib -->

   

    <style>
      #navigation {
        margin: 10px 0;
      }
      @media (max-width: 767px) {
        #title,
        #description {
          display: none;
        }
      }
    </style>
    <!-- blueimp Gallery styles -->
    <link
      rel="stylesheet"
      href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css"
    />
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="{{asset('lib/jQuery-File-Upload/css/jquery.fileupload.css')}}" />
    <link rel="stylesheet" href="{{asset('lib/jQuery-File-Upload/css/jquery.fileupload-ui.css')}}" />
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript>
    <link rel="stylesheet" href="{{asset('lib/jQuery-File-Upload/css/jquery.fileupload-noscript.css')}}"
    /></noscript>
    <noscript>
    <link rel="stylesheet" href="{{asset('lib/jQuery-File-Upload/css/jquery.fileupload-ui-noscript.css')}}"
    /></noscript>

    <!-- End upload file liib -->


	<div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Open Verify (  {{trans('system_autocheck.extra_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            

            <div class="row">
              <div class="col-lg-12 mb-4">
                <div class="card card-small mb-4">
                  
                    <div class="form-step-by-step">
  
                        <div class="stepwizard">
                            <div class="stepwizard-row setup-panel">

                                <div class="stepwizard-step">
                                    
                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                    <p>Vehicle Details</p>

                                </div>


                                <div class="stepwizard-step">

                                    @if($step_status->step_1 == "1")
                                    <a href="#step-2" type="button" class="btn btn-primary btn-circle" disabled="disabled">2</a>
                                    
                                    @else
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                   
                                    @endif

                                    <p> Resume & Average Market Price</p>

                                </div>
                                <div class="stepwizard-step">
                                    @if($step_status->step_2 == "1")
                                    <a href="#step-3" type="button" class="btn btn-primary btn-circle" disabled="disabled">3</a>

                                    @else
                                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                    @endif

                                    <p>Accident / Repair</p>

                                </div>
                                <div class="stepwizard-step">
                                    @if($step_status->step_3 == "1")
                                    <a href="#step-4" type="button" class="btn btn-primary btn-circle" disabled="disabled">4</a>

                                    @else

                                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                                    @endif

                                    <p>Odometer Reading</p>
                                </div>
                                <div class="stepwizard-step">

                                    @if($step_status->step_4 == "1")
                                    <a href="#step-5" type="button" class="btn btn-primary btn-circle" disabled="disabled">5</a>
                                    @else
                                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                                    @endif

                                    <p>Use</p>
                                </div>
                                <div class="stepwizard-step">

                                    @if($step_status->step_5 == "1")
                                    <a href="#step-6" type="button" class="btn btn-primary btn-circle" disabled="disabled">6</a>
                                    @else
                                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                                    @endif

                                    <p>Detailed</p>
                                </div>
                                <div class="stepwizard-step">

                                    @if($step_status->step_6 == "1")
                                    <a href="#step-7" type="button" class="btn btn-primary btn-circle" disabled="disabled">7</a>
                                    @else
                                    <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
                                    @endif

                                    <p>Manufacturer Recall</p>
                                </div>
                                <div class="stepwizard-step">

                                    @if($step_status->step_7 == "1")
                                    <a href="#step-8" type="button" class="btn btn-primary btn-circle" disabled="disabled">8</a>
                                    @else
                                    <a href="#step-8" type="button" class="btn btn-default btn-circle" disabled="disabled">8</a>
                                    @endif

                                    <p>Vehicle Assessment</p>
                                </div>
                                <div class="stepwizard-step">
                                    @if($step_status->step_8 == "1")
                                    <a href="#step-9" type="button" class="btn btn-primary btn-circle" disabled="disabled">9</a>
                                    @else
                                    <a href="#step-9" type="button" class="btn btn-default btn-circle" disabled="disabled">9</a>
                                    @endif
                                    <p>Vehicle Specification</p>
                                </div>

                                <div class="stepwizard-step">
                                    @if($step_status->step_9 == "1")
                                    <a href="#step-10" type="button" class="btn btn-primary btn-circle" disabled="disabled">10</a>
                                    @else
                                    <a href="#step-10" type="button" class="btn btn-default btn-circle" disabled="disabled">10</a>
                                    @endif
                                    <p>Auction Data</p>
                                </div>


                                <div class="stepwizard-step">
                                    
                                    @if($step_status->step_10 == "1")
                                    <a href="#step-11" type="button" class="btn btn-primary btn-circle" disabled="disabled">11</a>
                                    @else
                                    <a href="#step-11" type="button" class="btn btn-default btn-circle" disabled="disabled">11</a>
                                    @endif

                                    <p>Photo & Auction Sheets</p>
                                </div>

                            </div>
                        </div>
  
                            <div class="row setup-content" id="step-1">
                                <div class="col-md-12">
                                    <h3> Vehicle Details</h3>
                                    <hr class="step-title">
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            
                                            <!-- Token -->
                                            <div class="form-group">
                                                <input maxlength="100" type="hidden" class="form-control" placeholder="" name="_token" id="_token" value="{{ csrf_token() }}">
                                            </div>

                                            <!-- Id Vehicle -->
                                            
                                            <div class="form-group">
                                                <input maxlength="100" type="hidden" class="form-control" placeholder="" name="id_vehicle" value="{{$data->id_vehicle}}" id="id_vehicle">
                                            </div>

                                            <?php
                                                $blade_vehicle_detail = \App\Model\RVehicleDetails::where('id_vehicle', $data->id_vehicle)->latest()->first();
                                            ?>

                                            <div class="form-group">
                                                <label class="control-label">Chassis Number</label>
                                                <b><input maxlength="100" type="text" class="form-control" placeholder="" name="chassis_number" id="chassis_number" value="{{$data->vehicle}}" disabled=""></b>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Manufacture Date</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Manufacture Date" name="manufacture_date" id="manufacture_date">

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Make / Brand</label>
                                                
                                                <select name="brand" class="form-control" id="make" required>
                                                  <option value="" selected disabled hidden>- Please Select -</option>
                                                  @foreach($brand as $data)
                                                  <option value="{{$data->id}}">{{$data->brand}} </option>
                                                  @endforeach
                                                </select>



                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Model</label>

                                                <select name="model" class="form-control" id="model" required="">
                                                  <option value="" selected disabled hidden>- Please Select -</option>
                                                  @foreach($model as $data)
                                                  <option value="{{$data->id}}|{{$data->brand_id}}">{{$data->model}} </option>
                                                  @endforeach
                                                </select>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Body</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Body" name="body" id="body">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Grade</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Grade" name="grade" id="grade">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Engine Drive</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Engine Drive" name="vehicledetails_enginedrive" id="vehicledetails_enginedrive">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Transmission</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Transmission" name="vehicledetails_transmission" id="vehicledetails_transmission">
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" style="float: right" id="step_1">Next</button>

                                </div>
                            </div>


                            <div class="row setup-content" id="step-2">

                                <div class="col-md-12">

                                    <h3> Resume & Average Market Price</h3>
                                    <hr class="step-title">

                                    <div class="form-group">
                                        <label class="control-label">Title information</label>
                                        
                                        <select class="form-control" name="vehicle_titleinformation" id="vehicle_titleinformation">
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            <option value="1">Deregistered to Export</option>
                                            <option value="0" selected="selected">Registered to Export</option>
                                            <option value="2">NA</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Accident / Repair</label>

                                        <select class="form-control" name="vehicle_accidentrepair" id="vehicle_accidentrepair">
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            <option value="1" selected="selected"> No Problem</option>
                                            <option value="0">Problem Found</option>
                                            <option value="2">NA</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Odometer rollback</label>
                                        
                                        <select class="form-control" name="vehicle_odometerrollback" id="vehicle_odometerrollback">
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            <option value="1" selected="selected"> No Problem</option>
                                            <option value="0">Problem Found</option>
                                            <option value="2">NA</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Manufacturer recall</label>

                                        <select class="form-control" name="vehicle_manufacturerrecall" id="vehicle_manufacturerrecall">
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            <option value="1" selected="selected"> No Problem</option>
                                            <option value="0">Problem Found</option>
                                            <option value="2">NA</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Safety grade</label>

                                        <select class="form-control" name="vehicle_safetygrade" id="vehicle_safetygrade">
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            <option value="1" selected="selected"> No Problem</option>
                                            <option value="0">Problem Found</option>
                                            <option value="2">NA</option>
                                        </select>


                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Contamination risk</label>

                                        <select class="form-control" name="vehicle_contaminationrisk" id="vehicle_contaminationrisk">
                                            <option value="" selected disabled hidden>- Please Select -</option>
                                            <option value="1" selected="selected"> No Problem</option>
                                            <option value="0">Problem Found</option>
                                            <option value="2">NA</option>
                                        </select>

                                    </div>


                                </div>
                              
                                <div class="col-md-12">
                                    
                                       
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Average Market Price</label>
                                                    <input maxlength="200" type="text" name="averageprice" id="averageprice" class=" money form-control" placeholder="e.g 40000" onkeypress="return isNumberKey(event)" value="0">

                                                    <!-- <input type="text" id="cloned" class="money-cloned" disabled="" /> -->

                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Currency</label>
                                                    
                                                    <select name="currency" class="form-control is-valid" name="currency" id="currency">
                                                      <option value="" selected disabled hidden>- Please Select Currency -</option>
                                                      @foreach($param_currency as $datacurrency)
                                                      <option value="{{$datacurrency->id_currency}}">{{$datacurrency->symbol}} - {{$datacurrency->currency}}</option>
                                                      @endforeach

                                                  </select>

                                                </div>
                                            </div>
                                          
                                        </div>
                                    
                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="step_2">Next</button>
                                    </div>
                                    
                                </div>
                              
                            </div>

                            <div class="row setup-content" id="step-3">
                              
                                <div class="col-md-12">
                                    <h3> Accident / Repair</h3>
                                    <hr class="step-title">

                                        <div class="row row-md-6">
                                            <div class="col-md-6">
                                                <p class="red-color">+ Collision</p>

                                                <div class="form-group">
                                                    <label class="control-label">Reported</label>             

                                                    <select class="form-control" name="collision_reported" id="collision_reported">
                                                        <option value="" disabled hidden>- Please Select -</option>
                                                        <option value="1">Reported</option>
                                                        <option value="0" selected="selected">Not reported </option>
                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Date Reported</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="collision_date_reported" id="collision_date_reported">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Data Source</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="collision_data_source" id="collision_data_source">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Detail</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Details" name="collision_detail" id="collision_detail">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Air Bag</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Air Bag" name="collision_air_bag" id="collision_air_bag">
                                                </div>

                                                
                                            </div>

                                            <div class="col-md-6">

                                                <p class="red-color">+ Malfunction</p>

                                                <div class="form-group">
                                                    <label class="control-label">Reported</label>
                                                    
                                                    <select class="form-control" name="malfunction_reported" id="malfunction_reported">
                                                        <option value=""  disabled hidden>- Please Select -</option>
                                                        <option value="1">Reported</option>
                                                        <option value="0" selected>Not reported </option>
                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Date Reported</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="malfunction_date_reported" id="malfunction_date_reported">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Data Source</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="malfunction_data_source" id="malfunction_data_source">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Detail</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Details" name="malfunction_detail" id="malfunction_detail">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Air Bag</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Air Bag" name="malfunction_air_bag" id="malfunction_air_bag">
                                                </div>

                                               

                                            </div>
                                          
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="red-color">+ Theft</p>

                                                <div class="form-group">
                                                    <label class="control-label">Reported</label>
                                                   
                                                    <select class="form-control" name="theft_reported" id="theft_reported">
                                                        <option value="" disabled hidden>- Please Select -</option>
                                                        <option value="1">Reported</option>
                                                        <option value="0" selected>Not reported </option>
                                                    </select>


                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Date Reported</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="theft_date_reported" id="theft_date_reported">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Data Source</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="theft_data_source" id="theft_data_source">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Detail</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Details" name="theft_detail" id="theft_detail">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Air Bag</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Air Bag" name="theft_air_bag" id="theft_air_bag">
                                                </div>

                                               
                                            </div>

                                            <div class="col-md-6">

                                                <p class="red-color">+ Fire Damage</p>

                                                <div class="form-group">
                                                    <label class="control-label">Reported</label>

                                                    <select class="form-control" name="firedamage_reported" id="firedamage_reported">
                                                        <option value=""  disabled hidden>- Please Select -</option>
                                                        <option value="1">Reported</option>
                                                        <option value="0" selected>Not reported </option>
                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Date Reported</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="firedamage_date_reported" id="firedamage_date_reported">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Data Source</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="firedamage_data_source" id="firedamage_data_source">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Detail</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Details" name="firedamage_detail" id="firedamage_detail">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Air Bag</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Air Bag" name="firedamage_air_bag" id="firedamage_air_bag">
                                                </div>

                                                
                                            </div>
                                          
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="red-color">+ Water Damage</p>

                                                <div class="form-group">
                                                    <label class="control-label">Reported</label>
                                                    
                                                    <select class="form-control" name="waterdamage_reported" id="waterdamage_reported">
                                                        <option value=""  disabled hidden>- Please Select -</option>
                                                        <option value="1">Reported</option>
                                                        <option value="0" selected>Not reported </option>
                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Date Reported</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="waterdamage_date_reported" id="waterdamage_date_reported">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Data Source</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="waterdamage_data_source" id="waterdamage_data_source">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Detail</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Details" name="waterdamage_detail" id="waterdamage_detail">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Air Bag</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Air Bag" name="waterdamage_air_bag" id="waterdamage_air_bag">
                                                </div>

                                                
                                            </div>

                                            <div class="col-md-6">

                                                <p class="red-color">+ Hail Damage</p>

                                                <div class="form-group">
                                                    <label class="control-label">Reported</label>

                                                    <select class="form-control" name="haildamage_reported" id="haildamage_reported">
                                                        <option value="" selected disabled hidden>- Please Select -</option>
                                                        <option value="1">Reported</option>
                                                        <option value="0" selected>Not reported </option>
                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Date Reported</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="haildamage_date_reported" id="haildamage_date_reported">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Data Source</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="haildamage_data_source" id="haildamage_data_source">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Detail</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Details" name="haildamage_detail" id="haildamage_detail">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Air Bag</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Air Bag" name="haildamage_air_bag" id="haildamage_air_bag">
                                                </div>
                                            </div>                                          
                                        </div>

                                    
                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="step_3">Next</button>
                                    </div>
                                    
                                </div>
                              
                            </div>


                            <div class="row setup-content" id="step-4">
                              
                                <div class="col-md-12">
                                    <h3> Odometer Reading</h3>
                                    <hr class="step-title">

                                    <div class="row" style="margin-top: 30px !important;">
                                        <div class="col-md-12">
                                            <table class='table table-bordered'>
                                                <thead>
                                                    <tr>
                                                        <td>Date Reported</td>
                                                        <td>Data Source</td>
                                                        <td>Odometer Reading (km)</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   @foreach($data_odometer_reading as $data)
                                                    <tr>
                                                        <td>{{$data->date}}</td>
                                                        <td>{{$data->source}}</td>
                                                        <td>{{$data->mileage}}</td>
                                                    </tr>
                                                    @endforeach
                                                   
                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>



                                    <div class="row" style="margin-top: 30px !important;">
                                        <div class="col-md-12">
                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Date Reported</td>
                                                        <td>Data Source</td>
                                                        <td>Odometer Reading (km)</td>
                                                        <td>Action</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                    <tr>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="odometer_datereported" id="odometer_datereported">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="odometer_datasource" id="odometer_datasource">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Odometer Reading" name="odometer_odometerreading" id="odometer_odometerreading">
                                                        </td>
                                                        <td>
                                                           <button class="btn btn-success submit-odometer add-odometer" id="step_4" type="button"><i class="glyphicon glyphicon-plus"></i>Submit</button>
                                                        </td>
                                                    </tr>
                                                   
                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>

                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="next_odometer">Next</button>
                                    </div>

                                  
                                </div>
                              
                            </div>


                           
                            <div class="row setup-content" id="step-5">
                                

                                      
                                <div class="col-md-12">
                                    <h3> Use History</h3>
                                    <hr class="step-title">

                                    <div class="row">

                                        <!-- <div class="col-md-12">
                                            <table border="0" class='table table-bordered' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td><b>Use in the contaminated regions</b></td>
                                                        <td><b>Radioactive contamination test fail</b></td>
                                                        <td><b>Commercial use</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data_use_history as $data_use_history)
                                                    <tr>
                                                        <td>{{ $data_use_history->contaminatedRegion }}</td>
                                                        <td>{{ $data_use_history->contaminationTest }}</td>
                                                        <td>{{ $data_use_history->commercialUsage }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>

                                            </table>
                                        </div> -->

                                        <!-- <div class="col-md-12 button" style="float: right">
                                            
                                            <button id="add-use-history" class="btn btn-success" style="float: right; margin-bottom: 30px !important;">Add</button>

                                        </div> -->


                                        <div class="col-md-12">
                                            <table border="0" class='table table-hover table-use-history' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Use in the contaminated regions</td>
                                                        <td>Radioactive contamination test fail</td>
                                                        <td>Commercial use</td>
                                                        <!-- <td>Action</td> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <!-- <input maxlength="100" type="text" class="form-control" placeholder="Contaminated Region" name="use_contaminatedregions" id="use_contaminatedregions"> -->

                                                            <select name="use_contaminatedregions" class="form-control" id="use_contaminatedregions" required>
                                                              <option value="" selected disabled hidden>- Please Select -</option>
                                                              <option value="0" selected>Not reported</option>
                                                              <option value="1">Reported</option>
                                                            </select>

                                                        </td>
                                                        <td>
                                                            <!-- <input maxlength="100" type="text" class="form-control" placeholder="Radioactive Contamination" name="odometer_radioactivecontamination" id="odometer_radioactivecontamination"> -->

                                                            <select name="odometer_radioactivecontamination" class="form-control" id="odometer_radioactivecontamination" required>
                                                              <option value="" selected disabled hidden>- Please Select -</option>
                                                              <option value="0" selected>Not reported</option>
                                                              <option value="1">Reported</option>
                                                            </select>

                                                        </td>
                                                        <td>
                                                            <!-- <input maxlength="100" type="text" class="form-control" placeholder="Commercial Use" name="odometer_commercialuse" id="odometer_commercialuse">-->

                                                            <select name="odometer_commercialuse" class="form-control" id="odometer_commercialuse" required>
                                                              <option value="" selected disabled hidden>- Please Select -</option>
                                                              <option value="0" selected>Not reported</option>
                                                              <option value="1">Reported</option>
                                                            </select>
                                                        </td>
                                                        <!-- <td>
                                                            <button class="btn btn-success increment-odometer" type="button" id="step_5"><i class="glyphicon glyphicon-plus"></i>Submit</button> 

                                                        </td> -->
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>

                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" id="step_5" type="button">Next</button>
                                    </div>

                                  
                                </div>
                              
                            </div>

                            <div class="row setup-content" id="step-6">

                              
                                <div class="col-md-12">
                                    <h3> Detailed History</h3>
                                    <hr class="step-title">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Event date</td>
                                                        <td>Location</td>
                                                        <td>Odometer reading</td>
                                                        <td>Data source</td>
                                                        <td>Details</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data_detail_history as $data)
                                                    <tr>
                                                        <td>{{ $data->date }}</td>
                                                        <td>{{ $data->location }}</td>
                                                        <td>{{ $data->mileage }}</td>
                                                        <td>{{ $data->source }}</td>
                                                        <td>{{ $data->action }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>

                                            </table>
                                        </div>


                                        <div class="col-md-12">
                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Event date</td>
                                                        <td>Location</td>
                                                        <td>Odometer reading</td>
                                                        <td>Data source</td>
                                                        <td>Details</td>
                                                        <td>Action</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Event Date" name="detailed_eventdate" id="detailed_eventdate">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Location" name="detailed_location" id="detailed_location">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Odometer reading (Km)" name="detailed_odometerreading" id="detailed_odometerreading">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Data source" name="detailed_datasource" id="detailed_datasource">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Details" name="detailed_details" id="detailed_details">
                                                        </td>
                                                        <td>
                                                            
                                                            <button class="btn btn-success increment-odometer" type="button" id="step_6"><i class="glyphicon glyphicon-plus"></i>Submit</button> 
                                                        
                                                        </td>
                                                    </tr>
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>

                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="next_detail">Next</button>
                                    </div>

                                  
                                </div>
                              
                            </div>


                            <div class="row setup-content" id="step-7">
                              
                                <div class="col-md-12">
                                    <h3> Manufacturer Recall</h3>
                                    <hr class="step-title">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Date Reported</td>
                                                        <td>Data Source</td>
                                                        <td>Affected Part</td>
                                                        <td>Details</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data_manufacturer_recall as $data)
                                                    <tr>
                                                        <td>{{ $data->date }}</td>
                                                        <td>{{ $data->source }}</td>
                                                        <td>{{ $data->affected_part }}</td>
                                                        <td>{{ $data->details }}</td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>

                                            </table>
                                        </div>

                                        <div class="col-md-12">
                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Date Reported</td>
                                                        <td>Data Source</td>
                                                        <td>Affected Part</td>
                                                        <td>Details</td>
                                                        <td>Action</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Date Reported" name="manufacture_datereported" id="manufacture_datereported">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Data Source" name="manufacture_datasource" id="manufacture_datasource">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Affected Part" name="manufacture_affectedpart" id="manufacture_affectedpart">
                                                        </td>

                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Details" name="manufacture_details" id="manufacture_details">
                                                        </td>

                                                        <td class="odometer-clone">
                                                            <button class="btn btn-success increment-odometer" type="button" id="step_7"><i class="glyphicon glyphicon-plus"></i>Submit</button> 
                                                        </td>
                                                    </tr>

                                                    <tr></tr>


                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>

                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="next_manufacture">Next</button>
                                    </div>

                                  
                                </div>
                              
                            </div>

                            <div class="row setup-content" id="step-8">
                              
                                <div class="col-md-12">
                                    <h3> Vehicle Assessment</h3>
                                    <hr class="step-title">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <p class="red-color">+ Driver's seat</p>

                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Points</td>
                                                        <td>Goal average</td>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Points" name="driverseat_point" id="driverseat_point">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Goal average" name="driverseat_goalaverage" id="driverseat_goalaverage">
                                                        </td>
                                                        
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <p class="red-color">+ Front passenger's seat</p>

                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Points</td>
                                                        <td>Goal average</td>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Points" name="frontpassenger_point" id="frontpassenger_point">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Goal average" name="frontpassenger_goalaverage" id="frontpassenger_goalaverage">
                                                        </td>
                                                        

                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <p class="red-color">+ Braking Performance Tests</p>

                                            <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">

                                                <thead>
                                                    <tr>
                                                        <td>Dry Road</td>
                                                        <td>Wet Road</td>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Dry Road" name="dry_road" id="dry_road">
                                                        </td>
                                                        <td>
                                                            <input maxlength="100" type="text" class="form-control" placeholder="Wet Road" name="wet_road" id="wet_road">
                                                        </td>
                                                        

                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>    
                                        
                                    </div>

                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="step_8">Next</button>
                                    </div>

                                  
                                </div>
                              
                            </div>

                            <div class="row setup-content" id="step-9">
                              
                                <div class="col-md-12">
                                    <h3>Vehicle Specification</h3>
                                    <hr class="step-title">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">1st gear ratio</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="1st gear ratio" name="vehicle_firstgear" id="vehicle_firstgear">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label">3rd gear ratio</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="3rd gear ratio" name="vehicle_thirdgear" id="vehicle_thirdgear">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">5th gear ratio</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="5th gear ratio" name="vehicle_fifthgear" id="vehicle_fifthgear">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Additional notes</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Additional notes" name="vehicle_addnote" id="vehicle_addnote">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Body rear overhang</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Body rear overhang" name="vehicle_bodyrear" id="vehicle_bodyrear">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Chassis number embossing position</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Chassis number embossing position" name="vehicle_embossing_position" id="vehicle_embossing_position">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Cylinders</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Cylinders" name="vehicle_cylinder" id="vehicle_cylinder">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Electric engine type</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Electric engine type" name="vehicle_electric_engine_type" id="vehicle_electric_engine_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Electric engine maximum torque</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Electric engine maximum torque" name="vehicle_electric_engine_torque" id="vehicle_electric_engine_torque">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Engine maximum power</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Engine maximum power" name="vehicle_engine_maximum_power" id="vehicle_engine_maximum_power">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Engine model</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Engine model" name="vehicle_engine_model" id="vehicle_engine_model">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Front shaft weight</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Front shaft weight" name="vehicle_front_shaft_weight" id="vehicle_front_shaft_weight">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Front stabilizer type</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Front stabilizer type" name="vehicle_front_stabilizer_type" id="vehicle_front_stabilizer_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Front tread</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Front tread" name="vehicle_front_tread" id="vehicle_front_tread">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Fuel tank equipment</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Fuel tank equipment" name="vehicle_fuel_tank_equipment" id="vehicle_fuel_tank_equipment">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Height</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Height" name="vehicle_height" id="vehicle_height">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Main brakes type</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Main brakes type" name="vehicle_main_brakes_type" id="vehicle_main_brakes_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Maximum speed</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Maximum speed" name="vehicle_maximum_speed" id="vehicle_maximum_speed">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Minimum turning radius</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Minimum turning radius" name="vehicle_minimum_turning_radius" id="vehicle_minimum_turning_radius">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Model code</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Model code" name="vehicle_model_code" id="vehicle_model_code">
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label">Rear shaft weight</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Rear shaft weight" name="vehicle_rear_shaft_weight" id="vehicle_rear_shaft_weight">
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label">Rear stabilizer type</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Maximum speed" name="vehicle_rear_stabilizer_type" id="vehicle_rear_stabilizer_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Rear tread</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Rear tread" name="vehicle_rear_tread" id="vehicle_rear_tread">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Riding capacity</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Riding capacity" name="vehicle_riding_capacity" id="vehicle_riding_capacity">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Specification code</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Specification code" name="vehicle_specification_code" id="vehicle_specification_code">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Transmission type</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Transmission type" name="vehicle_transmission_type" id="vehicle_transmission_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Wheel alignment</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Wheel alignment" name="vehicle_wheel_alignment" id="vehicle_wheel_alignment">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Width</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Width" name="vehicle_width" id="vehicle_width">
                                                </div>


                                            </div>



                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">2nd gear ratio</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="2nd gear ratio" name="vehicle_2nd_gear_ratio" id="vehicle_2nd_gear_ratio">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">4th gear ratio</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="4th gear ratio" name="vehicle_4th_gear_ratio" id="vehicle_4th_gear_ratio">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">6th gear ratio</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="6th gear ratio" name="vehicle_6th_gear_ratio" id="vehicle_6th_gear_ratio">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label">Airbag position, capacity</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Airbag position, capacity" name="vehicle_airbag_position_capacity" id="vehicle_airbag_position_capacity">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Body type</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Body type" name="vehicle_body_type" id="vehicle_body_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Classification code</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Classification code" name="vehicle_classification_code" id="vehicle_classification_code">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Displacement</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Displacement" name="vehicle_displacement" id="vehicle_displacement">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Electric engine maximum output</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Electric engine maximum output" name="vehicle_electric_engine_maximum_output" id="vehicle_electric_engine_maximum_output">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Electric engine power</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Electric engine power" name="vehicle_electric_engine_power" id="vehicle_electric_engine_power">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Engine maximum torque</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Engine maximum torque" name="vehicle_engine_maximum_torque" id="vehicle_engine_maximum_torque">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Frame type</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Frame type" name="vehicle_frame_type" id="vehicle_frame_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Front shock absorber type</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Front shock absorber type" name="vehicle_front_shock_absorber_type" id="vehicle_front_shock_absorber_type">
                                                </div>

                                                

                                                <div class="form-group">
                                                    <label class="control-label">Front tires size</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Front tires size" name="vehicle_front_tires_size" id="vehicle_front_tires_size">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Fuel consumption</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Fuel consumption" name="vehicle_fuel_consumption" id="vehicle_fuel_consumption">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Grade</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Grade" name="vehicle_grade" id="vehicle_grade">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Length</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Length" name="vehicle_length" id="vehicle_length">
                                                </div>

                                                @php 
                                                $getMake = DB::table('r_vehicle_details')
                                                            ->where('id_vehicle', $id)
                                                            ->latest()
                                                            ->first();
                                                @endphp

                                                <div class="form-group">
                                                    <label class="control-label">Make</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Make" name="vehicle_make" id="vehicle_make" 
                                                    @php if(!empty($getMake->make)){ @endphp
                                                     value="{{$getMake->make}}"
                                                    @php
                                                    }
                                                    @endphp
                                                    >
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Minimum ground clearance</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Minimum ground clearance" name="vehicle_minimum_ground_clearance" id="vehicle_minimum_ground_clearance">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Model</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Model" name="vehicle_model" id="vehicle_model"
                                                    @php 
                                                    if(!empty($getMake->make)){ 
                                                        @endphp
                                                     value="{{$getMake->model}}"
                                                      @php
                                                    }
                                                    @endphp>




                                                    

                                                   

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Mufflers number</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Mufflers number" name="vehicle_mufflers_number" id="vehicle_mufflers_number">
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label">Rear shock absorber type</label>
                                                    <input maxlength="100" type="text"  class="form-control" placeholder="Rear shock absorber type" name="vehicle_rear_shock_absorber_type" id="vehicle_rear_shock_absorber_type">
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label">Rear tires size</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Rear tires size" name="vehicle_rear_tires_size" id="vehicle_rear_tires_size">
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label">Reverse ratio</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Reverse ratio" name="vehicle_reverse_ratio" id="vehicle_reverse_ratio">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Side brakes type</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Side brakes type" name="vehicle_side_brakes_type" id="vehicle_side_brakes_type">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Stopping distance</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Stopping distance" name="vehicle_stopping_distance" id="vehicle_stopping_distance">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Weight</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Weight" name="vehicle_weight" id="vehicle_weight">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Wheelbase</label>
                                                    <input maxlength="100" type="text" class="form-control" placeholder="Wheelbase" name="vehicle_wheelbase" id="vehicle_wheelbase">
                                                </div>

                                                


                                            </div>
                                          
                                        </div>
                                    
                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="step_9">Next</button>
                                    </div>
                                    
                                </div>
                              
                            </div>

                            <div class="row setup-content" id="step-10">
                              
                                <div class="col-md-12">
                                    <h3> Auction Data</h3>
                                    <hr class="step-title">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Date</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="" name="auction_date" id="auction_date">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Auction name</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Auction name" name="auction_auctionname" id="auction_auctionname">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Make</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Make" name="auction_make" id="auction_make">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Reg. year</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Reg. year" name="auction_regyear" id="auction_regyear">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Displacement (cc)</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Displacement (cc)" name="auction_displacement" id="auction_displacement">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Color</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Color" name="auction_color" id="auction_color">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Result</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Result" name="auction_result" id="auction_result">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Problem type</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Problem type" name="auction_problemtype" id="auction_problemtype">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Contaminated</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Contaminated" name="auction_contaminated" id="auction_contaminated">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            

                                            <div class="form-group">
                                                <label class="control-label">Lot#</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Lot#" name="auction_lot" id="auction_lot">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Region</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Region" name="auction_region" id="auction_region">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Model</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Model" name="auction_model" id="auction_model">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Mileage (km)</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Mileage (km)" name="auction_mileage" id="auction_mileage">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Transmission</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Transmission" name="auction_transmission" id="auction_transmission">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Model code</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Model code" name="auction_model_code" id="auction_model_code">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Auction grade</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Auction grade" name="auction_auctiongrade" id="auction_auctiongrade">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Problem scale</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Problem scale" name="auction_problemscale" id="auction_problemscale">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Airbag</label>
                                                <input maxlength="100" type="text" class="form-control" placeholder="Airbag" name="auction_airbag" id="auction_airbag">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="button" style="float: right">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="step_10">Next</button>
                                    </div>

                                  
                                </div>
                              
                            </div>


                            <div class="row setup-content" id="step-11">
                              
                                <div class="col-md-12">
                                    <h3> PHOTOS AND AUCTION SHEETS</h3>
                                    <hr class="step-title">

                                    <div class="col-md-12">
                                        <table class='table table-bordered'>
                                            <thead>
                                                <tr>
                                                    <td width="5%">No</td>
                                                    <td>Photos</td>
                                                    <td>Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php $i = 1 ?>
                                               @foreach($data_aution_image as $data)
                                                <tr>
                                                    <td width="5%" align="center">{{ $i++ }}</td>
                                                    <td>
                                                        <a href="{{url('/')}}/extra_report/{{$id}}/{{$data->image}}" class="btn bg-light-blue btn-lg" target='_blank'><img width='110px' height='110px' src="{{ asset('extra_report/'.$id.'/'.$data->image) }}"></img></a>
                                                    </td>
                                                    <td>
                                                        <a href="{{url('ExtraReport/deletephoto/'.$data->id)}}" class="btn btn-danger btn-sm">Delete</a>
                                                    </td>
                                                </tr>

                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                    </div>



                                    <form id="uploadphotos" action="{{url('ExtraReport/extra-report/uploadphoto/'.$id)}}" method="post" enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                        <input type="file" name="photos_vehicle" id="changePicture1" class="hidden-input test1 form-control" onchange="this.form.submit()">
                                        <input type="hidden">

                                    </form>

                                    
                                  

                                <form id="uploadphotos" action="{{url('ExtraReport/open-verify/store/'.$id)}}" method="post" enctype="multipart/form-data">

                                {{ csrf_field() }}


                                    <div class="button" style="float: right; margin-top: 30px;">
                                        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                        <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
                                    </div>


                                </form>
                               
                                    
                                </div>         
                              
                            </div>

                           

  
                    </div>
                </div>
            </div>


            </div>
          </div>



@endsection





@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/jquery.dataTables.min.css" integrity="sha512-1k7mWiTNoyx2XtmI96o+hdjP8nn0f3Z2N4oF/9ZZRgijyV4omsKOXEnqL1gKQNPy2MTSP9rIEWGcH/CInulptA==" crossorigin="anonymous" />



    <!-- Datepicker for Month and Year -->

    <!-- Datepicker for Normally (Date, Month and Year) --> 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">
        $('#manufacture_date').datepicker({
          format: 'yyyy-mm',
          icons: {
            time: 'fa fa-time',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
          },
          startView: "months",
          minViewMode: "months"
        });

        $('#collision_date_reported').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#malfunction_date_reported').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#theft_date_reported').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#firedamage_date_reported').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#waterdamage_date_reported').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#haildamage_date_reported').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#odometer_datereported').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#auction_date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#detailed_eventdate').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#manufacture_datereported').datepicker({
            format: 'yyyy-mm-dd'
        });


        $('#auction_regyear').datepicker({
            format: 'yyyy',
            icons: {
            time: 'fa fa-time',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
          },
          startView: "years",
          minViewMode: "years"
        });

        
    </script>

   



<script type="text/javascript">
    $(function() {
  $("#example").DataTable();

  // Premade test data, you can also use your own
  //var testDataUrl = "https://raw.githubusercontent.com/chennighan/RapidlyPrototypeJavaScript/master/lesson4/data.json";

  //get/ExtraReport/extra-report/step-4
  var testDataUrl = "{{ url('/get/ExtraReport/extra-report/step-4') }}"; 

  $("#loadData").click(function() {
    loadData();
  });

  function loadData() {
    $.ajax({
      type: 'GET',
      url: testDataUrl,
      contentType: "text/plain",
      dataType: 'json',
      success: function (data) {
        myJsonData = data;
        populateDataTable(myJsonData);
      },
      error: function (e) {
        console.log("There was an error with your request...");
        console.log("error: " + JSON.stringify(e));
      }
    });
  }

  // populate the data table with JSON data
  function populateDataTable(data) {
    console.log("populating data table...");
    // clear the table before populating it with more data
    $("#example").DataTable().clear();
    var length = Object.keys(data.customers).length;
    for(var i = 1; i < length+1; i++) {
      var customer = data.customers['customer'+i];

      // You could also use an ajax property on the data table initialization
      $('#example').dataTable().fnAddData( [
        customer.date,
        customer.source,
        customer.mileage
      ]);
    }
  }
})();

</script>

<script type="text/javascript">
    var jsonData = '[{"rank":"9","content":"ssson","UID":"5"},{"rank":"6","content":"Tala","UID":"6"}]';

    $.ajax({
        url: '/echo/json/',
        type: 'POST',
        data: {
            json: jsonData
        },
        success: function (response) {

            response = $.parseJSON(response);

$(function() {
    $.each(response, function(i, item) {
        var $tr = $('<tr>').append(
            $('<td>').text(item.rank),
            $('<td>').text(item.content),
            $('<td>').text(item.UID)
        ); //.appendTo('#records_table');
        console.log($tr.wrap('<p>').html());
    });
});


            var trHTML = '';
            $.each(response, function (i, item) {
                trHTML += '<tr><td>' + item.date + '</td><td>' + item.location + '</td><td>' + item.source + '</td></tr>';
            });
            $('#records_table').append(trHTML);
        }
    });
</script>


<script type="text/javascript">

    $("#add-use-history").on("click", function () {
        var counter = 0;
        var newRow = $("<tr>");
        var cols = "";


        cols += '<td><input type="text" maxlength="50" class="form-control" name="use_contaminatedregions[]" placeholder="Contaminated Region"/></td>';
        cols += '<td><input type="text" class="form-control" maxlength="130" name="odometer_radioactivecontamination[]" placeholder="Radioactive Contamination"/></td>';
        cols += '<td><input type="text" class="form-control" maxlength="130" name="odometer_commercialuse[]" placeholder="Commercial Use"/></td>';
        
        cols += '<td class=" text-center"><button class="delete-use-history btn btn-danger"><i class="fa fa-times"></i></button></td>';
        newRow.append(cols);

        $("table.table-use-history").append(newRow);
            counter++;


        $("table.table-use-history").on("click", ".delete-use-history", function (event) {
            $(this).closest("tr").remove();       
            counter -= 1
        });    
    });

</script>

   

    <script>
        $(document).ready(function () {
            $('#listingUsers').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/get/ExtraReport/extra-report/step-4') }}",
                columns: [
                    {data: 0, name: 'date'},
                    {data: 1, name: 'source'},
                    {data: 2, name: 'mileage'}
                ]
            });
        });
    </script>

    


    <!--- change Uppercase -->
    <script type="text/javascript">
        $(function() {
            $('input').keyup(function() {
                this.value = this.value.toLocaleUpperCase();
            });
        });
    </script>
    <!-- End change Uppercase-->


    <!--- clone value -->
    <script type="text/javascript">
        $("money").on("keyup", function() {
            $("input.money-cloned").val(this.value);
        });
    </script>
    <!-- End clone value -->

    <!--- clone value & form -->
    <script type="text/javascript">

        $("input.money").on("keyup", function() {
            $("input.money-cloned").val(this.value);
        });

        
    </script>
     <!-- End clone value -->

    

    <!-- Button add increment 
    <script type="text/javascript">
        $("#increment_odometer").click(function () {
            var html = $(".form-odometer").clone(true,true).addClass("cloned").appendTo("#wrap_odometer");
            $("#wrap_odometer").after(html);
        });
    </script> -->
    <!-- End Button add increment -->


<!-- Button add increment -->
<script type="text/javascript">
    $(document).ready(function() {
      

      $(".add-odometer").click(function(){ 
          var html = $(".control-group-odometer").html();
          $("#wrap_odometer").after(html);
      });

      $("body").on("click",".btn-danger-odometer",function(){ 
          $(this).parents(".control-group-odometer").remove();
      });
    });
</script>
<!-- Button end add Odometer -->


<!-- Button add Image -->
<script type="text/javascript">
    $(document).ready(function() {
      $(".add-image").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".remove-image",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
<!-- End button add Image -->

<!-- Image Upload -->
<script type="text/javascript">
    function test1() {
        var form = document.getElementById('uploadphotos');
        form.submit();
    };
</script>
<!-- End Image -->


<script type="text/javascript">
    
    $('input.money').on('blur', function() {
         if (document.getElementById('averageprice').value == '') 
            { this.value = parseFloat("x,xx".replace(/,/g, "")) || 0.00 }

        else{
          const value = this.value.replace(/,/g, '');
          this.value = parseFloat(value).toLocaleString('en-US', {
            style: 'decimal',
            maximumFractionDigits: 2,
            minimumFractionDigits: 2
          });
       }
    });
   
</script>

<!-- Get data from table -->
<script type="text/javascript">

    $( "#xxx" ).click(function() {
                         
        $.ajax(
           {
              type:"GET",
              url:"{{ url('/get/ExtraReport/extra-report/step-4') }}",
              data:"userId=12345&userName=test"
           },
           success: function(data){
             alert('successful');
           }
        );
                  
    });
</script>
<!-- End get data from table -->





<script type="text/javascript">

    $( "#step_1" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var chassis_number      = $('#chassis_number').val();

        var manufacture_date     = $('#manufacture_date').val();
        var make                 = $('#make').val();
        var model                = $('#model').val();  
        var body                = $('#body').val();  
        var grade                = $('#grade').val(); 
        var vehicledetails_enginedrive                = $('#vehicledetails_enginedrive').val();  
        var vehicledetails_transmission                = $('#vehicledetails_transmission').val();       
          
        

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-1') }}",
            data: { _token : _token, id_vehicle: id_vehicle, chassis_number: chassis_number, manufacture_date: manufacture_date, make: make, model: model, body:body, grade:grade,  vehicledetails_enginedrive:vehicledetails_enginedrive, vehicledetails_transmission:vehicledetails_transmission},
            cache: false,
            dataType:'json',
            beforeSend: function () {

            },
            success: function (data) {
                

            },
            error: function () {
            }
        });              
    });

</script>

 


<script type="text/javascript">
    $( "#step_2" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var vehicle_titleinformation      = $('#vehicle_titleinformation').val();
        var vehicle_accidentrepair      = $('#vehicle_accidentrepair').val();
        var vehicle_odometerrollback      = $('#vehicle_odometerrollback').val();
        var vehicle_manufacturerrecall      = $('#vehicle_manufacturerrecall').val();
        var vehicle_safetygrade      = $('#vehicle_safetygrade').val();
        var vehicle_contaminationrisk      = $('#vehicle_contaminationrisk').val();
        var averageprice      = $('#averageprice').val();
        var currency     = $('#currency').val();               

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-2') }}",
            data: { _token : _token, id_vehicle: id_vehicle,  averageprice: averageprice, currency: currency, vehicle_contaminationrisk: vehicle_contaminationrisk, vehicle_safetygrade: vehicle_safetygrade, vehicle_manufacturerrecall: vehicle_manufacturerrecall, vehicle_odometerrollback: vehicle_odometerrollback,  vehicle_accidentrepair: vehicle_accidentrepair, vehicle_titleinformation: vehicle_titleinformation},
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {
                
            },
            error: function () {
            }
        });              
    });
</script>


<script type="text/javascript">
    $( "#step_3" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();

        var collision_reported      = $('#collision_reported').val();
        var collision_date_reported      = $('#collision_date_reported').val();
        var collision_data_source      = $('#collision_data_source').val();
        var collision_detail      = $('#collision_detail').val();
        var collision_air_bag      = $('#collision_air_bag').val();


        var malfunction_reported      = $('#malfunction_reported').val();
        var malfunction_date_reported      = $('#malfunction_date_reported').val();
        var malfunction_data_source     = $('#malfunction_data_source').val();   
        var malfunction_detail      = $('#malfunction_detail').val();
        var malfunction_air_bag     = $('#malfunction_air_bag').val();


        var theft_reported      = $('#theft_reported').val();
        var theft_date_reported      = $('#theft_date_reported').val();
        var theft_data_source     = $('#theft_data_source').val();   
        var theft_detail      = $('#theft_detail').val();
        var theft_air_bag     = $('#theft_air_bag').val(); 

        var firedamage_reported      = $('#firedamage_reported').val();
        var firedamage_date_reported      = $('#firedamage_date_reported').val();
        var firedamage_data_source     = $('#firedamage_data_source').val();   
        var firedamage_detail      = $('#firedamage_detail').val();
        var firedamage_air_bag     = $('#firedamage_air_bag').val();


        var waterdamage_reported      = $('#waterdamage_reported').val();
        var waterdamage_date_reported      = $('#waterdamage_date_reported').val();
        var waterdamage_data_source     = $('#waterdamage_data_source').val();   
        var waterdamage_detail      = $('#waterdamage_detail').val();
        var waterdamage_air_bag     = $('#waterdamage_air_bag').val();


        var haildamage_reported      = $('#haildamage_reported').val();
        var haildamage_date_reported      = $('#haildamage_date_reported').val();
        var haildamage_data_source     = $('#haildamage_data_source').val();   
        var haildamage_detail      = $('#haildamage_detail').val();
        var haildamage_air_bag     = $('#haildamage_air_bag').val();  


        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-3') }}",
            data: { _token : _token, id_vehicle: id_vehicle, 
            collision_reported:collision_reported,
            collision_date_reported:collision_date_reported,
            collision_data_source :collision_data_source,
            collision_detail      :collision_detail,
            collision_air_bag      :collision_air_bag,

            malfunction_reported      :malfunction_reported,
            malfunction_date_reported      :malfunction_date_reported,
            malfunction_data_source     :malfunction_data_source,   
            malfunction_detail      :malfunction_detail,
            malfunction_air_bag     :malfunction_air_bag,

            theft_reported      :theft_reported,
            theft_date_reported      :theft_date_reported,
            theft_data_source     :theft_data_source,   
            theft_detail      :theft_detail,
            theft_air_bag     :theft_air_bag, 

            firedamage_reported      :firedamage_reported,
            firedamage_date_reported      :firedamage_date_reported,
            firedamage_data_source     :firedamage_data_source,   
            firedamage_detail      :firedamage_detail,
            firedamage_air_bag     :firedamage_air_bag,

            waterdamage_reported      :waterdamage_reported,
            waterdamage_date_reported      :waterdamage_date_reported,
            waterdamage_data_source     :waterdamage_data_source,   
            waterdamage_detail      :waterdamage_detail,
            waterdamage_air_bag     :waterdamage_air_bag,

            haildamage_reported      :haildamage_reported,
            haildamage_date_reported      :haildamage_date_reported,
            haildamage_data_source     :haildamage_data_source,   
            haildamage_detail      :haildamage_detail,
            haildamage_air_bag     :haildamage_air_bag, 
            },
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {
                
            },
            error: function () {
            }
        });              
    });
</script>


<script type="text/javascript">
    $( "#step_4" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var odometer_datereported      = $('#odometer_datereported').val();
        var odometer_datasource      = $('#odometer_datasource').val();
        var odometer_odometerreading      = $('#odometer_odometerreading').val();
                       

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-4') }}",
            data: { _token : _token, id_vehicle: id_vehicle,  odometer_datereported: odometer_datereported, odometer_datasource: odometer_datasource, odometer_odometerreading: odometer_odometerreading },
            cache: false,
            beforeSend: function () {

            },
            success: function (results) {

               setInterval('location.reload()', 1000); 

                console.log(results);

            },
            error: function () {
            }
        });              
    });

</script>


<!-- Next Function Update Step Data -->
<script type="text/javascript">
    $( "#next_odometer" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/next-odometer') }}",
            data: { _token : _token, id_vehicle: id_vehicle },
            cache: false,
            beforeSend: function () {

            },
            success: function (results) {

            },
            error: function () {
            }
        });              
    });

    $( "#next_use" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/next-use') }}",
            data: { _token : _token, id_vehicle: id_vehicle },
            cache: false,
            beforeSend: function () {

            },
            success: function (results) {

            },
            error: function () {
            }
        });              
    });

     $( "#next_detail" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/next-detail') }}",
            data: { _token : _token, id_vehicle: id_vehicle },
            cache: false,
            beforeSend: function () {

            },
            success: function (results) {

            },
            error: function () {
            }
        });              
    });


    $( "#next_manufacture" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/next-manufacture') }}",
            data: { _token : _token, id_vehicle: id_vehicle },
            cache: false,
            beforeSend: function () {

            },
            success: function (results) {

            },
            error: function () {
            }
        });              
    });


     

</script>
<!-- End Next Function -->


<script type="text/javascript">

    $( "#step_5" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var use_contaminatedregions      = $('#use_contaminatedregions').val();
        var odometer_radioactivecontamination      = $('#odometer_radioactivecontamination').val();
        var odometer_commercialuse     = $('#odometer_commercialuse').val();
                   


        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-5') }}",
            data: { 
                _token : _token, 
                id_vehicle: id_vehicle,  
                use_contaminatedregions: use_contaminatedregions,
                odometer_radioactivecontamination: odometer_radioactivecontamination,
                odometer_commercialuse: odometer_commercialuse
            },
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {

               
                setInterval('location.reload()', 1000); 
                console.log(data);

            },
            error: function () {
            }
        });              
    });
</script>


<script type="text/javascript">
    
    $( "#step_6" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var detailed_eventdate      = $('#detailed_eventdate').val();
        var detailed_location      = $('#detailed_location').val();
        var detailed_odometerreading      = $('#detailed_odometerreading').val();
        var detailed_details = $('#detailed_details').val();
        var detailed_datasource = $('#detailed_datasource').val();
                   

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-6') }}",
            data: { 
                _token : _token, 
                id_vehicle: id_vehicle,  
                detailed_eventdate: detailed_eventdate,
                detailed_location: detailed_location,
                detailed_odometerreading: detailed_odometerreading,
                detailed_datasource: detailed_datasource,
                detailed_details: detailed_details

            },
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {

                
                setInterval('location.reload()', 1000); 
                console.log(data);
            },
            error: function () {
            }
        });              
    });
</script>



<script type="text/javascript">
    $( "#step_7" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var manufacture_datereported      = $('#manufacture_datereported').val();

        var manufacture_datasource     = $('#manufacture_datasource').val();
        var manufacture_affectedpart     = $('#manufacture_affectedpart').val();
        var manufacture_details     = $('#manufacture_details').val();
       
        

        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-7') }}",
            data: { _token : _token, id_vehicle: id_vehicle,  manufacture_datereported: manufacture_datereported, manufacture_datasource: manufacture_datasource, manufacture_affectedpart: manufacture_affectedpart, manufacture_details:manufacture_details},
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {
                setInterval('location.reload()', 1000); 
                console.log(data);
            },
            error: function () {
            }
        });              
    });
</script>

<script type="text/javascript">
    $( "#step_8" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var driverseat_point      = $('#driverseat_point').val();

        var driverseat_goalaverage     = $('#driverseat_goalaverage').val();

        var frontpassenger_point     = $('#frontpassenger_point').val();
        var frontpassenger_goalaverage     = $('#frontpassenger_goalaverage').val();
        var dry_road     = $('#dry_road').val();
        var wet_road     = $('#wet_road').val();
       
        
        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-8') }}",
            data: { _token : _token, id_vehicle: id_vehicle,  driverseat_point: driverseat_point, 
                driverseat_goalaverage: driverseat_goalaverage, frontpassenger_point: frontpassenger_point, frontpassenger_goalaverage:frontpassenger_goalaverage, dry_road: dry_road, wet_road: wet_road},
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {
                
            },
            error: function () {
            }
        });              
    });
</script>


<script type="text/javascript">
    $( "#step_9" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var vehicle_firstgear      = $('#vehicle_firstgear').val();
        var vehicle_thirdgear     = $('#vehicle_thirdgear').val();
        var vehicle_fifthgear     = $('#vehicle_fifthgear').val();
        var vehicle_addnote     = $('#vehicle_addnote').val();
        var vehicle_bodyrear     = $('#vehicle_bodyrear').val();
        var vehicle_embossing_position     = $('#vehicle_embossing_position').val();
        var vehicle_cylinder = $('#vehicle_cylinder').val();
        var vehicle_electric_engine_type = $('#vehicle_electric_engine_type').val();
        var vehicle_electric_engine_torque = $('#vehicle_electric_engine_torque').val();
        var vehicle_engine_maximum_power = $('#vehicle_engine_maximum_power').val();
        var vehicle_engine_model = $('#vehicle_engine_model').val();
        var vehicle_front_shaft_weight = $('#vehicle_front_shaft_weight').val();
        var vehicle_front_stabilizer_type = $('#vehicle_front_stabilizer_type').val();
        var vehicle_front_tread = $('#vehicle_front_tread').val();
        var vehicle_fuel_tank_equipment = $('#vehicle_fuel_tank_equipment').val();
        var vehicle_height = $('#vehicle_height').val();
        var vehicle_main_brakes_type = $('#vehicle_main_brakes_type').val();
        var vehicle_maximum_speed = $('#vehicle_maximum_speed').val();
        var vehicle_minimum_turning_radius = $('#vehicle_minimum_turning_radius').val();
        var vehicle_model_code = $('#vehicle_model_code').val();
        var vehicle_rear_shaft_weight = $('#vehicle_rear_shaft_weight').val();
        var vehicle_rear_stabilizer_type = $('#vehicle_rear_stabilizer_type').val();
        var vehicle_rear_tread = $('#vehicle_rear_tread').val();
        var vehicle_riding_capacity = $('#vehicle_riding_capacity').val();
        var vehicle_specification_code = $('#vehicle_specification_code').val();
        var vehicle_transmission_type = $('#vehicle_transmission_type').val();
        var vehicle_wheel_alignment = $('#vehicle_wheel_alignment').val();
        var vehicle_width = $('#vehicle_width').val();
        var vehicle_2nd_gear_ratio = $('#vehicle_2nd_gear_ratio').val();
        var vehicle_4th_gear_ratio = $('#vehicle_4th_gear_ratio').val();
        var vehicle_6th_gear_ratio = $('#vehicle_6th_gear_ratio').val();
        var vehicle_airbag_position_capacity = $('#vehicle_airbag_position_capacity').val();
        var vehicle_body_type = $('#vehicle_body_type').val();
        var vehicle_classification_code = $('#vehicle_classification_code').val();
        var vehicle_displacement = $('#vehicle_displacement').val();
        var vehicle_electric_engine_maximum_output = $('#vehicle_electric_engine_maximum_output').val();
        var vehicle_electric_engine_power = $('#vehicle_electric_engine_power').val();
        var vehicle_engine_maximum_torque = $('#vehicle_engine_maximum_torque').val();
        var vehicle_frame_type = $('#vehicle_frame_type').val();
        var vehicle_front_shock_absorber_type = $('#vehicle_front_shock_absorber_type').val();
        var vehicle_front_tires_size = $('#vehicle_front_tires_size').val();
        var vehicle_fuel_consumption = $('#vehicle_fuel_consumption').val();
        var vehicle_grade = $('#vehicle_grade').val();
        var vehicle_length = $('#vehicle_length').val();
        var vehicle_make = $('#vehicle_make').val();
        var vehicle_minimum_ground_clearance = $('#vehicle_minimum_ground_clearance').val();
        var vehicle_model = $('#vehicle_model').val();
        var vehicle_mufflers_number = $('#vehicle_mufflers_number').val();
        var vehicle_rear_shock_absorber_type = $('#vehicle_rear_shock_absorber_type').val();
        var vehicle_rear_tires_size = $('#vehicle_rear_tires_size').val();
        var vehicle_reverse_ratio = $('#vehicle_reverse_ratio').val();
        var vehicle_side_brakes_type = $('#vehicle_side_brakes_type').val();
        var vehicle_stopping_distance = $('#vehicle_stopping_distance').val();
        var vehicle_weight = $('#vehicle_weight').val();
        var vehicle_wheelbase = $('#vehicle_wheelbase').val();

       
        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-9') }}",
            data: { _token : _token, id_vehicle: id_vehicle,  
                vehicle_firstgear: vehicle_firstgear,  vehicle_thirdgear: vehicle_thirdgear, vehicle_fifthgear: vehicle_fifthgear,
                vehicle_addnote: vehicle_addnote,
                vehicle_bodyrear: vehicle_bodyrear,
                vehicle_embossing_position: vehicle_embossing_position,
                vehicle_cylinder: vehicle_cylinder,
                vehicle_electric_engine_type: vehicle_electric_engine_type, 
                vehicle_electric_engine_torque: vehicle_electric_engine_torque,
                vehicle_engine_maximum_power: vehicle_engine_maximum_power, 
                vehicle_engine_model: vehicle_engine_model,
                vehicle_front_shaft_weight: vehicle_front_shaft_weight,
                vehicle_front_stabilizer_type: vehicle_front_stabilizer_type,
                vehicle_front_tread: vehicle_front_tread,
                vehicle_fuel_tank_equipment: vehicle_fuel_tank_equipment,
                vehicle_height: vehicle_height,
                vehicle_main_brakes_type: vehicle_main_brakes_type,
                vehicle_maximum_speed: vehicle_maximum_speed,
                vehicle_minimum_turning_radius: vehicle_minimum_turning_radius,
                vehicle_model_code: vehicle_model_code,
                vehicle_rear_shaft_weight: vehicle_rear_shaft_weight,
                vehicle_rear_stabilizer_type: vehicle_rear_stabilizer_type,
                vehicle_rear_tread: vehicle_rear_tread,
                vehicle_riding_capacity: vehicle_riding_capacity,
                vehicle_specification_code: vehicle_specification_code,
                vehicle_transmission_type: vehicle_transmission_type,
                vehicle_wheel_alignment: vehicle_wheel_alignment,
                vehicle_width: vehicle_width,
                vehicle_2nd_gear_ratio: vehicle_2nd_gear_ratio,
                vehicle_4th_gear_ratio: vehicle_4th_gear_ratio,
                vehicle_6th_gear_ratio: vehicle_6th_gear_ratio,
                vehicle_airbag_position_capacity: vehicle_airbag_position_capacity,
                vehicle_body_type: vehicle_body_type,
                vehicle_classification_code: vehicle_classification_code,
                vehicle_displacement: vehicle_displacement,
                vehicle_electric_engine_maximum_output: vehicle_electric_engine_maximum_output,
                vehicle_electric_engine_power: vehicle_electric_engine_power,
                vehicle_engine_maximum_torque: vehicle_engine_maximum_torque,
                vehicle_frame_type: vehicle_frame_type,
                vehicle_front_shock_absorber_type: vehicle_front_shock_absorber_type,
                vehicle_front_tires_size: vehicle_front_tires_size,
                vehicle_fuel_consumption: vehicle_fuel_consumption,
                vehicle_grade: vehicle_grade,
                vehicle_length: vehicle_length,
                vehicle_make : vehicle_make, 
                vehicle_minimum_ground_clearance :vehicle_minimum_ground_clearance, 
                vehicle_model: vehicle_model,
                vehicle_mufflers_number: vehicle_mufflers_number,
                vehicle_rear_shock_absorber_type: vehicle_rear_shock_absorber_type,
                vehicle_rear_tires_size : vehicle_rear_tires_size,
                vehicle_reverse_ratio : vehicle_reverse_ratio,
                vehicle_side_brakes_type: vehicle_side_brakes_type,
                vehicle_stopping_distance: vehicle_stopping_distance,
                vehicle_weight: vehicle_weight,
                vehicle_wheelbase: vehicle_wheelbase

            },
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {
                
            },
            error: function () {
            }
        });              
    });
</script>


<script type="text/javascript">
    $( "#step_10" ).click(function() {
        var _token               = $('#token').val();
        var id_vehicle           = $('#id_vehicle').val();
        var auction_date = $('#auction_date').val();
        var auction_auctionname = $('#auction_auctionname').val();
        var auction_make = $('#auction_make').val();
        var auction_regyear = $('#auction_regyear').val();
        var auction_displacement = $('#auction_displacement').val();
        var auction_color = $('#auction_color').val();
        var auction_result = $('#auction_result').val();
        var auction_problemtype = $('#auction_problemtype').val();
        var auction_contaminated = $('#auction_contaminated').val();
        var auction_lot = $('#auction_lot').val();
        var auction_region = $('#auction_region').val();
        var auction_model = $('#auction_model').val();
        var auction_mileage = $('#auction_mileage').val();
        var auction_transmission = $('#auction_transmission').val();
        var auction_model_code = $('#auction_model_code').val();
        var auction_auctiongrade = $('#auction_auctiongrade').val();
        var auction_problemscale= $('#auction_problemscale').val();
       
        
        $.ajax({
            type: "POST",
            url: "{{ url('ExtraReport/extra-report/step-10') }}",
            data: { 
                _token : _token, 
                id_vehicle: id_vehicle,
                auction_date: auction_date,
                auction_auctionname: auction_auctionname,
                auction_make: auction_make,
                auction_regyear: auction_regyear,
                auction_displacement: auction_displacement,
                auction_color: auction_color,
                auction_result: auction_result,
                auction_problemtype: auction_problemtype,
                auction_contaminated: auction_contaminated,
                auction_lot: auction_lot,
                auction_region: auction_region,
                auction_model: auction_model,
                auction_mileage: auction_mileage,
                auction_transmission: auction_transmission,
                auction_model_code: auction_model_code,
                auction_auctiongrade: auction_auctiongrade,
                auction_problemscale: auction_problemscale



            },
            cache: false,
            beforeSend: function () {

            },
            success: function (data) {
                
            },
            error: function () {
            }
        });              
    });
</script>


<!-- Image Submit --> 
<script type="text/javascript">
    function test1() {
        var form = document.getElementById('photos');
        form.submit();
    };
</script>




<script type="text/javascript">
    
    $(function () {
        'use strict';

        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'ExtraReport/extra-report/photo';

        $('#photos').fileupload({
            url: url,
            dataType: 'json',
            success: function ( data) {
                
                var fi =  data.file;
                if(fi =='error'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='ext'){
                    alert("Only '.pdf,.png,.jpeg,.jpg' formats are allowed.");
                }
                else if(fi =='size'){
                    alert("Please upload file below 44 MB");
                }
                else{
                    
                    alert('Successfully upload document');
                    window.location.href = window.location.href;
                }
             
        }
    
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
       
    });
</script>
<!-- End Image Submit --> 


<!-- Next, Previous dan Submit Step -->

<script type="text/javascript">
	$(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn'),
  		  allPrevBtn = $('.prevBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });
  
  allPrevBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

          prevStepWizard.removeAttr('disabled').trigger('click');
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>



    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model').children().clone();
      
        $('#make').change(function() {
          $('#model').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model').append(this);
                 }
            });
          $('#model').val('');
        });
    });
    </script>



@endpush