@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Sent Vehicle (MN) {{config('autocheck.extra_report')}}</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                    <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/ExtraReport/generate-and-sent')}}" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            @if($me == 'VER')
                            <!-- <input type="submit" value="Generate & Send Vehicle " class="mb-2 btn btn-primary mr-2 btn-lg" style="float: right;"> -->


                        <div class="input-group" style="margin-bottom: 30px; margin-top: 30px; float: right;" >
                          <select class="custom-select" id="inputGroupSelect04" name="partner" required="" onchange="showDiv(this)">
                            <option value="" selected disabled hidden="">--Please Select Partner--</option>
                            @foreach($par_overseas as $partner)
                            <option value="{{$partner->email}}">{{$partner->email}}</option>
                            @endforeach
                          </select>
                          <div class="input-group-append">
                            <input type="submit" value="Generate & Send Vehicle" class="btn btn-primary" >
                          </div>
                        </div>


                       
                        @endif



		                <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle </th>
                                  <th>Date Request</th>
                                  <th>Status</th>
                                  <th>Req From</th>
				                          <th>Type Report</th>

                                  <th align="center">Detail</th>
                                  <th>Action <input type="checkbox" id="checkAll"></th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}} </td>
                                  
                                  <td>{{$data->created_at}}</td>
                                  

                                  <!-- Status -->
                                  <td align="center">
                                    @if($me == 'VER')
                                      @if($data->status == '10' OR $data->status == '20')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">New</i>
                                      @else
                                      @endif
                                    @endif

                                    @if($me == 'AD')
                                      @if($data->is_sent == 1)
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Send</i>
                                      @else
                                      <i href="#" class="card-post__category badge badge-pill badge-danger">Not Yet</i>
                                      @endif

                                    @endif
                                  </td>

                                  <!-- End of API Status -->

                                  <td><!-- Req from -->
                                    @if(!empty($data->request_by->name))
                                    {{$data->request_by->name}}

                                    <i href="#" class="card-post__category badge badge-pill badge-danger">{{$data->request_by->group_by}}</i>

                                    @endif


                                  </td><!-- End Req from -->

				                          <td align="center"> <!-- Type Report -->
					
					
                                    @if(!empty($data->type_report))
					
                              				@if($data->type_report == "3")
                              				<i href="#" class="card-post__category badge badge-pill badge-warning"> {{config('autocheck.half_report')}} </i>
                                        
                                    @elseif($data->type_report == "4")
                            				<i href="#" class="card-post__category badge badge-pill badge-danger"> {{config('autocheck.full_report')}} </i>
                            				
                            				@elseif($data->type_report == "2")
                            				<i href="#" class="card-post__category badge badge-pill badge-success"> {{config('autocheck.extra_report')}} </i>
                            				@endif

                                  @endif
				                        </td>

                                  <!-- Action -->
                                  <td align="center">

                                    

                                  <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1{{$data->id}}">
                                      <i class="material-icons">details</i>
                                    </button>

                                    


                                  </td>
                                  <!-- History Search -->
                                  <td align="center">
                                      @if($me == 'VER')
                                      <input type="checkbox"  name="id[]" value="{{$data->id_vehicle}}" class="friends" />

                                      <input type="hidden"  name="ci[]" value="{{$data->id}}">
                                      
                                      <input type="hidden"  name="ktp[]" value="{{$data->id_vehicle}}">

                                      <input type="hidden"  name="ids[]" value="">
                                      @endif



                                       @if($me == 'AD')

                                          <div class="custom-control custom-toggle custom-toggle-sm mb-1">
                                            @if($data->is_sent == 1)
                                            <a href="{{url('/update-send/'.$data->id)}}" class="lever switch-col-red"> 
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1">Unsend</button>
                                            </a>
                                              
                                            @else
                                              
                                            @endif
                                        </div>
   


                                       @endif
                                  </td>

                                  <!-- End History Search -->
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
                      </table>
		                </div>
                    
                    </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

       
          
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm1{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('reject-before-sent-kadealer/'.$data->id_vehicle)}}">
                        {{ csrf_field() }}
                      
                        <input type="hidden" name="vh" value="{{$data->vehicle}}">

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}}</b></h2>
                        </div>

                        <hr>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          <h3>@if(!empty($data->supplier->supplier))
                            {{$data->supplier->supplier}}
                            @endif
                          </h3>
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          <h3>
                            @if(!empty($data->type->type_vehicle))
                            {{$data->type->type_vehicle}}
                            @endif
                          </h3>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          <h3>
                           @if(!empty($data->co->country_origin))
                            {{$data->co->country_origin}}
                          @endif
                          </h3>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          <h3>
                            @if(!empty($data->brand->brand))
                            {{$data->brand->brand}}
                            @endif
                          </h3>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          <h3>
                          @if(!empty($data->model_brand->model))
                            {{$data->model_brand->model}}
                          @endif
                          </h3>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$data->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$data->vehicle_registered_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            <h3>
                              @if(!empty($data->fuel->fuel))
                              {{$data->fuel->fuel}}
                              @endif
                            </h3>
                          </div>
                          
                          <hr>

                          

                          @if(!empty($data->request_by->group_by)){

                                <input type="text" name="group_submit_by" value="{{$data->request_by->group_by}}">
                            }
			                   @endif

                          <b><h2 style="color: red; margin-top: 20px">Not Found</h2></b>
                          <div class="form-group col-md-12">
                            <label for="feDescription">Remark</label>
                            <textarea name="remark" class="form-control" rows="5"></textarea>
                          </div>


                      </div>
                      


                       
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-accent">Save</button>
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach
            

@endsection


@push('js')

  <script type="text/javascript">
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
    });
  </script>

  <!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker" ).datepicker();
     });
    $(function() {
       $( "#datepicker2" ).datepicker();
     });
    </script>

   

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

  <!-- Not select -->
  <script type="text/javascript">
        $( '#popup-validation' ).on('submit', function(e) {
           if($( 'input[class^="friends"]:checked' ).length === 0) {
              alert( 'Please Select the Chassis' );
              e.preventDefault();
           }
        });
    </script>
    <!-- End not select -->

     <script type="text/javascript">
    function showDiv(select){
     if(select.value=="contact@carvx.jp"){
      document.getElementById('hidden_div').style.display = "block";
     } else{
      document.getElementById('hidden_div').style.display = "none";
     }
    }
  </script> 

@endpush
