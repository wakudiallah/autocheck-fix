@extends('admin.layout.template')

@section('content')


     
      <link rel="stylesheet" type="text/css"    href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>


        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">autocheckmalaysia4@gmail.com</span>
                <h3 class="page-title">Sent Vehicle( {{config("autocheck.full_report")}} API )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
          

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">


                      <h4>- Sent Today <span class="badge badge-pill badge-primary">{{$count_today_submit}}</span></h4>
                      

                    <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/FullReport/sent/api/partner')}}" >

                      <input type="hidden" name="_token" value="{{ csrf_token() }}">

                           
                      <input type="submit" value="Send To Partner" class="btn btn-lg btn-primary" style="float: right; margin-bottom: 20px !important">                           

                    <div class="table-responsive">
                      <table id="example" class="table table-bordered table-striped"  width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle </th>
                                  
                                  <th>Country Origin</th>
                                  <th>Date Request</th>
                                  <th>Status</th>
                                  <th>Req From</th>
                                  <th>Action <input type="checkbox" id="checkAll"></th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}} </td>
                                  
                                  <td>
                                    @if(!empty($data->co->country_origin))
                                    {{$data->co->country_origin}}
                                    @endif
                                  </td>
                                  <td>{{$data->created_at}}</td>
                                  

                                  <!-- Status -->
                                  <td align="center">
                                    @if($me == 'VER')
                                      @if($data->status == '10' OR $data->status == '20')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">New</i>
                                      @else
                                      @endif
                                    @endif

                                    @if($me == 'AD')
                                      @if($data->is_sent == 1)
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Send</i>
                                      @else
                                      <i href="#" class="card-post__category badge badge-pill badge-danger">Not Yet</i>
                                      @endif

                                    @endif
                                  </td>

                                  <!-- End of API Status -->

                                  <td><!-- Req from -->
                                    @if(!empty($data->request_by->name))
                                    {{$data->request_by->name}}

                                    <i href="#" class="card-post__category badge badge-pill badge-danger">{{$data->request_by->group_by}}</i>

                                    @endif
                                  </td><!-- End Req from -->

                                 
                                  <!-- History Search -->
                                  <td align="center">
                                      @if($me == 'VER')
                                      <input type="checkbox"  name="id[]" value="{{$data->id_vehicle}}" class="invitation-friends Bike"/>
                                      <input type="hidden"  name="ci[]" value="{{$data->id}}">                                     
                                      <input type="hidden"  name="vehicle[]" value="{{$data->vehicle}}">

                                      <input type="hidden"  name="ids[]" value="">
                                      @endif



                                       @if($me == 'AD')

                                          <div class="custom-control custom-toggle custom-toggle-sm mb-1">
                                            @if($data->is_sent == 1)
                                            <a href="{{url('/update-send/'.$data->id)}}" class="lever switch-col-red"> 
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1">Unsend</button>
                                            </a>
                                              
                                            @else
                                              
                                            @endif
                                        </div>
   


                                       @endif
                                  </td>

                                  <!-- End History Search -->
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
                      </table>
                    </div>


                    
                    </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>
    

@endsection


@push('js')

    <script type="text/javascript">
      $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
      });
    </script>

    <script>

        $(document).on('change','.Bike:gt(100)',function(){

            if(this.checked)
                  alert('Cannot more than'. config('autocheck.limit_submit_to_partner_everyday') .'checkbox');
                  this.checked = false;
        }); 
    </script>

    <script type="text/javascript">
        $( '#popup-validation' ).on('submit', function(e) {
           if($( 'input[class^="invitation-friends"]:checked' ).length === 0) {
              alert( 'Please! Select the chassis' );
              e.preventDefault();
           }
        });
    </script>  


    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>

  
    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

    <script>
      $(document).ready(function() {
        $('#example').DataTable();
      });
    </script>



    
    <!-- End not select -->

    <script type="text/javascript">
    function showDiv(select){
     if(select.value=="contact@carvx.jp"){
      document.getElementById('hidden_div').style.display = "block";
     } else{
      document.getElementById('hidden_div').style.display = "none";
     }
    }
    </script> 

@endpush
