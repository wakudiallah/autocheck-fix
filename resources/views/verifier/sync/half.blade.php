@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Sync Tasklist ( {{config('autocheck.half_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/HalfReport/sync/store')}}" >
                      
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <input type="submit" value="Sync Group" class="mb-2 btn btn-danger mr-2 btn-lg" style="float: right;"> 

                      <!-- <a href="{{url('sync_all')}}" class="mb-2 btn btn-danger mr-2 btn-lg" style="float: right;"> Sync Group </a> -->

	                  <div class="table-responsive">		      
                      <table id="example" class="table table-striped table-bordered"  width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Id</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>
                                  <th>Status</th>
                                  <th>Req From</th>
                                  <th>Change to Manual</th>
                                  <th align="center">Action <input type="checkbox" id="checkAll"> </th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>
                                    @if(!empty($data->status_carvx->report_id))

                                      {{ $data->status_carvx->report_id }}

                                    @endif
                                  </td>
                                  <td>{{$data->vehicle}}</td>
                                  
                                  <td>{{$data->created_at}}</td>
                                 
                                   

                                  <!-- Status -->
                                  <td align="center">
                                    
                                      @if($data->status == '40')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>
                                      @elseif($data->status == '10' OR $data->status == '20')
                                      <i  class="card-post__category badge badge-pill badge-warning">New</i>
                                      @elseif($data->status == '25')
                                      <i  class="card-post__category badge badge-pill badge-warning">In Progress</i>
                                      @elseif($data->status == '30')
                                      <i  class="card-post__category badge badge-pill badge-danger">Reject</i>
                                      @endif
                                    
                                      
                                      @if($me == 'VER')
                                      @if($data->status == '10')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                      @endif
                                  </td>
                                  <!-- End of Status -->


                                  <td>
                                    <p style="color: red">{{$data->request_by->name}} <b> ( {{$data->group->group_name}} )</b></p>                
                                  </td>

                                  <td align="center">
                                    <button type="button" class="btn btn-sm btn-danger mr-1" data-toggle="modal" data-target=".bd-verrify-manual{{$data->id}}">
                                          Verify Manual
                                    </button>
                                  </td>
                               

                                  <!-- Action -->
                                  <td align="center">
                                      <input type="checkbox"  name="id_vehicle[]" value="{{$data->id_vehicle}}" class="friends" />


                                      <input type="hidden"  name="id[]" value="{{$data->id}}">
                                      <input type="hidden"  name="vehicle[]" value="{{$data->vehicle}}">
                                      <!-- <input type="hidden"  name="ids[]" value="{{$data->id_vehicle}}"> -->
                                  </td>
                                  




                              </tr>
                            @endforeach
                              
                          </tbody>
                          
		                  </table>
	                  </div>

                      </form>

                    </li>
                  </ul>
              </div>
            </div>


            </div>


          </div>


        @foreach($data2 as $data)
        <div class="modal fade bd-verrify-manual{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Verify Manual ( {{$data->vehicle}} )</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <p><b>Are you sure this chassis will be manually verified?</b></p>
                  <a href="{{url('Share/Api-change-manual/'.$data->id_vehicle)}}" class="btn btn-sm btn-danger" style="margin-left: 5px ">Verify Manual </a>
                     
              </div>
              <div class="modal-footer">
                
                
              </div>

              </form> 


            </div>
          </div>
        </div>
        @endforeach
       
            

@endsection


@push('js')
  
  <script type="text/javascript">
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
    });
  </script>
  
  <!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <!-- Not select -->
    <script type="text/javascript">
        $( '#popup-validation' ).on('submit', function(e) {
           if($( 'input[class^="friends"]:checked' ).length === 0) {
              alert( 'Please Select the Chassis' );
              e.preventDefault();
           }
        });
    </script>
    <!-- End not select -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    

    @foreach($data3 as $data2)

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker{{$data2->id}}" ).datepicker();
     });
    $(function() {
       $( "#datepicker2{{$data2->id}}" ).datepicker();
     });
    </script>


    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>
    @endforeach

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script> -->

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush
