@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Sync Tasklist ( {{config('autocheck.extra_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/ExtraReport/sync/api')}}" >
                      
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"> 


                      <input type="submit" value="Sync Group" class="mb-2 btn btn-danger mr-2 btn-lg" style="float: right;">
			
		                <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered"  width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Id</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>            
                                  <th>Status</th>
                                  <th>Req From</th>         
                                  <th align="center">Action <input type="checkbox" id="checkAll"></th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>
                                    @if(!empty($data->status_carvx->report_id))

                                        {{ $data->status_carvx->report_id }}

                                    @endif
                                  </td>
                                  <td>{{$data->vehicle}}</td>
                                  <td>{{$data->created_at}}</td>
                                 

                                  <!-- Status -->
                                  <td align="center">
                                    
                                      
                                      @if($me == 'VER')
                                      @if($data->status == '10')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                      @endif
                                  </td>
                                  <!-- End of Status -->

                             

                                  <td>
                                    <p style="color: red">{{$data->request_by->name}} </p>
                                    
                                  </td>
                                  


                                  <!-- Action -->
                                  <td align="center">
                                      <input type="checkbox"  name="id_vehicle[]" value="{{$data->id_vehicle}}" class="invitation-friends" />


                                      <input type="hidden"  name="id[]" value="{{$data->id}}">
                                      <input type="hidden"  name="vehicle[]" value="{{$data->vehicle}}">
                                      <input type="hidden"  name="ids[]" value="{{$data->id_vehicle}}">
                                  </td>


                              </tr>
                            @endforeach
                              
                          </tbody>
                          
            					   </table>
            				   </div>

                      </form>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>
           

@endsection


@push('js')

  <script type="text/javascript">
    $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
    });
  </script>

  <!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script> -->

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>


  <script type="text/javascript">
      $( '#popup-validation' ).on('submit', function(e) {
         if($( 'input[class^="invitation-friends"]:checked' ).length === 0) {
            alert( 'Please! Select the chassis' );
            e.preventDefault();
         }
      });
  </script>



@endpush
