@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Check ID ( {{config('autocheck.extra_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                    
			
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered"  width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Date Request</th>
                                  <th>Status</th>
                                  <th width="10%">Req From</th>
                                  <th>Action</th>
                                  <th>History</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                 
                                <td width="5%">{{$i++}}</td>
                                <td>{{$data->vehicle}}</td>
                                <td>{{$data->created_at}}</td>
                                <!-- ========== Status ============= -->
                                <td align="center">
                                    
                                     
                                    @if($data->searching_by == 'NOT')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                    @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                    @endif
                                     
                                </td>
                                <!-- ========== End of Status ========== -->
                                <td align="center">
                                    {{$data->request_by->name}}
                                </td>

                                <td align="center">
                                    <button type="button" class="btn btn-sm btn-success mr-1" data-toggle="modal" data-target=".bd-example-modal-input-id{{$data->id}}">
                                          ID Report
                                    </button>

                                    <button type="button" class="btn btn-sm btn-danger mr-1" data-toggle="modal" data-target=".bd-verrify-manual{{$data->id}}">
                                          Verify Manual
                                    </button>

                                    
                                </td>
                                
                                
                                <!-- ========== History Search ========== -->
                                <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>

                                  

                                </td>
                                <!-- ========== End History Search ========== -->

                               

                              </tr>
                            @endforeach
                              
                          </tbody>
                          
            		      </table>
            		</div>

                      </form>



                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

        

        <!-- Modal Upload -->
        @foreach($data2 as $data)
          
          <div class="modal fade bd-example-modal-input-id{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Input ID Report ( {{$data->vehicle}} )</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      
                      <form method="POST" class="upload-file" id="register-form" action="{{ url('Share/Update-Id/'.$data->id_vehicle) }}" enctype='multipart/form-data' >
                          {{ csrf_field() }}
                        

                        <div class="form-row">
                          <div class="form-group col-md-12">
                            <label for="feDescription">ID Report</label>

                            <input type="text" name="engine" class="form-control" maxlength="100" placeholder="ID Report" required>
                            
                          </div>

                          <input type="hidden" name="vehicle" class="form-control" maxlength="100" placeholder="Vehicle" required value="{{$data->id_vehicle}}">
                          

                          <div class="form-group col-md-12">
                            <input type="hidden" name="vehicle" value="{{$data->vehicle}}">

                          </div>

                          <div id="status"></div>
                          <div id="photos" class="row"></div>

                          <hr>

                        </div>

                        <div class="form-row">
                          <input type="submit" name="submit" class="btn btn-sm btn-primary">

                          
                        </div>
                         
                  </div>
                  <div class="modal-footer">
                    
                    
                  </div>

                  </form> 


                </div>
              </div>
            </div>
        
          <div class="modal fade bd-verrify-manual{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Input Verify Manual ( {{$data->vehicle}} )</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <p><b>Are you sure this chassis will be manually verified?</b></p>
                      <a href="{{url('verify-not-found-id-carvx/'.$data->id_vehicle)}}" class="btn btn-sm btn-danger" style="margin-left: 5px ">Verify Manual </a>
                         
                  </div>
                  <div class="modal-footer">
                    
                    
                  </div>

                  </form> 


                </div>
              </div>
            </div>

        @endforeach
        <!-- End Modal upload -->
         
            

@endsection


@push('js')

  

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


  

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script> -->

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>





@endpush

