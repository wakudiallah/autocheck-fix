@extends('admin.layout.template')

@section('content')


          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Checking</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-7">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Vehicle Checking </h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">

                          <form method="POST" class="register-form" id="register-form" action="{{url('/save-vehicle-checking')}}">

                            {{ csrf_field() }}

                             <!-- parameter testing -->
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <!-- <label for="feFirstName" class="hidden">Is Test</label> -->
                                <input type="hidden" class="form-control hidden" id="feFirstName" placeholder="Is Test" value="1" name="is_test">
                              </div>
                            </div>


                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Vehicle Identification Number</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Vehicle Identification Number" value="" name="vehicle" required style="color: red; font-weight: bold">
                              </div>
                              
                            </div>

                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Supplier</label>
                                 <select name="supplier" class="form-control select2" required  data-show-subtext="true" data-live-search="true">
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($supplier as $data)
                                  <option value="{{$data->id}}">{{$data->supplier}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>

                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Type</label>
                                 <select id="feInputState" name="type" class="form-control" required>
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($type as $data)
                                  <option value="{{$data->id}}">{{$data->type_vehicle}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>


                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Country Origin</label>
                                 <select id="feInputState" name="country" class="form-control" required>
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($country as $data)
                                  <option value="{{$data->id}}">{{$data->country_origin}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>

                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Make / Brand</label>
                                 <select name="brand" class="form-control" id="brand_value" required>
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($brand as $data)
                                  <option value="{{$data->id}}">{{$data->brand}} </option>
                                  @endforeach
                                </select>
                              </div>
                            </div>

                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Model</label>
                                 <select name="model" class="form-control" id="model_value" required="">
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($model as $data)
                                  <option value="{{$data->id}}|{{$data->brand_id}}">{{$data->model}} </option>
                                  @endforeach
                                </select>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="feInputAddress">Engine Number / Engine Model</label>
                              <input type="text" class="form-control" id="feInputAddress" placeholder="Engine Number" placeholder="Engine Number" name="engine_number" required=""> 
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <input type="text" class="form-control" placeholder="Engine Capacity (cc)" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12" name="engine_capacity" required> 
                              </div>
                              <div class="form-group col-md-6">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <input type="text" class="form-control" id="datepicker" name="registered_date" placeholder="Vehicle Registered Date" required> 
                              </div>

                            </div>

                            <div class="form-group">
                              <label for="feInputAddress">Fuel</label>
                              <select id="feInputState" name="fuel" class="form-control" required>
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($fuel as $data)
                                  <option value="{{$data->id}}">{{$data->fuel}}</option>
                                  @endforeach
                                </select>
                            </div>

                            

                            <button type="submit" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="col-lg-5">
                <div class="card card-small mb-4 pt-3">
                  
                  <ul class="list-group list-group-flush">
                    
                    <li class="list-group-item p-4">
                      <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="65%">VIN </th>
                                  <th width="65%">Status </th>
                                  <th width="30%">Action</th>
                              </tr>
                          </thead>
                          <tbody>

                            <?php $i=1; ?>
                            @foreach($vehicle as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td width="65%">{{$data->vehicle}}</td>
                                  <td align="center">
                                    @if(!empty ($data->status_vehicle->status))
                                      @if($data->status_vehicle->status == '1')
                                      <img src="{{asset('images/check.png')}}" width="20" height="20" align="center">
                                      @elseif($data->status_vehicle->status == '0')
                                      <img src="{{asset('images/not.png')}}" width="20" height="20" align="center">
                                      @else

                                      @endif

                                    @else
                                      <img src="{{asset('images/proses.jpg')}}" width="30" height="20" align="center">
                                    @endif
                                  </td>
                                  <td width="30%">
                                      
                                      @if($data->status == "20")
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1" data-toggle="modal" data-target=".bd-example-modal-lg{{$data->id}}"> 
                                        <i class="material-icons">edit</i>
                                      </button>
                                      @endif
                                   

                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-lgdetail{{$data->id}}">
                                      <i class="material-icons">details</i>
                                    </button>

                                  </td>
                              </tr>

                            @endforeach

                              
                          </tbody>
                          
                      </table>
                      </div>

                    </li>
                  </ul>
                </div>
              </div>
             
            </div>
            <!-- End Default Light Table -->
          </div>


          <!-- Edit -->

          @foreach($vehicle as $data2)
          <div class="modal fade bd-example-modal-lg{{$data2->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Edit</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                  <form method="POST" class="register-form" id="register-form" action="{{url('/update-vehicle-checking/'.$data2->id)}}">
                        {{ csrf_field() }}
                      

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feDescription">Vehicle Identification Number</label>
                          <input type="text" class="form-control" name="vehicle_edit" placeholder="Vehicle Identification Number" value="{{$data2->vehicle}}" required="" disabled> 
                        </div>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          <select id="feInputState" name="supplier_edit" class="form-control">
                            
                            @foreach($supplier as $data)
                            
                             <option value="{{ $data->id }}" {{ $data2->supplier_id == $data->id ? 'selected' : '' }}>{{$data->supplier}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          <select name="type_edit" class="form-control">
                            
                            @foreach($type as $data)
                            
                             <option value="{{ $data->id }}" {{ $data2->type_id == $data->id ? 'selected' : '' }}>{{$data->type_vehicle}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin</label>
                          <select id="feInputState" name="country_edit" class="form-control">
                            
                            @foreach($country as $data)
                            
                             <option value="{{ $data->id }}" {{ $data2->country_origin_id == $data->id ? 'selected' : '' }}>{{$data->country_origin}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          <select id="brand_value_edit" name="brand_edit" class="form-control">
                            <option value="" selected disabled hidden>- Please Select -</option>

                            @foreach($brand as $data)
                            
                             <option value="{{ $data->id }}" {{ $data2->brand_id == $data->id ? 'selected' : '' }}>{{$data->brand}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          <select id="model_value_edit" name="model_edit" class="form-control">
                            <option value="" selected disabled hidden>- Please Select -</option>
                            
                            @foreach($model as $data)
                            
                             <option value="{{ $data->id }}|{{$data->brand_id}}" {{ $data2->model_id == $data->id ? 'selected' : '' }}>{{$data->model}}</option>

                            @endforeach
                          </select> 
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number / Engine Model</label>
                            <input type="text" class="form-control"  placeholder="Engine Number" name="engine_number_edit" value="{{$data2->engine_number}}"> 
                        </div>

                              <div class="form-group col-md-6">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <input type="text" class="form-control" placeholder="Engine Capacity (cc)" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="10" name="engine_capacity_edit" value="{{$data2->cc}}"> 
                              </div>

                              <?php  $tarikh =  date('m/d/Y ', strtotime($data2->vehicle_registered_date)); ?>

                              <div class="form-group col-md-6">
                                <label for="feInputCity">Vehicle Registered Date Edit</label>
                                <input type="text" class="form-control" id="datepicker2{{$data->id}}" name="registered_date_edit" placeholder="Vehicle Registered Date" value="{{$tarikh}}"> 
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel{{$data2->fuel_id}}</label>
                            <select id="feInputState" name="fuel_edit" class="form-control">
                              
                              @foreach($fuel as $data)
                              
                               <option value="{{ $data->id }}" {{ $data2->fuel_id == $data->id ? 'selected' : '' }}>{{$data->fuel}}</option>

                              @endforeach
                            </select>
                          </div>

                      </div>
                      


                       
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <input type="submit" value="Save Changes" name="" class="btn btn-primary">
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach




          <!-- Detail -->

          @foreach($vehicle as $data3)
          <div class="modal fade bd-example-modal-lgdetail{{$data3->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="">
                        {{ csrf_field() }}
                      

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="feDescription">Vehicle Identification Number</label>
                          <input type="text" class="form-control" id="feFirstName" placeholder="Vehicle Identification Number" value="{{$data3->vehicle}}" disabled=""> 
                        </div>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          <select class="form-control" disabled>
                            
                            @foreach($supplier as $data)
                            
                             <option value="{{ $data->id }}" {{ $data3->supplier_id == $data->id ? 'selected' : '' }}>{{$data->supplier}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          <select class="form-control" disabled>
                            
                            @foreach($type as $data)
                            
                             <option value="{{ $data->id }}" {{ $data3->type_id == $data->id ? 'selected' : '' }}>{{$data->type_vehicle}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          <select id="feInputState"  class="form-control" disabled>
                            
                            @foreach($country as $data)
                            
                             <option value="{{ $data->id }}" {{ $data3->country_origin_id == $data->id ? 'selected' : '' }}>{{$data->country_origin}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          <select id="feInputState" class="form-control" disabled>
                            
                            @foreach($brand as $data)
                            
                             <option value="{{ $data->id }}" {{ $data3->brand_id == $data->id ? 'selected' : '' }}>{{$data->brand}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                            <select  class="form-control" disabled>
                            @foreach($model as $data)
                            
                             <option value="{{ $data->id }}" {{ $data3->model_id == $data->id ? 'selected' : '' }}>{{$data->model}}</option>

                            @endforeach
                          </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number / Engine Model</label>
                            <input type="text" class="form-control" id="feInputAddress"  placeholder="Engine Number" disabled value="{{$data3->engine_number}}"> 
                        </div>

                              <div class="form-group col-md-6">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <input type="text" class="form-control" placeholder="Engine Capacity (cc)" id="feInputCity" disabled value="{{$data3->cc}}"> 
                              </div>
                              <div class="form-group col-md-6">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <input type="text" class="form-control" id="" disabled placeholder="Vehicle Registered Date" value="{{$data3->vehicle_registered_date}}"> 
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            <select id="feInputState" disabled="" class="form-control">
                              
                              @foreach($fuel as $data)
                              
                               <option value="{{ $data->id }}" {{ $data3->fuel_id == $data->id ? 'selected' : '' }}>{{$data->fuel}}</option>

                              @endforeach
                            </select>
                          </div>

                      </div>
                       
                </div>
                
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach


@endsection

@push('addjs')

  <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value').children().clone();
      
        $('#brand_value').change(function() {
          $('#model_value').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value').append(this);
                 }
            });
          $('#model_value').val('');
        });
    });
  </script>


    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit').children().clone();
      
        $('#brand_value_edit').change(function() {
          $('#model_value_edit').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit').append(this);
                 }
            });
          $('#model_value_edit').val('');
        });
    });
    </script>

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    

    <script>
      $(function() {
         $( "#datepicker" ).datepicker();
       });

      $(function() {
         $( "#datepicker2" ).datepicker();
       });
    </script>

@endpush


@push('js')

      <style type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"></style>

    <style type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css"></style>

     <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
    </script>

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>

@endpush