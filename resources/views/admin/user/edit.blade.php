@extends('admin.layout.template')

@section('content')
	
		<div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">User</h3>
              </div>
            </div>
            <!-- End Page Header -->

            <div class="row">

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit User</h6>
                  </div>
                
	                <ul class="list-group list-group-flush">
	                    <li class="list-group-item px-3">
	                      
	                    	<form method="POST" class="register-form" id="register-form" action="{{url('/update-user/'.$data->id)}}" >

                         {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Name" value="{{$data->name}}" name="name" required=""> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="email" class="form-control" id="feLastName" placeholder="Email " value="{{$data->email}}" name="email" required=""> 
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Password</label>
                                <input type="password" class="form-control" id="feEmailAddress" placeholder="Password" value="" name="password" required="">
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Phone</label>
                                <input type="text" class="form-control" id="fePassword" placeholder="Phone" name="phone" value="{{$data->phone}}"> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Role </label>
                                <select class="form-control" name="role">
                                  
                                  @foreach($role as $data1)
                                  <option value="{{$data1->id_role}}">{{$data1->role}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>


                            <button type="submit" class="btn btn-accent" style="float: right;">Save</button>
                        </form>


	                    </li>
	                </ul>
	              </div>
            </div>
        </div>
   </div>
	

@endsection