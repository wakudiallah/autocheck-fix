@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
      <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">User ( {{trans('system_autocheck.full_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                 
                  <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New User ( {{trans('system_autocheck.full_report')}} )</a>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th width="20%">User Name</th>
                                  <th>Email</th>
                                  <th>Group name</th>
                                  <!-- <th>Phone</th> -->
                                  <th>Admin Group</th>
                                  <th>Status</th>
                                  <th align="center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $data)
                              
                              <tr>
                                  <td>{{$i++}}</td>
                                  <td width="20%">{{$data->name}}</td>
                                  <td>{{$data->email}}</td>
                                  <td>
                                    {{$data->group_name}}
                                  </td>
                                  <!-- <td>{{$data->phone}}</td> -->
                                  
                                  <td align="center">
                                    @if($data->is_status_admin_group == '1')
                                      <i  class="card-post__category badge badge-pill badge-success">Yes</i>
                                    @else
                                      
                                    @endif
                                  </td>   
                                  <td>
                                    @if($data->status == '1')
                                      Active
                                    @else
                                      Non Active
                                    @endif
                                  </td>
                                  <td  align="center">
                                    
                                    
                                     <input id="toggle-trigger" 
                                        type="checkbox" data-toggle="toggle" class="togglefunction" data-on="Active" data-off="Deactivate"  data-id="{{$data->id}}" data-onstyle="success" data-offstyle="danger" {{ $data->status ? 'checked' : '' }}>


                                    <!-- <a href="{{url('user/full-report/edit/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1" title="Edit"> 
                                        <i class="material-icons">create</i>
                                      </button>
                                    </a> -->


                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#ModalDetail{{$data->id}}" >
                                        <i class="material-icons">details</i>
                                    </button>
                                    
                                   
                                  </td>
                                  
                                   
                              </tr>

                              

                            @endforeach 
                          </tbody>
                          
                      </table>
                    </div>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>

          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New User {{trans('system_autocheck.full_report')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/user/full-report/save')}}">
                      {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Name" value="" name="name" required="" id="username"> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="email" class="form-control" id="feLastName" placeholder="Email " value="" name="email" required=""> 
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Password</label>
                                <input type="password" class="form-control" id="feEmailAddress" placeholder="Password" value="" name="password" required>
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Phone</label>
                                <input type="text" class="form-control" id="fePassword" placeholder="Phone" name="phone" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="fePassword">User Group</label>
                                <select class="selectpicker form-control" name="group" data-live-search="true" required>
                                  <option value="" selected disabled hidden>- Select -</option>
                                  @foreach($group as $data)
                                  <option value="{{$data->user_id}}">{{$data->user_id}}--{{$data->group_name}}</option>
                                  @endforeach
                                </select>
                              </div>

                              <!--<div class="form-group col-md-12 other" id="two" style="display: none">
                                <label>Group</label>
                                <select class="form-control" name="user_group" >
                                  @foreach($group as $data)
                                    @if($data->user_id != "KA")
                                    <option value="{{$data->user_id}}">{{$data->group_name}}</option>
                                    @endif
                                  @endforeach
                                </select>
                              </div>-->




                              <div class="form-group col-md-12 admin" id="two" style="display: none" for="defaultChecked">
                                <label>Admin Group</label>
                                <div class="row">
                                  <div class="col-md-2">
                                    <input type="radio" class="custom-control" id="defaultChecked" name="admin_status" value="1" checked>
                                  </div>
                                  <div class="col-md-4">
                                    <label class="form-group" for="defaultUnchecked">Yes</label>
                                  </div>

                                  <div class="col-md-2">
                                    <input type="radio" class="custom-control" id="defaultChecked1" name="admin_status" value="0">
                                  </div>
                                  <div class="col-md-4">
                                    <label class="form-group" for="defaultUnchecked1">No</label>
                                  </div>

                                </div>
                              </div>


                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal -->


            @include('admin.user.user_creation._modal_detail_user_full')


            

@endsection


@push('js')

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


    <script>
        $(function() {
            $('.togglefunction').change(function() {
                
                var status = $(this).prop('checked') == true ? 1 : 0; 
                var id = $(this).data('id'); 
                 
                $.ajax({
                    type: "GET",
                    url: "{{url('user/full-report/update-status')}}",
                    data: {'status': status, 'id': id},
                    success: function(data){
                    
                        setInterval('location.reload()', 1000); 

                    }
                });
            })
        })
    </script>

  <script type="text/javascript">
        var Privileges = jQuery('#one');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'NA') { 
                $('.other').show();
                $('.admin').show();
            }

            else if($(this).val() == 'KA'){
                $('.other').hide();
                $('.admin').show();
            }
            
            else{
                $('.admin').hide();
                $('.other').hide();
            } 

        });
    </script>


    <script type="text/javascript">

        $(document).ready(function(){
            $('#username').keyup(check_username); //use keyup,blur, or change
        });

        function check_username(){
            var username = $('#username').val();

            jQuery.ajax({
                    type: 'POST',
                    url: "{{ url('/user/check_duplicate_username') }}",
                    data: 'username='+ username,
                    cache: false,
                    success: function(response){
                        if(response == 0){
                           alert('available')
                        }
                        else {
                             alert('not available')
                             //do perform other actions like displaying error messages etc.,
                        }
                    }
                });
        }
    </script>




    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

@endpush