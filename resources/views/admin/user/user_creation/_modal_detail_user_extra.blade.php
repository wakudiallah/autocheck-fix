    <!-- Modal Detail --> 
    @foreach($data_detail as $data)
        <div class="modal fade bd-example-modal-lg" id="ModalDetail{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail User {{trans('system_autocheck.extra_report')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Phone</th>
                          <th scope="col">Address</th>
                          <th scope="col">Fax</th>
                          <th scope="col">Last Login</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">{{$data->name}}</th>
                          <td>{{$data->email}}</td>
                          <td>{{$data->phone}}</td>
                          <td>{{$data->address}}</td>
                          <td>{{$data->fax}}</td>
                          <td>{{$data->last_login}}</td>
                        </tr>
                        
                      </tbody>
                    </table>
                </div>
              </div>
              
            </div>
          </div>
        </div>
    @endforeach
    <!-- End Modal Detail --> 