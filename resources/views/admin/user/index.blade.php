@extends('admin.layout.template')

@section('content')

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>




        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">User</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                 
                  <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New User</a>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th>User Name</th>
                                  <th>Email</th>
                                  <th>Role</th>
                                  <th>Phone</th>
                                  <th>Status</th>
                                  <th>Last login time</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td>{{$i++}}</td>
                                  <td>{{$data->name}}</td>
                                  <td>{{$data->email}}</td>
                                  <td>
                                    @if($data->role_id == 'AD')
                                      <i  class="card-post__category badge badge-pill badge-danger">{{$data->role->role}}</i>
                                    
                                    @elseif($data->role_id == 'US')
                                      <i href="#" class="card-post__category badge badge-pill badge-dark">{{$data->role->role}}</i>
                                    @elseif($data->role_id == 'KA')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">{{$data->role->role}}</i>
                                      @if(!empty($data->user_kartam->branch))
                                      <i href="#" class="card-post__category badge badge-pill badge-warning">{{$data->user_kartam->branch}}</i>
                                      @endif

                                    @elseif($data->role_id == 'MAN')
                                      <i href="#" class="card-post__category badge badge-pill badge-info">{{$data->role->role}}</i>

                                    @elseif($data->role_id == 'MAR')
                                      <i href="#" class="card-post__category badge badge-pill badge-dark">{{$data->role->role}}</i>

                                    @elseif($data->role_id == 'VER')
                                      <i href="#" class="card-post__category badge badge-pill badge-warning">{{$data->role->role}}</i>
                                      @elseif($data->role_id == 'NA')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">{{$data->role->role}}</i>

                                      

                                      @if(!empty($data->user_group->group_name))
                                      <i href="#" class="card-post__category badge badge-pill badge-warning">{{$data->user_group->group_name}}</i>
                                      @endif
                                    @endif
                                  </td>
                                  <td>{{$data->phone}}</td>
                                  <td>
                                    @if($data->status == 1)
                                      Active
                                    @else
                                      Non Active
                                    @endif
                                  </td>
                                   
                                  <td>{{$data->last_login}} </td>
                                  <td>

                                     <input id="toggle-trigger" 
                                        type="checkbox" data-toggle="toggle" class="togglefunction" data-on="Active" data-off="Deactivate"  data-id="{{$data->id}}" data-onstyle="success" data-offstyle="danger" {{ $data->status ? 'checked' : '' }}>

                                        
                                    <a href="{{url('edit-user/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="material-icons">create</i>
                                      </button>
                                    </a>
                                    <!-- <button type="button" class="mb-2 btn btn-sm btn-warning mr-1">
                                      <i class="material-icons">details</i>
                                    </button> 
                                    
                                    <a href="{{url('delete-user/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                        <i class="material-icons">delete</i>
                                      </button>
                                    </a> -->

                                  </td>
                                  
                              </tr>
                            @endforeach 
                          </tbody>
                          
                      </table>
                      </div>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>

          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/save-user')}}">
                      {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Name" value="" name="name" required=""> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="email" class="form-control" id="feLastName" placeholder="Email " value="" name="email" required=""> 
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Password</label>
                                <input type="password" class="form-control" id="feEmailAddress" placeholder="Password" value="" name="password">
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Phone</label>
                                <input type="text" class="form-control" id="fePassword" placeholder="Phone" name="phone" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="fePassword">Role</label>
                                <select class="form-control" name="role" id="one">
                                  <option value="" selected disabled hidden>- Select -</option>
                                  @foreach($role as $data)
                                  <option value="{{$data->id_role}}">{{$data->role}}</option>
                                  @endforeach
                                </select>
                              </div>

                              <div class="form-group col-md-12 other" id="two" style="display: none">
                                <label>Group</label>
                                <select class="form-control" name="user_group" >
                                  @foreach($group as $data)
                                    @if($data->user_id != "KA")
                                    <option value="{{$data->user_id}}">{{$data->group_name}}</option>
                                    @endif
                                  @endforeach
                                </select>
                              </div>

                              <div class="form-group col-md-12 admin" id="two" style="display: none" for="defaultChecked">
                                <label>Admin Group</label>
                                <div class="row">
                                  <div class="col-md-2">
                                    <input type="radio" class="custom-control" id="defaultChecked" name="admin_status" value="1" checked>
                                  </div>
                                  <div class="col-md-4">
                                    <label class="form-group" for="defaultUnchecked">Yes</label>
                                  </div>

                                  <div class="col-md-2">
                                    <input type="radio" class="custom-control" id="defaultChecked1" name="admin_status" value="0">
                                  </div>
                                  <div class="col-md-4">
                                    <label class="form-group" for="defaultUnchecked1">No</label>
                                  </div>

                                </div>
                              </div>


                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>

          <!-- Modal -->

@endsection


@push('js')

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


    <script>
        $(function() {
            $('.togglefunction').change(function() {
                
                var status = $(this).prop('checked') == true ? 1 : 0; 
                var id = $(this).data('id'); 
                 
                $.ajax({
                    type: "GET",
                    url: "{{url('user/update-status')}}",
                    data: {'status': status, 'id': id},
                    success: function(data){
                    
                        setInterval('location.reload()', 1000); 

                    }
                });
            })
        })
    </script>


  <script type="text/javascript">
        var Privileges = jQuery('#one');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'NA') { //NA is kadealer
                $('.other').show();
                $('.admin').show();
            }

            else if($(this).val() == 'KA'){
                $('.other').hide();
                $('.admin').show();
            }
            
            else{
                $('.admin').hide();
                $('.other').hide();
            } 

        });
    </script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

@endpush