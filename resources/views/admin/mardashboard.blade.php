@extends('admin.layout.template')

@section('content')

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Dashboard</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks -->
    <div class="row">
      
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Verified Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_verified}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--decrease"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-3"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Verifications</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_pending}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-4"></canvas>
          </div>
        </div>
      </div>

      
      <!-- <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <a class="mb-2  mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1" style="cursor: pointer">

                  <span class="stats-small__label text-uppercase">Total Group</span>
                

                  <h6 class="stats-small__value count my-3">{{$total_group}}</h6>
                </a>   
                                                    
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>-->

      
      
    </div>


     <!-- Chart -->
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">{{config('autocheck.full_report')}} Vehicle Submit</h6>
          </div>
          <div class="card-body pt-0">

              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>

      <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">{{config('autocheck.half_report')}} Vehicle Submit</h6>
          </div>
          <div class="card-body pt-0">

              <div id="container_cardealer" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>


    </div>


    <!-- End Chart -->



    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      
     	<!-- Balance -->
      <div class="col-lg-6 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">Balance</h6>
          </div>
          <div class="table-responsive card-body py-0">
            <table class="table table-striped table-bordered" id="example" style="font-size: 11px !important">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Group</th>
                      <th>Balance</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                    <?php $i=1; ?>
                    @foreach($balance as $data)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>
                            {{$data->group_name}}
                      </td>
                      <td>
                            RM {{number_format($data->balance, 2 , '.' , ',' )}} 
                        </td>
                      
                    </tr>
                    @endforeach
                  </tbody>
            </table>
          </div>
          
        </div>
      </div>
      <!-- End Balance -->


      <!-- End Users Stats -->
      <!-- Users By Device Stats -->
      <div class="col-lg-6 col-md-6 col-sm-12 mb-4">
        
      </div>
      <!-- End Users By Device Stats -->
      
    </div>

    <!-- Modal User Group -->
     @foreach($data_group as $data)
      <div class="modal fade bd-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">User Group </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                
            <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >  
              <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Gorup</th>
                    <th>Total User</th>
                </tr>
              </thead>
              <tbody>
              <?php $i=1; ?>
              @foreach($data_group as $data)
              <tr>
                <td>{{$i++}}</td>
                <td>{{$data->group_name}}</td>
                <td>{{$data->total_group_user->count()}}</td>
              </tr>
              @endforeach

              </tbody> 
            </table> 
            </div>
            <div class="modal-footer">
              
              
            </div>


          </div>
        </div>
      </div>
      @endforeach
    <!-- end Modal User Group -->


  </div>


@endsection


@push('js')

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');
    });
  </script>



   <script type="text/javascript">

    $(function () { 
        
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Vehicle'
            },
            subtitle: {
                text: (new Date()).getFullYear()
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Vehicle (vehicle)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} app</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [
            <?php 
            foreach($usergroup_full as $usergroup) { 
                
                if($usergroup->count_jan->count() >= 1){
                    $count_1 = $usergroup->count_jan->count();
                }else{
                    $count_1 = 0;
                }


                if( $usergroup->count_feb->count()  >= 1 ){
                    $count_2 = $usergroup->count_feb->count();

                }else{
                    $count_2 = 0;
                }


                if( $usergroup->count_mar->count()  >= 1 ){
                    $count_3 = $usergroup->count_mar->count();

                }else{
                    $count_3 = 0;
                }


                if( $usergroup->count_apr->count()  >= 1 ){
                    $count_4 = $usergroup->count_apr->count();

                }else{
                    $count_4 = 0;
                }

                if( $usergroup->count_may->count()  >= 1 ){
                    $count_5 = $usergroup->count_may->count();

                }else{
                    $count_5 = 0;
                }


                if( $usergroup->count_jun->count()  >= 1 ){
                    $count_6 = $usergroup->count_jun->count();

                }else{
                    $count_6 = 0;
                }


                if( $usergroup->count_jul->count()  >= 1 ){
                    $count_7 = $usergroup->count_jul->count();

                }else{
                    $count_7 = 0;
                }


                if( $usergroup->count_aug->count()  >= 1 ){
                    $count_8 = $usergroup->count_aug->count();

                }else{
                    $count_8 = 0;
                }


                if( $usergroup->count_sep->count()  >= 1 ){
                    $count_9 = $usergroup->count_sep->count();

                }else{
                    $count_9 = 0;
                }


                if( $usergroup->count_oct->count()  >= 1 ){
                    $count_10 = $usergroup->count_oct->count();

                }else{
                    $count_10 = 0;
                }


                if( $usergroup->count_nov->count()  >= 1 ){
                    $count_11 = $usergroup->count_nov->count();

                }else{
                    $count_11 = 0;
                }


                if( $usergroup->count_des->count()  >= 1 ){
                    $count_12 = $usergroup->count_des->count();

                }else{
                    $count_12 = 0;
                }


            ?>


            {
                name: '<?php echo $usergroup->group_name ?>',
                data: [
                <?php echo $count_1 ?>
                ,<?php echo $count_2 ?>
                ,<?php echo $count_3 ?>
                ,<?php echo $count_4 ?>
                ,<?php echo $count_5 ?>
                , <?php echo $count_7 ?>
                , <?php echo $count_8 ?>
                , <?php echo $count_9 ?>
                , <?php echo $count_10 ?>
                , <?php echo $count_11 ?>
                , <?php echo $count_12 ?>
                
                ]
                

            }, 

            <?php } ?>


            ]
        });

        });
    </script>


    
    <script type="text/javascript">

        $(function () { 
            
            Highcharts.chart('container_cardealer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Vehicle'
                },
                subtitle: {
                    text: (new Date()).getFullYear()
                },
                xAxis: {
                    categories: [
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Vehicle (vehicle)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f} app</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },

                series: [
                <?php 
                foreach($usergroup_car_dealer as $usergroup) { 
                    
                    if($usergroup->count_jan_car_dealer->count() >= 1){
                        $count_1_car_dealer = $usergroup->count_jan_car_dealer->count();
                    }else{
                        $count_1_car_dealer = 0;
                    }


                    if( $usergroup->count_feb_car_dealer->count()  >= 1 ){
                        $count_2_car_dealer = $usergroup->count_feb_car_dealer->count();

                    }else{
                        $count_2_car_dealer = 0;
                    }


                    if( $usergroup->count_mar_car_dealer->count()  >= 1 ){
                        $count_3_car_dealer = $usergroup->count_mar_car_dealer->count();

                    }else{
                        $count_3_car_dealer = 0;
                    }


                    if( $usergroup->count_apr_car_dealer->count()  >= 1 ){
                        $count_4_car_dealer = $usergroup->count_apr_car_dealer->count();

                    }else{
                        $count_4_car_dealer = 0;
                    }

                    if( $usergroup->count_may_car_dealer->count()  >= 1 ){
                        $count_5_car_dealer = $usergroup->count_may_car_dealer->count();

                    }else{
                        $count_5_car_dealer = 0;
                    }


                    if( $usergroup->count_jun_car_dealer->count()  >= 1 ){
                        $count_6_car_dealer = $usergroup->count_jun_car_dealer->count();

                    }else{
                        $count_6_car_dealer = 0;
                    }


                    if( $usergroup->count_jul_car_dealer->count()  >= 1 ){
                        $count_7_car_dealer = $usergroup->count_jul_car_dealer->count();

                    }else{
                        $count_7_car_dealer = 0;
                    }


                    if( $usergroup->count_aug_car_dealer->count()  >= 1 ){
                        $count_8_car_dealer = $usergroup->count_aug_car_dealer->count();

                    }else{
                        $count_8_car_dealer = 0;
                    }


                    if( $usergroup->count_sep_car_dealer->count()  >= 1 ){
                        $count_9_car_dealer = $usergroup->count_sep_car_dealer->count();

                    }else{
                        $count_9_car_dealer = 0;
                    }


                    if( $usergroup->count_oct_car_dealer->count()  >= 1 ){
                        $count_10_car_dealer = $usergroup->count_oct_car_dealer->count();

                    }else{
                        $count_10_car_dealer = 0;
                    }


                    if( $usergroup->count_nov_car_dealer->count()  >= 1 ){
                        $count_11_car_dealer = $usergroup->count_nov_car_dealer->count();

                    }else{
                        $count_11_car_dealer = 0;
                    }


                    if( $usergroup->count_des_car_dealer->count()  >= 1 ){
                        $count_12_car_dealer = $usergroup->count_des_car_dealer->count();

                    }else{
                        $count_12_car_dealer = 0;
                    }


                ?>


                {
                    name: '<?php echo $usergroup->group_name ?>',
                    data: [
                    <?php echo $count_1_car_dealer ?>
                    ,<?php echo $count_2_car_dealer ?>
                    ,<?php echo $count_3_car_dealer ?>
                    ,<?php echo $count_4_car_dealer ?>
                    ,<?php echo $count_5_car_dealer ?>
                    , <?php echo $count_7_car_dealer ?>
                    , <?php echo $count_8_car_dealer ?>
                    , <?php echo $count_9_car_dealer ?>
                    , <?php echo $count_10_car_dealer ?>
                    , <?php echo $count_11_car_dealer ?>
                    , <?php echo $count_12_car_dealer ?>
                    
                    ]
                    

                }, 

                <?php } ?>


                ]
            });

            });
    </script>


<!-- Pie Diagram -->
<script type="text/javascript">

  var list_name_group_user = <?php echo $list_name_group_user; ?>;
  var list_name_group_total = <?php echo $list_name_group_total; ?>;

  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: list_name_group_user,
      datasets: [{
        backgroundColor: [
          "#2ecc71",
          "#e74c3c",
          "#f1c40f",
          "#3498db",
          "#95a5a6",
          "#9b59b6",
          "#34495e"
        ],
        data: list_name_group_total
      }]
    }
  });
</script>
<!-- end pie diagram -->

<!-- Line chart -->
<script type="text/javascript">

  var list_line_chart = <?php echo $list_line_chart; ?>;
  

  Highcharts.chart('container_line', {
    chart: {
        marginBottom: 80
    },
    title: {
                text: 'Income '
            },

    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        labels: {
            rotation: 90
        }
    },

      series: [{

          name: 'Naza',
          data: list_line_chart
      },
      {
          name: 'Kastam',
          data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }]
      
  });
</script>
<!-- End Chart -->





<script type="text/javascript">
  Highcharts.chart('containerx', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Browser market shares in January, 2018'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Chrome',
            y: 61.41
        }, {
            name: 'Internet Explorer',
            y: 11.84
        }, {
            name: 'Firefox',
            y: 10.85
        }, {
            name: 'Edge',
            y: 4.67
        }, {
            name: 'Safari',
            y: 4.18
        }, {
            name: 'Sogou Explorer',
            y: 1.64
        }, {
            name: 'Opera',
            y: 1.6
        }, {
            name: 'QQ',
            y: 1.2
        }, {
            name: 'Other',
            y: 2.61
        }]
    }]
});
</script>

@endpush
