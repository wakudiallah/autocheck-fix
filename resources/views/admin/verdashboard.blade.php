@extends('admin.layout.template')

@section('content')


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Dashboard</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks -->
    
    
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">{{config('autocheck.half_report')}}</h6>
          </div>
          <div class="card-body p-0">
            <ul class="list-group list-group-small list-group-flush">
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sent MN</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_half_sent_mn}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sent API</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_half_sent_api}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Check ID</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_half_checkid}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sync</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_half_sync}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Open Verify</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_half_openverify}}</span>
              </li>
              
            </ul>
          </div>
          <div class="card-footer border-top">
            
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-4 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">{{config('autocheck.full_report')}}</h6>
          </div>
          <div class="card-body p-0">
            <ul class="list-group list-group-small list-group-flush">
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sent MN</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_full_sent_mn}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sent API</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_full_sent_api}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Check ID</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_full_checkid}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sync</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_full_sync}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Open Verify</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_full_openverify}}</span>
              </li>
              
            </ul>
          </div>
          <div class="card-footer border-top">
            
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-4 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">{{config('autocheck.extra_report')}}</h6>
          </div>
          <div class="card-body p-0">
            <ul class="list-group list-group-small list-group-flush">
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sent MN</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_extra_sent_mn}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sent API</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_extra_sent_api}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Check ID</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_extra_checkid}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Sync</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_extra_sync}}</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Open Verify</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">{{$count_extra_openverify}}</span>
              </li>
              
            </ul>
          </div>
          <div class="card-footer border-top">
            
          </div>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Verified Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_verified_by_me}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--decrease"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-3"></canvas>
          </div>
        </div>
      </div>

      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Processed </span>
                <h6 class="stats-small__value count my-3">{{$total_processed}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--decrease"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-3"></canvas>
          </div>
        </div>
      </div>

      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Not Found</span>
                <h6 class="stats-small__value count my-3">{{$total_reject_by_me}}
                </h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-4"></canvas>
          </div>
        </div>
      </div>

    
      <!-- <div class="col-lg col-md-4 col-sm-12 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Sent (Manual)</span>

                <h6 class="stats-small__value count my-3">{{$total_pending_sent_mn}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--decrease"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-5"></canvas>
          </div>
        </div>
      </div>
     
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Sent (API)</span>
                <h6 class="stats-small__value count my-3">{{$total_pending_sent_api}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div> -->

    </div>


   


    <!--
    <div class="row">
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Sent Vehicle (Kastam)</span>
                <h6 class="stats-small__value count my-3">12</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>

      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Sent Vehicle (Kadealer)</span>
                <h6 class="stats-small__value count my-3">12</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>


    </div> -->

    <!-- Chart -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Vehicle Chart</h6>
          </div>
          <div class="card-body pt-0">

              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Chart -->

    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      <div class="col-lg-7 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Current Month</h6>
          </div>
          <div class="card-body pt-0">
            
            <canvas id="chartJSContainer" height="150" style="max-width: 100% !important;"></canvas>
          </div>
        </div>
      </div>
      <!-- End Users Stats -->
      <!-- Users By Device Stats -->
      <div class="col-lg-5 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">Member Activity</h6>
          </div>
          <div class="card-body d-flex py-0">
            
            <div class="table-responsive">
              <table id="example3" class="table table-striped table-bordered" cellspacing="0" width="100%" >

                  <thead>
                    <tr>
                      <th>Member</th>
                      <th>Activity</th>
                      <th>Date Activity</th>
                      <th>Case</th>
                      <th>St Verify</th>
                      <th>History</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                    @foreach($data as $data)
                    <tr>
                      <td>
                        @if(!empty($data->history_per_user->name))
                        {{$data->history_per_user->name}}
                        @endif
                      </td>
                      <td>{{$data->parameter_history_id}}</td>
                      <td>{{$data->created_at}}</td>
                      <td>
                        @if(!empty($data->history_per_user->last_login))
                        {{$data->history_per_user->last_login}}
                        @endif
                      </td>

                      <td>{{$data->vehicle_id}}</td>
                      <td>@if(!empty($data->status_case->status))
                            @if($data->status_case->status == '20' OR $data->status_case->status == '10')
                            New
                            @elseif($data->status_case->status == '40')
                            Complete
                            @else
                            Cancel
                            @endif
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>

          </div>
        </div>
      </div>
      <!-- End Users By Device Stats -->
      
    </div>
  </div>


@endsection

@push('js')

  <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example3').DataTable();
      } );
  </script>


   <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

  <script type="text/javascript">

    $(function () { 
        var data_pending_vehicle = <?php echo $list_pending_vehicle; ?>;
        var data_verify_vehicle = <?php echo $list_verify_vehicle; ?>;
        var data_rejected_vehicle = <?php echo $list_rejected_vehicle; ?>;


        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Vehicle'
            },
            subtitle: {
                text: (new Date()).getFullYear()
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Vehicle (vehicle)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} app</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: 'Vehicle Checked',
                data: data_verify_vehicle
                

            }, {
                name: 'Open Vehicle Verify',
                data: data_pending_vehicle

            

            }, {
                name: 'Not Found Vehicle',
                data: data_rejected_vehicle

            }]
        });

        });
  </script>

  <!-- Line Chart -->
  <script type="text/javascript">
    var datex = <?php echo  $date_this_month ?>;
    var list_data_half = <?php echo $list_date_this_month_half ?>;
    var list_data_full = <?php echo $list_date_this_month_full ?>;
    var list_data_extra = <?php echo $list_date_this_month_extra ?>;

    var options = {
      type: 'line',
      data: {
        labels: datex,
        datasets: [
          {
            label: 'Half Report',
            data: list_data_half,
            backgroundColor: "rgba(255,165,0,0.2)",
            borderColor: "rgba(186,120,7,1)",
            borderWidth: 1
          },  
          {
            label: 'Full Report',
            data: list_data_full,
            backgroundColor: "rgba(255,99,132,0.2)",
            borderColor: "rgba(255,99,132,1)",
            borderWidth: 1
          },
          {
            label: 'Extra Report',
            data: list_data_extra,
            backgroundColor: "rgba(25,60,22,0.2)",
            borderColor: "rgba(25,60,22,1)",
            borderWidth: 1
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              reverse: false
            }
          }]
        }
      }
    }

    var ctx = document.getElementById('chartJSContainer').getContext('2d');
    new Chart(ctx, options);
  </script>
  <!-- End Line Chart -->
  

   <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example3').DataTable();
      } );
  </script>

@endpush


