@extends('admin.layout.template_dashboard')

@section('content')
	
		<div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Currency</h3>
              </div>
            </div>
            <!-- End Page Header -->

            <div class="row">

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Currency</h6>
                  </div>
                
	                <ul class="list-group list-group-flush">
	                    <li class="list-group-item px-3">
	                      
	                    	<form method="POST" class="register-form" id="register-form" action="{{url('update-currency/'.$data->id)}}" >

                         {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">ID </label>
                                <input type="text" class="form-control"  placeholder="Id" value="{{$data->id_currency}}" name="id" required=""> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feFirstName">Currency </label>
                                <input type="text" class="form-control"  placeholder="Symbol" value="{{$data->symbol}}" name="symbol" required=""> 
                              </div>
                              
                              <div class="form-group col-md-12">
                                <label for="feLastName">Currency</label>
                                <textarea name="currency" class="form-control" rows="5" placeholder="Desc">{{$data->currency}}</textarea>
                              </div>
                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right;">Save</button>
                        </form>


	                    </li>
	                </ul>
	              </div>
            </div>
        </div>
   </div>
	

@endsection