@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Currency</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                      
                      <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New Currency</a>
                    </div>
                </div>
            </div>

            <div class="row">

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <?php $role       = Auth::user()->role_id; ?>

                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Id</th>
                                  <th>Symbol</th>
                                  <th>Currency</th>
                                  <th>Status</th>
                                  @if($role == "AD")
                                  <th width="20%">Action</th>
                                  @endif
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->id_currency}}</td>
                                  <td>{{$data->symbol}}</td>
                                  <td>{{$data->currency}}</td>
                                  <td>
                                    @if($data->status = 1)
                                      <div class="custom-control custom-toggle custom-toggle-sm mb-1">
                                        <input type="checkbox" id="customToggle2" name="customToggle2" class="custom-control-input" checked="checked">
                                        <label class="custom-control-label" for="customToggle2">Checked</label>
                                      </div>
                                    @else
                                      Non Active
                                    @endif
                                  </td>

                                  @if($role == "AD")
                                  <td width="20%">
                                    <a href="{{url('edit-currency/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="material-icons">edit</i>
                                      </button>
                                    </a>
                                   
                                   <a href="{{url('delete-currency/'.$data->id)}}">
                                    <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                      <i class="material-icons">delete</i>
                                    </button>
                                  </a>

                                  </td>
                                  @endif
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New Currency</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/save-currency')}}">

                         {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName"> Id</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Id" value="" name="id_currency" required=""> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feFirstName"> Symbol</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Symbol" value="" name="symbol" required=""> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Currency</label>
                                <textarea name="currency" class="form-control" rows="5" placeholder="Desc"></textarea>
                              </div>
                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right;">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>

          <!-- Modal -->

@endsection


@push('js')

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  </script>

@endpush