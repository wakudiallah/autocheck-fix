@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Email </h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                      
                      <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New Email</a>
                    </div>
                </div>
            </div>

            <div class="row">

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      

                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Email</th>
                                  <th>Desc</th>
                                  <th width="20%">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->email}}</td>
                                  <td>{{$data->desc}}</td>
                                  <td width="20%">
                                    <a href="{{url('edit-email/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="material-icons">edit</i>
                                      </button>
                                    </a>
                                   
                                   <a href="{{url('delete-email/'.$data->id)}}">
                                    <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                      <i class="material-icons">delete</i>
                                    </button>
                                  </a>

                                  </td>
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/save-email')}}">

                         {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Email</label>
                                <input type="email" class="form-control" placeholder="Email" value="" name="email" required=""> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feFirstName"> Desc </label>
                                <input type="text" class="form-control" placeholder="Desc" value="" name="desc" required=""> 
                              </div>
                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right;">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>

          <!-- Modal -->

@endsection


@push('js')


  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  </script>

@endpush