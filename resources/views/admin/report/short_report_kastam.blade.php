<!DOCTYPE html>

<html>

<head>

	<title>Report</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('images/fav.ico')}}">

	<style type="text/css">
		body{ 
			font-family: calibri !important;
			font-size: 12px;
		}


		@media print {
		    tr.vendorListHeading {
		        background-color: #1a4567 !important;
		        -webkit-print-color-adjust: exact; 
		    }
		}

	@media print {
	    .vendorListHeading th {
	        color: black !important;
	    }
	}
	</style>

</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->


	<table width="100%" style="margin-top: -80px !important">
		<tr>
			<td width="40%" align="center" style="margin-left: 0px !important">
				<br><br><br>
				
				<img src="{{asset('images/marii.jpg')}}" style="width:180px;height:120px; text-align:center"/><br>
				
			</td>
			<td width="30%">
				<!-- <p style="line-height: normal; margin-top: 0px !important"><b style="font-size: 12px !important">Verification Serial Number</b><br>
				</p> -->
				
			</td>
			<td width="30%" style="text-align: right;">
				<!-- <img src="{{asset('images/marii.jpg')}}" style="width:150px; height:70px; margin-top: -50px !important"/> -->
				
			</td>
		</tr>
	</table>

	
	@php
		$tarikh =  date('d-M-Y ', strtotime($data->created_at)); 

	@endphp

	<table width="100%" style="margin-bottom:10px">
		<tr>
			<td align="left">
				<table>
					<tr>
						<td><h5>Requested By</h5>  </td>
						<td>:&nbsp;</td>
						<td><h5>{{$data->request_by->name}}</h5></td>
					</tr>
					<tr>
						<td><h5>Date : </h5></td>
						<td>:&nbsp;</td>
						<td><h5>{{$tarikh}} </h5></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					
				</table>
			</td>
			

			<td width="20%">
				<table>
					<tr>
						<td>
							<p align="center"><img src="{{asset('images/auto.jpg')}}" style="width:130px;height:60px; text-align:center"/></p>
						</td>
					</tr>
					<tr>
						<td>
							<?php $url = Request::url(); ?>
				 <p align="center"><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(120)->generate(url($url))) !!} "> </p>
						</td>
					</tr>
				</table>
				

				 
			</td>
		</tr>
	</table>


	<table width="100%">
		<tr>
			<td colspan="6" align="center"><b><h3 style="border-radius:20px; font-size: 17px !important; border: 1px #d65353 solid; overflow: hidden; background: #d65353; padding-top: 10px; padding-bottom: 10px; color: #ffffff"><b>Vehicle Detail</b></h3></b></td>
			
		</tr>
	</table>

	<style type="text/css">
		.field-color{
			border-radius:20px;
			overflow: hidden; 
			background: #eeeeee; 
			padding-top: 5px; 
			padding-bottom: 5px; 
			padding-left: 10px
		}

		.field-non-color{
			border-radius:20px;
			overflow: hidden; 
			background: #ffffff; 
			padding-top: 5px; 
			padding-bottom: 5px; 
			padding-left: 10px
		}
	</style>


	<table width="100%">
		<tr>
			<td align="left" width="50%"><p style="border-radius:20px; overflow: hidden; background: #eeeeee; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">1. Vin / Chassis :  <b>{{$api_kastam->vehicle}}</b></p></td>
			
			@php
			$manufactureDate =  date('d F Y ', strtotime($api_kastam->manufactureDate));

			$date_of_origin =  date('d F Y ', strtotime($api_kastam->date_of_origin));
			@endphp 

			<td align="left"  width="50%"><p style="border-radius:20px; overflow: hidden; background: #eeeeee; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">7. Date Of Manufacture :  <b>{{$api_kastam->manufactureDate}}</b> </p></td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td align="left" width="50%"><p style="border-radius:20px; border: 1px #eeeeee solid; overflow: hidden; background: #ffffff; padding-top: 10px; padding-bottom: 10px;  padding-left: 10px">2. Make : <b>{{$api_kastam->make}}</b></p></td>
		
			<td align="left" width="50%"><p style="border-radius:20px; border: 1px #eeeeee solid; overflow: hidden; background: #ffffff; padding-top: 10px; padding-bottom: 10px;  padding-left: 10px">8. Date of original registration : <b>{{$date_of_origin}}</b></p></td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td align="left" width="50%"><p style="border-radius:20px; overflow: hidden; background: #eeeeee; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">3. Model : <b>{{$api_kastam->model}}</b></p></td>
		
			<td align="left" width="50%"><p style="border-radius:20px; overflow: hidden; background: #eeeeee; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">9. Drive : <b>{{$api_kastam->drive}}</b></p></td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td align="left" width="50%"><p style="border-radius:20px; border: 1px #eeeeee solid; overflow: hidden; background: #ffffffff; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">4. Engine Code / Engine Number : <b><b>{{$api_kastam->engine}}</b></b></p></td>
		
			<td align="left" width="50%"><p style="border-radius:20px; border: 1px #eeeeee solid;  overflow: hidden; background: #ffffffff; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">10. Transmission : <b><b>{{$api_kastam->transmission}}</b></p></td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td align="left" width="50%"><p style="border-radius:20px; overflow: hidden; background: #eeeeee; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">5. Body : <b><b>{{$api_kastam->body}}</b></b></p></td>
		
			<td align="left" width="50%"><p style="border-radius:20px;  overflow: hidden; background: #eeeeee; padding-top: 10px; padding-bottom: 10px; padding-left: 10px">11. Displacement (cc) : <b>{{$api_kastam->displacement_cc}}</b> </p></td>
		</tr>
	</table>


	<table width="100%">
		<tr>
			<td align="left" width="50%"><p style="border-radius:20px; border: 1px #eeeeee solid; overflow: hidden; background: #ffffff; padding-top: 10px; padding-bottom: 10px;  padding-left: 10px">6. Car Grade : <b>{{$api_kastam->grade}}</b> </p></td>
		
			<td align="left" width="50%"></td>
		</tr>
	</table>
			
	

	<table width="100%" style="margin-bottom: 20px;">
		<tr>
			<td colspan="6" align="center"><b><h3 style="border-radius:20px; font-size: 17px !important; border: 1px #d65353 solid; overflow: hidden; background: #d65353; padding-top: 10px; padding-bottom: 10px; color: #ffffff"><b>Average Market Price</b></h3></b></td>
			
		</tr>
	</table>


	<?php 

            $endpoint = 'live';
            $access_key = '1b0fd13e42ae1c5932e70779ae0a24ff';

            // Initialize CURL:
            $ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Store the data:
            $json = curl_exec($ch);
            curl_close($ch);

            // Decode JSON response:
            $exchangeRates = json_decode($json, true);

            $count_country= count($exchangeRates);
	

            $price = $api_kastam->average_market;

            $currency_usdyen = $exchangeRates["quotes"]["USDJPY"];
            $currency_usdmyr = $exchangeRates["quotes"]["USDMYR"];
            //$yen = ;

            $priceusd = $price / $currency_usdyen;
            $pricemyr = $priceusd * $currency_usdmyr;


            //echo "price usd" . $priceusd ."=="; 
            //echo "price myr" . $pricemyr;
	?>


	<?php $tot_yen  = number_format($api_kastam->average_market, 0, ',', ',');

				 	$tot_myr  = number_format($pricemyr, 0, ',', ',');

				 	$rate =  $pricemyr / $price;

				 	$rete6 = round($rate, 6);
                  ?>


	<table width="100%" style="padding-left: 40px; padding-right: 40px ">
		<tr>
			<td align="left"><p>
				<img src="{{asset('images/yen3.jpg')}}" width="40%" ></p>
			</td>

			<td width="60%"><h2>JPY {{$tot_yen}}</h2></td>
		
			<td align="left" width="80%" style="padding-left: 40px">
				<p>Conversion rate based on current mid market rates derived from the mid point between “buy” and “sell”.</p>
			</td>
		</tr>

		<tr>
			<td align="left">
				<p><img src="{{asset('images/rm.jpg')}}" width="40%" ></p>
			</td>

			<td  width="60%">

				

                  <h2>MYR {{$tot_myr}}<h2>
				
			</td>
		
			<td align="left" width="80%"  style="padding-left: 40px">
				<p>Exchange rate as at @php date("Y-m-d") @endphp <b>currencylayer.com </b>:
				<b>1 JPY = {{$rete6}} RM</b></p>
			</td>
		</tr>

	</table>


	<table width="100%" style="margin-bottom: 10;" align="center">
		@foreach($image_api as $image_api)
		<p style="text-align:center;"><img src="{{$image_api->image}}" width="85%" align="center" style="margin-top: 10px"> </p><br>
		
		<img src="data:image/png;base64, {{ $image_api->base64 }}" class="img-fluid" alt="Image Preview" />

		@endforeach
		
	</table>

	@php $date_complete = date('d F Y', strtotime($api_kastam->created_at)) @endphp

	<table width="100%" style="padding: 40px">
		<tr>
			<td>
			<p style="border-radius:20px; border: 1px #eeeeee solid; overflow: hidden; background: #ffffff; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; background: #eeeeee;"> <b>The Autocheck vehicle History Report is based only on information supplied to us by our overseas partners and availabe as at {{$date_complete}} other information about this vehicle, including problems, may not have been reported to Autocheck. Use of this report as one important tool, along with a vehicle inspection and test drive, to make a better informed decision for your further action.</b></p>
			</td>
		</tr>

		<tr>
			<td>
				<p style="border-radius:20px; border: 1px #eeeeee solid; overflow: hidden; background: #ffffff; padding-top: 10px; padding-bottom: 10px;  padding-left: 10px;"><b>Autocheck report depends on it sources for the accuracy and reliability of its information. therefore, no responsibility is assumed by Autocheck or its agents for errors or omissions in this report. </b></p>
			</td>
		</tr>

		<tr>
			<td>
				<p style="border-radius:20px; border: 1px #eeeeee solid; overflow: hidden; background: #ffffff; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; background: #eeeeee;"> <b>
			Autocheck further expressly disclaims all warranties, express or implied, including any implied warranties of merchantability or fitness for a particular purpose </b>
		</p>
			</td>
		</tr>

	
	</table>

	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center"><b>Autocheck</b> - Vehicle Report Detail</td>
		</tr>
	</table>


</body>
</html>
