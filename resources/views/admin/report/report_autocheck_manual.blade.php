<!DOCTYPE html>

<html>

<head>

	<title>Report</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('images/fav.ico')}}">

	<style type="text/css">
		body{ 
			font-family: calibri !important;
			font-size: 12px;
		}


		@media print {
		    tr.vendorListHeading {
		        background-color: #1a4567 !important;
		        -webkit-print-color-adjust: exact; 
		    }
		}

	@media print {
	    .vendorListHeading th {
	        color: black !important;
	    }
	}
	</style>

</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->


	<table width="100%" style="margin-top: -50px !important">
		<tr>
			<td width="40%" align="center" style="margin-left: 0px !important">
				<br><br><br>
				
				<img src="{{asset('images/auto.jpg')}}" style="width:110px;height:50px; text-align:center"/><br>
				<h5 style="line-height: normal;"> <b>Autocheck Verification Report</b></h5>
			</td>
			<td width="30%">
				<p style="line-height: normal; margin-top: -50px !important"><b style="font-size: 12px !important">Verification Serial Number</b><br>{{$data->status_vehicle->ver_sn}}</p>
				
			</td>
			<td width="30%" style="text-align: right;">
				<img src="{{asset('images/logo_miti.jpg')}}" style="width:150px; height:70px; margin-top: -50px !important"/>
				
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td colspan="6" align="center"><b><h3 style="font-size: 17px !important"><u><b>VERIFICATION REPORT</b></u></h3></b></td>
			
		</tr>
	</table>


	<table width="100%">
		<tr>
			<td align="left">
				<table>
					<tr>
						<td>Requested By  </td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>{{$data->request_by->name}}</td>
					</tr>
					<tr>
						<td>Email Address</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>{{$data->request_by->email}} | {{$data->request_by->phone}}</td>
					</tr>
					<tr>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Verified By</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>
							@if(empty($data->status_vehicle->verify_by))
								Verifier
							@else
								{{$data->name_verifier->name}}
							@endif
						</td>
					</tr>
					<tr>
						<td>Verified Date</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<?php $tarikh =  date('d F Y ', strtotime($data->updated_at)); ?>

						<td>{{$tarikh}}</td>
					</tr>
				</table>
			</td>
			

			<td width="20%">
				 <?php $url = Request::url(); ?>
				 <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(160)->generate(url($url))) !!} ">
			</td>
		</tr>
	</table>


	<div class="row" style="margin-top: 0px !important;" >
		<table class="table table-striped"  width="100%"  style="border: 1px solid #def0f7; border-collapse: collapse;">
			<thead>
				<tr>
					<th>VIN</th>
				    <th colspan="2">{{$data->vehicle}}</th>
				</tr>
			</thead>

			<tbody>

			    <tr>
			    	<td style="background-color: #def0f7">Country Origin</td>
			    	<td style="background-color: #def0f7">{{$data->co->country_origin}}</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			    </tr>

			    <tr>
			     	<td>Make / Brand</td>
			     	<td>{{$data->brand->brand}}</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">Model / Variance</td>
			     	<td style="background-color: #def0f7">{{$data->model_brand->model}}</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    
			    <tr>

			    	<?php 
			    	if($data->searching_by == 'API'){
			    	$engine_number         = $data->vehicle_api->engine_number;
        			
        			}
        			else{
        				$engine_number         = $data->vehicle_manual->engine_number;
        				
        			}
        			?>


			     	<td>Engine Number / Engine Model</td>
			     	<td>{{$engine_number}}</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">Cubic Capacity (CC)</td>
			     	<td style="background-color: #def0f7">

			     		<?php 
					    	if($data->searching_by == 'API'){
					    	$cc         = $data->vehicle_api->cc;
		        			
		        			}
		        			else{
		        				$cc         = $data->vehicle_manual->cc;
		        				
		        			}
		        		?>
			     		{{$cc}}
			     	</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    <tr>
			     	<td>Fuel Type</td>
			     	<td>{{$data->fuel->fuel}}</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    <tr>
			    	<?php 
			    	if($data->searching_by == 'API'){
			    	$y         = explode('-', $data->vehicle_api->year_manufacture);
        			$year = $y[0];
        			}
        			else{
        				$y         = explode('-', $data->vehicle_manual->year_manufacture);
        				$year = $y[0];
        			}
        			?>

			     	<td style="background-color: #def0f7">Year of Manufacture</td>
			     	<td style="background-color: #def0f7">{{$year}}</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    <tr>

			    	<?php 
			    	if($data->searching_by == 'API'){
			    	$regist_date =  date('F Y ', strtotime($data->vehicle_api->registation_date)); 
			    	}else{
			    		$regist_date =  date('F Y ', strtotime($data->vehicle_manual->registation_date)); 
			    	}

			    	?>

			     	<td>First Registration Date</td>
			     	<td>{{$regist_date}}</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    
			    
		    
		 	</tbody>
		</table>
	</div>


	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center"><b>Autocheck</b> - Vehicle Report Detail</td>
		</tr>
	</table>


</body>
</html>
