<!DOCTYPE html>

<html>

<head>

	<title>Report</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('images/fav.ico')}}">

	<style type="text/css">
		body{ 
			font-family: calibri !important;
			font-size: 12px;
		}


		@media print {
		    tr.vendorListHeading {
		        background-color: #1a4567 !important;
		        -webkit-print-color-adjust: exact; 
		    }
		}

	@media print {
	    .vendorListHeading th {
	        color: black !important;
	    }
	}
	</style>

</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->


	<table width="100%" style="margin-top: -50px !important">
		<tr>
			<td width="40%" align="center" style="margin-left: 0px !important">
				<br><br><br>
				
				<img src="{{asset('images/auto.jpg')}}" style="width:110px;height:50px; text-align:center"/><br>
				<h5 style="line-height: normal;"> <b>Autocheck Report</b></h5>
			</td>
			<td width="30%">
				<p style="line-height: normal; margin-top: -50px !important"><b style="font-size: 12px !important">Verification Serial Number</b><br>{{$data->status_vehicle->ver_sn}}</p>
				
			</td>
			<td width="30%" style="text-align: right;">
				<img src="{{asset('images/logo_miti.jpg')}}" style="width:150px; height:70px; margin-top: -50px !important"/>
				
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td colspan="6" align="center"><b><h3 style="font-size: 17px !important"><u><b>AUTOCHECK REPORT</b></u></h3></b></td>
			
		</tr>
	</table>


	<table width="100%">
		<tr>
			<td align="left">
				<table>
					<tr>
						<td>Requested By  </td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>KASTAM</td>
					</tr>
					<tr>
						<td>Email Address</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>{{$data->request_by->email}} | {{$data->request_by->phone}}</td>
					</tr>
					<tr>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Verified By</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>
							@if(empty($data->status_vehicle->verify_by))
								Verifier
							@else
								{{$data->name_verifier->name}}
							@endif
						</td>
					</tr>
					<tr>
						<td>Verified Date</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<?php $tarikh =  date('d F Y ', strtotime($data->updated_at)); ?>

						<td>{{$tarikh}}</td>
					</tr>
				</table>
			</td>
			

			<td width="20%">
				 <?php $url = Request::url(); ?>
				 <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(160)->generate(url($url))) !!} ">
			</td>
		</tr>
	</table>


	<div class="row" style="margin-top: 0px !important;" >
		<table class="table table-striped"  width="100%"  style="border: 1px solid #def0f7; border-collapse: collapse;">
			<thead>
				<tr>
					<th>CHASSIS / VIN</th>
				    <th colspan="2">ANH20 - 8311977</th>
				</tr>
			</thead>

			<tbody>

			    

			    <tr>
			     	<td>MAKE / BRAND</td>
			     	<td>TOYOTA</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">MODEL / VARIANCE</td>
			     	<td style="background-color: #def0f7">VELLFIRE</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td>DATE OF ORIGINAL REGISTRATION</td>
			     	<td>DECEMBER 2013</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">BODY</td>
			     	<td style="background-color: #def0f7">DBA - ANH20W</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			     <tr>
			     	<td>ENGINE</td>
			     	<td>2AZ-FE</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">GRADE</td>
			     	<td style="background-color: #def0f7">2.4Z G EDITION</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			     <tr>
			     	<td>DRIVE</td>
			     	<td>2 WD</td>
			    	<td align="center"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">TRANSMISSION</td>
			     	<td style="background-color: #def0f7">AT</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.jpg')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>


			    
		    
		 	</tbody>
		</table>
	</div>


	<table style="margin-top: 60px">
		<tr>
			<td colspan="3">
				<p style="color: #FFFFFF"><b>This vehicle does not qualify for Buyback Guarantee</b></p>
			</td>
			<td colspan="2">
				<p><b>Average Market Price</b></p>
			</td>
		</tr>

		<tr>
			<td>
				<img src="" width="8%" height="10%">
			</td>
			<td colspan="2">
				<p style="color: #FFFFFF">Unfortunately, this vehicle does not qualify for our
Buyback Guarantee program.</p>
			</td>
			<td>
				<img src="{{asset('images/report/yen.jpg')}}" width="10%" height="10%">
			</td>
			<td>
				 
				<h2>¥<b>2,480,000 </b></h2>

				<h2>RM<b> 100,287</b></h2>
				<h5>Rate : RM 24.7288 </h5>
			</td>
		</tr>
	</table>


<!-- https://carvx.jp/auction-images/20191004-2fc8cd05/bbed3f1c8c04f3d2e80cd75789b36dc6d308f99f.jpg





  -->
	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center">
				<img src="https://carvx.jp/auction-images/20190924-772b742b/8acb99d55768a2f983096c55c18f6b3fe3488926.jpg" width="800" height="500">
			</td>
		</tr>
	</table>


	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center">
				<img src="https://carvx.jp/auction-images/20191004-2fc8cd05/91f18a94ca9094b3afbdf6ed3ee825c4f261a883.jpg" >
			</td>
		</tr>
	</table>

	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center">
				<img src="https://carvx.jp/auction-images/20191004-2fc8cd05/8134c4978434ed177124cbfdd4acf72bdf1a9598.jpg">
			</td>
		</tr>
	</table>

	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center">
				<img src="https://carvx.jp/auction-images/20191004-2fc8cd05/a1efbccc4845b2b41bad679340615e6757e52ba8.jpg">
			</td>
		</tr>
	</table>

	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center">
				<img src="https://carvx.jp/auction-images/20191004-2fc8cd05/156a64c93c462a7707a2fa0cafd60a7fd191001e.jpg">
			</td>
		</tr>
	</table>


	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center">
				<img src="https://carvx.jp/auction-images/20191004-2fc8cd05/b712a1ae429ed8a419f0e18232eece04b33f9d19.jpg">
			</td>
		</tr>
	</table>

	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center">
				<img src="https://carvx.jp/auction-images/20191004-2fc8cd05/7beca45488bcef2ce61054235667ab514707b881.jpg">
			</td>
		</tr>
	</table>


	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center"><b>Autocheck</b> - Vehicle Report Detail</td>
		</tr>
	</table>


</body>
</html>