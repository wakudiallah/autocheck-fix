<!DOCTYPE html>

<html>

<head>

	<title>Report {{$data->VehicleIdNumber}}</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('images/fav.ico')}}">

	<style type="text/css">
		body{ 
			font-family: calibri !important;
			font-size: 12px;
		}

		td {
		   
		    height:15px !important;
		}


		@media print {
		    tr.vendorListHeading {
		        background-color: #1a4567 !important;
		        -webkit-print-color-adjust: exact; 
		    }
		}

	@media print {
	    .vendorListHeading th {
	        color: black !important;
	    }
	}
	</style>

</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->


	<table width="100%" style="margin-top: -50px !important">
		<tr>
			<td width="40%" align="center" style="margin-left: 0px !important">
				<br><br><br>
				
				<img src="{{asset('images/auto.jpg')}}" style="width:110px;height:50px; text-align:center"/><br>
				<h5 style="line-height: normal;"> <b>Autocheck Verification Report</b></h5>
			</td>
			<td width="30%">
				<p style="line-height: normal; margin-top: -50px !important"><b style="font-size: 12px !important">Verification Serial Number</b>
					<br>
					<?php $tarikh =  date('dmY ', strtotime($data->CreationTime)); ?>
						
						00OLD{{$tarikh}}
					</p>
				
			</td>
			<td width="30%" style="text-align: right;">
				
				<img src="{{asset('images/logo_miti.png')}}" style="width:150px; height:70px; margin-top: -50px !important"/>
				
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td colspan="6" align="center"><b><h3 style="font-size: 17px !important"><u><b>VERIFICATION REPORT</b></u></h3></b></td>
			
		</tr>
	</table>


	<table width="100%">
		<tr>
			<td align="left">
				<table>
					<tr>
						<td>Requested By</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>{{ $data->userpast->name }}</td>
					</tr>
					<tr>
						<td>Email Address</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td> {{ $data->userpast->email }}</td>
					</tr>
					<tr>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Verified By</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<td>
							
								Verifier
							
						</td>
					</tr>
					<tr>
						<td>Verified Date</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:&nbsp;</td>
						<?php $tarikh1 =  date('d F Y ', strtotime($data->VerifiedDate)); ?>

						<td>{{$tarikh1}}</td>
					</tr>
				</table>
			</td>
			

			<td width="20%">
				 <?php $url = Request::url(); ?>
				 <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate(url($url))) !!} ">
			</td>
		</tr>
	</table>


	<div class="row" style="margin-top: 0px !important;" >
		<table class="table table-striped"  width="100%"  style="border: 1px solid #def0f7; border-collapse: collapse;">
			<thead>
				<tr style="height:15px !important">
					<th>VIN</th>
				    <th colspan="2">{{$data->VehicleIdNumber}}</th>
				</tr>
			</thead>

			<tbody>

			    <tr>
			    	<td style="background-color: #def0f7; height: 2px !important">Country Origin</td>
			    	<td style="background-color: #def0f7; height: 2px !important">
			    		@if($data->CountryId == '6')
                          Japan
                          @elseif($data->CountryId == '7')
                          United Kingdom
                          @elseif($data->CountryId == '8')
                          Netherland
                          @elseif($data->CountryId == '9')
                          Germany
                          @elseif($data->CountryId == '10')
                          United States Of America
                          @endif
                    </td>
			    	<td align="center" style="background-color: #def0f7; height: 2px !important"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>
			    </tr>

			    <tr>
			     	<td>Make / Brand</td>
			     	<td>
			     		@if(!empty($data->modelpast->par_model->brand))
                          {{$data->modelpast->par_model->brand}}
                        @endif
			     	</td>
			    	<td align="center"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">Model / Variance</td>
			     	<td style="background-color: #def0f7">
			     		@if(!empty($data->modelpast->id))
                          {{$data->modelpast->model}}
                        @endif
			     	</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    
			    <tr>

			    	

			     	<td>Engine Number / Engine Model</td>
			     	<td>{{$data->EngineNumber}}</td>
			    	<td align="center"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td style="background-color: #def0f7">Cubic Capacity (CC)</td>
			     	<td style="background-color: #def0f7">{{$data->EngineCapacity}}</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    <tr>
			     	<td>Fuel Type</td>
			     	<td>
			     		@if($data->FuelId == "4")
                        Petrol
                      @endif
			     	</td>
			    	<td align="center"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    <tr>
			    	
			    	<?php
				    		$y         = explode('-', $data->RegisterDate);
	        				$year = $y[0];
			    	?>

			     	<td style="background-color: #def0f7">Year of Manufacture</td>
			     	<td style="background-color: #def0f7">
			     		{{$year}}
			     	</td>
			    	<td align="center" style="background-color: #def0f7"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    <tr>


			     	<td>First Registration Date</td>
			     	<td>
			     		<?php $tarikh3 =  date('F Y ', strtotime($data->RegisterDate)); ?>
                           {{$tarikh3}}
                       </td>
			    	<td align="center"><img src="{{asset('images/check.png')}}" width="20" height="20"></td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    
			    
		    
		 	</tbody>
		</table>
	</div>


	<table width="100%" style="margin-top: 20px !important">
		<tr>
			<td align="center"><b>Autocheck</b> - Vehicle Report Detail</td>
		</tr>
	</table>


</body>
</html>
