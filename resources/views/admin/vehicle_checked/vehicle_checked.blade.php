@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Checked </h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <!--
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                      
                      <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New Fee</a>
                    </div>
                </div>
            </div>
          -->

            <div class="row">

              <?php $me = Auth::user()->role_id; ?>

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
		                <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Vehicle</th>
                                  <th>Verification Serial Number</th>
                                  <th>Date Request</th>
                                  <th>Status</th>
                                  <th>API Status</th>
                                  <th width="20%" align="center">Action</th>
                                  <th>History</th>
                                  @if($me == "AD")
                                  <th>From</th>
                                  @endif
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->vehicle}}</td>
                                  

                                  <td>
                                    @if(!empty($data->status_vehicle->ver_sn))
                                    {{$data->status_vehicle->ver_sn}}
                                    @endif
                                    
                                  </td>
                                  <td>{{$data->created_at}}</td>
                                  

                                  <!-- Status -->
                                  <td align="center">
                                    
                                      @if($data->status == '40')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">Complete</i>
                                      @elseif($data->status == '10' OR $data->status == '20')
                                      <i  class="card-post__category badge badge-pill badge-warning">Pending</i>
                                      @else($data->status == '30')
                                      <i  class="card-post__category badge badge-pill badge-danger">Reject</i>
                                      @endif
                                    
                                      
                                      @if($me == 'VER')
                                      @if($data->status == '10')

                                      <i  class="card-post__category badge badge-pill badge-warning">Manual</i>
                                      @else
                                      <i  class="card-post__category badge badge-pill badge-warning">API</i>
                                      @endif
                                      @endif
                                  </td>
                                  <!-- End of Status -->

                                  <!-- Sent Status -->
                                   <td>
                                    @if($data->searching_by == 'API')

                                      
                                        <i  class="card-post__category badge badge-pill badge-info">API</i>
                                    @else
                                        <i  class="card-post__category badge badge-pill badge-danger">Not API</i>
                                      
                                    @endif
                                  </td> 
                                  <!-- End of Sent Status -->

                                  <!-- Action -->
                                  <td width="20%" align="center">

                                    

                                      @if($data->status == '40' OR $data->status == '30')

                                      <!-- download -->
                                      @if($data->group_by == "NA")
                                      <a href="{{url('report-autocheck/'.$data->id_vehicle)}}" target="_blank">
                                        <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                          <i class="material-icons">save_alt</i>
                                        </button>
                                      </a>
                                      @elseif($data->group_by == "KA")

                                        <!-- download by manual -->
                                          @if($data->searching_by == "NOT")
                                            
                                            <a href="{{ url('/') }}/report/manual/{{$data->full_report}}" target="_blank">
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                <i class="material-icons">save_alt</i>
                                              </button>
                                            </a>

                                          @elseif($data->searching_by == "API")

                                            <a href="{{url('report-carvx/'.$data->id_vehicle)}}" target="_blank">
                                              <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                                <i class="material-icons">save_alt</i>
                                              </button>
                                            </a>

                                          @endif
                                        <!-- download by API -->


                                      @endif


                                      <!-- end download -->

                                    
                                     <a href="{{url('report-autocheck/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                        <i class="material-icons">print</i>
                                      </button>
                                    </a>
                                    @endif
                                  

                                  <!-- Detail -->
                                  @if($me == "NA") <!-- group user -->
                                  <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1{{$data->id}}">
                                      <i class="material-icons">details</i>
                                  </button>
                                  @else <!-- End group user -->

                                  <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target=".bd-example-modal-sm-kastam1{{$data->id}}">
                                      <i class="material-icons">details</i>
                                  </button>

                                  @endif
                                  <!--  ENd Detail -->

                                    

                                  @if($me == "VER")

                                    @if($data->status_vehicle->status == '0')
                                    <!-- =======  For verify data ====================-->
                                    <!-- <a href="{{url('verify-vehicle/'.$data->id_vehicle)}}"> -->
                                    <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  data-toggle="modal" data-target=".bd-example-modal-sm2{{$data->id_vehicle}}">
                                      <i class="material-icons">details</i>
                                    </button>
                                    <!-- </a> -->

                                    <!-- ============ End Verify ========== -->
                                    @endif
                                    
                                  @endif

                                  </td>
                                  <!-- History Search -->
                                  <td align="center">
                                    <a href="{{url('history-vehicle/'.$data->id_vehicle)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-vehicle/'.$data->id_vehicle)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>
                                  </td>

                                  <!-- End History Search -->
                                  @if($me == "AD")
                                  <td>
                                    <b>
                                      @if(!empty($data->group->group_name))
                                    {{$data->group->group_name}}
                                    @endif
                                      </b>
                                  </td>
                                  @endif
                              </tr>
                            @endforeach
                              
                          </tbody>
                          
		      </table>
		</div>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>

          
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm1{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="">
                        {{ csrf_field() }}
                      


                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}}</b></h2>
                        </div>

                        <hr>

                       <div class="form-group col-md-12">
                          <label for="feDescription">Supplier</label>
                          @if(!empty($data->supplier->supplier))
                          <h3>{{$data->supplier->supplier}}</h3>
                          @endif
                        </div>

                        <hr>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Type</label>
                          @if(!empty($data->type->type_vehicle))
                          <h3>{{$data->type->type_vehicle}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Country Origin </label>
                          @if(!empty($data->co->country_origin))
                          <h3>{{$data->co->country_origin}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Make / Brand</label>
                          @if(!empty($data->brand->brand))
                          <h3>{{$data->brand->brand}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                          <label for="feDescription">Model</label>
                          @if(!empty($data->model_brand->model))
                          <h3>{{$data->model_brand->model}}</h3>
                          @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="feInputAddress">Engine Number</label>
                            <h3>{{$data->engine_number}}</h3>
                        </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <h3>{{$data->cc}}</h3> 
                              </div>

                              <div class="form-group col-md-12">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <h3>{{$data->vehicle_registered_date}}</h3>
                              </div>


                          <div class="form-group col-md-12">
                            <label for="feDescription">Fuel</label>
                            @if(!empty($data->fuel->fuel))
                            <h3>{{$data->fuel->fuel}}</h3>
                            @endif
                          </div>

                      </div>
                      


                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach


          <!-- Modal detail for Kastam -->
          @foreach($data2 as $data)
          <div class="modal fade bd-example-modal-sm-kastam1{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Vehicle Checking Detail </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="">
                        {{ csrf_field() }}
                      


                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2><b>{{$data->vehicle}}</b></h2>
                        </div>

                        <hr>

                       

                      </div>
                      


                       
                </div>
                <div class="modal-footer">
                  
                  
                </div>

                </form> 


              </div>
            </div>
          </div>
          @endforeach
          <!-- End modal detail for Kastam -->
            

@endsection


@push('js')

  
<!-- 
  <script type="text/javascript">
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
  }
  </script> -->

    <script type="text/javascript">
      function print(url) {
          var printWindow = window.open( '' );
          printWindow.print();
      };
    </script>


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker" ).datepicker();
     });
    $(function() {
       $( "#datepicker2" ).datepicker();
     });
    </script>

    @foreach($data3 as $data2)
    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit{{$data2->id}}').children().clone();
      
        $('#brand_value_edit{{$data2->id}}').change(function() {
          $('#model_value_edit{{$data2->id}}').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit{{$data2->id}}').append(this);
                 }
            });
          $('#model_value_edit{{$data2->id}}').val('');
        });
    });
    </script>
    @endforeach

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>



@endpush
