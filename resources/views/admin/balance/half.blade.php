@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Balance ( {{config('autocheck.half_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
            


            <div class="row">

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      

                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Group</th>
                                  <th>Balance</th>
                                  <th width="20%">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            @foreach($data as $data)
                              @if($data->user_group_type_report->type_report_id == "Half")
                              <tr>
                                  <td width="5%">{{$i++}}</td>
                                  <td>{{$data->group_name}}</td>
                                  <td>
                                    RM {{number_format($data->balance, 2 , '.' , ',' )}} 
                                   
                                  </td>
                                  <td width="20%">
                                    <a href="#">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1" data-toggle="modal" data-target="#exampleModal{{$data->id}}"> 
                                        <i class="material-icons">edit</i>
                                      </button>
                                    </a>

                                    <a href="{{url('history-topup/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1"  onclick="window.open('{{url('history-topup/'.$data->id)}}', 'newwindow', 'width=600,height=400'); return false;"> 
                                        <i class="material-icons">history</i>
                                      </button>
                                    </a>

                                  </td>
                              </tr>
                              @endif
                            @endforeach
                              
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>



            </div>
          </div>



        @foreach($data2 as $data)
          	@if($data->user_group_type_report->type_report_id == "Half")
            <!-- Modal -->
            <div class="modal fade" id="exampleModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Balance {{$data->group_name}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/save-balance/'.$data->id)}}">

                         {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName"> Balance </label>
                                <input type="text" class="form-control" id="" placeholder="Balance" value="{{number_format($data->balance, 2 , '.' , ',' )}}" name="" disabled=""> 

                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Add Balance</label>
                                <input type="text" class="form-control" id="number" placeholder="Balance" value="" name="balance" oninput="this.value=this.value.replace(/[^0-9]/g,'');"> 
                              </div>
                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right;">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>
            @endif
          @endforeach

          
         

@endsection


@push('js')

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  </script>

  <script type="text/javascript">
    document.getElementById("number").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  </script>

@endpush