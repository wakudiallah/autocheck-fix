@extends('admin.layout.template_dashboard')

@section('content')

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Dashboard</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks -->
    <div class="row">
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Balance </span>
                <h6 class="stats-small__value count my-3">RM {{number_format($sum_balance, 2 , '.' , ',' )}} </h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Verified Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_verified}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--decrease"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-3"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Verifications</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_pending}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-4"></canvas>
          </div>
        </div>
      </div>
      
    </div>
    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      <div class="col-lg-7 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0"> User Group</h6>
          </div>
          <div class="card-body pt-0">
            
            <canvas id="myChart" style="min-width: 310px; height: 400px; margin: 0 auto"></canvas>

          </div>
        </div>
      </div>
      <!-- End Users Stats -->
      <!-- Users By Device Stats -->
      <div class="col-lg-5 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">Member Activity SS </h6>
          </div>
          <div class="card-body d-flex py-0">
            <div class="table-responsive">
            
                <table class="table table-striped table-bordered" id="example">
                  <thead>
                    <tr>
                      <th>Member</th>
                      <th>Activity</th>
                      <th>Date Activity</th>
                      <th>Last Login</th>
                      <th>Case</th>
                      <th>St Verify</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                    @foreach($data as $data)
                    <tr>
                      <td>
                        @if(!empty($data->history_per_user->name))
                        {{$data->history_per_user->name}}
                        @endif
                      </td>
                      <td>{{$data->parameter_history_id}}</td>
                      <td>{{$data->created_at}}</td>

                      <td>
                           @if(!empty($data->history_per_user->last_login))
                        {{$data->history_per_user->last_login}}
                          @endif
                        </td>

                      <td>{{$data->vehicle_id}}</td>
                      <td>@if(!empty($data->status_case->status))
                            @if($data->status_case->status == '20' OR $data->status_case->status == '10')
                            New
                            @elseif($data->status_case->status == '40')
                            Complete
                            @else
                            Cancel
                            @endif
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            
            </div>
          </div>
          <div class="card-footer border-top">
            <div class="row">
              
              <div class="col">
                
              </div>
              <div class="col text-right view-report">
                <a href="#">Full report X &rarr;</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Users By Device Stats -->
      <!-- New Draft Component -->
      <!--
      <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
        <!-- Quick Post -->

        <!--
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">New Draft</h6>
          </div>
          <div class="card-body d-flex flex-column">
            <form class="quick-post-form">
              <div class="form-group">
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Brave New World"> </div>
              <div class="form-group">
                <textarea class="form-control" placeholder="Words can be like X-rays if you use them properly..."></textarea>
              </div>
              <div class="form-group mb-0">
                <button type="submit" class="btn btn-accent">Create Draft</button>
              </div>
            </form>
          </div>
        </div>-->
        <!-- End Quick Post -->
      </div>
      <!-- End New Draft Component -->
      <!-- Discussions Component -->
      <!-- 
      <div class="col-lg-5 col-md-12 col-sm-12 mb-4">
        <div class="card card-small blog-comments">
          <div class="card-header border-bottom">
            <h6 class="m-0">Discussions</h6>
          </div>
          <div class="card-body p-0">
            <div class="blog-comments__item d-flex p-3">
              <div class="blog-comments__avatar mr-3">
                <img src="images/avatars/1.jpg" alt="User avatar" /> </div>
              <div class="blog-comments__content">
                <div class="blog-comments__meta text-muted">
                  <a class="text-secondary" href="#">James Johnson</a> on
                  <a class="text-secondary" href="#">Hello World!</a>
                  <span class="text-muted">– 3 days ago</span>
                </div>
                <p class="m-0 my-1 mb-2 text-muted">Well, the way they make shows is, they make one show ...</p>
                <div class="blog-comments__actions">
                  <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-white">
                      <span class="text-success">
                        <i class="material-icons">check</i>
                      </span> Approve </button>
                    <button type="button" class="btn btn-white">
                      <span class="text-danger">
                        <i class="material-icons">clear</i>
                      </span> Reject </button>
                    <button type="button" class="btn btn-white">
                      <span class="text-light">
                        <i class="material-icons">more_vert</i>
                      </span> Edit </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="blog-comments__item d-flex p-3">
              <div class="blog-comments__avatar mr-3">
                <img src="images/avatars/2.jpg" alt="User avatar" /> </div>
              <div class="blog-comments__content">
                <div class="blog-comments__meta text-muted">
                  <a class="text-secondary" href="#">James Johnson</a> on
                  <a class="text-secondary" href="#">Hello World!</a>
                  <span class="text-muted">– 4 days ago</span>
                </div>
                <p class="m-0 my-1 mb-2 text-muted">After the avalanche, it took us a week to climb out. Now...</p>
                <div class="blog-comments__actions">
                  <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-white">
                      <span class="text-success">
                        <i class="material-icons">check</i>
                      </span> Approve </button>
                    <button type="button" class="btn btn-white">
                      <span class="text-danger">
                        <i class="material-icons">clear</i>
                      </span> Reject </button>
                    <button type="button" class="btn btn-white">
                      <span class="text-light">
                        <i class="material-icons">more_vert</i>
                      </span> Edit </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="blog-comments__item d-flex p-3">
              <div class="blog-comments__avatar mr-3">
                <img src="images/avatars/3.jpg" alt="User avatar" /> </div>
              <div class="blog-comments__content">
                <div class="blog-comments__meta text-muted">
                  <a class="text-secondary" href="#">James Johnson</a> on
                  <a class="text-secondary" href="#">Hello World!</a>
                  <span class="text-muted">– 5 days ago</span>
                </div>
                <p class="m-0 my-1 mb-2 text-muted">My money's in that office, right? If she start giving me...</p>
                <div class="blog-comments__actions">
                  <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-white">
                      <span class="text-success">
                        <i class="material-icons">check</i>
                      </span> Approve </button>
                    <button type="button" class="btn btn-white">
                      <span class="text-danger">
                        <i class="material-icons">clear</i>
                      </span> Reject </button>
                    <button type="button" class="btn btn-white">
                      <span class="text-light">
                        <i class="material-icons">more_vert</i>
                      </span> Edit </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer border-top">
            <div class="row">
              <div class="col text-center view-report">
                <button type="submit" class="btn btn-white">View All Comments</button>
              </div>
            </div>
          </div>
        </div>
      </div>
        -->
      <!-- End Discussions Component -->
      <!-- Top Referrals Component -->
      <!-- 
      <div class="col-lg-3 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Top Referrals</h6>
          </div>
          <div class="card-body p-0">
            <ul class="list-group list-group-small list-group-flush">
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">GitHub</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">19,291</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Stack Overflow</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">11,201</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Hacker News</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">9,291</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Reddit</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">8,281</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">The Next Web</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">7,128</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Tech Crunch</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">6,218</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">YouTube</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">1,218</span>
              </li>
              <li class="list-group-item d-flex px-3">
                <span class="text-semibold text-fiord-blue">Adobe</span>
                <span class="ml-auto text-right text-semibold text-reagent-gray">827</span>
              </li>
            </ul>
          </div>
          <div class="card-footer border-top">
            <div class="row">
              <div class="col">
                <select class="custom-select custom-select-sm">
                  <option selected>Last Week</option>
                  <option value="1">Today</option>
                  <option value="2">Last Month</option>
                  <option value="3">Last Year</option>
                </select>
              </div>
              <div class="col text-right view-report">
                <a href="#">Full report &rarr;</a>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <!-- End Top Referrals Component -->
    </div>
  </div>


@endsection


@push('js')

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  </script>


  <!-- Pie Diagram -->
  <script type="text/javascript">

    var list_name_group_user = <?php echo $list_name_group_user; ?>;
    var list_name_group_total = <?php echo $list_name_group_total; ?>;

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: list_name_group_user,
        datasets: [{
          backgroundColor: [
            "#2ecc71",
            "#e74c3c",
            "#f1c40f",
            "#3498db",
            "#95a5a6",
            "#9b59b6",
            "#34495e"
          ],
          data: list_name_group_total
        }]
      }
    });
  </script>
  <!-- end pie diagram -->

@endpush