@extends('admin.layout.template')

@section('content')


  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>


<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Dashboard</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks -->


    <div class="row">
      
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Balance (Group)</span>

                <?php $role_me  = Auth::user()->role_id; ?>
                <?php $branch_me = Auth::user()->is_role_kastam; ?>
                <?php 

                  $mygroup = Auth::user()->real_user_group_id;

                ?>

                <?php $balance = DB::table('user_groups')->where('user_id', $mygroup)->limit('1')->first(); 
                    $sum_balance = $balance->balance;
                ?>

                @if($sum_balance <= '250')
                <h6 class="stats-small__value count my-3" style="color: red">RM {{number_format($sum_balance, 2 , '.' , ',' )}} </h6>
                @else

                <h6 class="stats-small__value count my-3">RM {{number_format($sum_balance, 2 , '.' , ',' )}} </h6>
                @endif
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>
      
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Vehicles (Group)</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Verified Vehicles (Group)</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_verified}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--decrease"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-3"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Verifications (Group)</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_pending}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-4"></canvas>
          </div>
        </div>
      </div>
      
    </div>
    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      <div class="col-lg-7 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Vehicle Chart</h6>
          </div>
          <div class="card-body pt-0">
            
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>
      <!-- End Users Stats -->
      <!-- Users By Device Stats -->
      <div class="col-lg-5 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">Member Activity</h6>
          </div>
          <div class="card-body d-flex py-0">
            <div class="table-responsive">
            <table class="table table-striped table-bordered" id="example" style="font-size: 11px !important">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Activity</th>
                      <th>Date Activity</th>
                      <th>Case</th>
                      <th>Verify status</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                    @foreach($user_kastam as $data)
                    <tr>
                      <td>{{$data->name}}</td>
                      <td>@if(!empty($data->history->parameter_history_id))
                        {{$data->history->parameter_history_id}}
                        @endif</td>
                      <td>
                        @if(!empty($data->history->created_at))
                        {{$data->history->created_at}}
                        @endif
                      </td>
                      <td>@if(!empty($data->case->vehicle))
                        {{$data->case->vehicle}}
                        @endif
                      </td>
                      <td>
                        @if(!empty($data->case->status))
                          @if($data->case->status == "40")
                            Complete
                          @elseif($data->case->status == "30")
                            Not Found
                          @elseif($data->case->status == "20" OR $data->case->status == "10")
                            New
                          @endif
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
          </div>
          <div class="card-footer border-top">
            <div class="row">
              
              <div class="col">
                
              </div>
              <div class="col text-right view-report">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Users By Device Stats -->
      
      
    </div>
  </div>


@endsection

@push('js')

  

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  </script>


  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

  <script type="text/javascript">

    $(function () { 


        var data_verified = <?php echo $list_verify_kastam; ?>;
        var data_pending = <?php echo $list_pending_group_kastam; ?>;
        var data_rejected = <?php echo $list_rejected_group_kastam; ?>;




        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Vehicle'
            },
            subtitle: {
                text: (new Date()).getFullYear()
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Vehicle (vehicle)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} Vc</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: 'Total Verified',
                
                data: data_verified
                

            }, {
                name: 'Total Pending',
                data: data_pending

            

            }, {
                name: 'Total Not Found',
                data: data_rejected

            }]
        });

        });
</script>


@endpush
