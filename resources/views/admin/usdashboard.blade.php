@extends('admin.layout.template')

@section('content')

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Dashboard</h3>
      </div>
    </div>
    <!-- End Page Header -->
    
    


    <!-- Small Stats Blocks -->
    <div class="row">
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Balance (RM)</span>
                @if($total_balance->balance <= "250")

                  <h6 class="stats-small__value count my-3" style="color: red">RM
                
                    @if(!empty($total_balance->balance))
                      <?php 
                      
                      $tot_balance  = number_format($total_balance->balance, 2 , '.' , ',' ); 
                      ?>
                      
                      {{$tot_balance}}
                      @endif
                    </h6>

                @else


                <h6 class="stats-small__value count my-3">RM 
                
                @if(!empty($total_balance->balance))
                  <?php 
                  
                  $tot_balance  = number_format($total_balance->balance, 2 , '.' , ',' ); 
                  ?>

                  {{$tot_balance}}
                  @endif
                </h6>

                @endif
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>

      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Vehicle Checked</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_verified }}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_pending}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div>

    </div>

   
    

  </div>


@endsection