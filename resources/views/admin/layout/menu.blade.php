<style>
  .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    color: #818d9e;
    text-align: center;
  }
</style>

        <div class="nav-wrapper">
            <ul class="nav flex-column">

              <!-- Dashboard  -->    
              <?php

                $buyer  = Auth::user()->akses_buyer;
                $buyer_group  = Auth::user()->akses_buyer;
                $admin  = Auth::user()->role_id;
                $verify  = Auth::user()->akses_verify;
                $kastam  = Auth::user()->akses_kastam;
                $status_admin  = Auth::user()->is_status_admin_group;
                $is_status_admin_group = Auth::user()->is_status_admin_group;

                $group_by = Auth::user()->group_by;
               ?>
              
              
              <li class="nav-item">
                <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" href="{{url('dashboard')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Dashboard</span>
                </a>
              </li>


              

             
              @if($admin == 'VER')
        
        
              
              <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('sent-vehicle') ? 'active' : '' }}" href="{{url('sent-vehicle')}}">
                  <i class="material-icons">email</i>
                  <span>Sent Vehicle</span>
                </a>
              </li> -->

              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">email</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Sent Vehicle (MN)</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/sent/mn') ? 'active' : '' }}" href="{{url('HalfReport/sent/mn')}}">Sent Vehicle ( {{config('autocheck.half_report')}} )</a></li>
                      <li><a class="nav-link sub {{ Request::is('FullReport/sent/mn') ? 'active' : '' }}" href="{{url('FullReport/sent/mn')}}"> Sent Vehicle ( {{config('autocheck.full_report')}} )</a></li>
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/sent/mn') ? 'active' : '' }}" href="{{url('ExtraReport/sent/mn')}}">Sent Vehicle ( {{config('autocheck.extra_report')}} )</a></li>
                      

                    </ul>
                  </li> 
                </ul>
              </aside>



              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">email</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Sent Vehicle (API)</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/sent/api') ? 'active' : '' }}" href="{{url('HalfReport/sent/api')}}">Sent Vehicle ( {{config('autocheck.half_report')}} )</a></li>
                          <li><a class="nav-link sub {{ Request::is('FullReport/sent/api') ? 'active' : '' }}" href="{{url('FullReport/sent/api')}}"> Sent Vehicle ( {{config('autocheck.full_report')}} )</a></li>
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/sent/api') ? 'active' : '' }}" href="{{url('ExtraReport/sent/api')}}">Sent Vehicle ( {{config('autocheck.extra_report')}} )</a></li>
                      
                    </ul>
                  </li> 
                </ul>
              </aside>


        <aside class="side-nav dropdown-toggle dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">check_circle</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Check ID</a>
                    <ul class="side-nav-dropdown">
                      
                          <li><a class="nav-link sub {{ Request::is('HalfReport/check-id') ? 'active' : '' }}" href="{{url('HalfReport/check-id')}}">Check ID ( {{config('autocheck.half_report')}} )</a></li>

                          <li><a class="nav-link sub {{ Request::is('FullReport/check-id') ? 'active' : '' }}" href="{{url('FullReport/check-id')}}">Check ID ( {{config("autocheck.full_report")}} )</a></li>

                          <li><a class="nav-link sub {{ Request::is('ExtraReport/check-id') ? 'active' : '' }}" href="{{url('ExtraReport/check-id')}}">Check ID ( {{config('autocheck.extra_report')}} )</a></li>
                      
                    </ul>
                  </li> 
                </ul>
              </aside>

              
              <!--
              <aside class="side-nav nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">email</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Sent Vehicle</a>
                    <ul class="side-nav-dropdown">
                      <li><a href="{{url('sent-vehicle')}}">Sent Vehicle (Car Dealer)</a></li>
                      <li><a href="{{url('sent-vehicle-kastam')}}">Sent Vehicle (Kastam)</a></li>
                    </ul>
                  </li>
                  
                  
                </ul>
              </aside>
              -->

              @endif


              @if($admin == 'VER')

              <aside class="side-nav dropdown-toggle dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">check_circle</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Sync Tasklist</a>
                    <ul class="side-nav-dropdown">
                          <li><a class="nav-link sub {{ Request::is('HalfReport/sync/api') ? 'active' : '' }}" href="{{url('HalfReport/sync/api')}}">Sync Tasklist ( {{config('autocheck.half_report')}} )</a></li>

                          <li><a class="nav-link sub {{ Request::is('FullReport/sync/api') ? 'active' : '' }}" href="{{url('FullReport/sync/api')}}">Sync Tasklist ( {{ config("autocheck.full_report") }} )</a></li>

                          <!-- <li><a class="nav-link sub {{ Request::is('verification-shortreport-kastam-mn') ? 'active' : '' }}" href="{{url('verification-shortreport-kastam-mn')}}">Sync Tasklist ( {{ config("autocheck.full_report") }} - Manual )</a></li>-->                  
                          <li><a class="nav-link sub {{ Request::is('ExtraReport/sync/api') ? 'active' : '' }}" href="{{url('ExtraReport/sync/api')}}">Sync Tasklist (  {{config('autocheck.extra_report')}}  )</a></li>

                      
                    </ul>
                  </li> 
                </ul>
              </aside>

                <!-- 
                  <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">check_circle</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Sync Tasklist</a>
                    <ul class="side-nav-dropdown">
                      <li><a href="{{url('verification-tasklist')}}">Sync Tasklist (Short Report)</a></li>
                      <li><a href="{{url('verification-tasklist-kastam')}}">Sync Tasklist (Full Report)</a></li>

                      <li><a href="{{url('verification-shortreport-kastam')}}">Sync Tasklist (Short Kastam Report)</a></li>
                    </ul>
                  </li> 
                </ul>
              </aside> -->

              @endif

                <!-- 
              @if($admin == 'VER')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('verification-tasklist') ? 'active' : '' }}" href="{{url('verification-tasklist')}}">
                  <i class="material-icons">check_circle</i>
                  <span>Verification Tasklist</span>
                </a>
              </li>
              @endif -->

              @if($admin == 'MAN')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('verification-tasklist') ? 'active' : '' }}" href="{{url('verification-tasklist')}}">
                  <i class="material-icons">check_circle</i>
                  <span>Income</span>
                </a>
              </li>
              @endif

              
              @if($admin != 'NA' AND $admin != 'VER' AND $admin != 'US' AND $admin != 'MAN' AND $admin != 'MAR' AND $admin != 'AD')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('search-vehicle') ? 'active' : '' }}" href="{{url('search-vehicle')}}">
                  <i class="material-icons">search</i>
                  <span>Search Vehicle</span>
                </a>
              </li>
              @endif
              <!-- End of Dashboard -->


              @if($buyer == 1)

              @endif


              @if($admin == 'NA')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle-checking') ? 'active' : '' }}" href="{{url('vehicle-checking')}}">
                  <i class="material-icons">search</i>
                  <span>Vehicle Checking</span>
                </a>
              </li>
              @endif


              @if($admin == 'US')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('Extra/vehicle-checking') ? 'active' : '' }}" href="{{url('Extra/vehicle-checking')}}">
                  <i class="material-icons">search</i>
                  <span>Vehicle Checking</span>
                </a>
              </li>
              @endif


              @if($admin == 'VER')
              <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('open-vehicle-verify') ? 'active' : '' }}" href="{{url('open-vehicle-verify')}}">
                  <i class="material-icons">search</i>
                  <span>Open Vehicle Verify</span>
                </a>
              </li> -->


              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">search</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Open Vehicle Verify</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/open-verify/index') ? 'active' : '' }}" href="{{url('HalfReport/open-verify/index')}}">Open Vehicle Verify ( {{trans('system_autocheck.half_report')}} )</a></li>

                      <li><a class="nav-link sub {{ Request::is('FullReport/open-verify/index') ? 'active' : '' }}" href="{{url('FullReport/open-verify/index')}}">Open Vehicle Verify ( {{trans('system_autocheck.full_report')}} )</a></li>

                      <li><a class="nav-link sub {{ Request::is('ExtraReport/open-verify/index') ? 'active' : '' }}" href="{{url('ExtraReport/open-verify/index')}}">Open Vehicle Verify ( {{trans('system_autocheck.extra_report')}} )</a></li>
                    </ul>
                  </li> 
                </ul>
            </aside>

        <!-- 
        <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">search</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Open Vehicle Verify (Manual & API)</a>
                    <ul class="side-nav-dropdown">
                      <li><a href="{{url('open-vehicle-verify-short-report-naza')}}">Open Vehicle Verify (Short Report)</a></li>
                      <li><a href="{{url('open-vehicle-verify-full-report')}}">Open Vehicle Verify (Full Report)</a></li>
                      <li><a href="{{url('open-vehicle-verify-kastam-report')}}">Open Vehicle Verify (Short Kastam Report)</a></li>
                    </ul>
                  </li> 
                </ul>
              </aside> -->

              @endif

              @if($admin == 'VER' OR $admin == 'MAR')
              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">class</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Vehicle Checked </a>
                    <ul class="side-nav-dropdown">
            
                      <li><a class="nav-link sub {{ Request::is('HalfReport/vehicle-checked') ? 'active' : '' }}" href="{{url('HalfReport/vehicle-checked')}}"> Vehicle Checked ( {{config('autocheck.half_report')}} )</a></li>                      
                      <li><a class="nav-link sub {{ Request::is('FullReport/vehicle-checked') ? 'active' : '' }}" href="{{url('FullReport/vehicle-checked')}}">Vehicle Checked ( {{config("autocheck.full_report")}} ) </a></li>                      
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/vehicle-checked') ? 'active' : '' }}" href="{{url('ExtraReport/vehicle-checked')}}"> Vehicle Checked ( {{config('autocheck.extra_report')}} )</a></li>
                    </ul>
                  </li>
                </ul>
              </aside>
              @endif



              @if($admin == 'US') <!-- Extra -->
              <li class="nav-item">
                <a class="nav-link {{ Request::is('extra/vehicle-checked') ? 'active' : '' }}" href="{{url('extra/vehicle-checked')}}">
                  <i class="material-icons">check_box</i>
                  <span>Vehicle Checked</span>
                </a>
              </li> 
              @endif

              @if($admin == 'KA') <!-- Full -->
              <li class="nav-item">
                <a class="nav-link {{ Request::is('full/vehicle-checked') ? 'active' : '' }}" href="{{url('full/vehicle-checked')}}">
                  <i class="material-icons">check_box</i>
                  <span>Vehicle Checked</span>
                </a>
              </li> 
              @endif


              @if($admin == 'NA') <!-- Half -->
              <li class="nav-item">
                <a class="nav-link {{ Request::is('half/vehicle-checked') ? 'active' : '' }}" href="{{url('half/vehicle-checked')}}">
                  <i class="material-icons">check_box</i>
                  <span>Vehicle Checked</span>
                </a>
              </li> 
              @endif


              @if(($admin == 'NA') OR ($admin == 'KA') OR ($admin == 'US'))
              <li class="nav-item">
                <a class="nav-link {{ Request::is('balance-history') ? 'active' : '' }}" href="{{url('balance-history')}}">
                  <i class="material-icons">history</i>
                  <span>Balance History</span>
                </a>
              </li>
              @endif

              <?php $nama_saya  = Auth::user()->id; ?>
              @if($nama_saya == '8')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('topup-balance') ? 'active' : '' }}" href="{{url('topup-balance')}}">
                  <i class="material-icons">system_update</i>
                  <span>Topup Balance </span>
                </a>
              </li>
              @endif


              @if($admin == 'AD' || $admin == 'MAR')

             <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle-chart') ? 'active' : '' }}" href="{{url('vehicle-chart')}}">
                  <i class="material-icons">insert_chart</i>
                  <span>Vehicle Chart</span>
                </a>
              </li>

               <li class="nav-item">
                <a class="nav-link {{ Request::is('invoice') ? 'active' : '' }}" href="{{url('invoice')}}">
                  <i class="material-icons">book</i>
                  <span>Invoice</span>
                </a>
              </li> -->

            
              <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('branch') ? 'active' : '' }}" href="{{url('branch')}}">
                  <i class="material-icons">layers</i>
                  <span>Branches</span>
                </a>
              </li> -->

              <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('user-group') ? 'active' : '' }}" href="{{url('user-group')}}">
                  <i class="material-icons">person</i>
                  <span>User Group</span>
                </a>
              </li> -->

              <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('user') ? 'active' : '' }}" href="{{url('user')}}">
                  <i class="material-icons">person</i>
                  <span>User</span>
                </a>
              </li> --> 

              <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('balance') ? 'active' : '' }}" href="{{url('balance')}}">
                  <i class="material-icons">money</i>
                  <span>Balance</span>
                </a>
              </li> -->


            

        
            <!-- <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">person</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> User</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a href="{{url('user/short-report')}}">User Short Report</a></li> 
                      <li><a href="{{url('user/full-report')}}">User Full Report</a></li>
                      <li><a href="{{url('user/extra-report')}}">User Extra Report</a></li>
                     <li><a href="{{url('user/api')}}">User API</a></li>
                    </ul>
                  </li>
                    
                </ul>
            </aside> -->


            <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">                  
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">person</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> User Group Creation</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('user-group/half-report') ? 'active' : '' }}" href="{{url('user-group/half-report')}}">User Group {{config('autocheck.half_report')}}</a></li>
                      <li><a class="nav-link sub {{ Request::is('user-group/full-report') ? 'active' : '' }}" href="{{url('user-group/full-report')}}">User Group {{config('autocheck.full_report')}}</a></li>
                      <li><a class="nav-link sub {{ Request::is('user-group/extra-report') ? 'active' : '' }}" href="{{url('user-group/extra-report')}}">User Group {{config('autocheck.extra_report')}}</a></li>
                      <li><a class="nav-link sub {{ Request::is('user/api') ? 'active' : '' }}" href="{{url('user/api')}}">User API</a></li>
                    </ul>
                  </li>
                    
                </ul>
            </aside>


            <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">person</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> User Creation</a>
                    <ul class="side-nav-dropdown">
                      <li><a class="nav-link sub {{ Request::is('user/half-report') ? 'active' : '' }}" href="{{url('user/half-report')}}">User Half Report</a></li>
                      <li><a class="nav-link sub {{ Request::is('user/full-report') ? 'active' : '' }}" href="{{url('user/full-report')}}">User  Full Report</a></li>
                      <li><a class="nav-link sub {{ Request::is('user/extra-report') ? 'active' : '' }}" href="{{url('user/extra-report')}}">User  Extra Report</a></li>
                      <li><a class="nav-link sub {{ Request::is('user') ? 'active' : '' }}" href="{{url('user')}}">User  Admin</a></li>
                    </ul>
                  </li>
                    
                </ul>
            </aside>

              @endif







              @if(($admin == 'AD') OR ($admin == 'NA' AND $is_status_admin_group == "1"))

              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">settings</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Setup Parameter</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('brand') ? 'active' : '' }}" href="{{url('brand')}}">Brand</a></li>

                      <!-- <li><a class="nav-link sub {{ Request::is('currency') ? 'active' : '' }}" href="{{url('currency')}}">Currency</a></li> -->

                      <li><a class="nav-link sub {{ Request::is('par-model') ? 'active' : '' }}" href="{{url('par-model')}}">Model</a></li>
                      <li><a class="nav-link sub {{ Request::is('supplier') ? 'active' : '' }}" href="{{url('supplier')}}">Supplier</a></li>
                      
                      @if($admin == 'AD')
                      <li><a class="nav-link sub {{ Request::is('role') ? 'active' : '' }}" href="{{url('role')}}">Role</a></li>
                      <li><a class="nav-link sub {{ Request::is('group') ? 'active' : '' }}" href="{{url('group')}}">User Group</a></li>
                      <li><a class="nav-link sub {{ Request::is('type-vehicle') ? 'active' : '' }}" href="{{url('type-vehicle')}}">Type Vehicle</a></li>
                      <li><a class="nav-link sub {{ Request::is('supplier') ? 'active' : '' }}" href="{{url('supplier')}}">Supplier</a></li>
                      <li><a class="nav-link sub {{ Request::is('country-origin') ? 'active' : '' }}" href="{{url('country-origin')}}">Country Origin</a></li>

                      <li><a class="nav-link sub {{ Request::is('partner-overseas') ? 'active' : '' }}" href="{{url('partner-overseas')}}">Partner Overseas</a></li>

                      <li><a class="nav-link sub {{ Request::is('fuel') ? 'active' : '' }}" href="{{url('fuel')}}">Fuel</a></li>
                      <li><a class="nav-link sub {{ Request::is('fee') ? 'active' : '' }}" href="{{url('fee')}}">Fee</a></li>
                      <li><a class="nav-link sub {{ Request::is('package') ? 'active' : '' }}" href="{{url('package')}}">Package</a></li>
                      <li><a class="nav-link sub {{ Request::is('email') ? 'active' : '' }}" href="{{url('email')}}">Email</a></li>
                      <li><a class="nav-link sub {{ Request::is('branch') ? 'active' : '' }}" href="{{url('branch')}}">Branch</a></li>
                      @endif
                    </ul>
                  </li>
                  
                  
                </ul>
              </aside>
              @endif



              @if(($admin == 'VER') OR ($group_by == 'NAZA') )
              <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle-past') ? 'active' : '' }}" href="{{url('vehicle-past')}}">
                  <i class="material-icons">autorenew</i>
                  <span>Vehicle Past</span>
                </a>
              </li>
              @endif

              
              @if($admin == 'VER')                
                
                <!-- <li class="nav-item">
                    <a class="nav-link {{ Request::is('vehicle-verify-not-found') ? 'active' : '' }}" href="{{url('vehicle-verify-not-found')}}">
                      <i class="material-icons">rule</i>
                      <span>Verify Vehicle Not Found</span>
                    </a>
                </li> -->



              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">rule</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Verify Vehicle Not Found</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/verifynotfound') ? 'active' : '' }}" href="{{url('HalfReport/verifynotfound')}}">Verify Vehicle Not Found ( {{config('autocheck.half_report')}} )</a></li>
                      <li><a class="nav-link sub {{ Request::is('FullReport/verifynotfound') ? 'active' : '' }}" href="{{url('FullReport/verifynotfound')}}">Verify Vehicle Not Found ( {{config('autocheck.full_report')}} )</a></li>
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/verifynotfound') ? 'active' : '' }}" href="{{url('ExtraReport/verifynotfound')}}">Verify Vehicle Not Found ( {{config('autocheck.extra_report')}} )</a></li>
                      

                    </ul>
                  </li> 
                </ul>
              </aside>

              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">cancel</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Vehicle Not Found</a>
                    
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/notfound') ? 'active' : '' }}" href="{{url('HalfReport/notfound')}}"> Vehicle Not Found ( {{config('autocheck.half_report')}} )</a></li>
                      <li><a class="nav-link sub {{ Request::is('FullReport/notfound') ? 'active' : '' }}" href="{{url('FullReport/notfound')}}"> Vehicle Not Found ( {{config('autocheck.full_report')}} )</a></li>
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/notfound') ? 'active' : '' }}" href="{{url('ExtraReport/notfound')}}"> Vehicle Not Found ( {{config('autocheck.extra_report')}} )</a></li>
                      

                    </ul>
                  </li> 
                </ul>
              </aside>

              @endif


              @if(($admin == 'KA') OR ($admin == 'NA')  OR ($admin == 'US'))
              <li class="nav-item">
                <a class="nav-link {{ Request::is('not-found') ? 'active' : '' }}" href="{{url('not-found')}}">
                  <i class="material-icons">cancel</i>
                  <span>Vehicle Not Found</span>
                </a>
              </li>
              @endif

              @if($admin == 'VER')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('batch-sent') ? 'active' : '' }}" href="{{url('batch-sent')}}">
                  <i class="material-icons">account_box</i>
                  <span>Batch Sent</span>
                </a>
              </li>
              @endif

              <!-- <li class="nav-item">
                <a class="nav-link {{ Request::is('balance') ? 'active' : '' }}" href="{{url('balance')}}">
                  <i class="material-icons">money</i>
                  <span>Balance</span>
                </a>
              </li> -->

              @if($admin == 'AD')
              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">money</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Balance</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('balance/half') ? 'active' : '' }}" href="{{url('balance/half')}}">Balance ( {{config('autocheck.half_report')}} )</a></li>

                      <li><a class="nav-link sub {{ Request::is('balance/full') ? 'active' : '' }}" href="{{url('balance/full')}}"> Balance ( {{config('autocheck.full_report')}} )</a></li>

                      <li><a class="nav-link sub {{ Request::is('balance/extra') ? 'active' : '' }}" href="{{url('balance/extra')}}"> Balance ( {{config('autocheck.extra_report')}} )</a></li>
                      

                    </ul>
                  </li> 
                </ul>
            </aside>

              @endif

              
              @if(($admin == 'MAN') OR ($admin == 'MAR') OR ($admin == 'AD'))
              
              
              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">email</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Report - Monthly Report</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/report/monthly') ? 'active' : '' }}" href="{{url('HalfReport/report/monthly')}}">Monthly Report ( {{config('autocheck.half_report')}} )</a></li>
                      
                      <li><a class="nav-link sub {{ Request::is('FullReport/report/monthly') ? 'active' : '' }}" href="{{url('FullReport/report/monthly')}}"> Monthly Report ( {{config('autocheck.full_report')}} )</a></li>
                      
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/reportmanagement/monthly') ? 'active' : '' }}" href="{{url('ExtraReport/reportmanagement/monthly')}}">Monthly Report ( {{config('autocheck.extra_report')}} )</a></li>
                      

                    </ul>
                  </li> 
                </ul>
              </aside>


              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">email</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Report - Summary Report</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/report/summary') ? 'active' : '' }}" href="{{url('HalfReport/report/summary')}}">Summary Report ( {{config('autocheck.half_report')}} )</a></li>
                      
                      <li><a class="nav-link sub {{ Request::is('FullReport/report/summary') ? 'active' : '' }}" href="{{url('FullReport/report/summary')}}"> Summary Report ( {{config('autocheck.full_report')}} )</a></li>
                      
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/reportmanagement/summary') ? 'active' : '' }}" href="{{url('ExtraReport/reportmanagement/summary')}}">Summary Report ( {{config('autocheck.extra_report')}} )</a></li>
                      

                    </ul>
                  </li> 
                </ul>
              </aside>


              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">email</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Report - Inprogress </a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('HalfReport/inprogress') ? 'active' : '' }}" href="{{url('HalfReport/inprogress')}}">Inprogress  ( {{config('autocheck.half_report')}} )</a></li>
                      
                      <li><a class="nav-link sub {{ Request::is('FullReport/inprogress') ? 'active' : '' }}" href="{{url('FullReport/inprogress')}}"> Inprogress  ( {{config('autocheck.full_report')}} )</a></li>
                      
                      <li><a class="nav-link sub {{ Request::is('ExtraReport/inprogress') ? 'active' : '' }}" href="{{url('ExtraReport/inprogress')}}">Inprogress  ( {{config('autocheck.extra_report')}} )</a></li>
                      

                    </ul>
                  </li> 
                </ul>
              </aside>


              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">class</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Report - Chart</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a class="nav-link sub {{ Request::is('report-chart') ? 'active' : '' }}" href="{{url('report-chart')}}">Report Chart Volume</a></li>
                      
                      <!-- 
                      <li><a class="nav-link sub {{ Request::is('report-chart-old') ? 'active' : '' }}" href="{{url('report-chart-old')}}"> Old Report Chart Volume</a></li>
                      -->
                 
                    </ul>
                  </li> 
                </ul>
              </aside>


              <li class="nav-item">
                <a class="nav-link {{ Request::is('logs') ? 'active' : '' }}" href="{{url('logs')}}">
                  <i class="material-icons">error</i>
                  <span>Log</span>
                </a>
              </li>



              <!-- <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">class</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Report </a>
                    <ul class="side-nav-dropdown">
                      @if($admin == 'MAN' OR $admin == 'VER')
                      <li><a class="nav-link sub {{ Request::is('report-autocheck-management') ? 'active' : '' }}" href="{{url('report-autocheck-management')}}">Buyer Group Report</a></li>
                      @endif

                      @if($admin != 'NA')
                      <li><a class="nav-link sub {{ Request::is('report-summary') ? 'active' : '' }}" href="{{url('report-summary')}}"> Summary Report</a></li>
                      <li><a class="nav-link sub {{ Request::is('report-monthly') ? 'active' : '' }}" href="{{url('report-monthly')}}"> Monthly Report</a></li>
                     <li><a class="nav-link sub {{ Request::is('report-chart') ? 'active' : '' }}" href="{{url('report-chart')}}"> Chart Volume</a></li>
                     <li><a class="nav-link sub {{ Request::is('report-chart-old') ? 'active' : '' }}" href="{{url('report-chart-old')}}"> Chart Old Volume</a></li>
                      @endif


                     
                      @if($admin == 'NA')

                        @if(Auth::user()->is_status_admin_group == "1")
                      <li><a href="{{url('report-vehicle')}}"> Vehicle Report</a></li>
                        @endif

                      @endif

                    </ul>
                  </li>
                  
                  
                </ul>
              </aside> -->

              @endif

              <!-- ============== Verify Menu ==========  -->

              <!-- ============== End Verify Menu ==========  -->

             
             <li class="nav-item">
                <a class="nav-link {{ Request::is('account') ? 'active' : '' }}" href="{{url('account')}}">
                  <i class="material-icons">account_box</i>
                  <span>Account</span>
                </a>
              </li>
              

              <li class="nav-item">
                <a class="nav-link {{ Request::is('logout') ? 'active' : '' }}" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  <i class="material-icons">&#xE879;</i>
                  <span>Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
              </li>

               <!-- <img src="{{asset('images/mari-logo.png')}}"> -->

            </ul>
          </div>



      <div class="footer">
        <img src="{{asset('images/mari12.png')}}">
        <p style="font-size: 10px">Powered By MAI <br>
        v4.0.0.0 [20211101]</p>
      </div>


      <footer class="main-footer">
        <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="#">Home</a>
            </li>
          </ul>
        <img src="{{asset('images/mari2.png')}}">
      </footer>


    <script src='https://code.jquery.com/jquery-latest.js'></script>
      
     


      <script type="text/javascript">

    $(function () {


      (function () {

        var aside = $('.side-nav'),

            showAsideBtn = $('.show-side-btn'),

            contents = $('#contents');

        showAsideBtn.on("click", function () {

          $("#" + $(this).data('show')).toggleClass('show-side-nav');

          contents.toggleClass('margin');

        });

        if ($(window).width() <= 767) {

          aside.addClass('show-side-nav');

        }
        $(window).on('resize', function () {

          if ($(window).width() > 767) {

            aside.removeClass('show-side-nav');

          }

        });

        // dropdown menu in the side nav
        var slideNavDropdown = $('.side-nav-dropdown');

        $('.side-nav .categories li').on('click', function () {

          $(this).toggleClass('opend').siblings().removeClass('opend');

          if ($(this).hasClass('opend')) {

            $(this).find('.side-nav-dropdown').slideToggle('fast');

            $(this).siblings().find('.side-nav-dropdown').slideUp('fast');

          } else {

            $(this).find('.side-nav-dropdown').slideUp('fast');

          }

        });

        $('.side-nav .close-aside').on('click', function () {

          $('#' + $(this).data('close')).addClass('show-side-nav');

          contents.removeClass('margin');

        });

      }());

      // Start chart

      

    });
      </script>
