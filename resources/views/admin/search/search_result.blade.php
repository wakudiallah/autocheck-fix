@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Searching</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">
              <div class="col-lg-12 mb-4">
                
                <!-- Input & Button Groups -->
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Search</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <form>

                        <div class="input-group mb-5">
                          <input type="text" class="form-control form-control-lg" placeholder="Vehicles" aria-label="Recipient's username" aria-describedby="basic-addon2">
                          <div class="input-group-append">
                            <button class="btn btn-white" type="button">Search</button>
                          </div>
                        </div>
                        
                      </form>
                    </li>
                    
                      
                  </ul>

                </div>
                <!-- / Input & Button Groups -->
              </div>

              
            </div>

            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Search</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
              

                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Action</th>
                                  <th>Checking Date Request</th>
                                  <th>Vehicle Identification Number</th>
                                  <th>Status</th>
                                  <th></th>
                                  <th>Salary</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>Colleen Hurst</td>
                                  <td>Javascript Developer</td>
                                  <td>San Francisco</td>
                                  <td>39</td>
                                  <td>2009/09/15</td>
                                  <td>$205,500</td>
                              </tr>
                              <tr>
                                  <td>Sonya Frost</td>
                                  <td>Software Engineer</td>
                                  <td>Edinburgh</td>
                                  <td>23</td>
                                  <td>2008/12/13</td>
                                  <td>$103,600</td>
                              </tr>
                              <tr>
                                  <td>Jena Gaines</td>
                                  <td>Office Manager</td>
                                  <td>London</td>
                                  <td>30</td>
                                  <td>2008/12/19</td>
                                  <td>$90,560</td>
                              </tr>
                              
                              <tr>
                                  <td>Jonas Alexander</td>
                                  <td>Developer</td>
                                  <td>San Francisco</td>
                                  <td>30</td>
                                  <td>2010/07/14</td>
                                  <td>$86,500</td>
                              </tr>
                              <tr>
                                  <td>Shad Decker</td>
                                  <td>Regional Director</td>
                                  <td>Edinburgh</td>
                                  <td>51</td>
                                  <td>2008/11/13</td>
                                  <td>$183,000</td>
                              </tr>
                              <tr>
                                  <td>Michael Bruce</td>
                                  <td>Javascript Developer</td>
                                  <td>Singapore</td>
                                  <td>29</td>
                                  <td>2011/06/27</td>
                                  <td>$183,000</td>
                              </tr>
                              <tr>
                                  <td>Donna Snider</td>
                                  <td>Customer Support</td>
                                  <td>New York</td>
                                  <td>27</td>
                                  <td>2011/01/25</td>
                                  <td>$112,000</td>
                              </tr>
                          </tbody>
                          <tfoot>
                              <tr>
                                  <th>Name</th>
                                  <th>Position</th>
                                  <th>Office</th>
                                  <th>Age</th>
                                  <th>Start date</th>
                                  <th>Salary</th>
                              </tr>
                          </tfoot>
                      </table>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>


@endsection


@push('js')


  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

@endpush