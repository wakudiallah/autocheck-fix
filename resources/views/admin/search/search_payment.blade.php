@extends('admin.layout.template_dashboard')

@section('content')

	<div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Payment</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-4">
                <div class="card card-small mb-4 pt-3">
                  <div class="card-header border-bottom text-center">
                    <div class="mb-3 mx-auto">
                      <img class="" src="{{asset('images/mobil/car.png')}}" alt="User Avatar" width="110"> </div>
                    <h4 class="mb-0">TOYOTA XXXX</h4>
                    <span class="text-muted d-block mb-2">AGH30-0028338</span>
                    
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-4">
                      <div class="progress-wrapper">
                        <strong class="text-muted d-block mb-2">Process</strong>
                        <div class="progress progress-sm">
                          <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="74" aria-valuemin="0" aria-valuemax="100" style="width: 74%;">
                            <span class="progress-value">74%</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item p-4">
                      <strong class="text-muted d-block mb-2">Description</strong>
                      <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Payment Details</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          <form>

	                            <div class="form-row">

	                             
	                            </div>

	                            <div class="form-group mb-4">
	                              <label for="feInputAddress">Verification Serial Number</label>
	                              <input type="text" class="form-control" id="feInputAddress" value="00000784G" readonly=""> 
	                          	</div>

	                          	<div class="form-group mb-4">
	                              <label for="feInputAddress">Vehicle Identification Number</label>
	                              <input type="text" class="form-control" id="feInputAddress" value="AGH30-0028338" readonly=""> 
	                          	</div>

	                          	

	                          	<hr class="mb-4">

	                            

	                            <div class="row">
	                            	
	                            <div class="col-md-1"></div>

	                            <div class="col-md-10">

	                            <div class="form-row">
	                              <div class="form-group col-md-2">
	                                <label for="feInputCity">Requested By</label>
	                               </div>
	                                
	                              <div class="form-group col-md-10">
	                                <input type="text" class="form-control" id="feFirstName" placeholder="First Name" value="Wakudiallah" readonly>
	                              </div>
	                            </div>


	                            <div class="form-row">
	                              <div class="form-group col-md-2">
	                                <label for="feInputCity">Email Address</label>
	                               </div>
	                                
	                              <div class="form-group col-md-10">
	                                <input type="text" class="form-control" id="feLastName" placeholder="Last Name" value="wakudiallah05@gmail.com" readonly> 
	                              </div>
	                            </div>


	                            <div class="form-row">
	                              <div class="form-group col-md-2">
	                                <label for="feInputCity">Submitted Date</label>
	                               </div>
	                                
	                              <div class="form-group col-md-10">
	                                <input type="text" class="form-control" id="feLastName" placeholder="Last Name" value="24-05-2019" readonly> 
	                              </div>
	                            </div>


	                            <hr class="mb-4">
								

								</div>
	                            <div class="col-md-1"></div>
	                            </div>

	                            <div class="row">
		                            <div class="col-md-1"></div>
		                            <div class="col-md-10">

		                            	<div class="row">
		                            		<div class="col-md-2"></div>
		                            		<div class="col-md-10">
	                                			<label for="feInputCity">Total Payment : <i style="margin-left: 300px">RM 25</i></label>
		                            			<hr>
		                            		</div>
		                            	</div>


		                        	</div>
		                            <div class="col-md-1"></div>
	                            </div>


	                            
	                            <button type="submit" class="btn  btn-lg bg-warning" style="float: right;">Pay</button>
	                            

                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
          </div>

@endsection