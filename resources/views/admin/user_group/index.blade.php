@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

      <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

      <link rel="stylesheet" type="text/css"    href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">User</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                 
                  <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New User</a>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th>User Name</th>
                                  <th>Email</th>
                                  <th>Role</th>
                                  <th>Branch</th>                                  
                                  <th>Admin Group</th>
                                  <th>Phone</th>
                                  <th>Status</th>
                                  <th>Last login time</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td>{{$i++}}</td>
                                  <td>{{$data->name}}</td>
                                  <td>{{$data->email}}</td>
                                  <td>
                                    @if($data->role_id == 'AD')
                                      <i  class="card-post__category badge badge-pill badge-danger">{{$data->role->role}}</i>
                                    
                                    @elseif($data->role_id == 'US')
                                      <i href="#" class="card-post__category badge badge-pill badge-dark">{{$data->role->role}}</i>
                                    @elseif($data->role_id == 'KA')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">{{$data->role->role}}</i>

                                    @elseif($data->role_id == 'MAN')
                                      <i href="#" class="card-post__category badge badge-pill badge-info">{{$data->role->role}}</i>

                                    @elseif($data->role_id == 'MAR')
                                      <i href="#" class="card-post__category badge badge-pill badge-dark">{{$data->role->role}}</i>

                                    @elseif($data->role_id == 'VER')
                                      <i href="#" class="card-post__category badge badge-pill badge-warning">{{$data->role->role}}</i>
                                      @elseif($data->role_id == 'NA')
                                      <i href="#" class="card-post__category badge badge-pill badge-primary">{{$data->role->role}}</i>

                                      

                                      @if(!empty($data->user_group->group_name))
                                      <i href="#" class="card-post__category badge badge-pill badge-warning">{{$data->user_group->group_name}}</i>
                                      @endif
                                    @endif
                                  </td>
                                  <td>
                                    @if(!empty($data->is_role_kastam))
                                    <!-- {{$data->is_role_kastam}} -->
                                    {{$data->user_kartam->branch}} 
                                    @endif
                                  </td>
                                  <td align="center">
                                    @if($data->is_status_admin_group == '1')
                                      <i  class="card-post__category badge badge-pill badge-success">Yes</i>
                                    @else
                                      
                                    @endif
                                  </td>   
                                  <td>{{$data->phone}}</td>
                                  <td>
                                    @if($data->status = 1)
                                      Active
                                    @else
                                      Non Active

                                      <input id="toggle-trigger" type="checkbox" data-toggle="toggle" data-on="1" data-off="0" data-size="mini" class="togglefunction" data-id="" data-onstyle="success" data-offstyle="danger" {{ $data->is_active ? 'checked' : '' }}>


                                    @endif
                                  </td>
                                  
                                  <td>{{$data->last_login}}</td>
                                  <td>
                                    <a href="{{url('edit-user-group/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="material-icons">create</i>
                                      </button>
                                    </a>

                                    <!-- 
                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1" data-toggle="modal" data-target="#exampleModal{{$data->id}}">
                                      <i class="material-icons">details</i>
                                    </button>
                                  -->

                                    
                                    <a href="{{url('delete-user-group/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                        <i class="material-icons">delete</i>
                                      </button>
                                    </a>

                                  </td>
                                  
                              </tr>
                            @endforeach 
                          </tbody>
                          
                      </table>
                      </div>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>

          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/save-user-group')}}">
                      {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Name" value="" name="name" required=""> 
                              </div>


                              <div class="form-group col-md-12">
                                <label for="feFirstName"> Branch</label>
                                <select id="feInputState" name="branch" class="form-control" required="">
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($branch as $branch)
                                  <option value="{{$branch->id}}">{{$branch->branch}}</option>
                                  @endforeach
                                </select>
                              </div>



                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="email" class="form-control" id="feLastName" placeholder="Email " value="" name="email" required=""> 
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Password</label>
                                <input type="password" class="form-control" id="feEmailAddress" placeholder="Password" value="" name="password">
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Phone</label>
                                <input type="text" class="form-control" id="fePassword" placeholder="Phone" name="phone" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"> 
                              </div>



                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal -->

            

@endsection

            @foreach($data_detail as $data)
            <!-- Modal -->
            <div class="modal fade" id="exampleModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail {{$data->name}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Name" value="" name="name" required=""> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="email" class="form-control" id="feLastName" placeholder="Email " value="" name="email" required=""> 
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Password</label>
                                <input type="password" class="form-control" id="feEmailAddress" placeholder="Password" value="" name="password">
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Phone</label>
                                <input type="text" class="form-control" id="fePassword" placeholder="Phone" name="phone" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"> 
                              </div>


                            </div>
                            

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal -->
            @endforeach


@push('js')

  <script type="text/javascript">
        var Privileges = jQuery('#one');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'NA') { //NA is kadealer
                $('.other').show();
                $('.admin').show();
            }

            else if($(this).val() == 'KA'){
                $('.other').hide();
                $('.admin').show();
            }
            
            else{
                $('.admin').hide();
                $('.other').hide();
            } 

        });
    </script>

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

@endpush