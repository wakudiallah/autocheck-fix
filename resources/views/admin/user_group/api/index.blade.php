@extends('admin.layout.template_dashboard')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
      <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

      <link rel="stylesheet" type="text/css"    href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>

      
        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">User API</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            <div class="row">
              <div class="col-lg-12 mb-4">
                <div style="float: right;" class="mb-3">
                 
                  <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New User</a>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">

                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th>Id</th>
                                  <th>User Name</th>
                                  <th>Email</th>
                                  <th>Type Report</th> 
                                  <th>Status</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td>{{ $i++ }}</td>
                                  <td>{{ $data->id }}</td>
                                  <td>{{ $data->user_api_status_to_user->name }}</td>
                                  <td>{{ $data->user_api_status_to_user->email }}</td>
                                  <td>{{ $data->user_api_status_to_parameter_type_report->desc }}</td>
                                  <td>
                                      @if($data->status == "1")
                                            Active
                                      @else
                                            Non Active
                                      @endif
                                  </td>
                                  <td align="center">
                                    
                                    <!-- <a href="{{url('user/api/edit/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="material-icons">create</i>
                                      </button>
                                    </a>
                                    
                                    <a href="{{url('user/api/delete/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                        <i class="material-icons">delete</i>
                                      </button>
                                    </a> -->

                                    <input id="toggle-trigger" 
                                        type="checkbox" data-toggle="toggle" class="togglefunction" data-on="Active" data-off="Deactivate"  data-id="{{$data->id}}" data-onstyle="success" data-offstyle="danger" {{ $data->status ? 'checked' : '' }}>


                                  </td>
                                  
                              </tr>
                            @endforeach 
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>



            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/user/api/submit')}}">
                      {{ csrf_field() }}

                            <div class="form-row">
                              

                              <div class="form-group col-md-12">
                                <label for="feFirstName"> User</label>
                                <select id="feInputState" name="user" class="form-control" required="">
                                  <option value="" selected disabled hidden>- Please Select -</option>

                                  @foreach($user_data as $user_data)
                                  <option value="{{$user_data->id}}">{{$user_data->name}} - {{$user_data->email}}</option>
                                  @endforeach
                                </select>
                              </div>

                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Type Report</label>
                                <select id="feInputState" name="type_report" class="form-control" required="">
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  @foreach($param_type_report_get as $param_type_report_get)
                                  <option value="{{$param_type_report_get->id}}">{{$param_type_report_get->desc}}</option>
                                  @endforeach
                                </select>

                              </div>
                              



                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal -->

           

@endsection

            


@push('js')
    
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


    <script>
        $(function() {
            $('.togglefunction').change(function() {
                
                var status = $(this).prop('checked') == true ? 1 : 0; 
                var user_id = $(this).data('id'); 
                 
                $.ajax({
                    type: "GET",
                    url: "{{url('user/api/change-status')}}",
                    data: {'status': status, 'user_id': user_id},
                    success: function(data){
                    
                    setInterval('location.reload()', 1000); 

                    }
                });
            })
        })
    </script>



    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

  <script type="text/javascript">
       $(document).ready(function () {
          $('select').selectize({
              sortField: 'text'
          });
      });
  </script>

@endpush
