@extends('admin.layout.template')

@section('content')

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title"> Inprogress ( {{config('autocheck.full_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
           

            <div class="row">
              <div class="col-md-2 mb-4"></div>

              <div class="col-md-8 mb-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                      <h6 class="m-0">Inprogress</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item px-3">
                        <form method="POST" class="" id="register-form" action="{{url('FullReport/report/monthly/store')}}">
                          {{ csrf_field() }}

                        <div class="form-row">
                              
                          <div class="form-group col-md-12">
                            <label for="fePassword">Buyer Group</label>
                             <select name="buyer_group" class="form-control select2" required  data-show-subtext="true" data-live-search="true">
                              <option value="" selected disabled hidden>- Please Select -</option>

                              <option value="All">All</option>

                              @foreach($user_group as $data)
                               
                                @if($data->user_group_type_report->type_report_id == "Full")
                                  <option value="{{$data->user_id}}" {{ $buyer_group == $data->user_id ? 'selected' : '' }}>{{$data->group_name}}</option>
                                @endif
                              
                              @endforeach
                            </select>
                          </div>
                        </div>

                       
                        <div class="form-row">
                          <div class="form-group col-md-12">
                            <label for="feFirstName">Year</label>
                            <input type="text" class="form-control" id="datepicker" placeholder="Year" value="{{$date}}" name="month" required>
                          </div>

                        
                          
                        </div>

                        <button type="submit" class="btn btn-accent" style="float: right">Submit</button>

                        </form>

                      </li>
                    </ul>
                </div>
              </div>

              <div class="col-md-2 mb-4"></div>

            </div>


            <div class="row">

              <div class="col-md-12 mb-4">
                <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <form method="POST" class="" id="register-form" action="{{url('FullReport/inprogress/print')}}">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" value="{{$buyer_group}}" name="buyer_group" >   

                        <input type="hidden" class="form-control" value="{{$date}}" name="month" >    

                        @if(!empty($count_report)) 

                        <button type="submit" class="btn btn-accent" style="float: right;">Export</button>

                        @endif

                    </form>
			               <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="20%">Vehicle</th>
                                  <th width="20%">Month</th>
                                  <th width="20%">Buyer Group</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php 
                              $i = 1;
                            ?>

                            @foreach($vin as $data)


                            <tr>
                              <td>{{$i++}}</td>
                              <td>{{$data->vehicle}}</td>
                              <td>{{date("M",strtotime($data->created_at))}}</td>
                              <td> @if(!empty($data->group_company->group_name))
                                  {{$data->group_company->group_name}}
                                @endif</td>
                            </tr>
                            
                            @endforeach
                          </tbody>
                          
                      </table>
			               </div>
                    </li>
                  </ul>
              </div>
              </div>
            </div>

          </div>

          

            

@endsection


@push('addjs')

    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker" ).datepicker(
          {
              format: "yyyy",
              viewMode: "years", 
              minViewMode: "years"
          }
        );
     });

    $(function() {
       $( "#datepicker2" ).datepicker(
          {
              format: "yyyy",
              viewMode: "years", 
              minViewMode: "years"
          }
        );
     });

    
    </script>

@endpush


@push('js')


  <<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush
