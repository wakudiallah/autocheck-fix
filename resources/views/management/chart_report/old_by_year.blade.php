@extends('admin.layout.template_dashboard')

@section('content')

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>


  <script src="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap.min.css"></script>


<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        
        <h3 class="page-title">Report - Chart Old</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks -->

    <div class="row">

      <div class="col-lg-6 col-md-6 col-sm-6 mb-4"></div>

      <div class="col-lg-6 col-md-6 col-sm-6 mb-4" style="text-align: right !important">
      <!-- Example single danger button -->
        <div class="btn-group" style="text-align: right !important">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <b>Year</b>
            </button>
            <div class="dropdown-menu">
                @foreach($year_vehicle as $datax)
                    <a class="dropdown-item" href="{{url('report-chart-old/'.$datax->year)}}">{{$datax->year}}</a>
                @endforeach   
            </div>
        </div>
      </div>
    </div>


    <!-- Chart -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            
            <div class="row">
                <div class="col-md-6">
                    <h6 class="m-0">Kastam Vehicle Submit</h6>
                </div>
                <div class="col-md-6" style="text-align: right">
                    
                </div>
            </div>
          </div>
          <div class="card-body pt-0">

              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>

    </div>


    <!-- End Chart -->

    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      <!-- 
      <div class="col-lg-6 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">User Group Chart</h6>
          </div>
          <div class="card-body pt-0">

              <canvas id="myChart" style="min-width: 310px; height: 400px; margin: 0 auto"></canvas>

          </div>
        </div>
      </div>
        -->






      <!-- End Users Stats -->
      <!-- 
      <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Income Chart</h6>
          </div>
          <div class="card-body pt-0">
              <div id="container_line" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
              
          </div>
        </div>
      </div>
        -->
      
      
    </div>

    <!-- Modal User Group -->
     @foreach($data_group as $data)
      <div class="modal fade bd-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">User Group </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div class=" table-responsive">
              <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" >  
                <thead>
                  <tr>
                      <th width="5%">No</th>
                      <th>Gorup</th>
                      <th>Total User</th>
                  </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
                @foreach($data_group as $data)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$data->group_name}}</td>
                  <td>{{$data->total_group_user->count()}}</td>
                </tr>
                @endforeach

                </tbody> 
              </table> 
            </div>
            </div>
            <div class="modal-footer">
              
              
            </div>


          </div>
        </div>
      </div>
      @endforeach
    <!-- end Modal User Group -->


  </div>


@endsection


@push('js')

    
  
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
     
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        } );


        $(document).ready(function() {
            $('#example2').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        } );

  </script>

  <script type="text/javascript">
    $(document).ready(function() {
    $('#dataTable').DataTable();
});
  </script>


  <script type="text/javascript">
    $(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');
    });
  </script>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>





    <script type="text/javascript">

    $(function () { 
        
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Vehicle'
            },
            subtitle: {
                text: <?php echo $id ?>
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Vehicle (vehicle)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} app</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [
            <?php 
            foreach($usergroup as $usergroup) { 

                
                
                if($usergroup->count_jan_by_year->count() >= 1){
                    $count_1 = $usergroup->count_jan_by_year->count();
                }else{
                    $count_1 = 0;
                }


                if( $usergroup->count_feb_by_year->count()  >= 1 ){
                    $count_2 = $usergroup->count_feb_by_year->count();

                }else{
                    $count_2 = 0;
                }


                if( $usergroup->count_mar_by_year->count()  >= 1 ){
                    $count_3 = $usergroup->count_mar_by_year->count();

                }else{
                    $count_3 = 0;
                }


                if( $usergroup->count_apr_by_year->count()  >= 1 ){
                    $count_4 = $usergroup->count_apr_by_year->count();

                }else{
                    $count_4 = 0;
                }

                if( $usergroup->count_may_by_year->count()  >= 1 ){
                    $count_5 = $usergroup->count_may_by_year->count();

                }else{
                    $count_5 = 0;
                }


                if( $usergroup->count_jun_by_year->count()  >= 1 ){
                    $count_6 = $usergroup->count_jun_by_year->count();

                }else{
                    $count_6 = 0;
                }


                if( $usergroup->count_jul_by_year->count()  >= 1 ){
                    $count_7 = $usergroup->count_jul_by_year->count();

                }else{
                    $count_7 = 0;
                }


                if( $usergroup->count_aug_by_year->count()  >= 1 ){
                    $count_8 = $usergroup->count_aug_by_year->count();

                }else{
                    $count_8 = 0;
                }


                if( $usergroup->count_sep_by_year->count()  >= 1 ){
                    $count_9 = $usergroup->count_sep_by_year->count();

                }else{
                    $count_9 = 0;
                }


                if( $usergroup->count_oct_by_year->count()  >= 1 ){
                    $count_10 = $usergroup->count_oct_by_year->count();

                }else{
                    $count_10 = 0;
                }


                if( $usergroup->count_nov_by_year->count()  >= 1 ){
                    $count_11 = $usergroup->count_nov_by_year->count();

                }else{
                    $count_11 = 0;
                }


                if( $usergroup->count_des_by_year->count()  >= 1 ){
                    $count_12 = $usergroup->count_des_by_year->count();

                }else{
                    $count_12 = 0;
                }


            ?>


            {
                name: '<?php echo $usergroup->user_group_old_name ?>',
                data: [
                <?php echo $count_1 ?>
                ,<?php echo $count_2 ?>
                ,<?php echo $count_3 ?>
                ,<?php echo $count_4 ?>
                ,<?php echo $count_5 ?>
                , <?php echo $count_7 ?>
                , <?php echo $count_8 ?>
                , <?php echo $count_9 ?>
                , <?php echo $count_10 ?>
                , <?php echo $count_11 ?>
                , <?php echo $count_12 ?>
                
                ]
                

            }, 

            <?php } ?>


            ]
        });

        });
    </script>




@endpush
