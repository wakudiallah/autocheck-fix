@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title"> Monthly Report </h3>
              </div>
            </div>
            <!-- End Page Header -->
           

            <div class="row">
              <div class="col-md-2 mb-4"></div>

              <div class="col-md-8 mb-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                      <h6 class="m-0">Monthly Report</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item px-3">
                        <form method="POST" class="" id="register-form" action="report_monthly_view">
                          {{ csrf_field() }}

                        <div class="form-row">
                              
                          <div class="form-group col-md-12">
                            <label for="fePassword">Buyer Group</label>
                             <select name="buyer_group" class="form-control select2" required  data-show-subtext="true" data-live-search="true">
                              <option value="" selected disabled hidden>- Please Select -</option>
                              @foreach($user_group as $data)
                              <option value="{{$data->user_id}}" {{ $buyer_group == $data->user_id ? 'selected' : '' }}>{{$data->group_name}}</option>

                              
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <?php $getmonth =  $separate_month.'-'.$separate_year ?>

                        <div class="form-row">
                          <div class="form-group col-md-12">
                            <label for="feFirstName">Month</label>
                            <input type="text" class="form-control" id="datepicker" placeholder="Month" value="{{$getmonth}}" name="month" required>
                          </div>

                        
                          
                        </div>

                        <button type="submit" class="btn btn-accent" style="float: right">Submit</button>

                        </form>

                      </li>
                    </ul>
                </div>
              </div>

              <div class="col-md-2 mb-4"></div>

            </div>


            <div class="row">

              <div class="col-md-12 mb-4">
                <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <form method="POST" class="" id="register-form" action="report-monthly-print">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" value="{{$buyer_group}}" name="buyer_group" >   

                        <input type="hidden" class="form-control" value="{{$date}}" name="month" >    

                        <button type="submit" class="btn btn-accent" style="float: right;">Export</button>

                    </form>
			               <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="20%">Submission Date</th>
                                  <th width="20%">Qty</th>
                                  <th width="20%">Cost (Per Case)</th>
                                  <th width="20%">Total</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php 
                              $i = 1;
                            ?>

                            @foreach($report as $data)


                            <?php ?>

                            <tr>
                              <td>{{$i++}}</td>
                              <td>

                            <?php $tarikh =  date('d/m/Y ', strtotime($data->date)); ?>
                                  {{$tarikh}}
                              </td>
                              <td>{{$data->total}}</td>
                              <td>{{$fee->fee}}</td>
                              <td>
                                <?php $tot = $data->total * $fee->fee;
                                  $total =  number_format($tot, 2 , '.' , ',' ) ;
                                ?>
                                {{$total}}



                              </td>
                              
                            </tr>
                            
                            @endforeach
                          </tbody>
                          
                      </table>
			</div>
                    </li>
                  </ul>
              </div>
              </div>
            </div>

          </div>

          

            

@endsection


@push('js')

  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

  <script>
    $(function() {
       $( "#datepicker" ).datepicker(
          {
              format: "mm-yyyy",
              viewMode: "months", 
              minViewMode: "months"
          }
        );
     });

    $(function() {
       $( "#datepicker2" ).datepicker(
          {
              format: "mm-yyyy",
              viewMode: "months", 
              minViewMode: "months"
          }
        );
     });

    
    </script>



  <<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush
