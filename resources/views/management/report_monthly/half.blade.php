@extends('admin.layout.template')

@section('content')

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Monthly Report ( {{config('autocheck.half_report')}} )</h3>
              </div>
            </div>
            <!-- End Page Header -->
           

            <div class="row">
              <div class="col-md-2 mb-4"></div>

              <div class="col-md-8 mb-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                      <h6 class="m-0">Monthly Report</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item px-3">
                        
                        <form method="POST" class="" id="register-form" action="{{url('HalfReport/report/monthly/store')}}">
                          {{ csrf_field() }}

                        <div class="form-row">
                              
                          <div class="form-group col-md-12">
                            <label for="fePassword">Buyer Group</label>
                             <select name="buyer_group" class="form-control select2" required  data-show-subtext="true" data-live-search="true">
                              <option value="" selected disabled hidden>- Please Select -</option>

                                <option value="All">All</option>

                              @foreach($data as $data)
                                @if($data->user_group_type_report->type_report_id == "Half")
                                
                                  <option value="{{$data->user_id}}">{{$data->group_name}}</option>

                                @endif
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="form-row">
                          <div class="form-group col-md-12">
                            <label for="feFirstName">Month</label>
                            <input type="text" class="form-control" id="datepicker" placeholder="Month" value="" name="month" required>
                          </div>

                          
                        </div>

                        <button type="submit" class="btn btn-accent" style="float: right">Submit</button>

                        </form>

                      </li>
                    </ul>
                </div>
              </div>

              <div class="col-md-2 mb-4"></div>

            </div>
          </div>

            
@endsection


@push('addjs')

  <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker" ).datepicker(
          {
              format: "mm-yyyy",
              viewMode: "months", 
              minViewMode: "months"
          }
        );
     });

    $(function() {
       $( "#datepicker2" ).datepicker(
          {
              format: "mm-yyyy",
              viewMode: "months", 
              minViewMode: "months"
          }
        );
     });

    </script>


@endpush