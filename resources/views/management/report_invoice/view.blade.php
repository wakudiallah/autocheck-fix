@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Invoice Group</h3>
              </div>
            </div>
            <!-- End Page Header -->
           

            <div class="row">
              <div class="col-md-2 mb-4"></div>

              <div class="col-md-8 mb-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                      <h6 class="m-0">Invoice Group</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item px-3">
                        <form method="POST" class="report-autocheck-management-view" id="register-form" action="">
                          {{ csrf_field() }}

                        <div class="form-row">
                              
                          <div class="form-group col-md-12">
                            <label for="fePassword">Buyer Group</label>
                             <select name="buyer_group" class="form-control select2" required  data-show-subtext="true" data-live-search="true">
                               <option value="" selected disabled hidden>- Please Select -</option>
                              <!--<option value="All">All</option> -->
                             
                            </select>
                          </div>
                        </div>

                       

                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="feFirstName">From Date</label>
                            <input type="text" class="form-control" id="datepicker" placeholder="From Date" value="" name="from" required>
                          </div>

                          <div class="form-group col-md-6">
                            <label for="feFirstName">To Date</label>
                            <input type="text" class="form-control" id="datepicker2" placeholder="To Date" value="" name="to" required>
                          </div>
                          
                        </div>

                        <button type="submit" class="btn btn-accent" style="float: right">Submit</button>

                        </form>

                      </li>
                    </ul>
                </div>
              </div>

              <div class="col-md-2 mb-4"></div>

            </div>


            <div class="row">
              <div class="col-md-12 mb-4">
                <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      

                      <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="20%">Buyer</th>
                                  <th width="20%">Total</th>
                                  <th width="20%">API</th>
                                  <th width="20%">Manual</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>

                                @foreach($data as $data)
                                <tr>
                                  <td>{{$i++}}</td>
                                  <td>{{$data->test}}</td>
                                  <td>{{$data->total}}</td>
                                  <td>{{$data->total_api}}</td>
                                  <td>{{$data->total_not}}</td>
                                </tr>
                              @endforeach

                           
                              
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
              </div>
            </div>


            
          </div>

@endsection


@push('js')

  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script>
  $(function() {
     $( "#datepicker" ).datepicker();
   });
  $(function() {
     $( "#datepicker2" ).datepicker();
   });
  </script>


  <<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush