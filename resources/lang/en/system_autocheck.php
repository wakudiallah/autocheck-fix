<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'half_report' => 'Half Report',
    'full_report' => 'Full Report',
    'extra_report' => 'Extra Report',

    'active' => 'Active',
    'deactivate' => 'Deactivate',


    'vehile_verify_not_found' => 'Verify Vehicle Not Found'


];

